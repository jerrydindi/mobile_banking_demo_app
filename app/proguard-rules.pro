# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

#-allowaccessmodification

#-optimizationpasses 5
#-dontusemixedcaseclassnames
#-dontskipnonpubliclibraryclasses
#-dontskipnonpubliclibraryclassmembers
#-dontpreverify
#-verbose

#-optimizations code/simplification/arithmetic,!code/simplification/cast,!field/*,! class/merging/*,!method/inlining/*

#-dontwarn javax.annotation.**
#-dontwarn android.app.**
#-dontwarn android.support.**
#-dontwarn android.view.**
#-dontwarn android.widget.**
#-dontwarn com.google.common.primitives.**
#-dontwarn **CompatHoneycomb
#-dontwarn **CompatHoneycombMR2
#-dontwarn **CompatCreatorHoneycombMR2

-keep class !ke.co.payconnect.BuildConfig, !ke.co.payconnect.** { *; }

#-keep class !ke.co.payconnect.** { *; }
#-keep class !ke.co.payconnect.mocash.mocash.view.activity.MinistatementEnquiryActivity,ke.co.payconnect.** { *; }
#-flattenpackagehierarchy 'kk'
#-repackageclasses 'o'
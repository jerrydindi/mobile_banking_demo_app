package ke.co.payconnect.mocash.chama.view.adapter.secretary;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import es.dmoral.toasty.Toasty;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.AccountEntity;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;
import ke.co.payconnect.mocash.chama.util.Auth;
import ke.co.payconnect.mocash.chama.util.Config;
import ke.co.payconnect.mocash.chama.util.SendTo;
import ke.co.payconnect.mocash.chama.view.activity.secretary.structured.AddTransactionStructuredActivity;
import ke.co.payconnect.mocash.chama.view.activity.secretary.unstructured.AddTransactionActivity;
import ke.co.payconnect.mocash.chama.viewmodel.AccountViewModel;

import static ke.co.payconnect.mocash.chama.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.chama.util.Config.RETRIES;
import static ke.co.payconnect.mocash.chama.util.Config.TIMEOUT_MS;

@SuppressWarnings("FieldCanBeLocal")
public class SelectMemberAddTransactionsRecyclerViewAdapter extends RecyclerView.Adapter<SelectMemberAddTransactionsRecyclerViewAdapter.MembersHolder> {
    private final Context context;
    private List<MemberEntity> memberEntities;
    private List<MemberEntity> memberEntitiesFiltered;
    private AccountViewModel accountViewModel;
    private SendTo st;

    public SelectMemberAddTransactionsRecyclerViewAdapter(Context context, List<MemberEntity> memberEntities) {
        this.context = context;
        this.memberEntities = memberEntities;
        this.memberEntitiesFiltered = memberEntities;
        this.accountViewModel = ViewModelProviders.of((FragmentActivity) context).get(AccountViewModel.class);
        st = new SendTo(context);
    }

    @NonNull
    @Override
    public MembersHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_list_members, parent, false);
        return new MembersHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final MembersHolder holder, int position) {
        try {
            final MemberEntity memberEntity = memberEntitiesFiltered.get(position);
            final int colour = context.getResources().getColor(R.color.colorPrimary);
            final String memberName = memberEntity.getFirstName() + " " + memberEntity.getLastName();

            holder.tdRoundIcon = TextDrawable.builder()
                    .beginConfig()
                    .endConfig()
                    .buildRoundRect(memberEntity.getFirstName().substring(0, 1).toUpperCase() + memberEntity.getLastName().substring(0, 1).toUpperCase(), colour, 100);

            holder.ivStructMembers.setImageDrawable(holder.tdRoundIcon);
            holder.tvNameStructMembers.setText(memberName);

            holder.clStructMembers.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!Auth.isAuthorized("secretary")) {
                        Toasty.info(context, R.string.action_not_permitted, Toast.LENGTH_SHORT, true).show();
                        return;
                    }

                    int groupType = 0;
                    groupType = AppController.getInstance().getChamaPreferenceManager().getGroupType();

                    switch (groupType) {
                        case 0:
                        default:
                            Toasty.info(context, "Invalid group type. Kindly contact the administrator", Toast.LENGTH_SHORT, true).show();
                            break;
                        case 1:
                            String phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();
                            String groupNumber = AppController.getInstance().getChamaPreferenceManager().getGroupNumber();

                            final HashMap<String, String> map = new HashMap<>();
                            map.put("phone_number", phoneNumber);
                            map.put("group_number", groupNumber);
                            map.put("member_phone_number", memberEntity.getPhoneNumber());

                            final String sessionToken = AppController.getInstance().getChamaPreferenceManager().getSessionToken();

                            fetchMemberAccounts(holder, map, sessionToken);

                            break;
                        case 2:
                            Intent i = new Intent(context, AddTransactionActivity.class);
                            i.putExtra("selected_member_phone", memberEntity.getPhoneNumber());
                            i.putExtra("txn_category", "TXN-GRP-POSTING");
                            i.putExtra("customer_type", "member");

                            context.startActivity(i);

                            ((Activity) context).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                            break;
                    }
                }
            });
        } catch (Exception ignored) {
        }
    }

    @Override
    public int getItemCount() {
        return memberEntitiesFiltered.size();
    }

    class MembersHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout clStructMembers;
        private ImageView ivStructMembers;
        private TextView tvNameStructMembers;
        private TextDrawable tdRoundIcon;

        MembersHolder(View view) {
            super(view);

            clStructMembers = view.findViewById(R.id.clStructMembers);
            ivStructMembers = view.findViewById(R.id.ivStructMembers);
            tvNameStructMembers = view.findViewById(R.id.tvNameStructMembers);
        }
    }

    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    memberEntitiesFiltered = memberEntities;
                } else {
                    List<MemberEntity> filteredMemberEntities = new LinkedList<>();

                    for (MemberEntity memberEntity : memberEntities) {

                        if (memberEntity.getFirstName().toLowerCase().trim().contains(charString) || memberEntity.getLastName().toLowerCase().trim().contains(charString))
                            filteredMemberEntities.add(memberEntity);
                    }

                    memberEntitiesFiltered = filteredMemberEntities;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = memberEntitiesFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                memberEntitiesFiltered = (List<MemberEntity>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface MemberAdapterSearchListener {
        void onMemberSearched(MemberEntity memberEntity);
    }


    HurlStack hurlStack = new HurlStack() {
        @Override
        protected HttpURLConnection createConnection(URL url) throws IOException {
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
            try {
                httpsURLConnection.setSSLSocketFactory(st.getSSLSocketFactory());
                httpsURLConnection.setHostnameVerifier(st.getHostnameVerifier());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return httpsURLConnection;
        }
    };

    private void fetchMemberAccounts(@NonNull MembersHolder holder, final HashMap<String, String> map, final String sessionToken) {
        final ProgressDialog loading = ProgressDialog.show(context, null, "Fetching member accounts", false, false);

        final String requestTag = "chama_fetch_member_accounts_request";
        final String url = Config.FETCH_MEMBER_ACCOUNTS;
        RequestQueue requestQueue = Volley.newRequestQueue(context);

        JsonObjectRequest fetchMemberAccountsReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(final JSONObject response) {
                        try {

                            loading.dismiss();

                            switch (response.getString("status")) {
                                case "success":
                                    loading.show();
                                    loading.setMessage("Please wait");

                                    // Add accounts
                                    JSONArray accountsArray = response.getJSONArray("accounts");

                                    // Validate accounts
                                    if (accountsArray.length() <= 0) {
                                        onCompleteAlertDialog("Could not fetch member accounts");
                                        return;
                                    }

                                    if (accountsArray.length() > 0) {

                                        // Clear persistent accounts
                                        accountViewModel.deleteAllMemberAccounts(map.get("member_phone_number"));

                                        for (int i = 0; i < accountsArray.length(); i++) {
                                            AccountEntity accountEntity = new AccountEntity();

                                            JSONObject accountObject = accountsArray.getJSONObject(i);

                                            accountEntity.setAccountName(accountObject.getString("accountName"));
                                            accountEntity.setAccountNumber(accountObject.getString("accountNumber"));
                                            accountEntity.setMemberPhoneNumber(map.get("member_phone_number"));

                                            accountViewModel.insertAccount(accountEntity);
                                        }
                                    }

                                    actionAddTransaction(map.get("member_phone_number"));

                                    loading.dismiss();

                                    break;

                                case "failed":
                                case "fatal":
                                default:
                                    onCompleteAlertDialog("Could not fetch member accounts");
                                    break;
                            }

                        } catch (JSONException e) {
                            loading.dismiss();
                            onCompleteAlertDialog("Could not fetch member accounts");

                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                loading.dismiss();
                onCompleteAlertDialog("Could not fetch member accounts");

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error")) {
                                    Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":

                                        Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();

                                        break;

                                    default:
                                        Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + context.getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } else {
                    if (error instanceof NoConnectionError) {
                        Toasty.info(context, "No connection to the server", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof TimeoutError) {
                        Toasty.info(context, "The request has timed out", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof AuthFailureError) {
                        Toasty.info(context, "Authentication failed", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof ServerError) {
                        Toasty.info(context, context.getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof NetworkError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof ParseError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else {
                        Toasty.info(context, context.getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    }
                }

            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionToken);

                return headers;
            }

        };

        fetchMemberAccountsReq.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));
        requestQueue.add(fetchMemberAccountsReq);
    }

    private void actionAddTransaction(String memberPhoneNumber) {
        Intent i = new Intent(context, AddTransactionStructuredActivity.class);
        i.putExtra("selected_member_phone", memberPhoneNumber);
        i.putExtra("customer_type", "member");
        context.startActivity(i);

        ((Activity) context).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void onCompleteAlertDialog(String message) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(false);
        builder.setTitle(context.getString(R.string.notification));
        builder.setIcon(R.drawable.ic_unaitas);
        builder.setMessage(message);
        builder.setPositiveButton(context.getString(R.string.ok), dialogClickListener);
        builder.show();
    }
}

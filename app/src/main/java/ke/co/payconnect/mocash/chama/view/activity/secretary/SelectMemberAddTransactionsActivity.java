package ke.co.payconnect.mocash.chama.view.activity.secretary;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import es.dmoral.toasty.Toasty;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.AccountEntity;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;

import ke.co.payconnect.mocash.chama.util.Access;
import ke.co.payconnect.mocash.chama.util.Auth;
import ke.co.payconnect.mocash.chama.util.Config;

import ke.co.payconnect.mocash.chama.util.SendTo;
import ke.co.payconnect.mocash.chama.view.activity.MainChamaActivity;
import ke.co.payconnect.mocash.chama.view.activity.secretary.structured.AddTransactionStructuredActivity;
import ke.co.payconnect.mocash.chama.view.adapter.secretary.SelectMemberAddTransactionsRecyclerViewAdapter;
import ke.co.payconnect.mocash.chama.viewmodel.AccountViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.TransactionViewModel;

import static ke.co.payconnect.mocash.chama.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.chama.util.Config.RETRIES;
import static ke.co.payconnect.mocash.chama.util.Config.TIMEOUT_MS;

public class SelectMemberAddTransactionsActivity extends AppCompatActivity
        implements SelectMemberAddTransactionsRecyclerViewAdapter.MemberAdapterSearchListener {
    private final Activity activity = this;
    private final Context context = this;

    private Toolbar tbMemberTxn;
    private TextView tvConnectivityStatusMemberTxn;
    private TextView tvHeaderTotalBeforePosting;
    private TextView tvHeaderAmountTotalBeforePosting;
    private TextView tvHeaderTotalAfterPosting;
    private TextView tvHeaderAmountTotalAfterPosting;
    private CardView cvGroupNameTitle;
    private TextView tvCreditGroupTransactionNameStruct;
    private RecyclerView rvMemberTxn;
    private TextView tvNoDataMemberTxn;
    private CardView cvSubmitTransactionsPendApproval;
    private CardView cvCancelTransactionsPendApproval;
    private ImageView ivGroupTxn;

    private SearchManager searchManager;
    private SearchView searchView;
    private LinearLayoutManager layoutManager;
    private DividerItemDecoration divideItemDecoration;
    public SelectMemberAddTransactionsRecyclerViewAdapter adapter;
    private TextDrawable tdRoundIcon;

    private MemberViewModel memberViewModel;
    private AccountViewModel accountViewModel;
    private TransactionViewModel transactionViewModel;

    private String sessionTotal;
    private String amountPosted;

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork;
            if (cm != null) {
                activeNetwork = cm.getActiveNetworkInfo();

                boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

                if (isConnected)
                    tvConnectivityStatusMemberTxn.setVisibility(View.GONE);
                else
                    tvConnectivityStatusMemberTxn.setVisibility(View.VISIBLE);
            } else {
                tvConnectivityStatusMemberTxn.setVisibility(View.GONE);
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_member_add_transactions);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        else
            registerReceiver(connectionReceiver, intentFilter);

        tvConnectivityStatusMemberTxn = findViewById(R.id.tvConnectivityStatusMemberTxn);
        tbMemberTxn = findViewById(R.id.tbSelectMemberTblBanking);
        setSupportActionBar(tbMemberTxn);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbMemberTxn.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        tvHeaderTotalBeforePosting = findViewById(R.id.tvHeaderTotalBeforePosting);
        tvHeaderAmountTotalBeforePosting = findViewById(R.id.tvHeaderAmountTotalBeforePosting);
        tvHeaderTotalAfterPosting = findViewById(R.id.tvHeaderTotalAfterPosting);
        tvHeaderAmountTotalAfterPosting = findViewById(R.id.tvHeaderAmountTotalAfterPosting);

        cvGroupNameTitle = findViewById(R.id.cvGroupNameTitle);
        tvCreditGroupTransactionNameStruct = findViewById(R.id.tvCreditGroupTransactionNameStruct);
        rvMemberTxn = findViewById(R.id.rvMemberTxn);
        tvNoDataMemberTxn = findViewById(R.id.tvNoDataMemberTxn);
        cvSubmitTransactionsPendApproval = findViewById(R.id.cvSubmitTransactionsPendApproval);
        cvCancelTransactionsPendApproval = findViewById(R.id.cvCancelTransactionsPendApproval);
        ivGroupTxn = findViewById(R.id.ivGroupTxn);

        layoutManager = new LinearLayoutManager(context);
        divideItemDecoration = new DividerItemDecoration(context, LinearLayoutManager.VERTICAL);
        rvMemberTxn.addItemDecoration(divideItemDecoration);

        memberViewModel = ViewModelProviders.of(this).get(MemberViewModel.class);
        accountViewModel = ViewModelProviders.of(this).get(AccountViewModel.class);
        transactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel.class);

        setupRecyclerView();

        getData();

        setHeader();

        setListeners();
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        Toasty.info(context, R.string.action_not_permitted, Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public boolean onKeyDown(int key_code, KeyEvent key_event) {
        if (key_code == KeyEvent.KEYCODE_BACK) {
            super.onKeyDown(key_code, key_event);
            return true;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();

        if (id == R.id.action_search)
            return true;

        return super.onOptionsItemSelected(menuItem);
    }

    private void setupRecyclerView() {
        memberViewModel.getMembersLiveList().observe((LifecycleOwner) context, new Observer<List<MemberEntity>>() {
            @Override
            public void onChanged(List<MemberEntity> memberEntities) {

                if (memberEntities.size() > 0) {
                    toggleDataViews(true);

                    adapter = new SelectMemberAddTransactionsRecyclerViewAdapter(context, memberEntities);
                    adapter.notifyDataSetChanged();

                    rvMemberTxn.setAdapter(adapter);
                    rvMemberTxn.setLayoutManager(layoutManager);
                } else
                    toggleDataViews(false);

            }
        });
    }

    public void getData() {


        sessionTotal = Config.sessionTotal;
        amountPosted = String.valueOf(Config.totalPosted);

    }

    public void setHeader() {
        String groupName = AppController.getInstance().getChamaPreferenceManager().getGroupName().toUpperCase();
        final int colour = context.getResources().getColor(R.color.colorPrimary);

        tdRoundIcon = TextDrawable.builder()
                .beginConfig()
                .endConfig()
                .buildRoundRect(groupName.substring(0, 1).toUpperCase(), colour, 100);
        ivGroupTxn.setImageDrawable(tdRoundIcon);

        tvCreditGroupTransactionNameStruct.setText(groupName);
        tvHeaderAmountTotalBeforePosting.setText(sessionTotal);
        tvHeaderAmountTotalAfterPosting.setText(amountPosted);
    }

    private void setListeners() {
        cvGroupNameTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Auth.isAuthorized("secretary")) {
                    Toasty.info(context, R.string.action_not_permitted, Toast.LENGTH_SHORT, true).show();
                    return;
                }

                int groupType = 0;
                groupType = AppController.getInstance().getChamaPreferenceManager().getGroupType();

                switch (groupType) {
                    case 0:
                    default:
                        Toasty.info(context, "Invalid group type. Kindly contact the administrator", Toast.LENGTH_SHORT, true).show();
                        break;
                    case 1:
                        String phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();
                        String groupNumber = AppController.getInstance().getChamaPreferenceManager().getGroupNumber();

                        final HashMap<String, String> map = new HashMap<>();
                        map.put("phone_number", phoneNumber);
                        map.put("group_number", groupNumber);

                        final String sessionToken = AppController.getInstance().getChamaPreferenceManager().getSessionToken();

                        fetchIndividualGroupAccounts(map, sessionToken);


                        break;
                }

            }
        });

        cvCancelTransactionsPendApproval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCompleteTransactions("rejected");

            }
        });

        cvSubmitTransactionsPendApproval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!cvSubmitTransactionsPendApproval.isEnabled()) {
                    Toasty.info(context, R.string.already_submitted, Toast.LENGTH_SHORT, true).show();
                    return;
                }

                onCompleteTransactions("accepted");
                cvSubmitTransactionsPendApproval.setEnabled(false);

            }
        });
    }

    @Override
    public void onMemberSearched(MemberEntity memberEntity) {

    }

    private void toggleDataViews(boolean hasItems) {
        if (hasItems)
            tvNoDataMemberTxn.setVisibility(View.GONE);
        else
            tvNoDataMemberTxn.setVisibility(View.VISIBLE);
    }

    private void onCompleteTransactions(final String action) {
        String phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();
        String groupNumber = AppController.getInstance().getChamaPreferenceManager().getGroupNumber();
        int groupType = AppController.getInstance().getChamaPreferenceManager().getGroupType();

        final HashMap<String, String> map = new HashMap<>();
        map.put("session_total", Config.sessionTotal);
        map.put("session_stamp", Config.sessionStamp);
        if (action.equals("accepted")) {
            map.put("session_action", "1");
        } else {
            map.put("session_action", "0");
        }
        map.put("group_type", String.valueOf(groupType));
        map.put("phone_number", phoneNumber);
        map.put("group_number", groupNumber);
        map.put("tag", "TFINCOMPLETE");


        final String sessionToken = AppController.getInstance().getChamaPreferenceManager().getSessionToken();
        processRequest(map, sessionToken);
    }

    SendTo st = new SendTo(context);
    HurlStack hurlStack = new HurlStack() {
        @Override
        protected HttpURLConnection createConnection(URL url) throws IOException {
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
            try {
                httpsURLConnection.setSSLSocketFactory(st.getSSLSocketFactory());
                httpsURLConnection.setHostnameVerifier(st.getHostnameVerifier());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return httpsURLConnection;
        }
    };

    private void processRequest(final HashMap<String, String> map, final String sessionToken) {
        final ProgressDialog loading = ProgressDialog.show(this, null, "Processing approval", false, false);


        final String requestTag = "chama_complete_all_transaction_postings";
        final String url = Config.TRANSACTION_POST_COMPLETE;
        RequestQueue requestQueue = Volley.newRequestQueue(SelectMemberAddTransactionsActivity.this);

        JsonObjectRequest addPostingCompleteReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            loading.dismiss();

                            switch (response.getString("status")) {
                                case "success":
                                    onComplete("Action submitted successfully");

                                    break;

                                case "failed":
                                case "fatal":
                                default:
                                    onComplete("Action could not be committed at the moment");
                                    break;
                            }
                        } catch (JSONException e) {
                            loading.dismiss();
                            Toasty.info(context, "An error occurred", Toast.LENGTH_LONG, true).show();
                            onComplete("Action could not be committed at the moment");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();

                onComplete("Action could not be committed at the moment");
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error")) {
                                    Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":

                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();

                                        break;

                                    default:
                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } else {
                    if (error instanceof NoConnectionError) {
                        Toasty.info(context, "No connection to the server", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof TimeoutError) {
                        Toasty.info(context, "The request has timed out", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof AuthFailureError) {
                        Toasty.info(context, "Authentication failed", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof ServerError) {
                        Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof NetworkError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof ParseError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else {
                        Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    }
                }
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionToken);

                return headers;
            }

        };

        addPostingCompleteReq.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));
        requestQueue.add(addPostingCompleteReq);
    }

    private void fetchIndividualGroupAccounts(final HashMap<String, String> map, final String sessionToken) {
        final ProgressDialog loading = ProgressDialog.show(context, null, "Fetching group accounts", false, false);

        final String requestTag = "chama_fetch_group_accounts_request";
        final String url = Config.FETCH_GROUP_ACCOUNTS;
        RequestQueue requestQueue = Volley.newRequestQueue(SelectMemberAddTransactionsActivity.this);

        JsonObjectRequest fetchGroupAccountsReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(final JSONObject response) {
                        try {
                            loading.dismiss();

                            switch (response.getString("status")) {
                                case "success":
                                    loading.show();
                                    loading.setMessage("Please wait");

                                    // Add accounts
                                    JSONArray groupAccountsArray = response.getJSONArray("accounts");

                                    // Validate accounts
                                    if (groupAccountsArray.length() <= 0) {
                                        onCompleteAlertDialog("Could not fetch group accounts");

                                        return;
                                    }


                                    if (groupAccountsArray.length() > 0) {

                                        // Clear persistent accounts
                                        accountViewModel.deleteAllGroupAccounts(map.get("group_number"));

                                        for (int i = 0; i < groupAccountsArray.length(); i++) {
                                            AccountEntity accountEntity = new AccountEntity();

                                            JSONObject groupAccountObject = groupAccountsArray.getJSONObject(i);

                                            accountEntity.setAccountName(groupAccountObject.getString("accountName"));
                                            accountEntity.setAccountNumber(groupAccountObject.getString("accountNumber"));
                                            accountEntity.setGroupNumber(map.get("group_number"));
                                            accountViewModel.insertAccount(accountEntity);
                                        }
                                    }
                                    actionAddTransaction();


                                    loading.dismiss();

                                    break;

                                case "failed":
                                case "fatal":
                                default:
                                    onCompleteAlertDialog("Could not fetch group accounts");
                                    break;
                            }
                        } catch (JSONException e) {
                            loading.dismiss();
                            onCompleteAlertDialog("Could not fetch group accounts");

                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                onCompleteAlertDialog("Could not fetch group accounts");

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error")) {
                                    Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":
                                    default:
                                        Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + context.getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } else {
                    if (error instanceof NoConnectionError) {
                        Toasty.info(context, "No connection to the server", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof TimeoutError) {
                        Toasty.info(context, "The request has timed out", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof AuthFailureError) {
                        Toasty.info(context, "Authentication failed", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof ServerError) {
                        Toasty.info(context, context.getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof NetworkError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof ParseError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else {
                        Toasty.info(context, context.getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    }
                }
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionToken);

                return headers;
            }

        };

        fetchGroupAccountsReq.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));
        requestQueue.add(fetchGroupAccountsReq);

    }

    private void onComplete(String message) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                    case DialogInterface.BUTTON_NEGATIVE:
                        startActivity(new Intent(context, MainChamaActivity.class));

                        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(true);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.ok), dialogClickListener);
        builder.show();
    }

    private void actionAddTransaction() {
        Intent i = new Intent(context, AddTransactionStructuredActivity.class);
        i.putExtra("group_number", AppController.getInstance().getChamaPreferenceManager().getGroupNumber());
        i.putExtra("customer_type", "group");
        context.startActivity(i);

        ((Activity) context).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void onCompleteAlertDialog(String message) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        };
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(false);
        builder.setTitle(context.getString(R.string.notification));
        builder.setIcon(R.drawable.ic_unaitas);
        builder.setMessage(message);
        builder.setPositiveButton(context.getString(R.string.ok), dialogClickListener);
        builder.show();
    }

}

package ke.co.payconnect.mocash.chama.data.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class ApprovalWithdrawEntity {
    @PrimaryKey(autoGenerate = true)
    private int uid;
    @ColumnInfo(name = "txn_id")
    private String txnID;
    @ColumnInfo(name = "maker_phone_number")
    private String makerPhoneNumber;
    @ColumnInfo(name = "amount")
    private String amount;
    @ColumnInfo(name = "destination")
    private String destination;
    @ColumnInfo(name = "narration")
    private String narration;
    @ColumnInfo(name = "withdraw_date")
    private String withdrawDate;
    @ColumnInfo(name = "approval_id")
    private String approvalID;
    @ColumnInfo(name = "approval_status")
    private String approvalStatus;
    @ColumnInfo(name = "approval_type")
    private String approvalType;
    @ColumnInfo(name = "approval_narration")
    private String approvalNarration;
    @ColumnInfo(name = "approval_date")
    private String approvalDate;


    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getTxnID() {
        return txnID;
    }

    public void setTxnID(String txnID) {
        this.txnID = txnID;
    }

    public String getMakerPhoneNumber() {
        return makerPhoneNumber;
    }

    public void setMakerPhoneNumber(String makerPhoneNumber) {
        this.makerPhoneNumber = makerPhoneNumber;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public String getWithdrawDate() {
        return withdrawDate;
    }

    public void setWithdrawDate(String withdrawDate) {
        this.withdrawDate = withdrawDate;
    }

    public String getApprovalID() {
        return approvalID;
    }

    public void setApprovalID(String approvalID) {
        this.approvalID = approvalID;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getApprovalType() {
        return approvalType;
    }

    public void setApprovalType(String approvalType) {
        this.approvalType = approvalType;
    }

    public String getApprovalNarration() {
        return approvalNarration;
    }

    public void setApprovalNarration(String approvalNarration) {
        this.approvalNarration = approvalNarration;
    }

    public String getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(String approvalDate) {
        this.approvalDate = approvalDate;
    }
}

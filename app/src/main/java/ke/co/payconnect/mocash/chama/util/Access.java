package ke.co.payconnect.mocash.chama.util;

import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.view.activity.LoginPinChamaActivity;
import ke.co.payconnect.mocash.mocash.view.activity.LandingActivity;

public class Access {

    public static void startSession() {
        long sessionStart = System.currentTimeMillis() / 1000;
        AppController.getInstance().getChamaPreferenceManager().setSessionStart(sessionStart);
    }

    public static void forceSessionExpiry() {
        long newSessionStart = AppController.getInstance().getChamaPreferenceManager().getSessionStart() - (Config.sessionExpiry + 1);

        AppController.getInstance().getChamaPreferenceManager().setSessionStart(newSessionStart);
    }

    public static void sessionStatusCheck(Activity activity) {
        /*
        Get saved time
        Get time now
        Compare. If the session has expired, perform logout
         */
        long timeNow = System.currentTimeMillis() / 1000;
        long sessionStartTime = AppController.getInstance().getChamaPreferenceManager().getSessionStart();

        if (sessionStartTime != 0) { // 0 -> No value has been saved yet
            if ((timeNow - sessionStartTime) > Config.sessionExpiry)
                logout(activity, true, true);
        }
    }

    public static void logout(Activity activity, boolean isStateRemember, boolean isSessionTimeout) {
        // Update logon status
        AppController.getInstance().getChamaPreferenceManager().setLogon(false);

        if (isSessionTimeout)
            Toasty.info(activity, activity.getString(R.string.session_timeout), Toast.LENGTH_SHORT, true).show();

        Intent i;

        if (isStateRemember)
            i = new Intent(activity, LoginPinChamaActivity.class);
        else
            i = new Intent(activity, LandingActivity.class);

        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

        activity.startActivity(i);

        activity.overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }
}

package ke.co.payconnect.mocash.mocash.view.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.transition.Slide;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Connection;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.mocash.util.Access;
import ke.co.payconnect.mocash.mocash.util.Config;

import static ke.co.payconnect.mocash.mocash.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.mocash.util.Config.RETRIES;
import static ke.co.payconnect.mocash.mocash.util.Config.TIMEOUT_MS;

public class AlertsActivity extends AppCompatActivity {
    private final Context context = this;
    private final Activity activity = this;

    // Views
    private Toolbar tbAlerts;
    private Switch swCreditAlerts;
    private ConstraintLayout clContainerAlerts;
    private TextView tvConnectivityStatusAlerts;

    // This boolean acts as the expected state for the switch toggle actions
    // For example: A checked state will expect a + boolean value, and vice versa
    private boolean creditSwitchState = true;

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            // With animated visibility transitions
            if (isConnected) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    Slide slideVisible = new Slide();
                    slideVisible.setSlideEdge(Gravity.TOP);
                    TransitionManager.beginDelayedTransition(clContainerAlerts, slideVisible);
                    tvConnectivityStatusAlerts.setVisibility(View.GONE);
                } else {
                    tvConnectivityStatusAlerts.setVisibility(View.GONE);
                }
            } else {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    Slide slideInvisible = new Slide();
                    slideInvisible.setSlideEdge(Gravity.TOP);
                    TransitionManager.beginDelayedTransition(clContainerAlerts, slideInvisible);
                    tvConnectivityStatusAlerts.setVisibility(View.VISIBLE);
                } else {
                    tvConnectivityStatusAlerts.setVisibility(View.VISIBLE);
                }
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alerts);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        } else {
            registerReceiver(connectionReceiver, intentFilter);
        }

        tbAlerts = findViewById(R.id.tbAlerts);
        setSupportActionBar(tbAlerts);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbAlerts.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        swCreditAlerts = findViewById(R.id.swCreditAlerts);
        clContainerAlerts = findViewById(R.id.clContainerAlerts);
        tvConnectivityStatusAlerts = findViewById(R.id.tvConnectivityStatusAlerts);

        if (AppController.getInstance().getMocashPreferenceManager().getDebitAlert()) {
            creditSwitchState = true;
            swCreditAlerts.setChecked(true);
        } else {
            creditSwitchState = false;
            swCreditAlerts.setChecked(true);
        }

        setListeners();
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void setListeners() {
        swCreditAlerts.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (creditSwitchState) {
                    creditSwitchState = false;
                    confirmActionAlert("AS");
                } else {
                    creditSwitchState = true;
                    confirmActionAlert("AU");
                }
            }
        });
    }

    private void confirmActionAlert(final String sub) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:

                        if (tvConnectivityStatusAlerts.getVisibility() == View.VISIBLE) {
                            Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                        } else {
                            if (new Connection(context).IsInternetConnected())
                                processRequest(sub);
                            else
                                Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                        }
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:

                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.confirm_action));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setPositiveButton(getString(R.string.proceed), dialogClickListener);
        builder.setNegativeButton(getString(R.string.cancel), dialogClickListener);
        if (sub.equals("AS")) {
            builder.setMessage("You are about to subscribe for SMS alerts for debits");
            builder.show();
        } else if (sub.equals("AU")) {
            builder.setMessage("You are about to unsubscribe from SMS alerts for debits");
            builder.show();
        }
    }

    private void responseAlert(String message) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.ok), dialogClickListener);
        builder.show();
    }

    private void notifySessionExpired(String message) {
        Access.forceSessionExpiry();

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        boolean stateRemember = AppController.getInstance().getMocashPreferenceManager().getRememberPhone();
                        Access.logout(activity, stateRemember, false);

                        Toasty.info(context, R.string.logout_message_post, Toast.LENGTH_SHORT, true).show();
                        break;
                }
            }
        };
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(false);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.sign_in), dialogClickListener);
        builder.show();
    }

    private void processRequest(String sub) {
        final ProgressDialog loading = ProgressDialog.show(this, null, "Processing request...", false, false);

        String mobileNumber = AppController.getInstance().getMocashPreferenceManager().getUser().getPhone();
        final String sessionToken = AppController.getInstance().getMocashPreferenceManager().getSessionToken();

        String requestTag = "alerts_request";
        String url = Config.ALERTS_URL;

        Map<String, String> map = new HashMap<>();
        map.put("phone_number", mobileNumber);
        map.put("account", mobileNumber);
        map.put("tag", sub);

        JsonObjectRequest alertsReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            loading.dismiss();

                            switch (response.getString("status")) {
                                case "success":
                                    AppController.getInstance().getMocashPreferenceManager().setDebiAlerts(true);
                                    responseAlert(response.getString("message"));
                                    break;

                                case "failed":
                                    AppController.getInstance().getMocashPreferenceManager().setDebiAlerts(false);
                                    Toasty.info(context, "Failed to grant your request", Toast.LENGTH_LONG, true).show();
                                    break;

                                default:
                                    AppController.getInstance().getMocashPreferenceManager().setDebiAlerts(true);
                                    Toasty.info(context, "The request could not be completed at this moment", Toast.LENGTH_LONG, true).show();
                                    break;
                            }

                        } catch (JSONException e) {
                            loading.dismiss();
                            AppController.getInstance().getMocashPreferenceManager().setDebiAlerts(false);
                            Toasty.info(context, "An error occurred", Toast.LENGTH_LONG, true).show();

                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                AppController.getInstance().getMocashPreferenceManager().setDebiAlerts(false);

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 403:
                            notifySessionExpired("Authorization failed: Your session has expired. Kindly sign-in to access this option");
                            break;
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error.")) {
                                    Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":
                                /* Validation errors - Sample response
                                  {
                                      "status": "fatal",
                                        "errors": [{
                                          "location": "body",
                                          "param": "agent_number",
                                          "msg": "Validation failed | CO::00-1"
                                        }]
                                 }
                                 */

                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();

                                        break;

                                    default:
                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                }
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionToken);

                return headers;
            }
        };

        alertsReq.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));

        // Adding request to queue
        AppController.getInstance().addToRequestQueue(alertsReq, requestTag);
    }
}

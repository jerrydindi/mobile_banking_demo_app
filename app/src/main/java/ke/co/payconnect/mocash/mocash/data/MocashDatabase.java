package ke.co.payconnect.mocash.mocash.data;

import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.room.Database;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import android.content.Context;
import androidx.annotation.NonNull;

import ke.co.payconnect.mocash.mocash.data.dao.AccountDao;
import ke.co.payconnect.mocash.mocash.data.dao.AccountDestinationDao;
import ke.co.payconnect.mocash.mocash.data.dao.FavDao;
import ke.co.payconnect.mocash.mocash.data.dao.LocationDao;
import ke.co.payconnect.mocash.mocash.data.dao.EmployerDao;
import ke.co.payconnect.mocash.mocash.data.entity.AccountDestinationEntity;
import ke.co.payconnect.mocash.mocash.data.entity.AccountEntity;
import ke.co.payconnect.mocash.mocash.data.entity.FavEntity;
import ke.co.payconnect.mocash.mocash.data.entity.LocationEntity;
import ke.co.payconnect.mocash.mocash.data.entity.EmployerEntity;
import ke.co.payconnect.mocash.mocash.util.Config;

@Database(
        entities = {
                AccountEntity.class,
                AccountDestinationEntity.class,
                FavEntity.class,
                LocationEntity.class,
                EmployerEntity.class
        },
        version = 1)
public abstract class MocashDatabase extends RoomDatabase {

    private static MocashDatabase INSTANCE;

    public static MocashDatabase getMocashDatabaseInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), MocashDatabase.class, Config.DATABASE_NAME)
                    .allowMainThreadQueries() // Allowed for 'if exists' queries and other exceptions
                    .fallbackToDestructiveMigration()
                    .build();
        }

        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    public abstract AccountDao accountDao();

    public abstract AccountDestinationDao accountDestinationDao();

    public abstract FavDao favDao();

    public abstract LocationDao locationDao();

    public abstract EmployerDao employerDao();

    @NonNull
    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
        return null;
    }

    @NonNull
    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return null;
    }
}


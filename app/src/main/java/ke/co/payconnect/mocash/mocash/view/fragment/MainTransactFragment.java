package ke.co.payconnect.mocash.mocash.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.mocash.view.activity.AccountsActivity;
import ke.co.payconnect.mocash.mocash.view.activity.AirtimeActivity;
import ke.co.payconnect.mocash.mocash.view.activity.EnquiriesActivity;
import ke.co.payconnect.mocash.mocash.view.activity.FundsTransferBaseActivity;
import ke.co.payconnect.mocash.mocash.view.activity.LoanActivity;
import ke.co.payconnect.mocash.mocash.view.activity.MobileMoneyOwnActivity;
import ke.co.payconnect.mocash.mocash.view.activity.PayBillsActivity;

public class MainTransactFragment extends Fragment {

    // Views
    private CardView cvAccountsTransact;
    private CardView cvEnquiriesTransact;
    private CardView cvFTTransact;
    private CardView cvWithdrawTransact;
    private CardView cvAirtimeTransact;
    private CardView cvPayBillTransact;
    private CardView cvLoansTransact;


    private OnMainTransactInteractionListener mListener;

    public MainTransactFragment() {
        // Required empty public constructor
    }


    public static MainTransactFragment newInstance() {
        return new MainTransactFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main_transact, container, false);

        cvAccountsTransact = view.findViewById(R.id.cvAccountsTransact);
        cvEnquiriesTransact = view.findViewById(R.id.cvEnquiriesTransact);
        cvFTTransact = view.findViewById(R.id.cvFTTransact);
        cvWithdrawTransact = view.findViewById(R.id.cvWithdrawTransact);
        cvAirtimeTransact = view.findViewById(R.id.cvAirtimeTransact);
        cvPayBillTransact = view.findViewById(R.id.cvPayBillTransact);
        cvLoansTransact = view.findViewById(R.id.cvLoansTransact);

        setListeners();

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onMainTransactionInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMainTransactInteractionListener) {
            mListener = (OnMainTransactInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnMainTransactInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnMainTransactInteractionListener {
        void onMainTransactionInteraction(Uri uri);
    }

    private void setListeners() {
        cvAccountsTransact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), AccountsActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvEnquiriesTransact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), EnquiriesActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvFTTransact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), FundsTransferBaseActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvWithdrawTransact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), MobileMoneyOwnActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvAirtimeTransact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), AirtimeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvPayBillTransact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), PayBillsActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvLoansTransact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), LoanActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });
    }
}

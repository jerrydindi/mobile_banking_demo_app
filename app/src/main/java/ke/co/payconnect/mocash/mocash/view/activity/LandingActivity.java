package ke.co.payconnect.mocash.mocash.view.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Connection;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.view.activity.LoginChamaActivity;
import ke.co.payconnect.mocash.mocash.util.Location;

public class LandingActivity extends AppCompatActivity {
    private final Context context = this;

    // Views
    private Button btLoginStartLanding;
    private Button btOpenAccountStartLanding;
    private CardView cvMpesaLanding;
    private CardView cvFTLanding;
    private CardView cvAirtimeBillsLanding;
    private CardView cvFeedbackLanding;
    private CardView cvLocateLanding;
    private CardView cvContactsLanding;
    private TextView tvConnectivityStatusLanding;
    private CardView cvLoginStartLanding;
    private TextView tvPoweredLanding;
    private CardView cvChamaLoginStartLanding;

    private ProgressDialog loading;

    private boolean doubleBackToExitPressedOnce = false;

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if (isConnected)
                tvConnectivityStatusLanding.setVisibility(View.GONE);
            else
                tvConnectivityStatusLanding.setVisibility(View.VISIBLE);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);

        // Translucent status bar
        if (Build.VERSION.SDK_INT >= 21)
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        } else {
            registerReceiver(connectionReceiver, intentFilter);
        }

        btLoginStartLanding = findViewById(R.id.btLoginStartLanding);
        btOpenAccountStartLanding = findViewById(R.id.btOpenAccountStartLanding);
        cvMpesaLanding = findViewById(R.id.cvMpesaWithdrawLanding);
        cvFTLanding = findViewById(R.id.cvFTLanding);
        cvAirtimeBillsLanding = findViewById(R.id.cvAirtimeBillsLanding);
        cvFeedbackLanding = findViewById(R.id.cvFeedbackLanding);
        cvLocateLanding = findViewById(R.id.cvLocateLanding);
        cvContactsLanding = findViewById(R.id.cvContactsLanding);
        tvConnectivityStatusLanding = findViewById(R.id.tvConnectivityStatusLanding);
        cvLoginStartLanding = findViewById(R.id.cvLoginStartLanding);
        tvPoweredLanding = findViewById(R.id.tvPoweredLanding);
        cvChamaLoginStartLanding = findViewById(R.id.cvChamaLoginStartLanding);

        AppController.getInstance().getMocashPreferenceManager().clear();

        Toasty.Config.getInstance()
                .tintIcon(false)
                .setToastTypeface(ResourcesCompat.getFont(this, R.font.jag_w))
                .allowQueue(true)
                .apply();

        loading = ProgressDialog.show(context, null, "Loading", false, true);
        setListeners();

    }

    @Override
    protected void onResume() {
        super.onResume();

        loading.dismiss();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();

            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();

            overridePendingTransition(R.xml.fade_in, R.xml.fade_out);

            System.exit(0);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toasty.info(context, "Tap again to exit", Toast.LENGTH_SHORT, true).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    private void setListeners() {
        btLoginStartLanding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(context, MainActivity.class));

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        btOpenAccountStartLanding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        cvMpesaLanding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSnackBar(v, "withdraw to M-Pesa");
            }
        });

        cvFTLanding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSnackBar(v, "get a loan");
            }
        });

        cvAirtimeBillsLanding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSnackBar(v, "buy airtime or pay bills");
            }
        });

        cvFeedbackLanding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, FeedbackActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvLocateLanding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Check internet connectivity
                if (tvConnectivityStatusLanding.getVisibility() == View.VISIBLE) {
                    Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                } else {
                    if (new Connection(context).IsInternetConnected()) {

                        new Location(context).fetchLocations();
                    } else
                        Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                }

            }
        });

        cvContactsLanding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, InfoActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvLoginStartLanding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // startActivity(new Intent(context, LoginPhoneActivity.class));
                startActivity(new Intent(context, LoginActivity.class));

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        tvPoweredLanding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent webIntent = new Intent(Intent.ACTION_VIEW);
                String websiteUrl = "http://payconnect.co.ke/";
                webIntent.setData(Uri.parse(websiteUrl));
                startActivity(webIntent);
            }
        });

        cvChamaLoginStartLanding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(context, LoginChamaActivity.class));

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

    }

    public void showSnackBar(View view, String content) {
        final Snackbar snackbar = Snackbar
                .make(view, "Login to " + content, Snackbar.LENGTH_LONG)
                .setAction("Login", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(context, LoginPhoneActivity.class);
                        startActivity(i);

                        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                    }
                });

        snackbar.setActionTextColor(getResources().getColor(R.color.colorPrimary));
        snackbar.show();
    }
}

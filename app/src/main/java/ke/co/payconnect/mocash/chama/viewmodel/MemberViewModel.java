package ke.co.payconnect.mocash.chama.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.ChamaDatabase;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;

public class MemberViewModel extends AndroidViewModel {

    private final LiveData<List<MemberEntity>> memberLiveList;
    private final LiveData<List<MemberEntity>> normalMemberLiveList;
    private final List<MemberEntity> memberList;
    private final int memberCount;
    private ChamaDatabase db;

    public MemberViewModel(@NonNull Application application) {
        super(application);

        db = ChamaDatabase.getAppDatabaseInstance(this.getApplication());
        memberLiveList = db.memberDao().getMembersLiveList();
        normalMemberLiveList = db.memberDao().getMembersTypeLiveList("0");
        memberCount = db.memberDao().getMemberCount();
        memberList = db.memberDao().getActiveMembers(true);
    }

    public LiveData<List<MemberEntity>> getMembersLiveList() {
        return memberLiveList;
    }

    public LiveData<List<MemberEntity>> getNonMembersLiveList() {
        String phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();
        return db.memberDao().getNonMembersLiveList(phoneNumber, "1");
    }

    public LiveData<List<MemberEntity>> getNormalMembersLiveList() {
        return normalMemberLiveList;
    }

    public List<MemberEntity> getActiveMembersList() {
        return memberList;
    }

    public int getMemberCount() {
        return memberCount;
    }

    public MemberEntity findMemberByPhoneNumber(String phoneNumber) {

        return db.memberDao().findMemberByPhoneNumber(phoneNumber);
    }

    public boolean doesMemberExistWithPhoneNumber(String phoneNumber) {

        return db.memberDao().doesMemberExistWithPhoneNumber(phoneNumber);
    }

    public void insertMember(MemberEntity memberEntity) {
        db.memberDao().insertMember(memberEntity);
    }


    public void updateMember(MemberEntity memberEntity) {
        db.memberDao().updateMember(memberEntity);
    }

    public void deleteMember(MemberEntity memberEntity) {
        db.memberDao().deleteMember(memberEntity);
    }

    public void deleteAllMembers() {
        db.memberDao().deleteAllMembers();
    }
}

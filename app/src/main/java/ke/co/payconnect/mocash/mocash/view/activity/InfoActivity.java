package ke.co.payconnect.mocash.mocash.view.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Connection;
import ke.co.payconnect.mocash.R;

public class InfoActivity extends AppCompatActivity {
    private Context context = this;

    // Views
    private Toolbar tbInfo;
    private CardView cvCallInfo;
    private CardView cvEmailInfo;
    private CardView cvFacebookInfo;
    private CardView cvTwitterInfo;
    private CardView cvYoutubeInfo;
    private CardView cvWebInfo;
    private TextView tvViewFAQInfo;
    private TextView tvLocateHOInfo;
    private TextView tvViewShareInfo;
    private View vContactPhone;
    private TextView tvOneCallPhone;
    private TextView tvTwoCallPhone;
    private View vContactEmail;
    private TextView tvOneCopyEmail;

    private ProgressDialog loading;

    private String contactCentrePhoneNumber = "+254721244139";
    private String[] emailAddress = {"info@unaitas.com"};
    private String subject = "Unaitas MoCash";
    private String youtubeVideoURL = "_vyMRriOA60";
    private String twitterUsername = "unaitas";
    private static String FACEBOOK_URL = "https://www.facebook.com/Unaitas";
    private static String FACEBOOK_PAGE_ID = "Unaitas";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        tbInfo = findViewById(R.id.tbInfo);
        setSupportActionBar(tbInfo);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbInfo.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvCallInfo = findViewById(R.id.cvCallInfo);
        cvEmailInfo = findViewById(R.id.cvEmailInfo);
        cvFacebookInfo = findViewById(R.id.cvFacebookInfo);
        cvTwitterInfo = findViewById(R.id.cvTwitterInfo);
        cvYoutubeInfo = findViewById(R.id.cvYoutubeInfo);
        cvWebInfo = findViewById(R.id.cvWebInfo);
        tvViewFAQInfo = findViewById(R.id.tvViewFAQInfo);
        tvLocateHOInfo = findViewById(R.id.tvLocateHOInfo);
        tvViewShareInfo = findViewById(R.id.tvViewShareInfo);

        vContactPhone = getLayoutInflater().inflate(R.layout.fragment_info_phone_bottom_sheet, null);
        tvOneCallPhone = vContactPhone.findViewById(R.id.tvOneCallPhone);
        tvTwoCallPhone = vContactPhone.findViewById(R.id.tvTwoCallPhone);

        vContactEmail = getLayoutInflater().inflate(R.layout.fragment_info_email_bottom_sheet, null);
        tvOneCopyEmail = vContactEmail.findViewById(R.id.tvOneCopyEmail);

        loading = ProgressDialog.show(context, null, "Loading", false, true);

        setListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loading.dismiss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void showPhoneContacts() {
        if (vContactPhone.getParent() != null)
            ((ViewGroup) vContactPhone.getParent()).removeView(vContactPhone);

        final BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(vContactPhone);
        dialog.show();

        tvOneCallPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call("0721244139");
                dialog.dismiss();
            }
        });

        tvTwoCallPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call("0709253000");
                dialog.dismiss();
            }
        });

    }

    private void showEmailContacts() {
        if (vContactEmail.getParent() != null)
            ((ViewGroup) vContactEmail.getParent()).removeView(vContactEmail);

        final BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(vContactEmail);
        dialog.show();

        tvOneCopyEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loading = ProgressDialog.show(context, null, "Loading", false, true);
                dialog.dismiss();

                composeEmail(emailAddress, subject);
            }
        });

    }

    private void setListeners() {
        cvCallInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPhoneContacts();
            }
        });

        cvEmailInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEmailContacts();
            }
        });

        cvFacebookInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loading = ProgressDialog.show(context, null, "Loading", false, true);

                Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
                String facebookUrl = openFacebookPageURL(context);
                facebookIntent.setData(Uri.parse(facebookUrl));
                startActivity(facebookIntent);
            }
        });

        cvTwitterInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loading = ProgressDialog.show(context, null, "Loading", false, true);

                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=" + twitterUsername)));
                } catch (Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/#!/" + twitterUsername)));
                }
            }
        });

        cvYoutubeInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loading = ProgressDialog.show(context, null, "Loading", false, true);

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube://" + youtubeVideoURL));
                startActivity(intent);
            }
        });

        cvWebInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loading = ProgressDialog.show(context, null, "Loading", false, true);

                Intent webIntent = new Intent(Intent.ACTION_VIEW);
                String websiteUrl = "https://www.ic_unaitas.com";
                webIntent.setData(Uri.parse(websiteUrl));
                startActivity(webIntent);
            }
        });

        tvViewFAQInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, FaqActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        tvLocateHOInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new Connection(context).IsInternetConnected()) {
                    loading = ProgressDialog.show(context, null, "Loading", false, true);

                    startActivity(new Intent(context, LocationsHQActivity.class));
                } else
                    Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
            }
        });

        tvViewShareInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loading = ProgressDialog.show(context, null, "Loading", false, true);

                Intent i = new Intent(android.content.Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.share_subject));
                i.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.share_text));
                startActivity(Intent.createChooser(i, getString(R.string.share_via)));
            }
        });
    }

    public void composeEmail(String[] addresses, String subject) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public String openFacebookPageURL(Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) {
                //newer versions of fb app
                return "fb://facewebmodal/f?href=" + FACEBOOK_URL;
            } else {
                //older versions of fb app
                return "fb://page/" + FACEBOOK_PAGE_ID;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return FACEBOOK_URL; //normal web url
        }
    }

    private void CallAlert() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:" + contactCentrePhoneNumber));
                        startActivity(intent);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.confirm_action));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage("You are about to call Unaitas head office on " + contactCentrePhoneNumber);
        builder.setPositiveButton(getString(R.string.proceed), dialogClickListener);
        builder.setNegativeButton(getString(R.string.cancel), dialogClickListener);
        builder.show();
    }

    private void Call(String phone) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phone));
        startActivity(intent);
    }

}
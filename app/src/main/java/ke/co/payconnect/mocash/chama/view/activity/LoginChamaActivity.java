package ke.co.payconnect.mocash.chama.view.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Connection;
import io.captano.utility.Version;
import io.captano.utility.ui.UI;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.GroupEntity;
import ke.co.payconnect.mocash.chama.util.Config;

import ke.co.payconnect.mocash.chama.util.SendTo;
import ke.co.payconnect.mocash.chama.util.Utility;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalExitViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalSecretaryViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalTransactionViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.GroupViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.TransactionViewModel;
import ke.co.payconnect.mocash.mocash.view.activity.LandingActivity;

import static ke.co.payconnect.mocash.chama.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.chama.util.Config.RETRIES;
import static ke.co.payconnect.mocash.chama.util.Config.TIMEOUT_MS;

public class LoginChamaActivity extends AppCompatActivity {
    private final Context context = this;
    private final Activity activity = this;

    private TextView tvConnectivityStatusChamaLogin;
    private TextView tvAttemptsChamaLogin;
    private AutoCompleteTextView actvPhoneChamaLogin;
    private AutoCompleteTextView actvPinChamaLogin;
    private SwitchCompat swSavePhoneChamaLogin;
    private Button btChamaLogin;
    private TextView tvForgotPinChamaLogin;
    private TextView tvPoweredChamaLogin;

    private double appVersion = 0;

    private final String permission = Manifest.permission.READ_PHONE_STATE;
    private static final int PERMISSION_REQUEST_CODE = 1;

    private GroupViewModel groupViewModel;

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if (isConnected)
                tvConnectivityStatusChamaLogin.setVisibility(View.GONE);
            else
                tvConnectivityStatusChamaLogin.setVisibility(View.VISIBLE);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_chama);

        if (Build.VERSION.SDK_INT >= 21)
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        } else {
            registerReceiver(connectionReceiver, intentFilter);
        }

        AppController.getInstance().getChamaPreferenceManager().setRememberPhone(false);

        tvConnectivityStatusChamaLogin = findViewById(R.id.tvConnectivityStatusChamaLogin);
        tvAttemptsChamaLogin = findViewById(R.id.tvAttemptsChamaLogin);
        actvPhoneChamaLogin = findViewById(R.id.actvPhoneChamaLogin);
        actvPinChamaLogin = findViewById(R.id.actvPinChamaLogin);
        swSavePhoneChamaLogin = findViewById(R.id.swSavePhoneChamaLogin);
        btChamaLogin = findViewById(R.id.btChamaLogin);
        tvForgotPinChamaLogin = findViewById(R.id.tvForgotPinChamaLogin);
        tvPoweredChamaLogin = findViewById(R.id.tvPoweredChamaLogin);

        setListeners();

        checkPermission();

        getAppVersion();

        groupViewModel = ViewModelProviders.of(this).get(GroupViewModel.class);
    }

    @Override
    protected void onResume() {
        super.onResume();

        final ProgressDialog loading = ProgressDialog.show(this, null, "Please wait", false, false);

        try {
            groupViewModel.deleteAllGroups();
        } catch (Exception ignored) {
        }

        loading.dismiss();
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void checkPermission() {
        if (Build.VERSION.SDK_INT < 29) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if (!isPermissionGranted(permission)) {
                requestPermission(permission);
            } else {
                if (getPhone() != null) {
                    String imei = getPhone().get(0);
                    if (imei != null)
                        AppController.getInstance().getChamaPreferenceManager().setIMEI(imei);
                }
            }
        } else {
            AppController.getInstance().getChamaPreferenceManager().setIMEI("Not Supported|v" + Build.VERSION.SDK_INT);
        }
    }

    private boolean isPermissionGranted(String permission) {
        if (Build.VERSION.SDK_INT >= 23) {
            int result = ContextCompat.checkSelfPermission(activity, permission);
            return result == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }

    private void requestPermission(String permission) {
        ActivityCompat.requestPermissions(activity, new String[]{permission}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (getPhone() != null) {
                    String imei = getPhone().get(0);
                    if (imei != null)
                        AppController.getInstance().getChamaPreferenceManager().setIMEI(imei);
                }
                Toasty.info(context, "Permission granted", Toast.LENGTH_SHORT, true).show();
            } else {
                Toasty.info(context, "Permission denied", Toast.LENGTH_SHORT, true).show();
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    private ArrayList<String> getPhone() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        if (ActivityCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED)
            return null;


        ArrayList<String> phoneList = new ArrayList<>();
        phoneList.add(telephonyManager != null ? telephonyManager.getImei() : null);
        return phoneList;
    }

    private void getAppVersion() {
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            appVersion = Double.parseDouble(pInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void setListeners() {
        btChamaLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (tvConnectivityStatusChamaLogin.getVisibility() == View.VISIBLE) {
                        Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                    } else {
                        if (new Connection(context).IsInternetConnected()) {
                            String phoneNumber = actvPhoneChamaLogin.getText().toString().trim();
                            String pin = actvPinChamaLogin.getText().toString().trim();

                            validation(phoneNumber, pin);
                        } else {
                            Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        swSavePhoneChamaLogin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    AppController.getInstance().getChamaPreferenceManager().setRememberPhone(true);
                else
                    AppController.getInstance().getChamaPreferenceManager().setRememberPhone(false);

            }
        });

        tvForgotPinChamaLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotPinAlert();
            }
        });

        tvPoweredChamaLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent webIntent = new Intent(Intent.ACTION_VIEW);
                String websiteUrl = "http://payconnect.co.ke/";
                webIntent.setData(Uri.parse(websiteUrl));
                startActivity(webIntent);
            }
        });
    }


    private void validation(String phoneNumber, String pin) {
        if (phoneNumber.isEmpty()) {
            Toasty.info(context, getString(R.string.phone_required), Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (pin.isEmpty()) {
            Toasty.info(context, getString(R.string.pin_required), Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (phoneNumber.substring(0, 3).equals("254"))
            phoneNumber = phoneNumber.substring(3);

        if (phoneNumber.substring(0, 1).equals("0"))
            phoneNumber = phoneNumber.substring(1);

        new UI(activity).hideSoftInput();


        final String imei = AppController.getInstance().getChamaPreferenceManager().getIMEI();


        Map<String, String> map = new HashMap<>();
        map.put("phone_number", "254" + phoneNumber);
        map.put("pin", pin);
        map.put("version", String.valueOf(appVersion));
        if (imei == null) {
            notifyFailedLogin("The Request is not permitted. Your device has been blocked.");
        } else {
            map.put("imei", imei);
        }


        if (isPermissionGranted(permission))

            processLogin(map);
        else
            notifyPermissionRequired();

    }

    SendTo st = new SendTo(context);
    HurlStack hurlStack = new HurlStack() {
        @Override
        protected HttpURLConnection createConnection(URL url) throws IOException {
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
            try {
                httpsURLConnection.setSSLSocketFactory(st.getSSLSocketFactory());
                httpsURLConnection.setHostnameVerifier(st.getHostnameVerifier());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return httpsURLConnection;
        }
    };

    public void processLogin(final Map<String, String> map) {
        final ProgressDialog loading = ProgressDialog.show(this, null, "Authenticating", false, false);

        final String requestTag = "chama_login_request";
        final String url = Config.LOGIN_URL;

        RequestQueue requestQueue = Volley.newRequestQueue(LoginChamaActivity.this);


        JsonObjectRequest loginReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(final JSONObject response) {
                        try {
                            loading.dismiss();
                            resetForm();
                            tvAttemptsChamaLogin.setVisibility(View.INVISIBLE);
                            System.out.println(":::::RESPONSE:::::" + response);

                            switch (response.getString("status")) {
                                case "success":
                                    // Check if the system is under maintenance
                                    if (response.getBoolean("shutdown_android")) {
                                        notifyFailedLogin(response.getString("shutdown_message"));
                                        return;
                                    }

                                    // Save session token
                                    Config.sessionExpiry = response.getInt("device_session_expiry");
                                    String sessionToken = response.getString("token");
                                    if (!sessionToken.isEmpty())
                                        AppController.getInstance().getChamaPreferenceManager().setSessionToken(sessionToken);

                                    // Save user data
                                    AppController.getInstance().getChamaPreferenceManager().setPhoneNumber(map.get("phone_number"));
                                    AppController.getInstance().getChamaPreferenceManager().setPin(map.get("pin"));

                                    // Check first login status
                                    switch (response.getString("change_pin")) {
                                        case "0":
                                            // Add groups
                                            JSONArray groupsArray = response.getJSONArray("groups");

                                            // Validate groups
                                            if (groupsArray.length() <= 0) {
                                                Toasty.info(context, "Login failed", Toast.LENGTH_SHORT, true).show();
                                                return;
                                            } else {

                                                if (groupsArray.length() > 0) {

                                                    // Clear persistent groups
                                                    groupViewModel.deleteAllGroups();

                                                    for (int i = 0; i < groupsArray.length(); i++) {
                                                        GroupEntity groupEntity = new GroupEntity();

                                                        JSONObject groupObject = groupsArray.getJSONObject(i);

                                                        groupEntity.setName(groupObject.getString("groupName"));
                                                        groupEntity.setNumber(groupObject.getString("groupNumber"));
                                                        groupEntity.setType(groupObject.getString("groupType"));

                                                        groupViewModel.insertGroup(groupEntity);
                                                    }
                                                }

                                                // Check update status
                                                if (!response.getBoolean("is_update_due")) {
                                                    versionCheck(response.getInt("version"), response.getString("update_message"), response.getBoolean("force_update"));

                                                    tvAttemptsChamaLogin.setVisibility(View.INVISIBLE);

                                                } else {
                                                    Toasty.info(context, "Login successful", Toast.LENGTH_SHORT, true).show();

                                                    tvAttemptsChamaLogin.setVisibility(View.INVISIBLE);

                                                    actionSelectGroup();
                                                }
                                            }
                                            break;
                                        case "1":
                                            Toasty.info(context, "Kindly verify your login details", Toast.LENGTH_LONG, true).show();

                                            openChangePinActivity();
                                            break;
                                        default:
                                            Toasty.info(context, "This request could not be verified at the moment", Toast.LENGTH_LONG, true).show();
                                            break;
                                    }
                                    break;

                                case "failed":

                                    tvAttemptsChamaLogin.setVisibility(View.VISIBLE);

                                    try {
                                        if (response.getString("response_code").equals("009")) {
                                            if (response.getString("attempts").equals("1")) {
                                                tvAttemptsChamaLogin.setText(response.getString("attempts") + " attempt remaining");
                                            } else {
                                                tvAttemptsChamaLogin.setText(response.getString("attempts") + " attempts remaining");
                                            }

                                            Toasty.info(context, "Login failed. Invalid credentials", Toast.LENGTH_SHORT, true).show();

                                        } else if (response.getString("response_code").equals("012")) {

                                            tvAttemptsChamaLogin.setText("Your account has been locked");
                                            accountBlockedDialog(false);

                                        } else {
                                            Toasty.info(context, "Login failed. Invalid credentials", Toast.LENGTH_SHORT, true).show();
                                        }

                                    } catch (JSONException e) {
                                        Toasty.info(context, "An error occurred", Toast.LENGTH_LONG, true).show();
                                    }

                                    break;
                                case "timeout":
                                    Toasty.info(context, "The request has timed out", Toast.LENGTH_SHORT, true).show();
                                    break;

                                case "fatal":
                                default:
                                    Toasty.info(context, "Login failed", Toast.LENGTH_SHORT, true).show();
                                    break;
                            }

                        } catch (JSONException e) {
                            loading.dismiss();
                            Toasty.info(context, "An error occurred", Toast.LENGTH_LONG, true).show();

                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                loading.dismiss();
                resetForm();


                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error")) {
                                    Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":

                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();

                                        break;

                                    default:
                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } else {
                    if (error instanceof NoConnectionError) {
                        Toasty.info(context, "No connection to the server", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof TimeoutError) {
                        Toasty.info(context, "The request has timed out", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof AuthFailureError) {
                        Toasty.info(context, "Authentication failed", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof ServerError) {
                        Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof NetworkError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof ParseError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else {
                        Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    }
                }

            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        requestQueue.add(loginReq);
//        loginReq.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));
//        AppController.getInstance().addToRequestQueue(loginReq, requestTag);
    }


    private void actionSelectGroup() {
        Intent i = new Intent(context, LoginGroupChamaActivity.class);
        startActivity(i);

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void resetForm() {
        actvPinChamaLogin.getText().clear();
    }

    private void notifyFailedLogin(String message) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(true);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.ok), dialogClickListener);
        builder.show();
    }

    private void notifyPermissionRequired() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                checkPermission();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(true);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage("Phone state permission allows Unaitas Chama to get the device identifier. Please allow it to be able to proceed.");
        builder.setPositiveButton(getString(R.string.ok), dialogClickListener);
        builder.show();
    }

    private void notifyUpdateVersion(String message, boolean forceUpdate) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        actionUpdateApp();
                        break;
                    case DialogInterface.BUTTON_NEUTRAL:
                        Toasty.info(context, "Login successful", Toast.LENGTH_SHORT, true).show();
                        actionSelectGroup();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(true);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.update), dialogClickListener);
        if (!forceUpdate)
            builder.setNeutralButton(getString(R.string.action_login), dialogClickListener);
        builder.show();
    }

    private void actionUpdateApp() {
        /*
        Re-directs user to app listing on PlayStore
         */

        final String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    private void openChangePinActivity() {
        Intent i = new Intent(context, LoginNewChamaActivity.class);
        startActivity(i);

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void versionCheck(int newVersion, String versionUpdateMessage, boolean forceUpdate) {
        try {
            int currentVersion = Version.getVersionCode(getApplication());
            if (currentVersion < newVersion) {
                notifyUpdateVersion(versionUpdateMessage, forceUpdate);
            } else {
                Toasty.info(context, "Login successful", Toast.LENGTH_SHORT, true).show();
                actionSelectGroup();
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void accountBlockedDialog(final boolean goBack) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:

                        if (goBack) {
                            Intent i = new Intent(context, LandingActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);

                            overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                        }

                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        // Do nothing
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.warning));
        builder.setIcon(R.drawable.ic_fyi);
        builder.setMessage(getString(R.string.locked_message));
        builder.setPositiveButton(getString(R.string.ok), dialogClickListener);
        builder.show();
    }

    private void forgotPinAlert() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);

                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:

                        call();
                        break;
                }
            }
        };


        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.confirm_action));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage("Kindly contact customer care for assistance. \n\nContact: 0709253000");
        builder.setPositiveButton("call", dialogClickListener);
        builder.show();
    }

    private void call() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + "0709253000"));
        startActivity(intent);
    }
}
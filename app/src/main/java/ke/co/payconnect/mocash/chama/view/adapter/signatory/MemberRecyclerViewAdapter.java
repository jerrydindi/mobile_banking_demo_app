package ke.co.payconnect.mocash.chama.view.adapter.signatory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;

import java.util.LinkedList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;
import ke.co.payconnect.mocash.chama.view.activity.member.structured.MemberDetailsStructuredActivity;
import ke.co.payconnect.mocash.chama.view.activity.member.unstructured.MemberDetailsActivity;

public class MemberRecyclerViewAdapter extends RecyclerView.Adapter<MemberRecyclerViewAdapter.MemberViewHolder> {
    private final Context context;
    private List<MemberEntity> memberEntities;
    private List<MemberEntity> memberEntitiesFiltered;

    public MemberRecyclerViewAdapter(Context context, List<MemberEntity> memberEntities) {
        super();
        this.context = context;
        this.memberEntities = memberEntities;
        this.memberEntitiesFiltered = memberEntities;
    }

    @NonNull
    @Override
    public MemberViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new MemberViewHolder(viewGroup);
    }

    @Override
    public void onBindViewHolder(@NonNull MemberViewHolder holder, int position) {
        try {
            final MemberEntity memberEntity = memberEntitiesFiltered.get(position);
            final int colour = context.getResources().getColor(R.color.colorPrimary);
            final String memberName = memberEntity.getFirstName() + " " + memberEntity.getLastName();

            holder.tdRoundIcon = TextDrawable.builder()
                    .beginConfig()
                    .endConfig()
                    .buildRoundRect(memberEntity.getFirstName().substring(0, 1).toUpperCase() + memberEntity.getLastName().substring(0, 1).toUpperCase(), colour, 100);

            holder.ivStructMembers.setImageDrawable(holder.tdRoundIcon);
            holder.tvNameStructMembers.setText(memberName);

            holder.clStructMembers.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int groupType;
                    groupType = AppController.getInstance().getChamaPreferenceManager().getGroupType();

                    Intent i;

                    switch (groupType) {
                        default:
                            Toasty.info(context, "Invalid group type. Kindly contact the administrator", Toast.LENGTH_SHORT, true).show();
                            break;
                        case 1:
                            i = new Intent(context, MemberDetailsStructuredActivity.class);

                            i.putExtra("member_phone_number", memberEntity.getPhoneNumber());
                            context.startActivity(i);

                            ((Activity) context).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                            break;
                        case 2:
                            i = new Intent(context, MemberDetailsActivity.class);

                            i.putExtra("member_phone_number", memberEntity.getPhoneNumber());
                            context.startActivity(i);

                            ((Activity) context).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                            break;
                    }
                }
            });
        } catch (Exception ignored) {
        }
    }

    @Override
    public int getItemCount() {
        return this.memberEntitiesFiltered.size();
    }

    class MemberViewHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout clStructMembers;
        private ImageView ivStructMembers;
        private TextView tvNameStructMembers;
        private TextDrawable tdRoundIcon;

        MemberViewHolder(ViewGroup container) {
            super(LayoutInflater.from(container.getContext()).inflate(R.layout.struct_list_members, container, false));
            clStructMembers = itemView.findViewById(R.id.clStructMembers);
            ivStructMembers = itemView.findViewById(R.id.ivStructMembers);
            tvNameStructMembers = itemView.findViewById(R.id.tvNameStructMembers);
        }
    }

    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    memberEntitiesFiltered = memberEntities;
                } else {
                    List<MemberEntity> filteredMemberEntities = new LinkedList<>();

                    for (MemberEntity memberEntity : memberEntities) {

                        if (memberEntity.getFirstName().toLowerCase().trim().contains(charString) || memberEntity.getLastName().toLowerCase().trim().contains(charString))
                            filteredMemberEntities.add(memberEntity);
                    }

                    memberEntitiesFiltered = filteredMemberEntities;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = memberEntitiesFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                memberEntitiesFiltered = (List<MemberEntity>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface MemberAdapterSearchListener {
        void onMemberSearched(MemberEntity memberEntity);
    }
}
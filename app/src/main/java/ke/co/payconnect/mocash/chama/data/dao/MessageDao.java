package ke.co.payconnect.mocash.chama.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.entity.MessageEntity;


@Dao
public interface MessageDao {
    @Query("SELECT EXISTS(SELECT 1 FROM MessageEntity WHERE uid == :uid LIMIT 1)")
    boolean doesMessageExistWithUID(final String uid);

    @Query("SELECT uid, message, sender_phone_number, group_number, date_added FROM MessageEntity WHERE group_number == :groupNumber ORDER BY date_added ASC")
    LiveData<List<MessageEntity>> getMessagesLiveList(final String groupNumber);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMessage(MessageEntity... messageEntities);

    @Update
    void updateMessage(MessageEntity... messageEntities);

    @Delete
    void deleteMessage(MessageEntity messageEntities);
}

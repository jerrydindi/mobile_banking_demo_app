package ke.co.payconnect.mocash.mocash.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import android.os.AsyncTask;
import androidx.annotation.NonNull;

import java.util.List;

import ke.co.payconnect.mocash.mocash.data.MocashDatabase;
import ke.co.payconnect.mocash.mocash.data.entity.AccountDestinationEntity;

public class AccountDestinationViewModel extends AndroidViewModel {
    private final LiveData<List<AccountDestinationEntity>> accountList;
    private final List<AccountDestinationEntity> accountListList;
    private MocashDatabase db;

    public AccountDestinationViewModel(@NonNull Application application) {
        super(application);

        db = MocashDatabase.getMocashDatabaseInstance(this.getApplication());
        accountList = db.accountDestinationDao().getAllAccountsDestination();
        accountListList = db.accountDestinationDao().getAllAccountsDestinationList();
    }

    public LiveData<List<AccountDestinationEntity>> getAccountLiveList() {
        return accountList;
    }

    public List<AccountDestinationEntity> getAccountList(){
        return accountListList;
    }

    public boolean accountExists(String accountNumber) {
        return db.accountDestinationDao().accountDestinationExists(accountNumber);
    }

    public void insertAccount(AccountDestinationEntity account) {
        new InsertAsyncTask(db).execute(account);
    }

    public void updateAccount(AccountDestinationEntity account) {
        new UpdateAsyncTask(db).execute(account);
    }

    public void deleteAccount(AccountDestinationEntity account) {
        new DeleteAsyncTask(db).execute(account);
    }

    public void deleteAllAccounts() {
        new DeleteAllAsyncTask(db).execute();
    }


    private static class InsertAsyncTask extends AsyncTask<AccountDestinationEntity, Void, Void> {

        private MocashDatabase db;

        InsertAsyncTask(MocashDatabase mocashDatabase) {
            db = mocashDatabase;
        }

        @Override
        protected Void doInBackground(final AccountDestinationEntity... accountDestinationEntities) {
            db.accountDestinationDao().insertAccountDestination(accountDestinationEntities[0]);
            return null;
        }
    }

    private static class UpdateAsyncTask extends AsyncTask<AccountDestinationEntity, Void, Void> {

        private MocashDatabase db;

        UpdateAsyncTask(MocashDatabase mocashDatabase) {
            db = mocashDatabase;
        }

        @Override
        protected Void doInBackground(final AccountDestinationEntity... accountDestinationEntities) {
            db.accountDestinationDao().updateAccountDestination(accountDestinationEntities[0]);
            return null;
        }
    }

    private static class DeleteAsyncTask extends AsyncTask<AccountDestinationEntity, Void, Void> {

        private MocashDatabase db;

        DeleteAsyncTask(MocashDatabase mocashDatabase) {
            db = mocashDatabase;
        }

        @Override
        protected Void doInBackground(final AccountDestinationEntity... accountDestinationEntities) {
            db.accountDestinationDao().deleteAccountDestination(accountDestinationEntities[0]);
            return null;
        }
    }

    private static class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {

        private MocashDatabase db;

        DeleteAllAsyncTask(MocashDatabase mocashDatabase) {
            db = mocashDatabase;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            db.accountDestinationDao().deleteAllAccountsDestination();
            return null;
        }
    }
}

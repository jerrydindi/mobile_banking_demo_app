package ke.co.payconnect.mocash.chama.view.adapter.member;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.TransactionEntity;
import ke.co.payconnect.mocash.chama.util.Utility;
import ke.co.payconnect.mocash.chama.viewmodel.TransactionViewModel;

public class PersonalTransactionsDatesRecyclerAdapter extends RecyclerView.Adapter<PersonalTransactionsDatesRecyclerAdapter.PersonalTransactionDateViewHolder> {
    private final Context context;
    private List<String> transactionDates;

    private PersonalTransactionsRecyclerViewAdapter adapter;

    private TransactionViewModel transactionViewModel;
    private String txnCategory;

    public PersonalTransactionsDatesRecyclerAdapter(Context context, List<String> transactionDates, String txnCategory) {
        this.context = context;
        this.transactionDates = transactionDates;
        this.txnCategory = txnCategory;
    }

    @NonNull
    @Override
    public PersonalTransactionDateViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_personal_transactions_dates, parent, false);
        return new PersonalTransactionDateViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final PersonalTransactionDateViewHolder holder, int position) {
        try {
            final String transactionDate = transactionDates.get(position);
            String phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();

            Calendar calendar;
            String date = transactionDate;
            try {
                calendar = Utility.getDateTime(transactionDate, "yyyy-MM-dd");
                @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                date = dateFormat.format(calendar.getTime());
                holder.tvDateStructPersonalTxnDates.setText(date);
            } catch (ParseException e) {
                holder.tvDateStructPersonalTxnDates.setText(date);
            }

            setupRecyclerView(holder, phoneNumber, transactionDate);
        } catch (Exception ignored) {
        }
    }

    @Override
    public int getItemCount() {
        return this.transactionDates.size();
    }


    class PersonalTransactionDateViewHolder extends RecyclerView.ViewHolder {
        private RecyclerView rvStructPersonalTxnDates;
        private TextView tvDateStructPersonalTxnDates;
        private TextView tvNoDataStructPersonalTxnDates;

        PersonalTransactionDateViewHolder(@NonNull View view) {
            super(view);

            rvStructPersonalTxnDates = view.findViewById(R.id.rvStructPersonalTxnDates);
            tvDateStructPersonalTxnDates = view.findViewById(R.id.tvDateStructPersonalTxnDates);
            tvNoDataStructPersonalTxnDates = view.findViewById(R.id.tvNoDataStructPersonalTxnDates);
        }
    }

    private void setupRecyclerView(final PersonalTransactionDateViewHolder holder, String phoneNumber, String transactionDate) {
        transactionViewModel = ViewModelProviders.of((FragmentActivity) context).get(TransactionViewModel.class);
        transactionViewModel.getTransactionListByNumberAndTranDate(phoneNumber, transactionDate).observe((LifecycleOwner) context, new Observer<List<TransactionEntity>>() {
            @Override
            public void onChanged(List<TransactionEntity> transactionEntities) {
                if (transactionEntities.size() > 0) {

                    List<TransactionEntity> filteredTransactionEntities = new LinkedList<>();

                    for (TransactionEntity transactionEntity : transactionEntities) {
                        if (transactionEntity.getTxnCategory().equals(txnCategory)) {
                            // First check if the transactions match the specified category
                            // Then filter the result depending on the txn category
                            if (txnCategory.equals("TXN-GRP-POSTING")) {
                                // Group posting
                                if (transactionEntity.getTxnProcessingStatus().toLowerCase().trim().equals("processed"))
                                    filteredTransactionEntities.add(transactionEntity);
                            } else if (txnCategory.equals("TXN-TBL-BANKING")) {
                                // Table banking
                                if (transactionEntity.getTxnProcessingStatus().toLowerCase().trim().equals("authorised"))
                                    filteredTransactionEntities.add(transactionEntity);
                            } else {
                                // None | Won't execute
                                filteredTransactionEntities.add(transactionEntity);
                            }
                        }
                    }


                    if (filteredTransactionEntities.size() > 0) {
                        toggleDataViews(holder, true);

                        adapter = new PersonalTransactionsRecyclerViewAdapter(context, filteredTransactionEntities);
                        adapter.notifyDataSetChanged();
                        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
                        holder.rvStructPersonalTxnDates.setAdapter(adapter);
                        holder.rvStructPersonalTxnDates.setLayoutManager(layoutManager);
                    } else {
                        toggleDataViews(holder, false);
                    }
                }
            }
        });
    }

    private void toggleDataViews(PersonalTransactionDateViewHolder holder, boolean hasTransactions) {
        if (hasTransactions) {
            holder.tvNoDataStructPersonalTxnDates.setVisibility(View.GONE);
            holder.rvStructPersonalTxnDates.setVisibility(View.VISIBLE);
        } else {
            holder.tvNoDataStructPersonalTxnDates.setVisibility(View.VISIBLE);
            holder.rvStructPersonalTxnDates.setVisibility(View.GONE);
        }
    }
}

package ke.co.payconnect.mocash.chama.view.adapter.secretary;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Money;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.chama.data.entity.TransactionEntity;
import ke.co.payconnect.mocash.chama.util.Auth;
import ke.co.payconnect.mocash.chama.util.Utility;

import static ke.co.payconnect.mocash.chama.view.activity.secretary.unstructured.AddTransactionActivity.transactionList;

public class MemberTransactionRecyclerViewAdapter extends RecyclerView.Adapter<MemberTransactionRecyclerViewAdapter.TransactionViewHolder> {
    private List<TransactionEntity> transactionEntities;
    private final Context context;

    public static AddTransactionListener listener;

    public MemberTransactionRecyclerViewAdapter(Context context, List<TransactionEntity> transactionEntities) {
        this.context = context;
        this.transactionEntities = transactionEntities;

        try {
            listener = (AddTransactionListener) context;

        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement AddTransactionListener");
        }
    }

    @NonNull
    @Override
    public TransactionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_list_member_transactions_structured, parent, false);
        return new TransactionViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final TransactionViewHolder holder, final int position) {
        try {
            final TransactionEntity transactionEntity = transactionEntities.get(position);

            holder.tvNoStructMemberTxnStructured.setText(String.valueOf(position + 1));
            holder.tvTypeStructMemberTxnStructured.setText(transactionEntity.getTxnType());
            holder.tvAccountStructMemberTxnStructured.setText(transactionEntity.getTxnAccount());
            holder.tvAmountStructMemberTxnStructured.setText(Money.format(Double.parseDouble(transactionEntity.getAmount())));
            holder.tvStatusStructMemberTxnStructured.setText(transactionEntity.getTxnStatus());
            switch (transactionEntity.getTxnStatus()) {
                case "Pending upload":
                    holder.tvStatusStructMemberTxnStructured.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    break;
                case "Uploaded":
                    holder.tvStatusStructMemberTxnStructured.setTextColor(context.getResources().getColor(R.color.cBL1));
                    break;
            }

            listener.onAddTransactionListAdapter();

            holder.ivRemoveStructMemberTxnStructured.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!Auth.isAuthorized("secretary")) {
                        Toasty.info(context, R.string.action_not_permitted, Toast.LENGTH_SHORT, true).show();
                        return;
                    }

                    transactionEntities.remove(position);
                    notifyDataSetChanged();

                    listener.onAddTransactionTotalAdapter(Double.parseDouble(transactionEntity.getAmount()));
                    listener.onAddTransactionListAdapter();
                }
            });

            Calendar calendar;
            String date;
            try {
                calendar = Utility.getDateTime(transactionEntity.getTxnDate(), "yyyy-MM-dd");
                @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                date = dateFormat.format(calendar.getTime());
                holder.tvDateStructMemberTxnStructured.setText(date);
            } catch (ParseException e) {
                holder.tvDateStructMemberTxnStructured.setText(transactionEntity.getTxnDate());
            }
        } catch (Exception ignored) {
        }
    }

    @Override
    public int getItemCount() {
        return this.transactionEntities.size();
    }

    class TransactionViewHolder extends RecyclerView.ViewHolder {
        private TextView tvNoStructMemberTxnStructured;
        private TextView tvDateStructMemberTxnStructured;
        private TextView tvTypeStructMemberTxnStructured;
        private TextView tvAccountStructMemberTxnStructured;
        private TextView tvAmountStructMemberTxnStructured;
        private TextView tvStatusStructMemberTxnStructured;
        private ImageView ivRemoveStructMemberTxnStructured;

        TransactionViewHolder(View view) {
            super(view);

            tvNoStructMemberTxnStructured = view.findViewById(R.id.tvNoStructMemberTxnStructured);
            tvDateStructMemberTxnStructured = view.findViewById(R.id.tvDateStructMemberTxnStructured);
            tvTypeStructMemberTxnStructured = view.findViewById(R.id.tvTypeStructMemberTxnStructured);
            tvAccountStructMemberTxnStructured = view.findViewById(R.id.tvAccountStructMemberTxnStructured);
            tvAmountStructMemberTxnStructured = view.findViewById(R.id.tvAmountStructMemberTxnStructured);
            tvStatusStructMemberTxnStructured = view.findViewById(R.id.tvStatusStructMemberTxnStructured);
            ivRemoveStructMemberTxnStructured = view.findViewById(R.id.ivRemoveStructMemberTxnStructured);
        }

    }

    public interface AddTransactionListener {
        void onAddTransactionTotalAdapter(double newTotal);

        void onAddTransactionListAdapter();
    }
}

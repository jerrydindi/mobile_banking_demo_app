package ke.co.payconnect.mocash.chama.data;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

import ke.co.payconnect.mocash.chama.data.dao.AccountDao;
import ke.co.payconnect.mocash.chama.data.dao.ApprovalDao;
import ke.co.payconnect.mocash.chama.data.dao.ApprovalExitDao;
import ke.co.payconnect.mocash.chama.data.dao.ApprovalFullStatementDao;
import ke.co.payconnect.mocash.chama.data.dao.ApprovalGroupBalanceDao;
import ke.co.payconnect.mocash.chama.data.dao.ApprovalGroupLoanBalanceDao;
import ke.co.payconnect.mocash.chama.data.dao.ApprovalMemberAddDao;
import ke.co.payconnect.mocash.chama.data.dao.ApprovalMinistatementDao;
import ke.co.payconnect.mocash.chama.data.dao.ApprovalSecretaryDao;
import ke.co.payconnect.mocash.chama.data.dao.ApprovalTransactionDao;
import ke.co.payconnect.mocash.chama.data.dao.ApprovalWithdrawDao;
import ke.co.payconnect.mocash.chama.data.dao.GroupDao;
import ke.co.payconnect.mocash.chama.data.dao.MemberDao;
import ke.co.payconnect.mocash.chama.data.dao.MessageDao;
import ke.co.payconnect.mocash.chama.data.dao.PermissionDao;
import ke.co.payconnect.mocash.chama.data.dao.TransactionDao;
import ke.co.payconnect.mocash.chama.data.entity.AccountEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalExitEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalFullStatementEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalGroupBalanceEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalGroupLoanBalanceEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalMemberAddEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalMinistatementEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalSecretaryEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalTransactionEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalWithdrawEntity;
import ke.co.payconnect.mocash.chama.data.entity.GroupEntity;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;
import ke.co.payconnect.mocash.chama.data.entity.MessageEntity;
import ke.co.payconnect.mocash.chama.data.entity.PermissionEntity;
import ke.co.payconnect.mocash.chama.data.entity.TransactionEntity;
import ke.co.payconnect.mocash.chama.util.Config;

@Database(
        entities = {
                MemberEntity.class,
                TransactionEntity.class,
                PermissionEntity.class,
                ApprovalEntity.class,
                ApprovalTransactionEntity.class,
                ApprovalExitEntity.class,
                ApprovalSecretaryEntity.class,
                ApprovalFullStatementEntity.class,
                ApprovalGroupBalanceEntity.class,
                ApprovalMinistatementEntity.class,
                ApprovalWithdrawEntity.class,
                AccountEntity.class,
                GroupEntity.class,
                MessageEntity.class,
                ApprovalMemberAddEntity.class,
                ApprovalGroupLoanBalanceEntity.class

        },
        version = 4)
public abstract class ChamaDatabase extends RoomDatabase {

    private static ChamaDatabase INSTANCE;

    public static ChamaDatabase getAppDatabaseInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), ChamaDatabase.class, Config.DATABASE_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }

        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    public abstract MemberDao memberDao();

    public abstract TransactionDao transactionDao();

    public abstract PermissionDao permissionDao();

    public abstract ApprovalDao approvalDao();

    public abstract ApprovalTransactionDao approvalTransactionDao();

    public abstract ApprovalExitDao approvalExitDao();

    public abstract ApprovalSecretaryDao approvalSecretaryDao();

    public abstract ApprovalFullStatementDao approvalFullStatementDao();

    public abstract ApprovalGroupBalanceDao approvalGroupBalanceDao();

    public abstract ApprovalMinistatementDao approvalMinistatementDao();

    public abstract ApprovalWithdrawDao approvalWithdrawDao();

    public abstract ApprovalMemberAddDao approvalMemberAddDao();

    public abstract ApprovalGroupLoanBalanceDao approvalGroupLoanBalanceDao();

    public abstract AccountDao accountDao();

    public abstract GroupDao groupDao();

    public abstract MessageDao messageDao();

    @NonNull
    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
        return null;
    }

    @NonNull
    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return null;
    }
}

package ke.co.payconnect.mocash.chama.view.activity.secretary;

import android.app.Activity;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;

import java.util.List;

import es.dmoral.toasty.Toasty;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;
import ke.co.payconnect.mocash.chama.util.Access;
import ke.co.payconnect.mocash.chama.util.Auth;
import ke.co.payconnect.mocash.chama.view.activity.secretary.unstructured.AddTransactionActivity;
import ke.co.payconnect.mocash.chama.view.adapter.secretary.SelectMemberAddTableBankingTransactionRecyclerViewAdapter;
import ke.co.payconnect.mocash.chama.viewmodel.AccountViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;

public class SelectMemberTableBankingActivity extends AppCompatActivity
        implements SelectMemberAddTableBankingTransactionRecyclerViewAdapter.MemberAdapterSearchListener {
    private final Activity activity = this;
    private final Context context = this;

    private Toolbar tbSelectMemberTblBanking;
    private TextView tvConnectivityStatusMemberTxn;
    private CardView cvGroupNameTitle;
    private TextView tvCreditGroupTransactionNameStruct;
    private RecyclerView rvMemberTxn;
    private TextView tvNoDataMemberTxn;
    private CardView cvSubmitTransactionsPendApproval;
    private CardView cvCancelTransactionsPendApproval;
    private ImageView ivGroupTxn;

    private SearchManager searchManager;
    private SearchView searchView;
    private LinearLayoutManager layoutManager;
    private DividerItemDecoration divideItemDecoration;
    public SelectMemberAddTableBankingTransactionRecyclerViewAdapter adapter;
    private TextDrawable tdRoundIcon;

    private MemberViewModel memberViewModel;
    private AccountViewModel accountViewModel;

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork;
            if (cm != null) {
                activeNetwork = cm.getActiveNetworkInfo();

                boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

                if (isConnected)
                    tvConnectivityStatusMemberTxn.setVisibility(View.GONE);
                else
                    tvConnectivityStatusMemberTxn.setVisibility(View.VISIBLE);
            } else {
                tvConnectivityStatusMemberTxn.setVisibility(View.GONE);
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_member_table_banking);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        else
            registerReceiver(connectionReceiver, intentFilter);

        tvConnectivityStatusMemberTxn = findViewById(R.id.tvConnectivityStatusMemberTxn);
        tbSelectMemberTblBanking = findViewById(R.id.tbSelectMemberTblBanking);
        setSupportActionBar(tbSelectMemberTblBanking);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbSelectMemberTblBanking.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvGroupNameTitle = findViewById(R.id.cvGroupNameTitle);
        tvCreditGroupTransactionNameStruct = findViewById(R.id.tvCreditGroupTransactionNameStruct);
        rvMemberTxn = findViewById(R.id.rvMemberTxn);
        tvNoDataMemberTxn = findViewById(R.id.tvNoDataMemberTxn);
        cvSubmitTransactionsPendApproval = findViewById(R.id.cvSubmitTransactionsPendApproval);
        cvCancelTransactionsPendApproval = findViewById(R.id.cvCancelTransactionsPendApproval);
        ivGroupTxn = findViewById(R.id.ivGroupTxn);

        layoutManager = new LinearLayoutManager(context);
        divideItemDecoration = new DividerItemDecoration(context, LinearLayoutManager.VERTICAL);
        rvMemberTxn.addItemDecoration(divideItemDecoration);

        memberViewModel = ViewModelProviders.of(this).get(MemberViewModel.class);
        accountViewModel = ViewModelProviders.of(this).get(AccountViewModel.class);

        setupRecyclerView();

        setHeader();

        setListeners();
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    @Override
    public boolean onKeyDown(int key_code, KeyEvent key_event) {
        if (key_code == KeyEvent.KEYCODE_BACK) {
            super.onKeyDown(key_code, key_event);
            return true;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();

        if (id == R.id.action_search)
            return true;

        return super.onOptionsItemSelected(menuItem);
    }

    private void setupRecyclerView() {
        memberViewModel.getMembersLiveList().observe((LifecycleOwner) context, new Observer<List<MemberEntity>>() {
            @Override
            public void onChanged(List<MemberEntity> memberEntities) {

                if (memberEntities.size() > 0) {
                    toggleDataViews(true);

                    adapter = new SelectMemberAddTableBankingTransactionRecyclerViewAdapter(context, memberEntities);
                    adapter.notifyDataSetChanged();

                    rvMemberTxn.setAdapter(adapter);
                    rvMemberTxn.setLayoutManager(layoutManager);
                } else
                    toggleDataViews(false);

            }
        });
    }

    private void setHeader() {
        String groupName = AppController.getInstance().getChamaPreferenceManager().getGroupName().toUpperCase();
        final int colour = context.getResources().getColor(R.color.colorPrimary);

        tdRoundIcon = TextDrawable.builder()
                .beginConfig()
                .endConfig()
                .buildRoundRect(groupName.substring(0, 1).toUpperCase(), colour, 100);
        ivGroupTxn.setImageDrawable(tdRoundIcon);

        tvCreditGroupTransactionNameStruct.setText(groupName);
    }

    private void setListeners() {
        cvGroupNameTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Auth.isAuthorized("secretary")) {
                    Toasty.info(context, R.string.action_not_permitted, Toast.LENGTH_SHORT, true).show();
                    return;
                }
                Intent i = new Intent(context, AddTransactionActivity.class);
                i.putExtra("txn_category", "TXN-TBL-BANKING");
                i.putExtra("group_number", AppController.getInstance().getChamaPreferenceManager().getGroupNumber());
                i.putExtra("customer_type", "group");
                startActivity(i);

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);

            }
        });
    }

    @Override
    public void onMemberSearched(MemberEntity memberEntity) {

    }

    private void toggleDataViews(boolean hasItems) {
        if (hasItems)
            tvNoDataMemberTxn.setVisibility(View.GONE);
        else
            tvNoDataMemberTxn.setVisibility(View.VISIBLE);
    }
}


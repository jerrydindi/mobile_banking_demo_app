package ke.co.payconnect.mocash.mocash.view.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import io.captano.utility.ui.UIController;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.mocash.data.object.User;
import ke.co.payconnect.mocash.mocash.util.Access;

public class ProfileActivity extends AppCompatActivity {
    private Context context = this;
    private Activity activity = this;

    // Views
    private Toolbar toolbar;

    private TextView tvFirstnameProfile;
    private TextView tvLastnameProfile;
    private TextView tvPhoneProfile;
    private TextView tvIdNumberProfile;
    private TextView tvViewAccountsProfile;

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        toolbar = findViewById(R.id.tbProfile);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        tvFirstnameProfile = findViewById(R.id.tvFirstnameProfile);
        tvLastnameProfile = findViewById(R.id.tvLastnameProfile);
        tvPhoneProfile = findViewById(R.id.tvPhoneProfile);
        tvIdNumberProfile = findViewById(R.id.tvIdNumberProfile);
        tvViewAccountsProfile = findViewById(R.id.tvViewAccountsProfile);


        user = AppController.getInstance().getMocashPreferenceManager().getUser();
        if (user != null || AppController.getInstance().getMocashPreferenceManager().getUser().getFirstname() != null) {
            tvFirstnameProfile.setText(user.getFirstname().substring(0, 1).toUpperCase() + user.getFirstname().substring(1));
            tvLastnameProfile.setText(user.getLastname().substring(0, 1).toUpperCase() + user.getLastname().substring(1));
            tvPhoneProfile.setText("0" + user.getPhone().substring(3));
            tvIdNumberProfile.setText(user.getNationalID());
        } else {
            new UIController().redirect(activity, LoginActivity.class);
        }

        setListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void setListeners() {
        tvViewAccountsProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, AccountsActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });
    }
}

package ke.co.payconnect.mocash.chama.view.activity.member.unstructured;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;

import com.amulyakhare.textdrawable.TextDrawable;

import java.text.ParseException;
import java.util.HashMap;

import es.dmoral.toasty.Toasty;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;
import ke.co.payconnect.mocash.chama.util.Access;
import ke.co.payconnect.mocash.chama.util.Auth;
import ke.co.payconnect.mocash.chama.util.Utility;
import ke.co.payconnect.mocash.chama.view.activity.signatory.MemberEditActivity;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;

public class MemberDetailsActivity extends AppCompatActivity {
    private final Context context = this;
    private final Activity activity = this;

    private Toolbar tbMemberDetails;
    private TextView tvConnectivityStatusMemberDetails;
    private ImageView ivMemberDetails;
    private TextView tvStatusMemberDetails;
    private TextView tvMemberEditDetails;
    private TextDrawable tdMemberDetails;
    private TextView tvNameMemberDetails;
    private TextView tvIDNumberMemberDetails;
    private TextView tvPhoneNumberMemberDetails;
    private TextView tvDateRegisteredMemberDetails;
    private TextView tvRoleMemberDetails;

    private String phoneNumber;
    private boolean isActive;
    private String name;
    private String idNumber;
    private String dateRegistered;
    private String roleName;

    private MemberViewModel memberViewModel;

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if (isConnected)
                tvConnectivityStatusMemberDetails.setVisibility(View.GONE);
            else
                tvConnectivityStatusMemberDetails.setVisibility(View.VISIBLE);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_details);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        else
            registerReceiver(connectionReceiver, intentFilter);

        tvConnectivityStatusMemberDetails = findViewById(R.id.tvConnectivityStatusMemberDetails);
        tbMemberDetails = findViewById(R.id.tbMemberDetails);
        setSupportActionBar(tbMemberDetails);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbMemberDetails.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        ivMemberDetails = findViewById(R.id.ivMemberDetails);
        tvStatusMemberDetails = findViewById(R.id.tvStatusMemberDetails);
        tvMemberEditDetails = findViewById(R.id.tvMemberEditDetails);
        tvNameMemberDetails = findViewById(R.id.tvNameMemberDetails);
        tvIDNumberMemberDetails = findViewById(R.id.tvIDNumberMemberDetails);
        tvPhoneNumberMemberDetails = findViewById(R.id.tvPhoneNumberMemberDetails);
        tvDateRegisteredMemberDetails = findViewById(R.id.tvDateRegisteredMemberDetails);
        tvRoleMemberDetails = findViewById(R.id.tvRoleMemberDetails);

        memberViewModel = ViewModelProviders.of(this).get(MemberViewModel.class);

        getData();

        setListeners();
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void getData() {
        Intent i = new Intent(getIntent());
        phoneNumber = i.getStringExtra("member_phone_number");

        MemberEntity memberEntity = memberViewModel.findMemberByPhoneNumber(phoneNumber);

        isActive = memberEntity.isActive();
        int colour = getResources().getColor(R.color.colorPrimary);
        tdMemberDetails = TextDrawable.builder()
                .beginConfig()
                .endConfig()
                .buildRoundRect(memberEntity.getFirstName().substring(0, 1).toUpperCase() + memberEntity.getLastName().substring(0, 1).toUpperCase(), colour, 200);

        name = memberEntity.getFirstName() + " " + memberEntity.getLastName();
        idNumber = memberEntity.getIdNumber();
        dateRegistered = memberEntity.getDateRegistered();
        if (memberEntity.getSignatory().equals("0") && memberEntity.getSecretary().equals("0"))
            roleName = "Member";
        else if (memberEntity.getSignatory().equals("1") && memberEntity.getSecretary().equals("0"))
            roleName = "Signatory | Member";
        else if (memberEntity.getSignatory().equals("0") && memberEntity.getSecretary().equals("1"))
            roleName = "Secretary | Member";
        else if (memberEntity.getSignatory().equals("1") && memberEntity.getSecretary().equals("1"))
            roleName = "Signatory | Secretary | Member";

        setData();
    }

    private void setData() {
        ivMemberDetails.setImageDrawable(tdMemberDetails);
        if (isActive) {
            tvStatusMemberDetails.setText(getString(R.string.active));
            tvStatusMemberDetails.setTextColor(getResources().getColor(R.color.cBL1));
        } else {
            tvStatusMemberDetails.setText(getString(R.string.inactive));
            tvStatusMemberDetails.setTextColor(getResources().getColor(R.color.colorPrimary));
        }
        tvNameMemberDetails.setText(name);
        tvIDNumberMemberDetails.setText(idNumber);
        tvPhoneNumberMemberDetails.setText(phoneNumber);
        try {
            String formattedDate = Utility.getDate(dateRegistered, "yyyy-MM-dd HH:mm:ss");
            tvDateRegisteredMemberDetails.setText(formattedDate);
        } catch (ParseException e) {
            tvDateRegisteredMemberDetails.setText(dateRegistered);
        }
        tvRoleMemberDetails.setText(roleName);
    }

    private void setListeners() {
        MemberEntity memberEntity = memberViewModel.findMemberByPhoneNumber(phoneNumber);

        if(memberEntity.getSignatory().equals("1") || memberEntity.getSignatory().equals("0")) {
            tvMemberEditDetails.setVisibility(View.INVISIBLE);
        } else {
            tvMemberEditDetails.setVisibility(View.VISIBLE);
        }
        tvMemberEditDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Auth.isAuthorized("signatory")) {
                    Toasty.info(context, R.string.action_not_permitted, Toast.LENGTH_SHORT, true).show();
                    return;
                }

                Intent i = new Intent(context, MemberEditActivity.class);

                i.putExtra("member_phone_number", phoneNumber);
                context.startActivity(i);

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });
    }
}

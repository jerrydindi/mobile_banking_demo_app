package ke.co.payconnect.mocash.chama.util;

import ke.co.payconnect.mocash.application.AppController;

public class Auth {
    public static boolean isAuthorized(String permissionLevel) {
        String signatory = AppController.getInstance().getChamaPreferenceManager().getSignatoryRole(); // 1/ 0
        String secretary = AppController.getInstance().getChamaPreferenceManager().getSecretaryRole(); // 1/ 0

        if (signatory != null || secretary != null) {
            switch (permissionLevel) {
                case "signatory":
                    return signatory.equals("1");
                case "secretary":
                    return secretary.equals("1");
                default:
                    return false;
            }
        } else {
            return false;
        }

    }

}

package ke.co.payconnect.mocash.chama.view.activity;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.List;

import es.dmoral.toasty.Toasty;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.GroupEntity;
import ke.co.payconnect.mocash.chama.util.Access;
import ke.co.payconnect.mocash.chama.util.Utility;
import ke.co.payconnect.mocash.chama.view.activity.signatory.ApprovalAddMemberActivity;
import ke.co.payconnect.mocash.chama.view.adapter.LoginGroupRecyclerViewAdapter;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalExitViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalFullStatementViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalGroupBalanceViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalGroupLoanBalanceViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalMemberAddViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalMinistatementViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalSecretaryViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalTransactionViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalWithdrawViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.GroupViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.TransactionViewModel;

public class LoginGroupChamaActivity extends AppCompatActivity {
    private final Activity activity = this;
    private final Context context = this;

    //views
    private Toolbar tbGroupChamaLogin;
    private RecyclerView rvGroupChamaLogin;
    private TextView tvNoDataGroupChamaLogin;

    private LinearLayoutManager layoutManager;

    public LoginGroupRecyclerViewAdapter adapter;

    private GroupViewModel groupViewModel;
    private MemberViewModel memberViewModel;
    private TransactionViewModel transactionViewModel;
    private ApprovalViewModel approvalViewModel;
    private ApprovalTransactionViewModel approvalTransactionViewModel;
    private ApprovalSecretaryViewModel approvalSecretaryViewModel;
    private ApprovalExitViewModel approvalExitViewModel;
    private ApprovalGroupBalanceViewModel approvalGroupBalanceViewModel;
    private ApprovalMinistatementViewModel approvalMinistatementViewModel;
    private ApprovalFullStatementViewModel approvalFullStatementViewModel;
    private ApprovalWithdrawViewModel approvalWithdrawViewModel;
    private ApprovalMemberAddViewModel approvalMemberAddViewModel;
    private ApprovalGroupLoanBalanceViewModel approvalGroupLoanBalanceViewModel;

    private FirebaseFirestore firebaseFirestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_group_chama);

        // Translucent status bar
        if (Build.VERSION.SDK_INT >= 21) {
            Window w = getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }

        tbGroupChamaLogin = findViewById(R.id.tbGroupChamaLogin);

        setSupportActionBar(tbGroupChamaLogin);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        rvGroupChamaLogin = findViewById(R.id.rvGroupChamaLogin);
        tvNoDataGroupChamaLogin = findViewById(R.id.tvNoDataGroupChamaLogin);
        layoutManager = new LinearLayoutManager(context);

        memberViewModel = ViewModelProviders.of((FragmentActivity) context).get(MemberViewModel.class);
        transactionViewModel = ViewModelProviders.of((FragmentActivity) context).get(TransactionViewModel.class);
        approvalViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalViewModel.class);
        approvalTransactionViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalTransactionViewModel.class);
        approvalSecretaryViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalSecretaryViewModel.class);
        approvalExitViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalExitViewModel.class);
        approvalGroupBalanceViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalGroupBalanceViewModel.class);
        approvalMinistatementViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalMinistatementViewModel.class);
        approvalFullStatementViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalFullStatementViewModel.class);
        approvalWithdrawViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalWithdrawViewModel.class);
        approvalMemberAddViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalMemberAddViewModel.class);
        approvalGroupLoanBalanceViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalGroupLoanBalanceViewModel.class);

        setupRecyclerView();

        firebaseFirestore = FirebaseFirestore.getInstance();

        initDevice();
    }

    @Override
    protected void onResume() {
        super.onResume();

        final ProgressDialog loading = ProgressDialog.show(this, null, "Please wait", false, false);

        try {
            memberViewModel.deleteAllMembers();
            transactionViewModel.deleteAllTransactions();
            approvalViewModel.deleteAllApprovalsWhere("transaction");
            approvalTransactionViewModel.deleteAllApprovalTransactions();
            approvalViewModel.deleteAllApprovalsWhere("assignSecretary");
            approvalViewModel.deleteAllApprovalsWhere("assign-secretary");
            approvalSecretaryViewModel.deleteAllSecretaries();
            approvalViewModel.deleteAllApprovalsWhere("memberExit");
            approvalViewModel.deleteAllApprovalsWhere("exit");
            approvalExitViewModel.deleteAllExits();
            approvalViewModel.deleteAllApprovalsWhere("groupBalance");
            approvalViewModel.deleteAllApprovalsWhere("group-balance");
            approvalGroupBalanceViewModel.deleteAllGroupBalances();
            approvalViewModel.deleteAllApprovalsWhere("ministatement");
            approvalMinistatementViewModel.deleteAllMinistatements();
            approvalViewModel.deleteAllApprovalsWhere("full-statement");
            approvalFullStatementViewModel.deleteAllFullStatements();
            approvalViewModel.deleteAllApprovalsWhere("withdraw");
            approvalWithdrawViewModel.deleteAllApprovalWithdraws();
            approvalViewModel.deleteAllApprovalsWhere("member-add");
            approvalMemberAddViewModel.deleteAllMemberAddApprovals();
            approvalViewModel.deleteAllApprovalsWhere("group-loan-balance");
            approvalGroupLoanBalanceViewModel.deleteAllGroupLoanBalances();


        } catch (Exception ignored) {
        }

        loading.dismiss();
    }

    @Override
    public void onBackPressed() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        if (AppController.getInstance().getMocashPreferenceManager().getRememberPhone())
                            Access.logout(activity, true, false);
                        else
                            Access.logout(activity, false, false);

                        Toasty.info(context, "You have been logged out", Toast.LENGTH_SHORT, true).show();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage("You will be logged out");
        builder.setPositiveButton(getString(R.string.proceed), dialogClickListener);
        builder.setNegativeButton(getString(R.string.cancel), dialogClickListener);
        builder.show();

    }

    private void setupRecyclerView() {
        groupViewModel = ViewModelProviders.of(this).get(GroupViewModel.class);
        groupViewModel.getaccountList().observe((LifecycleOwner) context, new Observer<List<GroupEntity>>() {
            @Override
            public void onChanged(List<GroupEntity> groupEntities) {
                if (groupEntities.size() > 0) {
                    toggleDataViews(true);
                    adapter = new LoginGroupRecyclerViewAdapter(context, groupEntities);
                    adapter.notifyDataSetChanged();

                    rvGroupChamaLogin.setAdapter(adapter);
                    rvGroupChamaLogin.setLayoutManager(layoutManager);
                } else {
                    toggleDataViews(false);
                }

            }
        });
    }

    private void toggleDataViews(boolean hasItems) {
        if (hasItems)
            tvNoDataGroupChamaLogin.setVisibility(View.GONE);
        else
            tvNoDataGroupChamaLogin.setVisibility(View.VISIBLE);
    }

    private void initDevice() {
        AppController.getInstance();

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Utility.registerNotificationChannel(notificationManager);


        if (Utility.isDeviceSupported(context)) {
            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                        @Override
                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
                            if (!task.isSuccessful())
                                return;

                            String token = task.getResult().getToken();

                            AppController.getInstance().getChamaPreferenceManager().setDeviceFCMToken(token);
                            Utility.sendDeviceFCMToken(token, firebaseFirestore);

                        }
                    });

        } else {
            notifyUnsupportedDevice();
            return;
        }
    }

    private void notifyUnsupportedDevice() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                        finish();
                        System.exit(0);
                        break;
                }
            }
        };


        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.notification);
        builder.setIcon(R.drawable.ic_unaitas);
        builder.setMessage("Device not supported. You will not be able to receive notifications on this device.");
        builder.setPositiveButton(getString(R.string.close), dialogClickListener);
        builder.setCancelable(false);
        builder.show();
    }
}

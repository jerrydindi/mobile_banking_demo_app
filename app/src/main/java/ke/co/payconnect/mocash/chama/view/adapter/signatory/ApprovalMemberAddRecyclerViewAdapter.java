package ke.co.payconnect.mocash.chama.view.adapter.signatory;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalMemberAddEntity;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;
import ke.co.payconnect.mocash.chama.util.Auth;
import ke.co.payconnect.mocash.chama.util.Utility;
import ke.co.payconnect.mocash.chama.view.activity.signatory.ApprovalAddMemberActivity;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;

public class ApprovalMemberAddRecyclerViewAdapter extends RecyclerView.Adapter<ApprovalMemberAddRecyclerViewAdapter.ApprovalViewHolder> {
    private final Context context;
    private List<ApprovalMemberAddEntity> approvalMemberAddEntities;

    ApprovalMemberAddRecyclerViewAdapter(Context context, List<ApprovalMemberAddEntity> approvalMemberAddEntities) {
        this.context = context;
        this.approvalMemberAddEntities = approvalMemberAddEntities;
    }

    @NonNull
    @Override
    public ApprovalMemberAddRecyclerViewAdapter.ApprovalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_approval_group_member_add, parent, false);
        return new ApprovalMemberAddRecyclerViewAdapter.ApprovalViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ApprovalMemberAddRecyclerViewAdapter.ApprovalViewHolder holder, int position) {
        try {
            final ApprovalMemberAddEntity approvalEntity = approvalMemberAddEntities.get(position);

            MemberViewModel memberViewModel = ViewModelProviders.of((FragmentActivity) context).get(MemberViewModel.class);

            MemberEntity memberEntity = memberViewModel.findMemberByPhoneNumber(approvalEntity.getMakerPhonenumber());

            holder.tvNoStructApprovalGroupMemberAdd.setText(String.valueOf(position + 1));
            holder.tvMemberStructApprovalGroupMemberAdd.setText(memberEntity.getFirstName() + " " + memberEntity.getLastName());

            String addDate = approvalEntity.getApprovalDate();

            try {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                Date date = format.parse(addDate);
                Date dateToCompare = format.parse("2000-01-01");

                if ((date != null ? date.compareTo(dateToCompare) : 0) == 0) {
                    addDate = "NA";
                }
            } catch (ParseException ignored) {
            }

            if (!addDate.equals("NA")) {
                Calendar calendar;
                String date;
                try {
                    calendar = Utility.getDateTime(addDate, "yyyy-MM-dd");
                    @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    date = dateFormat.format(calendar.getTime());
                    holder.tvDateStructApprovalGroupMemberAdd.setText(date);
                } catch (ParseException e) {
                    holder.tvDateStructApprovalGroupMemberAdd.setText(approvalEntity.getAddDate());
                }
            } else {
                holder.tvDateStructApprovalGroupMemberAdd.setText(addDate);
            }

            if (!approvalEntity.getApprovalStatus().toLowerCase().equals("pending"))
                holder.clStructApprovalGroupMemberAdd.setBackgroundColor(context.getResources().getColor(R.color.cG6));

            holder.clStructApprovalGroupMemberAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!Auth.isAuthorized("signatory")) {
                        Toasty.info(context, R.string.action_not_permitted, Toast.LENGTH_SHORT, true).show();
                        return;
                    }

                    Intent i = new Intent(context, ApprovalAddMemberActivity.class);
                    i.putExtra("approval_id", approvalEntity.getApprovalID());
                    context.startActivity(i);

                    ((Activity) context).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                }
            });
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return this.approvalMemberAddEntities.size();
    }

    class ApprovalViewHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout clStructApprovalGroupMemberAdd;
        private TextView tvNoStructApprovalGroupMemberAdd;
        private TextView tvDateStructApprovalGroupMemberAdd;
        private TextView tvMemberStructApprovalGroupMemberAdd;

        ApprovalViewHolder(View view) {
            super(view);

            clStructApprovalGroupMemberAdd = view.findViewById(R.id.clStructApprovalGroupMemberAdd);
            tvNoStructApprovalGroupMemberAdd = view.findViewById(R.id.tvNoStructApprovalGroupMemberAdd);
            tvDateStructApprovalGroupMemberAdd = view.findViewById(R.id.tvDateStructApprovalGroupMemberAdd);
            tvMemberStructApprovalGroupMemberAdd = view.findViewById(R.id.tvMemberStructApprovalGroupMemberAdd);
        }

    }
}

package ke.co.payconnect.mocash.chama.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;


import ke.co.payconnect.mocash.chama.data.entity.ApprovalMemberAddEntity;


@Dao
public interface ApprovalMemberAddDao {

    @Query("SELECT uid, signatory_phonenumber, maker_phonenumber, add_date, approval_id, approval_status, approval_type, approval_narration, approval_date, narration FROM ApprovalMemberAddEntity WHERE approval_type == :approvalType ORDER BY uid DESC")
    LiveData<List<ApprovalMemberAddEntity>> getApprovalAddMembersByTypeLive(final String approvalType);

    @Query("SELECT uid, signatory_phonenumber, maker_phonenumber, approval_id, add_date, approval_status, approval_type, approval_narration, approval_date, narration FROM ApprovalMemberAddEntity WHERE approval_id == :approvalID ORDER BY uid DESC")
    ApprovalMemberAddEntity findMemberAddApprovalById(String approvalID);

    @Query("SELECT EXISTS(SELECT 1 FROM ApprovalMemberAddEntity WHERE approval_id == :approvalID LIMIT 1)")
    boolean doesMemberAddApprovalExistWithApprovalId(String approvalID);

    @Query("UPDATE ApprovalMemberAddEntity SET approval_status = :approvalStatus, approval_narration = :approvalNarration, approval_date = :approvalDate WHERE approval_id == :approvalID")
    void updateApprovalMemberAddSetData(final String approvalID, final String approvalStatus, final String approvalNarration, final String approvalDate);

    @Insert
    void insertApprovalMemberAdd(ApprovalMemberAddEntity... approvalMemberAddEntities);

    @Update
    void updateApprovalMemberAdd(ApprovalMemberAddEntity... approvalMemberAddEntities);

    @Delete
    void deleteApprovalMemberAdd(ApprovalMemberAddEntity... approvalMemberAddEntities);

    @Query("DELETE FROM ApprovalMemberAddEntity")
    void deleteAllMemberAddApprovals();
}

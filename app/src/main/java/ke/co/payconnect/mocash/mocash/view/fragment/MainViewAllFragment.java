package ke.co.payconnect.mocash.mocash.view.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Connection;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.mocash.view.activity.AccountsActivity;
import ke.co.payconnect.mocash.mocash.view.activity.AirtimeActivity;
import ke.co.payconnect.mocash.mocash.view.activity.AlertsActivity;
import ke.co.payconnect.mocash.mocash.view.activity.CardsActivity;
import ke.co.payconnect.mocash.mocash.view.activity.ChangePinActivity;
import ke.co.payconnect.mocash.mocash.view.activity.EnquiriesActivity;
import ke.co.payconnect.mocash.mocash.view.activity.FaqActivity;
import ke.co.payconnect.mocash.mocash.view.activity.FavouritesActivity;
import ke.co.payconnect.mocash.mocash.view.activity.FeedbackActivity;
import ke.co.payconnect.mocash.mocash.view.activity.FundsTransferBaseActivity;
import ke.co.payconnect.mocash.mocash.view.activity.InfoActivity;
import ke.co.payconnect.mocash.mocash.view.activity.LoanActivity;
import ke.co.payconnect.mocash.mocash.view.activity.LocationsActivity;
import ke.co.payconnect.mocash.mocash.view.activity.MobileMoneyOwnActivity;
import ke.co.payconnect.mocash.mocash.view.activity.PayBillsActivity;
import ke.co.payconnect.mocash.mocash.view.activity.ProfileActivity;

public class MainViewAllFragment extends Fragment {

    // Views
    private CardView cvAccountsViewAll;
    private CardView cvEnquiriesViewAll;
    private CardView cvAirtimeViewAll;
    private CardView cvWithdrawViewAll;
    private CardView cvFTViewAll;
    private CardView cvLoansViewAll;
    private CardView cvPayBillViewAll;
    private CardView cvCardsViewAll;
    private CardView cvChangePinViewAll;
    private CardView cvProfileViewAll;
    private CardView cvAlertsViewAll;
    private CardView cvFavViewAll;
    private CardView cvShareViewAll;
    private CardView cvFeedbackViewAll;
    private CardView cvInfoViewAll;
    private CardView cvLocateViewAll;
    private CardView cvFaqViewAll;

    private OnMainViewAllInteractionListener listener;

    private ProgressDialog loading;

    public static MainViewAllFragment newInstance() {
        return new MainViewAllFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main_view_all, container, false);

        cvAccountsViewAll = view.findViewById(R.id.cvAccountsViewAll);
        cvEnquiriesViewAll = view.findViewById(R.id.cvEnquiriesViewAll);
        cvAirtimeViewAll = view.findViewById(R.id.cvAirtimeViewAll);
        cvWithdrawViewAll = view.findViewById(R.id.cvWithdrawViewAll);
        cvFTViewAll = view.findViewById(R.id.cvFTViewAll);
        cvLoansViewAll = view.findViewById(R.id.cvLoansViewAll);
        cvPayBillViewAll = view.findViewById(R.id.cvPayBillViewAll);
        cvCardsViewAll = view.findViewById(R.id.cvCardsViewAll);
        cvChangePinViewAll = view.findViewById(R.id.cvChangePinViewAll);
        cvProfileViewAll = view.findViewById(R.id.cvProfileViewAll);
        cvAlertsViewAll = view.findViewById(R.id.cvAlertsViewAll);
        cvFavViewAll = view.findViewById(R.id.cvFavViewAll);
        cvShareViewAll = view.findViewById(R.id.cvShareViewAll);
        cvFeedbackViewAll = view.findViewById(R.id.cvFeedbackViewAll);
        cvInfoViewAll = view.findViewById(R.id.cvInfoViewAll);
        cvLocateViewAll = view.findViewById(R.id.cvLocateViewAll);
        cvFaqViewAll = view.findViewById(R.id.cvFaqViewAll);

        loading = ProgressDialog.show(getContext(), null, "Loading", false, true);

        setListeners();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMainViewAllInteractionListener)
            listener = (OnMainViewAllInteractionListener) context;
        else
            throw new RuntimeException(context.toString() + " must implement OnMainViewAllInteractionListener");

    }

    @Override
    public void onDetach() {
        super.onDetach();

        listener = null;
    }

    public interface OnMainViewAllInteractionListener {
        void onMainViewAllInteraction(Boolean inputText);
    }

    private void setListeners() {
        cvAccountsViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), AccountsActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvEnquiriesViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), EnquiriesActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvAirtimeViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), AirtimeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvWithdrawViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), MobileMoneyOwnActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvFTViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), FundsTransferBaseActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvLoansViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), LoanActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvPayBillViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), PayBillsActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvCardsViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), CardsActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvChangePinViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), ChangePinActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvProfileViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), ProfileActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvAlertsViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), AlertsActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvFavViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), FavouritesActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvShareViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loading = ProgressDialog.show(getContext(), null, "Loading", false, true);

                Intent i = new Intent(android.content.Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.share_subject));
                i.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.share_text));
                startActivity(Intent.createChooser(i, getString(R.string.share_via)));
            }

        });

        cvFeedbackViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), FeedbackActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvInfoViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), InfoActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvLocateViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (new Connection(getContext()).IsInternetConnected()) {
                    loading = ProgressDialog.show(getContext(), null, "Loading", false, true);

                    startActivity(new Intent(getContext(), LocationsActivity.class));
                } else {
                    if (getContext() != null)
                        Toasty.info(getContext(), R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                }
            }
        });

        cvFaqViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), FaqActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        loading.dismiss();
    }
}

package ke.co.payconnect.mocash.mocash.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import ke.co.payconnect.mocash.mocash.data.entity.AccountEntity;

@Dao
public interface AccountDao {
    @Query("SELECT EXISTS(SELECT 1 FROM AccountEntity WHERE account_number == :accountNumber LIMIT 1)")
    boolean accountExists(String accountNumber);

    // LiveData List
    @Query("SELECT DISTINCT uid, account_number, account_type FROM AccountEntity ORDER BY account_number ASC")
    LiveData<List<AccountEntity>> getAllAccounts();

    // Normal List
    @Query("SELECT DISTINCT uid, account_number, account_type FROM AccountEntity ORDER BY account_number ASC")
    List<AccountEntity> getAllAccountsList();

    @Query("SELECT uid, account_number, account_type FROM AccountEntity WHERE uid == :uid")
    AccountEntity findAccountByUID(int uid);

    @Query("SELECT uid, account_number, account_type FROM AccountEntity WHERE account_number == :accountNumber")
    AccountEntity findAccountByAccountNo(String accountNumber);

    @Query("SELECT COUNT(*) FROM AccountEntity")
    int getAccountsCount();

    @Query("SELECT uid FROM AccountEntity WHERE account_number == :accountNumber")
    int getAccountUID(String accountNumber);

    @Insert
    void insertAccount(AccountEntity... accountEntities);

    @Update
    void updateAccount(AccountEntity... accountEntities);

    @Delete
    void deleteAccount(AccountEntity accountModels);

    @Query("DELETE FROM AccountEntity")
    void deleteAllAccounts();
}

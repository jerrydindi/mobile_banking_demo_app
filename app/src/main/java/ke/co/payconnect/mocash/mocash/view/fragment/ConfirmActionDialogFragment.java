package ke.co.payconnect.mocash.mocash.view.fragment;


import android.app.Dialog;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.text.Html;
import android.text.Spanned;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.mocash.data.entity.FavEntity;
import ke.co.payconnect.mocash.mocash.viewmodel.FavViewModel;

public class ConfirmActionDialogFragment extends DialogFragment {

    private FavViewModel favViewModel;
    private ConfirmActionListener listener;

    public ConfirmActionDialogFragment newInstance(int title, String action, int uid, String accountName, String accountNumber, String phoneNumber, String extra) {
        ConfirmActionDialogFragment confirmFrag = new ConfirmActionDialogFragment();

        Bundle args = new Bundle();
        args.putInt("title", title);
        args.putString("action", action);
        args.putInt("uid", uid);
        args.putString("accountName", accountName);
        args.putString("accountNumber", accountNumber);
        args.putString("phoneNumber", phoneNumber);
        args.putString("extra", extra);

        confirmFrag.setArguments(args);
        return confirmFrag;
    }

    public interface ConfirmActionListener {
        void onConfirmActionListener(boolean inputText, String accountName);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the ConfirmTransactionListener so we can send events to the host
            listener = (ConfirmActionListener) getActivity();

        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString() + " must implement ConfirmTransactionListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        int title = 0;
        String action = "", extra = "";


        if (getArguments().getInt("title") > 0)
            title = getArguments().getInt("title");

        if (getArguments().getString("action") != null)
            action = getArguments().getString("action");

        final int uid = getArguments().getInt("uid");
        final String accountName = getArguments().getString("accountName");
        final String accountNumber = getArguments().getString("accountNumber");
        final String phoneNumber = getArguments().getString("phoneNumber");

        if (getArguments().getString("extra") != null)
            extra = getArguments().getString("extra");


//        Spanned message = Html.fromHtml("<font color='black'>You will be charged KES 0</font>");
        Spanned message = Html.fromHtml("");
        switch (action) {
            case "FAV_DELETE":
                message = Html.fromHtml("<font color='black'>Delete <b> " + accountName + "</b> from favourites?</font>");
                break;
            default:
                break;
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        alertDialog.setIcon(R.drawable.ic_app);
        builder.setTitle(title);
        builder.setMessage(message);

        builder.setPositiveButton(R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int buttonID) {
                        favViewModel = ViewModelProviders.of(getActivity()).get(FavViewModel.class);

                        ConfirmActionDialogFragment.this.dismiss();

                        FavEntity fav = new FavEntity();
                        fav.setUid(uid);
                        fav.setAccountName(accountName);
                        fav.setAccountNumber(accountNumber);
                        fav.setPhoneNumber(phoneNumber);

                        favViewModel.deleteFav(fav);

                        listener.onConfirmActionListener(true, accountName);
                    }
                }
        );
        builder.setNegativeButton(R.string.cancel,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int buttonID) {
                        ConfirmActionDialogFragment.this.dismiss();
                    }
                }
        );
        return builder.create();
    }
}

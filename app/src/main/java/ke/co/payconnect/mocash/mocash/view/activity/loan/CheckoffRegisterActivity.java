package ke.co.payconnect.mocash.mocash.view.activity.loan;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Connection;
import io.captano.utility.ui.UI;
import io.captano.utility.ui.UIHandler;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.mocash.data.object.User;
import ke.co.payconnect.mocash.mocash.util.Config;
import ke.co.payconnect.mocash.mocash.view.activity.LoginActivity;

import static ke.co.payconnect.mocash.mocash.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.mocash.util.Config.RETRIES;
import static ke.co.payconnect.mocash.mocash.util.Config.TIMEOUT_MS;

public class CheckoffRegisterActivity extends AppCompatActivity {
    private final Context context = this;
    private final Activity activity = this;

    // Views
    private TextView tvConnectivityStatusCheckoffRegister;
    private AutoCompleteTextView actvIdNumberCheckoffRegister;
    private AutoCompleteTextView actvSerialNumberCheckoffRegister;
    private CardView cvCheckoffRegister;

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if (isConnected)
                tvConnectivityStatusCheckoffRegister.setVisibility(View.GONE);
            else
                tvConnectivityStatusCheckoffRegister.setVisibility(View.VISIBLE);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkoff_register);

        if (Build.VERSION.SDK_INT >= 21)
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        } else {
            registerReceiver(connectionReceiver, intentFilter);
        }

        AppController.getInstance().getMocashPreferenceManager().setRememberPhone(false);

        tvConnectivityStatusCheckoffRegister = findViewById(R.id.tvConnectivityStatusCheckoffRegister);
        actvIdNumberCheckoffRegister = findViewById(R.id.actvIdNumberCheckoffRegister);
        actvSerialNumberCheckoffRegister = findViewById(R.id.actvSerialNumberCheckoffRegister);
        cvCheckoffRegister = findViewById(R.id.cvCheckoffRegister);

        setListeners();
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void setListeners() {
        cvCheckoffRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Internet connectivity check
                if (tvConnectivityStatusCheckoffRegister.getVisibility() == View.VISIBLE) {
                    Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                } else {
                    if (new Connection(context).IsInternetConnected())
                        validation(actvIdNumberCheckoffRegister.getText().toString().trim(), actvSerialNumberCheckoffRegister.getText().toString().trim());
                    else
                        Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                }
            }
        });
    }

    private void resetForm() {
        actvIdNumberCheckoffRegister.getText().clear();
        actvSerialNumberCheckoffRegister.getText().clear();
    }

    private void validation(String idNumber, String serialNumber) {
        if (idNumber.isEmpty()) {
            Toasty.info(context, getString(R.string.id_number_required), Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (serialNumber.isEmpty()) {
            Toasty.info(context, getString(R.string.serial_number_required), Toast.LENGTH_SHORT, true).show();
            return;
        }

        new UI(activity).hideSoftInput();

        processRequest(idNumber, serialNumber);
    }

    private void actionProcessLogin() {
        Intent i = new Intent(context, LoginActivity.class);
        startActivity(i);

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void notifyRegistrationResponse(String message) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        actionProcessLogin();
                        break;
                    case DialogInterface.BUTTON_NEUTRAL:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(true);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.ok), dialogClickListener);
        builder.show();
    }

    private void processRequest(final String idNumber, final String serialNumber) {
        final ProgressDialog loading = ProgressDialog.show(this, null, "Registering", false, false);

        final String requestTag = "checkoff_register";
        final String url = Config.CHECKOFF_REGISTER;

        final String phoneNumber = AppController.getInstance().getMocashPreferenceManager().getUser().getPhone();

        Map<String, String> map = new HashMap<>();
        map.put("phone_number", phoneNumber);
        map.put("account", phoneNumber);
        map.put("id_number", idNumber);
        map.put("serial_number", serialNumber);

        JsonObjectRequest checkoffRegisterRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            loading.dismiss();
                            resetForm();

                            switch (response.getString("status")) {
                                case "success":
                                    // Set user details in shared prefs
                                    User user = new User();
                                    user.setPhone(phoneNumber);
                                    user.setNationalID(idNumber);
                                    AppController.getInstance().getMocashPreferenceManager().setUser(user);

                                    notifyRegistrationResponse(response.getString("message"));
                                    break;

                                case "suspended":
                                case "failed":
                                    Toasty.info(context, "Action failed", Toast.LENGTH_SHORT, true).show();
                                    AppController.getInstance().getMocashPreferenceManager().clear();
                                    break;

                                default:
                                    Toasty.info(context, "The registration request could not be completed at this moment", Toast.LENGTH_LONG, true).show();
                                    AppController.getInstance().getMocashPreferenceManager().clear();
                                    break;
                            }

                        } catch (JSONException e) {
                            loading.dismiss();
                            resetForm();
                            Toasty.info(context, "An error occurred", Toast.LENGTH_LONG, true).show();
                            AppController.getInstance().getMocashPreferenceManager().clear();

                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                loading.dismiss();
                resetForm();
                AppController.getInstance().getMocashPreferenceManager().clear();

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error.")) {
                                    Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":
                                /* Validation errors - Sample response
                                  {
                                      "status": "fatal",
                                        "errors": [{
                                          "location": "body",
                                          "param": "agent_number",
                                          "msg": "Validation failed | CO::00-1"
                                        }]
                                 }
                                 */
                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();

                                        break;

                                    default:
                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } else {
                    if (error instanceof NoConnectionError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, "The service is currently unavailable.", Toast.LENGTH_SHORT, true).show();
                            }
                        });
                    } else if (error instanceof TimeoutError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, "The request has timed out.", Toast.LENGTH_SHORT, true).show();
                            }
                        });
                    } else if (error instanceof AuthFailureError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, "Authentication failed", Toast.LENGTH_SHORT, true).show();
                            }
                        });
                    } else if (error instanceof ServerError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            }
                        });
                    } else if (error instanceof NetworkError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, "Network error.", Toast.LENGTH_LONG, true).show();
                            }
                        });
                    } else if (error instanceof ParseError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, "Network error.", Toast.LENGTH_LONG, true).show();
                            }
                        });
                    } else {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            }
                        });
                    }
                }

            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        checkoffRegisterRequest.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));

        // Adding request to queue
        AppController.getInstance().addToRequestQueue(checkoffRegisterRequest, requestTag);
    }
}



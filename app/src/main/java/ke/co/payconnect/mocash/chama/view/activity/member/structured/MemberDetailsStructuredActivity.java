package ke.co.payconnect.mocash.chama.view.activity.member.structured;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import es.dmoral.toasty.Toasty;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.AccountEntity;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;
import ke.co.payconnect.mocash.chama.util.Access;
import ke.co.payconnect.mocash.chama.util.Auth;
import ke.co.payconnect.mocash.chama.util.Config;
import ke.co.payconnect.mocash.chama.util.SendTo;
import ke.co.payconnect.mocash.chama.util.Utility;
import ke.co.payconnect.mocash.chama.view.activity.signatory.MemberEditActivity;
import ke.co.payconnect.mocash.chama.view.adapter.signatory.AccountRecyclerViewAdapter;
import ke.co.payconnect.mocash.chama.viewmodel.AccountViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;

import static ke.co.payconnect.mocash.chama.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.chama.util.Config.RETRIES;
import static ke.co.payconnect.mocash.chama.util.Config.TIMEOUT_MS;

public class MemberDetailsStructuredActivity extends AppCompatActivity {
    private final Context context = this;
    private final Activity activity = this;

    private Toolbar tbMemberDetailsStructured;
    private TextView tvConnectivityStatusMemberDetailsStructured;
    private ImageView ivMemberDetailsStructured;
    private TextView tvStatusMemberDetailsStructured;
    private TextView tvMemberDetailsEdit;
    private TextDrawable tdMemberDetailsStructured;
    private TextView tvNameMemberDetailsStructured;
    private TextView tvIDNumberMemberDetailsStructured;
    private TextView tvDateOfBirthMemberDetailsStructured;
    private TextView tvPhoneNumberMemberDetailsStructured;
    private TextView tvDateRegisteredMemberDetailsStructured;
    private TextView tvRoleMemberDetailsStructured;
    private RecyclerView rvAccountsMemberDetailsStructured;
    private CoordinatorLayout cdlRefreshMemberDetailsStructured;

    private String memberPhoneNumber;
    private boolean isActive;
    private String name;
    private String idNumber;
    private String dateRegistered;
    private String roleName;
    private String dateOfBirth;

    private LinearLayoutManager layoutManager;
    private DividerItemDecoration dividerItemDecoration;

    public AccountRecyclerViewAdapter adapter;

    private MemberViewModel memberViewModel;
    private AccountViewModel accountViewModel;

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if (isConnected)
                tvConnectivityStatusMemberDetailsStructured.setVisibility(View.GONE);
            else
                tvConnectivityStatusMemberDetailsStructured.setVisibility(View.VISIBLE);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_details_structured);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        else
            registerReceiver(connectionReceiver, intentFilter);

        tvConnectivityStatusMemberDetailsStructured = findViewById(R.id.tvConnectivityStatusMemberDetailsStructured);
        tbMemberDetailsStructured = findViewById(R.id.tbMemberDetailsStructured);
        setSupportActionBar(tbMemberDetailsStructured);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbMemberDetailsStructured.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        ivMemberDetailsStructured = findViewById(R.id.ivMemberDetailsStructured);
        tvStatusMemberDetailsStructured = findViewById(R.id.tvStatusMemberDetailsStructured);
        tvMemberDetailsEdit = findViewById(R.id.tvMemberDetailsEdit);
        tvNameMemberDetailsStructured = findViewById(R.id.tvNameMemberDetailsStructured);
        tvIDNumberMemberDetailsStructured = findViewById(R.id.tvIDNumberMemberDetailsStructured);
        tvDateOfBirthMemberDetailsStructured = findViewById(R.id.tvDateOfBirthMemberDetailsStructured);
        tvPhoneNumberMemberDetailsStructured = findViewById(R.id.tvPhoneNumberMemberDetailsStructured);
        tvDateRegisteredMemberDetailsStructured = findViewById(R.id.tvDateRegisteredMemberDetailsStructured);
        tvRoleMemberDetailsStructured = findViewById(R.id.tvRoleMemberDetailsStructured);
        rvAccountsMemberDetailsStructured = findViewById(R.id.rvAccountsMemberDetailsStructured);
        cdlRefreshMemberDetailsStructured = findViewById(R.id.cdlRefreshMemberDetailsStructured);

        memberViewModel = ViewModelProviders.of(this).get(MemberViewModel.class);
        accountViewModel = ViewModelProviders.of(this).get(AccountViewModel.class);

        layoutManager = new LinearLayoutManager(context);
        dividerItemDecoration = new DividerItemDecoration(context, LinearLayoutManager.VERTICAL);
        rvAccountsMemberDetailsStructured.addItemDecoration(dividerItemDecoration);

        getData();

        setListeners();
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);

        checkAccountStatus();

        setupRecyclerView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void getData() {
        Intent i = new Intent(getIntent());
        memberPhoneNumber = i.getStringExtra("member_phone_number");

        MemberEntity memberEntity = memberViewModel.findMemberByPhoneNumber(memberPhoneNumber);

        isActive = memberEntity.isActive();
        int colour = getResources().getColor(R.color.colorPrimary);
        tdMemberDetailsStructured = TextDrawable.builder()
                .beginConfig()
                .endConfig()
                .buildRoundRect(memberEntity.getFirstName().substring(0, 1).toUpperCase() + memberEntity.getLastName().substring(0, 1).toUpperCase(), colour, 200);

        name = memberEntity.getFirstName() + " " + memberEntity.getLastName();
        idNumber = memberEntity.getIdNumber();
        dateOfBirth = memberEntity.getDateOfBirth();
        dateRegistered = memberEntity.getDateRegistered();
        if (memberEntity.getSignatory().equals("0") && memberEntity.getSecretary().equals("0"))
            roleName = "Member";
        else if (memberEntity.getSignatory().equals("1") && memberEntity.getSecretary().equals("0"))
            roleName = "Signatory | Member";
        else if (memberEntity.getSignatory().equals("0") && memberEntity.getSecretary().equals("1"))
            roleName = "Secretary | Member";
        else if (memberEntity.getSignatory().equals("1") && memberEntity.getSecretary().equals("1"))
            roleName = "Signatory | Secretary | Member";

        setData();
    }

    private void setData() {
        ivMemberDetailsStructured.setImageDrawable(tdMemberDetailsStructured);
        if (isActive) {
            tvStatusMemberDetailsStructured.setText(getString(R.string.active));
            tvStatusMemberDetailsStructured.setTextColor(getResources().getColor(R.color.cBL1));
        } else {
//            tvStatusMemberDetailsStructured.setText(getString(R.string.inactive));
//            tvStatusMemberDetailsStructured.setTextColor(getResources().getColor(R.color.colorPrimary));
            tvStatusMemberDetailsStructured.setText(getString(R.string.active));
            tvStatusMemberDetailsStructured.setTextColor(getResources().getColor(R.color.cBL1));
        }
        tvNameMemberDetailsStructured.setText(name);
        tvIDNumberMemberDetailsStructured.setText(idNumber);
        tvDateOfBirthMemberDetailsStructured.setText(dateOfBirth);
        tvPhoneNumberMemberDetailsStructured.setText(memberPhoneNumber);
        try {
            String formattedDate = Utility.getDate(dateRegistered, "yyyy-MM-dd HH:mm:ss");
            tvDateRegisteredMemberDetailsStructured.setText(formattedDate);
        } catch (ParseException e) {
            tvDateRegisteredMemberDetailsStructured.setText(dateRegistered);
        }

        tvRoleMemberDetailsStructured.setText(roleName);
    }

    private void setListeners() {
        MemberEntity memberEntity = memberViewModel.findMemberByPhoneNumber(memberPhoneNumber);
        cdlRefreshMemberDetailsStructured.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();
                String groupNumber = AppController.getInstance().getChamaPreferenceManager().getGroupNumber();

                final HashMap<String, String> map = new HashMap<>();
                map.put("phone_number", phoneNumber);
                map.put("group_number", groupNumber);
                map.put("member_phone_number", memberPhoneNumber);

                final String sessionToken = AppController.getInstance().getChamaPreferenceManager().getSessionToken();

                fetchMemberAccounts(map, sessionToken);

            }
        });

        if (memberEntity.getSignatory().equals("1") || memberEntity.getSignatory().equals("0")) {
            tvMemberDetailsEdit.setVisibility(View.INVISIBLE);
        } else {
            tvMemberDetailsEdit.setVisibility(View.VISIBLE);
        }
        tvMemberDetailsEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Auth.isAuthorized("signatory")) {
                    Toasty.info(context, R.string.action_not_permitted, Toast.LENGTH_SHORT, true).show();
                    return;
                }

                Intent i = new Intent(context, MemberEditActivity.class);

                i.putExtra("member_phone_number", memberPhoneNumber);
                context.startActivity(i);

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });
    }

    private void setupRecyclerView() {
        accountViewModel.getAllAccountsForMemberLive(memberPhoneNumber).observe((LifecycleOwner) context, new Observer<List<AccountEntity>>() {
            @Override
            public void onChanged(List<AccountEntity> accountEntities) {

                if (accountEntities.size() > 0) {
                    adapter = new AccountRecyclerViewAdapter(context, accountEntities);
                    adapter.notifyDataSetChanged();

                    rvAccountsMemberDetailsStructured.setAdapter(adapter);
                    rvAccountsMemberDetailsStructured.setLayoutManager(layoutManager);
                }

            }
        });
    }

    private void checkAccountStatus() {
        if (accountViewModel.getAccountCountForMember(memberPhoneNumber) > 0) {
            rvAccountsMemberDetailsStructured.setVisibility(View.VISIBLE);
            cdlRefreshMemberDetailsStructured.setVisibility(View.GONE);
        } else {
            rvAccountsMemberDetailsStructured.setVisibility(View.GONE);
            cdlRefreshMemberDetailsStructured.setVisibility(View.VISIBLE);
        }
    }

    SendTo st = new SendTo(context);
    HurlStack hurlStack = new HurlStack() {
        @Override
        protected HttpURLConnection createConnection(URL url) throws IOException {
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
            try {
                httpsURLConnection.setSSLSocketFactory(st.getSSLSocketFactory());
                httpsURLConnection.setHostnameVerifier(st.getHostnameVerifier());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return httpsURLConnection;
        }
    };


    private void fetchMemberAccounts(final HashMap<String, String> map, final String sessionToken) {
        final ProgressDialog loading = ProgressDialog.show(context, null, "Fetching member accounts", false, false);

        final String requestTag = "chama_fetch_member_accounts_request_2";
        final String url = Config.FETCH_MEMBER_ACCOUNTS;
        RequestQueue requestQueue = Volley.newRequestQueue(MemberDetailsStructuredActivity.this);

        JsonObjectRequest fetchMemberAccountsReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(final JSONObject response) {
                        try {

                            loading.dismiss();

                            switch (response.getString("status")) {
                                case "success":
                                    loading.show();
                                    loading.setMessage("Please wait");

                                    // Add accounts
                                    JSONArray accountsArray = response.getJSONArray("accounts");

                                    // Validate accounts
                                    if (accountsArray.length() <= 0) {
                                        onCompleteAlertDialog("Could not fetch member accounts");

                                        return;
                                    }

                                    if (accountsArray.length() > 0) {

                                        // Clear persistent accounts
                                        accountViewModel.deleteAllMemberAccounts(memberPhoneNumber);

                                        for (int i = 0; i < accountsArray.length(); i++) {
                                            AccountEntity accountEntity = new AccountEntity();

                                            JSONObject accountObject = accountsArray.getJSONObject(i);

                                            accountEntity.setAccountName(accountObject.getString("accountName"));
                                            accountEntity.setAccountNumber(accountObject.getString("accountNumber"));
                                            accountEntity.setMemberPhoneNumber(map.get("member_phone_number"));

                                            accountViewModel.insertAccount(accountEntity);
                                        }
                                    }

                                    checkAccountStatus();

                                    Toasty.info(context, "Accounts fetched successfully", Toast.LENGTH_SHORT, true).show();

                                    loading.dismiss();

                                    break;

                                case "failed":
                                case "fatal":
                                default:
                                    onCompleteAlertDialog("Could not fetch member accounts");
                                    break;
                            }

                        } catch (JSONException e) {
                            loading.dismiss();
                            onCompleteAlertDialog("Could not fetch member accounts");

                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                loading.dismiss();
                onCompleteAlertDialog("Could not fetch member accounts");

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error")) {
                                    Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":

                                        Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();

                                        break;

                                    default:
                                        Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + context.getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } else {
                    if (error instanceof NoConnectionError) {
                        Toasty.info(context, "No connection to the server", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof TimeoutError) {
                        Toasty.info(context, "The request has timed out", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof AuthFailureError) {
                        Toasty.info(context, "Authentication failed", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof ServerError) {
                        Toasty.info(context, context.getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof NetworkError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof ParseError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else {
                        Toasty.info(context, context.getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    }
                }

            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionToken);

                return headers;
            }

        };

        fetchMemberAccountsReq.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));
        requestQueue.add(fetchMemberAccountsReq);
    }

    private void onCompleteAlertDialog(String message) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(false);
        builder.setTitle(context.getString(R.string.notification));
        builder.setIcon(R.drawable.ic_unaitas);
        builder.setMessage(message);
        builder.setPositiveButton(context.getString(R.string.ok), dialogClickListener);
        builder.show();
    }
}

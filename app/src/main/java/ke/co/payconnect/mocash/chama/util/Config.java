package ke.co.payconnect.mocash.chama.util;


public class Config {
   // private static final String BASE_URL = "http://10.1.2.60:1400/v1/chama/";
   private static final String BASE_URL = "http://18.158.250.239:1400/v1/chama/";

    public static final String LOGIN_URL = BASE_URL + "login";
    public static final String CHANGE_PIN_URL = BASE_URL + "change-pin";
    public static final String FETCH_GROUP_DATA_URL = BASE_URL + "group/fetch-data";
    public static final String ADD_MEMBER = BASE_URL + "member/add";
    public static final String FETCH_MEMBER_ACCOUNTS = BASE_URL + "member/fetch-accounts";
    public static final String ADD_TRANSACTION = BASE_URL + "transaction/add";
    public static final String APPROVE_APPROVAL = BASE_URL + "approval/post";
    public static final String TRANSACTION_POST_COMPLETE = BASE_URL + "transactions-complete";
    public static final String FETCH_GROUP_ACCOUNTS = BASE_URL + "group/fetch-accounts";
    public static final String FETCH_LOAN_BALANCE = BASE_URL + "loan/balance";

    public static final String DATABASE_NAME = "chamaDB";

    // Volley request configs
    // The default socket timeout in milliseconds
    public static final int TIMEOUT_MS = 30000;
    public static final int RETRIES = 0;
    public static final float BACKOFF_MULTIPLIER = 1f;

    // In seconds
    public static int sessionExpiry = 600; // (Default) 2 Minutes

    // Firebase
    static final String FIREBASE_FCM_LEGACY_SERVER_KEY = "AAAAJH_-9fo:APA91bGu-SEnIcC_IJqrqeW9a80l6l0UxOOMo3SqkEP5KFPRgGtfl_LReWVb8vLtPUI6sfTUDf45F_38sXp5p3dawFuEdJf57AUDg1saSbBr1X7Uyy-iiRPYYUxf5ARAMt3D7XSv_vxC";
//            "AAAAHkqgLlk:APA91bHeRd7ps62R1P9KGlb3dfQGMtsV0SjdpT7xfdylBzf1Voa7A8kfuDoUEdv3HNQ7EUF8D4BDleo7xX5uKhwaKBqTyrJvAx_CeYnxoAmVBNn4AbQc3JDnNYPw2-iAwekAIRp5Fy2D";
    //static final String FIREBASE_FCM_LEGACY_SERVER_KEY = "AIzaSyDt2Ir-ynqfUsuYSXpC-rTrJO-O3aoVPzQ";
    public static String sessionTotal;
    public static String sessionStamp;
    public static String batchNoGenerated;
    public static int totalPosted;
}
package ke.co.payconnect.mocash.chama.viewmodel;

import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.ChamaDatabase;
import ke.co.payconnect.mocash.chama.data.entity.MessageEntity;

public class MessageViewModel extends AndroidViewModel {
    private ChamaDatabase db;

    public MessageViewModel(@NonNull Application application) {
        super(application);

        db = ChamaDatabase.getAppDatabaseInstance(this.getApplication());
    }

    public LiveData<List<MessageEntity>> getMessageLiveList(String groupNumber) {
        return db.messageDao().getMessagesLiveList(groupNumber);
    }

    public boolean doesMessageExistWithUID(String uid) {

        return db.messageDao().doesMessageExistWithUID(uid);
    }

    public void insertMessage(MessageEntity messageEntity) {
        db.messageDao().insertMessage(messageEntity);
    }

    public void updateMessage(MessageEntity messageEntity) {
        db.messageDao().updateMessage(messageEntity);
    }

    public void deleteMessage(MessageEntity message) {
        new MessageViewModel.DeleteAsyncTask(db).execute(message);
    }

    private static class DeleteAsyncTask extends AsyncTask<MessageEntity, Void, Void> {

        private ChamaDatabase db;

        DeleteAsyncTask(ChamaDatabase database) {
            db = database;
        }

        @Override
        protected Void doInBackground(final MessageEntity... messageEntities) {
            db.messageDao().deleteMessage(messageEntities[0]);
            return null;
        }
    }
}

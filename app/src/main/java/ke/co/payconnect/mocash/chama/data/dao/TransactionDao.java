package ke.co.payconnect.mocash.chama.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.entity.TransactionEntity;

@Dao
public interface TransactionDao {
    @Query("SELECT DISTINCT uid, member_phone_number, amount, txn_id, batch_number, maker_phone_number, txn_date, txn_trunc_date, txn_account, txn_narration, txn_type, txn_category, txn_status, txn_processing_status, session_time_stamp FROM TransactionEntity ORDER BY txn_date DESC")
    LiveData<List<TransactionEntity>> getAllTransactionsLive();

    @Query("SELECT uid, member_phone_number, amount, txn_id, batch_number, maker_phone_number, txn_date, txn_trunc_date, txn_account, txn_narration, txn_type, txn_category, txn_status, txn_processing_status, session_time_stamp FROM TransactionEntity WHERE txn_category == :txnCategory GROUP BY batch_number ORDER BY txn_date DESC")
    LiveData<List<TransactionEntity>> getSummaryTransactionsLive(final String txnCategory);

    @Query("SELECT uid, member_phone_number, amount, txn_id, batch_number, maker_phone_number, txn_date, txn_trunc_date, txn_account, txn_narration, txn_type, txn_category, txn_status, txn_processing_status, session_time_stamp FROM TransactionEntity WHERE session_time_stamp == :sessionTimeStamp GROUP BY batch_number ORDER BY txn_date DESC")
    List<TransactionEntity> getSummaryTransactionsLiveByTime(final String sessionTimeStamp);

    @Query("SELECT uid, member_phone_number, amount, txn_id, batch_number, maker_phone_number, txn_date, txn_trunc_date, txn_account, txn_narration, txn_type, txn_category, txn_status, txn_processing_status, session_time_stamp FROM TransactionEntity WHERE session_time_stamp == :sessionTimeStamp GROUP BY batch_number ORDER BY txn_date DESC")
    LiveData<List<TransactionEntity>> getSummaryTransactionsLiveByTimestamp(final String sessionTimeStamp);

    @Query("SELECT uid, member_phone_number, amount, txn_id, batch_number, maker_phone_number, txn_date, txn_trunc_date, txn_account, txn_narration, txn_type, txn_category, txn_status, txn_processing_status, session_time_stamp FROM TransactionEntity WHERE txn_processing_status != :txnProcessingStatus GROUP BY batch_number ORDER BY txn_date DESC LIMIT :limit")
    LiveData<List<TransactionEntity>> getSummaryLimitTransactionsLive(final int limit, final String txnProcessingStatus);

    @Query("SELECT uid, member_phone_number, amount, txn_id, batch_number, maker_phone_number, txn_date, txn_trunc_date, txn_account, txn_narration, txn_type, txn_category, txn_status, txn_processing_status, session_time_stamp FROM TransactionEntity WHERE batch_number == :batchNumber AND txn_processing_status != :txnProcessingStatus ORDER BY txn_date DESC")
    List<TransactionEntity> getBatchTransactions(final String batchNumber, final String txnProcessingStatus);

    @Query("SELECT uid, member_phone_number, amount, txn_id, batch_number, maker_phone_number, txn_date, txn_trunc_date, txn_account, txn_narration, txn_type, txn_category, txn_status, txn_processing_status, session_time_stamp FROM TransactionEntity WHERE batch_number == :batchNumber AND txn_processing_status != :txnProcessingStatus ORDER BY txn_date DESC")
    LiveData<List<TransactionEntity>> getBatchTransactionsLive(final String batchNumber, final String txnProcessingStatus);

    @Query("SELECT uid, member_phone_number, amount, txn_id, batch_number, maker_phone_number, txn_date, txn_trunc_date, txn_account, txn_narration, txn_type, txn_category, txn_status, txn_processing_status, session_time_stamp FROM TransactionEntity WHERE uid == :uid")
    TransactionEntity findTransactionByUID(int uid);

    @Query("SELECT uid, member_phone_number, amount, txn_id, batch_number, maker_phone_number, txn_date, txn_trunc_date, txn_account, txn_narration, txn_type, txn_category, txn_status, txn_processing_status, session_time_stamp FROM TransactionEntity ORDER BY txn_date DESC")
    List<TransactionEntity> getTransactionList();

    @Query("SELECT uid, member_phone_number, amount, txn_id, batch_number, maker_phone_number, txn_date, txn_trunc_date, txn_account, txn_narration, txn_type, txn_category, txn_status, txn_processing_status, session_time_stamp FROM TransactionEntity WHERE member_phone_number == :memberPhoneNumber AND txn_trunc_date == :txnTruncDate ORDER BY txn_date DESC")
    LiveData<List<TransactionEntity>> getTransactionListByNumberAndTranDate(final String memberPhoneNumber, String txnTruncDate);

    @Query("SELECT DISTINCT txn_trunc_date FROM TransactionEntity WHERE member_phone_number == :memberPhoneNumber AND txn_category == :txnCategory ORDER BY txn_date ASC")
    LiveData<List<String>> getSummaryMemberTransactionsList(final String memberPhoneNumber, final String txnCategory);

    @Query("SELECT DISTINCT session_time_stamp FROM TransactionEntity WHERE txn_category == :txnCategory ORDER BY session_time_stamp ASC")
    LiveData<List<String>> getSummaryGroupTransactionsList(final String txnCategory);

    @Query("SELECT DISTINCT uid, member_phone_number, amount, txn_id, batch_number, maker_phone_number, txn_date, txn_trunc_date, txn_account, txn_narration, txn_type, txn_category, txn_status, txn_processing_status,session_time_stamp  FROM TransactionEntity WHERE batch_number == :batch_number ORDER BY txn_date DESC")
    TransactionEntity getMemberTransactionsByBatchNumber(final String batch_number);

    @Query("SELECT uid, member_phone_number, amount, txn_id, batch_number, maker_phone_number, txn_date, txn_trunc_date, txn_account, txn_narration, txn_type, txn_category, txn_status, txn_processing_status,session_time_stamp FROM TransactionEntity WHERE txn_id == :txnID")
    TransactionEntity findTransactionByTxnID(final String txnID);

    @Query("SELECT amount FROM TransactionEntity WHERE session_time_stamp  ==:sessionTimeStamp ORDER BY txn_date DESC")
    List<String> getTransactionsLists(final String sessionTimeStamp);


    @Query("SELECT EXISTS(SELECT 1 FROM TransactionEntity WHERE txn_id == :txnID LIMIT 1)")
    boolean doesTransactionExistWithTxnID(final String txnID);

    @Insert
    void insertTransaction(TransactionEntity... transactionEntities);

    @Update
    void updateTransaction(TransactionEntity... transactionEntities);

    @Delete
    void deleteTransaction(TransactionEntity... transactionEntities);

    @Query("DELETE FROM TransactionEntity")
    void deleteAllTransactions();
}

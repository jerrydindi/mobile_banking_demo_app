package ke.co.payconnect.mocash.chama.view.activity.member;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import es.dmoral.toasty.Toasty;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;
import ke.co.payconnect.mocash.chama.util.Access;
import ke.co.payconnect.mocash.chama.util.Config;
import ke.co.payconnect.mocash.chama.util.SendTo;
import ke.co.payconnect.mocash.chama.view.adapter.signatory.MemberRecyclerViewAdapter;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;

import static ke.co.payconnect.mocash.chama.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.chama.util.Config.RETRIES;

public class MemberActivity extends AppCompatActivity implements
        MemberRecyclerViewAdapter.MemberAdapterSearchListener {
    private final Context context = this;
    private final Activity activity = this;

    private Toolbar tbMember;
    private TextView tvConnectivityStatusMember;
    private RecyclerView rvMemberList;
    private TextView tvNoDataMember;

    private SearchManager searchManager;
    private SearchView searchView;
    private LinearLayoutManager layoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private SwipeRefreshLayout swipeRefreshLayout;

    public MemberRecyclerViewAdapter adapter;
    private MemberViewModel memberViewModel;

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if (isConnected)
                tvConnectivityStatusMember.setVisibility(View.GONE);
            else
                tvConnectivityStatusMember.setVisibility(View.VISIBLE);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        else
            registerReceiver(connectionReceiver, intentFilter);

        tvConnectivityStatusMember = findViewById(R.id.tvConnectivityStatusMember);
        tbMember = findViewById(R.id.tbMember);
        setSupportActionBar(tbMember);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbMember.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        rvMemberList = findViewById(R.id.rvMember);
        tvNoDataMember = findViewById(R.id.tvNoDataMember);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);

        layoutManager = new LinearLayoutManager(context);
        dividerItemDecoration = new DividerItemDecoration(context, LinearLayoutManager.VERTICAL);
        rvMemberList.addItemDecoration(dividerItemDecoration);

        setListeners();
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);

        setupRecyclerView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();

        if (id == R.id.action_search)
            return true;

        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onMemberSearched(MemberEntity memberEntity) {

    }

    private void setupRecyclerView() {
        memberViewModel = ViewModelProviders.of(this).get(MemberViewModel.class);
        memberViewModel.getMembersLiveList().observe((LifecycleOwner) context, new Observer<List<MemberEntity>>() {
            @Override
            public void onChanged(List<MemberEntity> memberEntities) {

                if (memberEntities.size() > 0) {
                    toggleDataViews(true);
                    adapter = new MemberRecyclerViewAdapter(context, memberEntities);
                    adapter.notifyDataSetChanged();

                    rvMemberList.setAdapter(adapter);
                    rvMemberList.setLayoutManager(layoutManager);
                } else
                    toggleDataViews(false);

            }
        });
    }

    private void toggleDataViews(boolean hasItems) {
        if (hasItems)
            tvNoDataMember.setVisibility(View.GONE);
        else
            tvNoDataMember.setVisibility(View.VISIBLE);
    }

    private void setListeners() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                String phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();
                String groupNumber = AppController.getInstance().getChamaPreferenceManager().getGroupNumber();
                final int groupType = AppController.getInstance().getChamaPreferenceManager().getGroupType();
                final String sessionToken = AppController.getInstance().getChamaPreferenceManager().getSessionToken();

                final HashMap<String, String> map = new HashMap<>();
                map.put("phone_number", phoneNumber);
                map.put("group_number", groupNumber);

                fetchMembers(map, String.valueOf(groupType), sessionToken);
            }
        });
    }

    SendTo st = new SendTo(context);
    HurlStack hurlStack = new HurlStack() {
        @Override
        protected HttpURLConnection createConnection(URL url) throws IOException {
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
            try {
                httpsURLConnection.setSSLSocketFactory(st.getSSLSocketFactory());
                httpsURLConnection.setHostnameVerifier(st.getHostnameVerifier());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return httpsURLConnection;
        }
    };


    private void fetchMembers(HashMap<String, String> map, final String groupType, final String sessionToken) {
        final ProgressDialog loading = ProgressDialog.show(context, null, "Refreshing members", false, false);

        final String requestTag = "fetch_members_request";
        final String url = Config.FETCH_GROUP_DATA_URL;
        RequestQueue requestQueue = Volley.newRequestQueue(MemberActivity.this);

        JsonObjectRequest fetchMembersReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(final JSONObject response) {
                        try {
                            loading.dismiss();

                            switch (response.getString("status")) {
                                case "success":
                                    loading.show();
                                    loading.setMessage("Setting up data");

                                    AppController.getInstance().getChamaPreferenceManager().setGroupType(Integer.valueOf(groupType));

                                    JSONArray membersArray = response.getJSONArray("members");
                                    addMembers(membersArray);

                                    loading.dismiss();

                                    break;

                                case "failed":
                                case "fatal":
                                default:
                                    Toasty.info(context, "Failed to synchronise approvals", Toast.LENGTH_LONG, true).show();

                                    break;
                            }

                        } catch (JSONException e) {
                            loading.dismiss();
                            Toasty.info(context, "Failed to synchronise approvals", Toast.LENGTH_LONG, true).show();

                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                loading.dismiss();

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error")) {
                                    Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":
                                        Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();

                                        break;

                                    default:
                                        Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + context.getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } else {
                    if (error instanceof NoConnectionError) {
                        Toasty.info(context, "No connection to the server", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof TimeoutError) {
                        Toasty.info(context, "The request has timed out", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof AuthFailureError) {
                        Toasty.info(context, "Authentication failed", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof ServerError) {
                        Toasty.info(context, context.getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof NetworkError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof ParseError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else {
                        Toasty.info(context, context.getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    }
                }

            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionToken);

                return headers;
            }

        };

        fetchMembersReq.setRetryPolicy(new DefaultRetryPolicy(20000, RETRIES, BACKOFF_MULTIPLIER));
        requestQueue.add(fetchMembersReq);
    }

    private void addMembers(JSONArray membersArray) throws JSONException {
        memberViewModel.deleteAllMembers();
        if (membersArray.length() > 0) {

            for (int i = 0; i < membersArray.length(); i++) {
                MemberEntity memberEntity = new MemberEntity();

                JSONObject membersObject = membersArray.getJSONObject(i);

                memberEntity.setFirstName(membersObject.getString("firstName"));
                memberEntity.setLastName(membersObject.getString("lastName"));
                memberEntity.setPhoneNumber(membersObject.getString("memberPhoneNumber"));
                memberEntity.setIdNumber(membersObject.getString("idNumber"));
                memberEntity.setMakerPhoneNumber(membersObject.getString("makerPhoneNumber"));
                memberEntity.setDateRegistered(membersObject.getString("dateRegistered"));
                memberEntity.setSignatory(membersObject.getString("signatory"));
                memberEntity.setSecretary(membersObject.getString("secretary"));
                if (membersObject.getString("status").equals("1"))
                    memberEntity.setActive(true);
                else
                    memberEntity.setActive(false);

                memberViewModel.insertMember(memberEntity);

            }
        }
    }
}



package ke.co.payconnect.mocash.chama.view.activity.signatory;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.ViewModelProviders;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Time;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalFullStatementEntity;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;
import ke.co.payconnect.mocash.chama.util.Access;
import ke.co.payconnect.mocash.chama.util.Config;
import ke.co.payconnect.mocash.chama.util.SendTo;
import ke.co.payconnect.mocash.chama.util.Utility;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalFullStatementViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;

import static ke.co.payconnect.mocash.chama.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.chama.util.Config.RETRIES;
import static ke.co.payconnect.mocash.chama.util.Config.TIMEOUT_MS;

public class ApproveFullStatementActivity extends AppCompatActivity {
    private final Context context = this;
    private final Activity activity = this;

    private Toolbar tbApproveFullStatement;
    private TextView tvConnectivityStatusApproveFullStatement;
    private TextView tvMemberNameApproveFullStatement;
    private TextView tvDateAppliedApproveFullStatement;
    private TextView tvReasonApproveFullStatement;
    private TextView tvApprovalStatusApproveFullStatement;
    private TextView tvApprovalIDApproveFullStatement;
    private TextView tvApprovalTypeApproveFullStatement;
    private TextView tvApprovalDateApproveFullStatement;
    private TextView tvApprovalNarrationApproveFullStatement;
    private CardView cvRejectApproveFullStatement;
    private CardView cvAcceptApproveFullStatement;

    private String memberName;
    private String requestDate;
    private String narration;
    private String approvalStatus;
    private String approvalID;
    private String approvalType;
    private String approvalDate;
    private String approvalNarration;

    private ApprovalFullStatementViewModel approvalFullStatementViewModel;
    private MemberViewModel memberViewModel;


    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if (isConnected)
                tvConnectivityStatusApproveFullStatement.setVisibility(View.GONE);
            else
                tvConnectivityStatusApproveFullStatement.setVisibility(View.VISIBLE);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approve_full_statement);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        else
            registerReceiver(connectionReceiver, intentFilter);

        tvConnectivityStatusApproveFullStatement = findViewById(R.id.tvConnectivityStatusApproveFullStatement);
        tbApproveFullStatement = findViewById(R.id.tbApproveFullStatement);
        setSupportActionBar(tbApproveFullStatement);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbApproveFullStatement.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        tvMemberNameApproveFullStatement = findViewById(R.id.tvMemberNameApproveFullStatement);
        tvDateAppliedApproveFullStatement = findViewById(R.id.tvDateAppliedApproveFullStatement);
        tvReasonApproveFullStatement = findViewById(R.id.tvReasonApproveFullStatement);
        tvApprovalStatusApproveFullStatement = findViewById(R.id.tvApprovalStatusApproveFullStatement);
        tvApprovalIDApproveFullStatement = findViewById(R.id.tvApprovalIDApproveFullStatement);
        tvApprovalTypeApproveFullStatement = findViewById(R.id.tvApprovalTypeApproveFullStatement);
        tvApprovalDateApproveFullStatement = findViewById(R.id.tvApprovalDateApproveFullStatement);
        tvApprovalNarrationApproveFullStatement = findViewById(R.id.tvApprovalNarrationApproveFullStatement);
        cvRejectApproveFullStatement = findViewById(R.id.cvRejectApproveFullStatement);
        cvAcceptApproveFullStatement = findViewById(R.id.cvAcceptApproveFullStatement);

        approvalFullStatementViewModel = ViewModelProviders.of(this).get(ApprovalFullStatementViewModel.class);
        memberViewModel = ViewModelProviders.of(this).get(MemberViewModel.class);

        setListeners();

    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);

        getData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void getData() {
        Intent intent = getIntent();
        approvalID = intent.getStringExtra("approval_id");

        ApprovalFullStatementEntity approvalFullStatementEntity = approvalFullStatementViewModel.findFullStatementApprovalById(approvalID);
        approvalType = approvalFullStatementEntity.getApprovalType().toUpperCase().replace("-", " ");
        approvalStatus = approvalFullStatementEntity.getApprovalStatus().toUpperCase();
        if (approvalFullStatementEntity.getApprovalDate() != null) {
            approvalDate = approvalFullStatementEntity.getApprovalDate();

            try {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                Date date = format.parse(approvalDate);
                Date dateToCompare = format.parse("2000-01-01");

                if ((date != null ? date.compareTo(dateToCompare) : 0) == 0) {
                    approvalDate = "";
                }
            } catch (ParseException ignored) {
            }
        }

        MemberEntity memberEntity = memberViewModel.findMemberByPhoneNumber(approvalFullStatementEntity.getMemberPhoneNumber());

        memberName = memberEntity.getFirstName() + " " + memberEntity.getLastName();

        if (approvalFullStatementEntity.getRequestDate() != null) {
            requestDate = approvalFullStatementEntity.getRequestDate();

            try {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                Date date = format.parse(requestDate);
                Date dateToCompare = format.parse("2000-01-01");

                if ((date != null ? date.compareTo(dateToCompare) : 0) == 0) {
                    requestDate = "";
                }
            } catch (ParseException ignored) {
            }

        }

        narration = approvalFullStatementEntity.getNarration();
        if (narration.equals("0"))
            narration = "No data";

        approvalNarration = approvalFullStatementEntity.getApprovalNarration();
        if (approvalNarration.equals("0"))
            approvalNarration = "No data";

        toggleAcceptView();

        setData();
    }

    private void setData() {
        tvMemberNameApproveFullStatement.setText(memberName);
        if (!requestDate.isEmpty()) {
            try {
                String formattedDate = Utility.getDate(requestDate, "yyyy-MM-dd HH:mm:ss");
                tvDateAppliedApproveFullStatement.setText(formattedDate);
            } catch (ParseException e) {
                tvDateAppliedApproveFullStatement.setText(requestDate);
            }
        } else {
            tvDateAppliedApproveFullStatement.setText("NA");
        }

        if (narration != null) {
            if (!narration.isEmpty())
                tvReasonApproveFullStatement.setText(narration);
        }
        tvApprovalStatusApproveFullStatement.setText(approvalStatus);
        switch (approvalStatus) {
            case "PENDING":
                tvApprovalStatusApproveFullStatement.setTextColor(getResources().getColor(R.color.cO1));
                break;
            case "ACCEPTED":
                tvApprovalStatusApproveFullStatement.setTextColor(getResources().getColor(R.color.cBL0));
                break;
            case "REJECTED":
            case "FAILED":
                tvApprovalStatusApproveFullStatement.setTextColor(getResources().getColor(R.color.cR2));
                break;
        }

        tvApprovalIDApproveFullStatement.setText(approvalID);
        tvApprovalTypeApproveFullStatement.setText(approvalType);

        if (!approvalDate.isEmpty()) {
            try {
                String formattedDate = Utility.getDate(approvalDate, "yyyy-MM-dd HH:mm:ss");
                tvApprovalDateApproveFullStatement.setText(formattedDate);
            } catch (ParseException e) {
                tvApprovalDateApproveFullStatement.setText(approvalDate);
            }
        } else {
            tvApprovalDateApproveFullStatement.setText("NA");
        }

        if (approvalNarration != null) {
            if (!approvalNarration.isEmpty() && !approvalNarration.equals("NA"))
                tvApprovalNarrationApproveFullStatement.setText(approvalNarration);
        }
    }

    private void setListeners() {
        cvRejectApproveFullStatement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onApprovalApproval("rejected");
            }
        });

        cvAcceptApproveFullStatement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onApprovalApproval("accepted");
            }
        });
    }

    private void onApprovalApproval(final String action) {
        LayoutInflater li = LayoutInflater.from(context);
        @SuppressLint("InflateParams") View promptsView = li.inflate(R.layout.struct_accept_approval_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptsView);

        final EditText etStructAcceptApproval = promptsView.findViewById(R.id.etStructAcceptApproval);


        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setPositiveButton(getString(R.string.send),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        String narration = etStructAcceptApproval.getText().toString().trim();

                        if (narration.isEmpty())
                            narration = "NA";

                        String phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();
                        String groupNumber = AppController.getInstance().getChamaPreferenceManager().getGroupNumber();

                        final HashMap<String, String> map = new HashMap<>();
                        map.put("approval", action);
                        map.put("approval_id", approvalID);
                        map.put("approval_narration", narration);
                        map.put("phone_number", phoneNumber);
                        map.put("group_number", groupNumber);
                        map.put("tag", "A-TFULLSTAT");

                        final String sessionToken = AppController.getInstance().getChamaPreferenceManager().getSessionToken();
                        processRequest(map, sessionToken);
                    }
                })
                .setNeutralButton(getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void toggleAcceptView() {
        switch (approvalStatus) {
            case "PENDING":
            case "FAILED":
                cvRejectApproveFullStatement.setVisibility(View.VISIBLE);
                cvRejectApproveFullStatement.setEnabled(true);

                cvAcceptApproveFullStatement.setVisibility(View.VISIBLE);
                cvAcceptApproveFullStatement.setEnabled(true);
                break;
            case "ACCEPTED":
            case "REJECTED":
                cvRejectApproveFullStatement.setVisibility(View.GONE);
                cvRejectApproveFullStatement.setEnabled(false);

                cvAcceptApproveFullStatement.setVisibility(View.GONE);
                cvAcceptApproveFullStatement.setEnabled(false);
                break;
        }
    }

    SendTo st = new SendTo(context);
    HurlStack hurlStack = new HurlStack() {
        @Override
        protected HttpURLConnection createConnection(URL url) throws IOException {
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
            try {
                httpsURLConnection.setSSLSocketFactory(st.getSSLSocketFactory());
                httpsURLConnection.setHostnameVerifier(st.getHostnameVerifier());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return httpsURLConnection;
        }
    };

    private void processRequest(final HashMap<String, String> map, final String sessionToken) {

        final ProgressDialog loading = ProgressDialog.show(this, null, "Processing approval", false, false);


        final String requestTag = "chama_approve_exit_approval_request";
        final String url = Config.APPROVE_APPROVAL;
        RequestQueue requestQueue = Volley.newRequestQueue(ApproveFullStatementActivity.this);

        JsonObjectRequest addApprovalReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(final JSONObject response) {
                        try {

                            loading.dismiss();

                            switch (response.getString("status")) {
                                case "success":
                                    updateApprovalStatus(approvalID, map.get("approval"), map.get("approval_narration"));
                                    onComplete("Action submitted successfully", true);
                                    break;

                                case "failed":
                                case "fatal":
                                default:
                                    updateApprovalStatus(approvalID, "failed", map.get("approval_narration"));
                                    onComplete("Action could not be committed at the moment", false);
                                    break;
                            }

                        } catch (JSONException e) {
                            loading.dismiss();
                            Toasty.info(context, "An error occurred", Toast.LENGTH_LONG, true).show();

                            updateApprovalStatus(approvalID, "failed", map.get("approval_narration"));
                            onComplete("Action could not be committed at the moment", false);
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                loading.dismiss();

                updateApprovalStatus(approvalID, "failed", map.get("approval_narration"));
                onComplete("Action could not be committed at the moment", false);

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error")) {
                                    Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":

                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();

                                        break;

                                    default:
                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } else {
                    if (error instanceof NoConnectionError) {
                        Toasty.info(context, "No connection to the server", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof TimeoutError) {
                        Toasty.info(context, "The request has timed out", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof AuthFailureError) {
                        Toasty.info(context, "Authentication failed", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof ServerError) {
                        Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof NetworkError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof ParseError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else {
                        Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    }
                }

            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionToken);

                return headers;
            }

        };

        addApprovalReq.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));
        requestQueue.add(addApprovalReq);
    }

    private void updateApprovalStatus(String approvalID, String approvalStatus, String approvalNarration) {
        String approvalDate = Time.getDateTimeNow("yyyy-MM-dd HH:mm:ss");
        approvalFullStatementViewModel.updateApprovalFullStatementSetData(approvalID, approvalStatus, approvalNarration, approvalDate);

        getData();
    }

    private void onComplete(String message, final boolean isSuccessful) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    if (isSuccessful) {
                        finish();

                        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                    }
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(true);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.ok), dialogClickListener);
        builder.show();
    }
}
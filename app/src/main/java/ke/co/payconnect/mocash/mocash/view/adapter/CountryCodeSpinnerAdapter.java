package ke.co.payconnect.mocash.mocash.view.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import ke.co.payconnect.mocash.R;

public class CountryCodeSpinnerAdapter extends ArrayAdapter<Integer> {
    private Integer[] images;
    private String[] text;
    private Context context;

    public CountryCodeSpinnerAdapter(Context context, Integer[] images, String[] text) {
        super(context, 0, images);
        this.images = images;
        this.text = text;
        this.context = context;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        return getImageForPosition(position, convertView, parent);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        return getImageForPosition(position, convertView, parent);
    }

    private View getImageForPosition(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View row = inflater.inflate(R.layout.struct_sp_county_code, parent, false);

        TextView textView = row.findViewById(R.id.tvCcode);
        textView.setText(text[position]);

        ImageView imageView = row.findViewById(R.id.ivCcodeIcon);
        imageView.setImageResource(images[position]);

        return row;
    }
}

package ke.co.payconnect.mocash.chama.data.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class ApprovalSecretaryEntity {
    @PrimaryKey(autoGenerate = true)
    private int uid;
    @ColumnInfo(name = "current_secretary_phone_number")
    private String currentSecretaryPhoneNumber;
    @ColumnInfo(name = "proposed_secretary_phone_number")
    private String proposedSecretaryPhoneNumber;
    @ColumnInfo(name = "narration")
    private String narration;
    @ColumnInfo(name = "assign_date")
    private String assignDate;
    @ColumnInfo(name = "approval_id")
    private String approvalID;
    @ColumnInfo(name = "approval_status")
    private String approvalStatus;
    @ColumnInfo(name = "approval_type")
    private String approvalType;
    @ColumnInfo(name = "approval_narration")
    private String approvalNarration;
    @ColumnInfo(name = "approval_date")
    private String approvalDate;


    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getCurrentSecretaryPhoneNumber() {
        return currentSecretaryPhoneNumber;
    }

    public void setCurrentSecretaryPhoneNumber(String currentSecretaryPhoneNumber) {
        this.currentSecretaryPhoneNumber = currentSecretaryPhoneNumber;
    }

    public String getProposedSecretaryPhoneNumber() {
        return proposedSecretaryPhoneNumber;
    }

    public void setProposedSecretaryPhoneNumber(String proposedSecretaryPhoneNumber) {
        this.proposedSecretaryPhoneNumber = proposedSecretaryPhoneNumber;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public String getAssignDate() {
        return assignDate;
    }

    public void setAssignDate(String assignDate) {
        this.assignDate = assignDate;
    }

    public String getApprovalID() {
        return approvalID;
    }

    public void setApprovalID(String approvalID) {
        this.approvalID = approvalID;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getApprovalType() {
        return approvalType;
    }

    public void setApprovalType(String approvalType) {
        this.approvalType = approvalType;
    }

    public String getApprovalNarration() {
        return approvalNarration;
    }

    public void setApprovalNarration(String approvalNarration) {
        this.approvalNarration = approvalNarration;
    }

    public String getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(String approvalDate) {
        this.approvalDate = approvalDate;
    }
}

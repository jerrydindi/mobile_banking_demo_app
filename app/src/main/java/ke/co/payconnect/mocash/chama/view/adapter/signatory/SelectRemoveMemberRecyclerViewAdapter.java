package ke.co.payconnect.mocash.chama.view.adapter.signatory;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;

import java.util.LinkedList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;
import ke.co.payconnect.mocash.chama.util.Auth;
import ke.co.payconnect.mocash.chama.view.activity.signatory.RemoveMemberActivity;

public class SelectRemoveMemberRecyclerViewAdapter extends RecyclerView.Adapter<SelectRemoveMemberRecyclerViewAdapter.MembersHolder> {

    private final Context context;
    private List<MemberEntity> memberEntities;
    private List<MemberEntity> memberEntitiesFiltered;

    public SelectRemoveMemberRecyclerViewAdapter(Context context, List<MemberEntity> memberEntities) {
        this.context = context;
        this.memberEntities = memberEntities;
        this.memberEntitiesFiltered = memberEntities;
    }

    @NonNull
    @Override
    public SelectRemoveMemberRecyclerViewAdapter.MembersHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_list_members, parent, false);
        return new SelectRemoveMemberRecyclerViewAdapter.MembersHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final SelectRemoveMemberRecyclerViewAdapter.MembersHolder holder, int position) {
        try {
            final MemberEntity memberEntity = memberEntitiesFiltered.get(position);
            final int colour = context.getResources().getColor(R.color.colorPrimary);
            final String memberName = memberEntity.getFirstName() + " " + memberEntity.getLastName();

            holder.tdRoundIcon = TextDrawable.builder()
                    .beginConfig()
                    .endConfig()
                    .buildRoundRect(memberEntity.getFirstName().substring(0, 1).toUpperCase() + memberEntity.getLastName().substring(0, 1).toUpperCase(), colour, 100);

            holder.ivStructMembers.setImageDrawable(holder.tdRoundIcon);
            holder.tvNameStructMembers.setText(memberName);

            holder.clStructMembers.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!Auth.isAuthorized("signatory")) {
                        Toasty.info(context, R.string.action_not_permitted, Toast.LENGTH_SHORT, true).show();
                        return;
                    }

                    Intent i = new Intent(context, RemoveMemberActivity.class);
                    i.putExtra("selected_member_phone", memberEntity.getPhoneNumber());
                    context.startActivity(i);

                    ((Activity) context).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                }
            });
        } catch (Exception ignored) {
        }
    }

    @Override
    public int getItemCount() {
        return memberEntitiesFiltered.size();
    }

    class MembersHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout clStructMembers;
        private ImageView ivStructMembers;
        private TextView tvNameStructMembers;
        private TextDrawable tdRoundIcon;

        MembersHolder(View view) {
            super(view);

            clStructMembers = view.findViewById(R.id.clStructMembers);
            ivStructMembers = view.findViewById(R.id.ivStructMembers);
            tvNameStructMembers = view.findViewById(R.id.tvNameStructMembers);
        }
    }

    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    memberEntitiesFiltered = memberEntities;
                } else {
                    List<MemberEntity> filteredMemberEntities = new LinkedList<>();

                    for (MemberEntity memberEntity : memberEntities) {

                            if (memberEntity.getFirstName().toLowerCase().trim().contains(charString) || memberEntity.getLastName().toLowerCase().trim().contains(charString))
                                filteredMemberEntities.add(memberEntity);
                    }
                    memberEntitiesFiltered = filteredMemberEntities;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = memberEntitiesFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                memberEntitiesFiltered = (List<MemberEntity>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface MemberAdapterSearchListener {
        void onMemberSearched(MemberEntity memberEntity);
    }

}

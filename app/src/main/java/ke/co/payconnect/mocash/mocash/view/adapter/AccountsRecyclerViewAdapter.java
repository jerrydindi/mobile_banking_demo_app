package ke.co.payconnect.mocash.mocash.view.adapter;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;

import java.util.List;

import es.dmoral.toasty.Toasty;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.mocash.data.entity.AccountEntity;

import static android.content.Context.CLIPBOARD_SERVICE;

public class AccountsRecyclerViewAdapter extends RecyclerView.Adapter<AccountsRecyclerViewAdapter.AccountsHolder> {
    private final Context mContext;
    private List<AccountEntity> accountList;

    public AccountsRecyclerViewAdapter(Context context, List<AccountEntity> list) {
        accountList = list;
        mContext = context;
    }

    @NonNull
    @Override
    public AccountsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_accounts, parent, false);
        return new AccountsHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AccountsHolder holder, int position) {
        final AccountEntity account = accountList.get(position);

        int colour = mContext.getResources().getColor(R.color.colorPrimary);

        holder.tdRoundIcon = TextDrawable.builder()
                .beginConfig()
                .endConfig()
                .buildRoundRect(account.getAccountType().substring(0, 1).toUpperCase(), colour, 100);

        holder.ivAccountIcon.setImageDrawable(holder.tdRoundIcon);
        holder.tvAccountType.setText(account.getAccountType());
        holder.tvAccountNumber.setText(account.getAccountNumber());

        holder.rlAccounts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(account.getAccountType(), account.getAccountNumber());
                clipboard.setPrimaryClip(clip);
                Toasty.info(mContext, account.getAccountType() + " account number copied to clipboard", Toast.LENGTH_SHORT, true).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return accountList.size();
    }

    class AccountsHolder extends RecyclerView.ViewHolder {
        RelativeLayout rlAccounts;
        ImageView ivAccountIcon;
        TextView tvAccountType;
        TextView tvAccountNumber;
        private TextDrawable tdRoundIcon;

        AccountsHolder(View view) {
            super(view);

            rlAccounts = view.findViewById(R.id.rlAccounts);
            ivAccountIcon = view.findViewById(R.id.ivAccountIcon);
            tvAccountType = view.findViewById(R.id.tvAccountType);
            tvAccountNumber = view.findViewById(R.id.tvAccountNumber);

        }
    }
}

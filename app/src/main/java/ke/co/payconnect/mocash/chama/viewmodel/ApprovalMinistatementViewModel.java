package ke.co.payconnect.mocash.chama.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.ChamaDatabase;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalMinistatementEntity;

public class ApprovalMinistatementViewModel extends AndroidViewModel {

    private ChamaDatabase db;

    public ApprovalMinistatementViewModel(@NonNull Application application) {
        super(application);

        db = ChamaDatabase.getAppDatabaseInstance(this.getApplication());
    }

    public ApprovalMinistatementEntity findMinistatementApprovalById(String approvalId){
        return db.approvalMinistatementDao().findMinistatementApprovalById(approvalId);
    }

    public boolean doesMinistatementApprovalExistWithApprovalId(String approvalId){
        return db.approvalMinistatementDao().doesMinistatementApprovalExistWithApprovalId(approvalId);
    }

    public  void updateApprovalMinistatementSetData(String approvalID, String approvalStatus, String approvalNarration, String approvalDate) {
        db.approvalMinistatementDao().updateApprovalMinistatementSetData(approvalID, approvalStatus, approvalNarration, approvalDate);
    }

    public  LiveData<List<ApprovalMinistatementEntity>> getApprovalMinistatementsByTypeLive(String approvalType) {
        return  db.approvalMinistatementDao().getApprovalMinistatementsByTypeLive(approvalType);
    }

    public void insertApprovalMinistatement(ApprovalMinistatementEntity approvalTransactionEntity) {
        db.approvalMinistatementDao().insertApprovalMinistatement(approvalTransactionEntity);
    }

    public void deleteAllMinistatements() {
        db.approvalMinistatementDao().deleteAllApprovalMinistatements();
    }
}

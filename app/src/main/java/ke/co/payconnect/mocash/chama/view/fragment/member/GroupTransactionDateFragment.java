package ke.co.payconnect.mocash.chama.view.fragment.member;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.view.activity.member.TransactionActivity;
import ke.co.payconnect.mocash.chama.view.adapter.member.GroupTransactionsDatesRecyclerAdapter;
import ke.co.payconnect.mocash.chama.view.adapter.member.PersonalTransactionsDatesRecyclerAdapter;
import ke.co.payconnect.mocash.chama.viewmodel.TransactionViewModel;

public class GroupTransactionDateFragment extends Fragment {
    private TransactionViewModel transactionViewModel;

    private GroupTransactionsDatesRecyclerAdapter adapter;

    private OnGroupTransactionDateInteractionListener listener;

    public GroupTransactionDateFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        String phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();
        View view = inflater.inflate(R.layout.fragment_group_transaction_date, container, false);

        final String txnCategory = TransactionActivity.txnCategory;

        final RecyclerView rvPersonalGroupTimestamp = view.findViewById(R.id.rvGroupTransactionTimestamp);
        final TextView tvNoDataGroupTransactionTimestamp = view.findViewById(R.id.tvNoDataGroupTransactionTimestamp);

        transactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel.class);
        transactionViewModel.getSummaryGroupTransactionsList(txnCategory).observe((LifecycleOwner) getContext(), new Observer<List<String>>() {
            @Override
            public void onChanged(List<String> transactionDates) {

                if (transactionDates.size() > 0) {
                    System.out.println("transactionDates" + transactionDates);
                    toggleDataViews(tvNoDataGroupTransactionTimestamp, true);

                    adapter = new GroupTransactionsDatesRecyclerAdapter(getContext(), transactionDates, txnCategory);
                    adapter.notifyDataSetChanged();

                    LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                    rvPersonalGroupTimestamp.setAdapter(adapter);
                    rvPersonalGroupTimestamp.setLayoutManager(layoutManager);
                } else {
                    toggleDataViews(tvNoDataGroupTransactionTimestamp, false);
                }

            }
        });

        return view;

    }

    private void toggleDataViews(TextView tvNoData, boolean hasItems) {
        if (hasItems)
            tvNoData.setVisibility(View.GONE);
        else
            tvNoData.setVisibility(View.VISIBLE);
    }

    public void onButtonPressed(Uri uri) {
        if (listener != null) {
            listener.OnGroupTransactionDateInteraction(uri);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnGroupTransactionDateInteractionListener) {
            listener = (OnGroupTransactionDateInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnGroupTransactionDateInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public interface OnGroupTransactionDateInteractionListener {
        void OnGroupTransactionDateInteraction(Uri uri);
    }
}
package ke.co.payconnect.mocash.mocash.view.fragment;

import android.app.ProgressDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Connection;
import io.captano.utility.ui.UIController;
import io.captano.utility.ui.UIHandler;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.mocash.data.entity.AccountEntity;
import ke.co.payconnect.mocash.mocash.data.entity.FavEntity;
import ke.co.payconnect.mocash.mocash.util.Access;
import ke.co.payconnect.mocash.mocash.util.Config;
import ke.co.payconnect.mocash.mocash.view.activity.FavouritesActivity;
import ke.co.payconnect.mocash.mocash.view.activity.LoginActivity;
import ke.co.payconnect.mocash.mocash.view.adapter.AccountSpinnerAdapter;
import ke.co.payconnect.mocash.mocash.viewmodel.FavViewModel;

import static ke.co.payconnect.mocash.mocash.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.mocash.util.Config.RETRIES;
import static ke.co.payconnect.mocash.mocash.util.Config.TIMEOUT_MS;

public class FundsTransferOtherFragment extends Fragment implements
        ConfirmTransactionDialogFragment.ConfirmTransactionListener,
        PasswordInputFragment.PasswordInputListener {

    // Views
    private Spinner spAccountSourceOther, spCurrencyOther;
    private EditText etAccountDestinationOther;
    private EditText etAmountOther;
    private ImageView ivFavContacts;
    private ImageView ivFavContactsRemove;
    private TextView tvAccountNameDestinationFTOther;
    private TextView tvAccountNumberDestinationFTOther;
    private Button btSubmitOther;
    private TextView tvConnectivityStatusFTOther;

    private CoordinatorLayout coordinatorLayout;

    private FavViewModel favViewModel;

    private String accountName = "";
    private String accountNumber = "";
    private String phoneNumber = "";
    private String accountFromSelected = null;

    private String validatedAccountFrom = "";
    private String validatedAccountTo = "";
    private String validatedAmount = "0";

    private boolean isFavAvailable = false;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFundsTransferOtherInteractionListener mListener;

    public static FundsTransferOtherFragment newInstance() {
        return new FundsTransferOtherFragment();
    }


    // TODO: Rename and change types and number of parameters
    public static FundsTransferOtherFragment newInstance(String param1, String param2) {
        FundsTransferOtherFragment fragment = new FundsTransferOtherFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    private BroadcastReceiver mFavReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            accountName = intent.getStringExtra("accountName");
            accountNumber = intent.getStringExtra("accountNumber");
            phoneNumber = intent.getStringExtra("phoneNumber");

            if (phoneNumber.equals("-")) {
                if (getContext() != null)
                    Toasty.info(getContext(), getString(R.string.invalid_cr), Toast.LENGTH_SHORT, true).show();
                return;
            }

            tvAccountNameDestinationFTOther.setText(accountName);
            tvAccountNumberDestinationFTOther.setText(accountNumber);
            etAccountDestinationOther.setText(accountNumber);

            etAccountDestinationOther.setVisibility(View.GONE);
            ivFavContacts.setVisibility(View.GONE);

            ivFavContactsRemove.setVisibility(View.VISIBLE);
            tvAccountNameDestinationFTOther.setVisibility(View.VISIBLE);
            tvAccountNumberDestinationFTOther.setVisibility(View.VISIBLE);
        }
    };

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if (isConnected)
                tvConnectivityStatusFTOther.setVisibility(View.GONE);
            else
                tvConnectivityStatusFTOther.setVisibility(View.VISIBLE);

        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mFavReceiver, new IntentFilter("INTENT_FAV_FT"));

        favViewModel = ViewModelProviders.of(this).get(FavViewModel.class);

        favViewModel.getFavList().observe(getActivity(), new Observer<List<FavEntity>>() {
            @Override
            public void onChanged(@Nullable List<FavEntity> fav) {

                if (fav == null || fav.isEmpty())
                    isFavAvailable = false;
                else
                    isFavAvailable = true;

            }
        });
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Objects.requireNonNull(getActivity()).registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        } else {
            getActivity().registerReceiver(connectionReceiver, intentFilter);
        }


        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_funds_transfer_other, container, false);

        spAccountSourceOther = view.findViewById(R.id.spAccountSourceOther);
        spCurrencyOther = view.findViewById(R.id.spCurrencyOther);
        etAccountDestinationOther = view.findViewById(R.id.etAccountDestinationOther);
        etAmountOther = view.findViewById(R.id.etAmountOther);
        ivFavContacts = view.findViewById(R.id.ivFavContacts);
        ivFavContactsRemove = view.findViewById(R.id.ivFavContactsRemove);
        tvAccountNameDestinationFTOther = view.findViewById(R.id.tvAccountNameDestinationFTOther);
        tvAccountNumberDestinationFTOther = view.findViewById(R.id.tvAccountNumberDestinationFTOther);
        btSubmitOther = view.findViewById(R.id.btSubmitOther);
        tvConnectivityStatusFTOther = view.findViewById(R.id.tvConnectivityStatusFTOther);

        String[] currency = {"KES"};
        ArrayAdapter<String> currencyAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, currency);
        currencyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCurrencyOther.setAdapter(currencyAdapter);


        List<AccountEntity> accounts = AppController.getInstance().getAccounts();

        if (accounts != null) {

            AccountSpinnerAdapter adapter = new AccountSpinnerAdapter(this.getContext(), accounts);
            spAccountSourceOther.setAdapter(adapter);

            setListeners();

        } else {
            new UIController().redirect(getActivity(), LoginActivity.class);
        }

        return view;
    }

    public void setListeners() {
        btSubmitOther.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                // Internet connectivity check
                if (tvConnectivityStatusFTOther.getVisibility() == View.VISIBLE) {
                    Toasty.info(getContext(), getString(R.string.internet_required), Toast.LENGTH_SHORT, true).show();
                } else {
                    if (new Connection(getContext()).IsInternetConnected()) {
                        if (accountName.length() > 0)
//                            validation(accountFromSelected, etAccountDestinationOther.getText().toString() + " (" + accountName + ") ", etAmountOther.getText().toString());
                            validation(accountFromSelected, etAccountDestinationOther.getText().toString(), etAmountOther.getText().toString());
                        else
                            validation(accountFromSelected, etAccountDestinationOther.getText().toString(), etAmountOther.getText().toString());
                    } else {
                        Toasty.info(getContext(), getString(R.string.internet_required), Toast.LENGTH_SHORT, true).show();
                    }
                }


            }
        });

        ivFavContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFavAvailable) {
                    FavSelectDialogFragment dialog;

                    if (FundsTransferOtherFragment.this.getActivity().getSupportFragmentManager() != null) {
                        FragmentManager fm = FundsTransferOtherFragment.this.getActivity().getSupportFragmentManager();
                        dialog = FavSelectDialogFragment.newInstance("Saved Contacts", "FT");
                        dialog.show(fm, FavSelectDialogFragment.TAG);
                    }
                } else {
                    showSnackBar(v);
                }
            }
        });

        ivFavContactsRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etAccountDestinationOther.setVisibility(View.VISIBLE);
                ivFavContacts.setVisibility(View.VISIBLE);
                ivFavContactsRemove.setVisibility(View.GONE);
                tvAccountNameDestinationFTOther.setVisibility(View.GONE);
                tvAccountNumberDestinationFTOther.setVisibility(View.GONE);

                etAccountDestinationOther.getText().clear();
                accountName = "";
                accountNumber = "";
                phoneNumber = "";
            }
        });
        spAccountSourceOther.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {

                try {
                    accountFromSelected = ((TextView) v.findViewById(R.id.tvAccountNumberSpin)).getText().toString();
                } catch (Exception e) {
                    if (getContext() != null)
                        Toasty.info(getContext(), "Your session has timed out", Toast.LENGTH_SHORT, true).show();
                    new UIController().redirect(getActivity(), LoginActivity.class);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });


    }

    public void showSnackBar(View view) {
        final Snackbar snackbar = Snackbar
                .make(view, "You do not have any saved contacts", Snackbar.LENGTH_LONG)
                .setAction("ADD", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent i = new Intent(getContext(), FavouritesActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(i);
                    }
                });

        snackbar.setActionTextColor(getResources().getColor(R.color.colorPrimary));
        snackbar.show();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFundsTransferOtherInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFundsTransferOtherInteractionListener) {
            mListener = (OnFundsTransferOtherInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFundsTransferOwnInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();


        try {
            if (getContext() != null) {
                if (connectionReceiver != null)
                    getContext().unregisterReceiver(connectionReceiver);
            }
        } catch (Exception ignored) {
        }
    }

    @Override
    public void onPause() {
        super.onPause();


        try {
            if (getContext() != null) {
                if (connectionReceiver != null)
                    getContext().unregisterReceiver(connectionReceiver);
            }
        } catch (Exception ignored) {
        }
    }

    @Override
    public void onConfirmTransactionListener(boolean isConfirmed, String action) {
        if (isConfirmed) {
            if (action.equals("FT-OTHER")) {
                DialogFragment fragment = PasswordInputFragment.newInstance("FTOther");
                fragment.setTargetFragment(FundsTransferOtherFragment.this, 1);
                fragment.show(FundsTransferOtherFragment.this.getFragmentManager(), "dialog");

            } else {
                if (getContext() != null)
                    Toasty.info(getContext(), "Action could not be completed at the moment", Toast.LENGTH_LONG, true).show();
            }
        }
    }

    @Override
    public void onPasswordInputDialog(boolean isSuccessful, String tag) {
        if (isSuccessful)
            if (tag.equals("FTOther"))
                processTransaction();
            else {
                if (getContext() != null)
                    Toasty.info(getContext(), "Incorrect pin", Toast.LENGTH_SHORT, true).show();
            }
    }

    public interface OnFundsTransferOtherInteractionListener {
        // TODO: Update argument type and name
        void onFundsTransferOtherInteraction(Uri uri);
    }

    private void validation(String accountFrom, String accountTo, String amount) {
        if (accountTo == null || accountTo.isEmpty()) {
            if (getContext() != null)
                Toasty.info(getContext(), "Please enter a valid credit account", Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (amount == null || amount.isEmpty()) {
            if (getContext() != null)
                Toasty.info(getContext(), "Please enter a valid amount", Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (accountFrom.equals(accountTo)) {
            if (getContext() != null)
                Toasty.info(getContext(), "Please select a different credit account", Toast.LENGTH_SHORT, true).show();
            return;
        }

        validatedAccountFrom = accountFrom;
        validatedAccountTo = accountTo;
        validatedAmount = amount;

        double amountFormat = Double.parseDouble(amount);

        DialogFragment fragment = ConfirmTransactionDialogFragment.newInstance(R.string.confirm_transaction, accountFrom, accountTo, amountFormat, "FT-OTHER", "", "");
        fragment.setTargetFragment(FundsTransferOtherFragment.this, 1);
        fragment.show(FundsTransferOtherFragment.this.getFragmentManager(), "dialog");

    }

    private void notifySessionExpired(String message) {
        Access.forceSessionExpiry();

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        boolean stateRemember = AppController.getInstance().getMocashPreferenceManager().getRememberPhone();
                        Access.logout(getActivity(), stateRemember, false);

                        if (getContext() != null)
                            Toasty.info(getContext(), R.string.logout_message_post, Toast.LENGTH_SHORT, true).show();
                        break;
                }
            }
        };
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getContext(), R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(false);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.sign_in), dialogClickListener);
        builder.show();
    }

    public void processTransaction() {
        final ProgressDialog loading = ProgressDialog.show(getContext(), null, "Processing transaction", false, false);

        final String phoneNumber = AppController.getInstance().getMocashPreferenceManager().getUser().getPhone();
        final String idNumber = AppController.getInstance().getMocashPreferenceManager().getUser().getNationalID();
        final String sessionToken = AppController.getInstance().getMocashPreferenceManager().getSessionToken();

        final String requestTag = "ft_other_request";
        final String url = Config.FUNDS_TRANSFER_OTHER_URL;

        Map<String, String> map = new HashMap<>();
        map.put("phone_number", phoneNumber);
        map.put("id_number", idNumber);
        map.put("account", phoneNumber);
        map.put("account_from", validatedAccountFrom);
        map.put("account_to", validatedAccountTo);
        map.put("amount", validatedAmount);

        JsonObjectRequest ftOtherReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            loading.dismiss();
                            DialogFragment fragment = ConfirmTransactionDialogFragment
                                    .newInstance(R.string.response, "", "", 0, "RESULT",
                                            response.getString("message"),
                                            "");

                            switch (response.getString("status")) {
                                case "success":
                                    etAmountOther.getText().clear();
                                    etAccountDestinationOther.getText().clear();

                                    etAccountDestinationOther.setVisibility(View.VISIBLE);
                                    ivFavContacts.setVisibility(View.VISIBLE);
                                    ivFavContactsRemove.setVisibility(View.GONE);
                                    tvAccountNameDestinationFTOther.setVisibility(View.GONE);
                                    tvAccountNumberDestinationFTOther.setVisibility(View.GONE);

                                    accountName = "";
                                    accountNumber = "";
                                    FundsTransferOtherFragment.this.phoneNumber = "";

                                    // Show dialog
                                    fragment.setTargetFragment(FundsTransferOtherFragment.this, 1);
                                    fragment.show(FundsTransferOtherFragment.this.getFragmentManager(), "dialog");
                                    break;

                                case "failed":
                                    etAmountOther.getText().clear();
                                    etAccountDestinationOther.getText().clear();

                                    etAccountDestinationOther.setVisibility(View.VISIBLE);
                                    ivFavContacts.setVisibility(View.VISIBLE);
                                    ivFavContactsRemove.setVisibility(View.GONE);
                                    tvAccountNameDestinationFTOther.setVisibility(View.GONE);
                                    tvAccountNumberDestinationFTOther.setVisibility(View.GONE);

                                    accountName = "";
                                    accountNumber = "";
                                    FundsTransferOtherFragment.this.phoneNumber = "";

                                    // Show dialog
                                    fragment.setTargetFragment(FundsTransferOtherFragment.this, 1);
                                    fragment.show(FundsTransferOtherFragment.this.getFragmentManager(), "dialog");
                                    break;

                                default:
                                    etAmountOther.getText().clear();
                                    etAccountDestinationOther.getText().clear();

                                    etAccountDestinationOther.setVisibility(View.VISIBLE);
                                    ivFavContacts.setVisibility(View.VISIBLE);
                                    ivFavContactsRemove.setVisibility(View.GONE);
                                    tvAccountNameDestinationFTOther.setVisibility(View.GONE);
                                    tvAccountNumberDestinationFTOther.setVisibility(View.GONE);

                                    accountName = "";
                                    accountNumber = "";
                                    FundsTransferOtherFragment.this.phoneNumber = "";

                                    if (getContext() != null)
                                        Toasty.info(getContext(), "Failed to process the transaction", Toast.LENGTH_LONG, true).show();
                                    break;
                            }

                        } catch (JSONException e) {
                            loading.dismiss();

                            etAmountOther.getText().clear();
                            etAccountDestinationOther.getText().clear();

                            etAccountDestinationOther.setVisibility(View.VISIBLE);
                            ivFavContacts.setVisibility(View.VISIBLE);
                            ivFavContactsRemove.setVisibility(View.GONE);
                            tvAccountNameDestinationFTOther.setVisibility(View.GONE);
                            tvAccountNumberDestinationFTOther.setVisibility(View.GONE);

                            accountName = "";
                            accountNumber = "";
                            FundsTransferOtherFragment.this.phoneNumber = "";

                            if (getContext() != null)
                                Toasty.info(getContext(), "An error occurred", Toast.LENGTH_LONG, true).show();

                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();

                etAmountOther.getText().clear();
                etAccountDestinationOther.getText().clear();

                etAccountDestinationOther.setVisibility(View.VISIBLE);
                ivFavContacts.setVisibility(View.VISIBLE);
                ivFavContactsRemove.setVisibility(View.GONE);
                tvAccountNameDestinationFTOther.setVisibility(View.GONE);
                tvAccountNumberDestinationFTOther.setVisibility(View.GONE);

                accountName = "";
                accountNumber = "";
                FundsTransferOtherFragment.this.phoneNumber = "";

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 403:
                            notifySessionExpired("Authorization failed: Your session has expired. Kindly sign-in to access this option");
                            break;
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error.")) {
                                    Toasty.info(getContext(), getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":
                                /* validation errors - Sample response
                                  {
                                      "status": "fatal",
                                        "errors": [{
                                          "location": "body",
                                          "param": "agent_number",
                                          "msg": "validation failed | CO::00-1"
                                        }]
                                 }
                                 */

                                        if (getContext() != null)
                                            Toasty.info(getContext(), getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();

                                        break;

                                    default:
                                        if (getContext() != null)
                                            Toasty.info(getContext(), getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                if (getContext() != null)
                                    Toasty.info(getContext(), getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                if (getContext() != null)
                                    Toasty.info(getContext(), getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            if (getContext() != null)
                                Toasty.info(getContext(), "Error " + networkResponse.statusCode + ": " + getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } else {
                    if (error instanceof NoConnectionError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(getContext(), "The service is currently unavailable.", Toast.LENGTH_SHORT, true).show();
                            }
                        });
                    } else if (error instanceof TimeoutError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(getContext(), "The request has timed out.", Toast.LENGTH_SHORT, true).show();
                            }
                        });
                    } else if (error instanceof AuthFailureError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(getContext(), "Authentication failed", Toast.LENGTH_SHORT, true).show();
                            }
                        });
                    } else if (error instanceof ServerError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(getContext(), getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            }
                        });
                    } else if (error instanceof NetworkError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(getContext(), "Network error.", Toast.LENGTH_LONG, true).show();
                            }
                        });
                    } else if (error instanceof ParseError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(getContext(), "Network error.", Toast.LENGTH_LONG, true).show();
                            }
                        });
                    } else {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(getContext(), getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            }
                        });
                    }
                }
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionToken);

                return headers;
            }
        };

        ftOtherReq.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));

        // Adding request to queue
        AppController.getInstance().addToRequestQueue(ftOtherReq, requestTag);
    }
}
package ke.co.payconnect.mocash.mocash.view.activity;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.mocash.util.Access;
import ke.co.payconnect.mocash.mocash.view.adapter.ViewPagerAdapter;
import ke.co.payconnect.mocash.mocash.view.fragment.AirtimeSafaricomFragment;
import ke.co.payconnect.mocash.mocash.view.fragment.AirtimeSafaricomOtherFragment;

public class AirtimeBaseActivity extends AppCompatActivity implements
        AirtimeSafaricomFragment.OnAirtimeSafaricomInteractionListener,
        AirtimeSafaricomOtherFragment.OnAirtimeSafaricomOtherInteractionListener {
    private final Activity activity = this;

    // Views
    private Toolbar tbAirtimeBase;
    private ViewPager vpAirtimeBase;
    private TabLayout tlAirtimeBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_airtime_base);

        tbAirtimeBase = findViewById(R.id.tbAirtimeBase);
        setSupportActionBar(tbAirtimeBase);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbAirtimeBase.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        vpAirtimeBase = findViewById(R.id.vpAirtimeBase);

        setupViewPager(vpAirtimeBase);

        // Set Tabs inside Toolbar
        tlAirtimeBase = findViewById(R.id.tlAirtimeBase);
        tlAirtimeBase.setupWithViewPager(vpAirtimeBase);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    // Add fragments to tabs
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new AirtimeSafaricomFragment(), getString(R.string.my_num));
        viewPagerAdapter.addFragment(new AirtimeSafaricomOtherFragment(), getString(R.string.other_num));

        viewPager.setAdapter(viewPagerAdapter);
    }

    @Override
    public void onAirtimeSafaricomInteraction(Uri uri) {

    }

    @Override
    public void onAirtimeSafaricomOtherInteraction(Uri uri) {

    }
}

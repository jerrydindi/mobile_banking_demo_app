package ke.co.payconnect.mocash.mocash.view.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.text.Html;
import android.text.Spanned;

import io.captano.utility.Money;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.mocash.view.activity.MainActivity;

public class ConfirmTransactionDialogFragment extends DialogFragment {
    int title = 0;
    String action = "";
    String accountFrom = "";
    String accountTo = "";
    double amount = 0;
    String result = "";
    String resultExtra = "";
    private ConfirmTransactionListener listener = (ConfirmTransactionListener) getTargetFragment();

    public static ConfirmTransactionDialogFragment newInstance(int title, String accountFrom, String accountTo, double amount, String action, String result, String resultExtra) {
        ConfirmTransactionDialogFragment confirmFrag = new ConfirmTransactionDialogFragment();

        Bundle args = new Bundle();
        args.putInt("title", title);
        args.putString("accountFrom", accountFrom);
        args.putString("accountTo", accountTo);
        args.putDouble("amount", amount);
        args.putString("action", action);
        args.putString("result", result);
        args.putString("resultExtra", resultExtra);

        confirmFrag.setArguments(args);
        return confirmFrag;
    }

    public interface ConfirmTransactionListener {
        void onConfirmTransactionListener(boolean isConfirmed, String action);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the ConfirmTransactionListener so we can send events to the host
            listener = (ConfirmTransactionListener) getTargetFragment();

        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString() + " must implement ConfirmTransactionListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);


        if (getArguments().getString("action") != null)
            action = getArguments().getString("action");

        if (getArguments().getInt("title") > 0)
            title = getArguments().getInt("title");

        if (getArguments().getString("accountFrom") != null)
            accountFrom = getArguments().getString("accountFrom");

        if (getArguments().getString("accountTo") != null)
            accountTo = getArguments().getString("accountTo");

        amount = getArguments().getDouble("amount");

        if (getArguments().getString("result") != null)
            result = getArguments().getString("result");

        if (getArguments().getString("resultExtra") != null)
            resultExtra = getArguments().getString("resultExtra");

        final String resultComp = action;

//        Spanned message = Html.fromHtml("<font color='black'>You will be charged KES 0</font>");
        Spanned message = Html.fromHtml("");
        switch (action) {
            case "FT-OWN":
                message = Html.fromHtml("<font color='black'>Transfer <b><font color='#4f0000'> KES " +
                        Money.format(amount) + "</font></b> from <b><font color='#4f0000'>" +
                        accountFrom + "</font></b> to <b><font color='#4f0000'>" +
                        accountTo + "</font></b> <br /><br /> </font>"
//                       + "<font color='#595959'>You will be charged KES 0</font>");
                );
                break;
            case "FT-OTHER":
                message = Html.fromHtml("<font color='black'>Transfer <b><font color='#4f0000'> KES " +
                        Money.format(amount) + "</font></b> from <b>" +
                        accountFrom + "</b> to <b><font color='#4f0000'>" +
                        accountTo + "<font></b> <br /><br /> </font>"
//                        +"<font color='#595959'>You will be charged KES 0</font>");
                );
                break;
            case "MPESA-OWN":
                message = Html.fromHtml("<font color='black'>Withdraw <b><font color='#4f0000'> KES " +
                        Money.format(amount) + "</font></b> from <b><font color='#4f0000'>" +
                        accountFrom + "</font></b> to <b><font color='#4f0000'>" +
                        accountTo + "</font></b> <br /><br /> </font>"
//                        +"<font color='#595959'>You will be charged KES 0</font>");
                );
                break;
            case "MPESA-OTHER":
                message = Html.fromHtml("<font color='black'>Withdraw <b><font color='#4f0000'> KES " +
                        Money.format(amount) + "</font></b> from <b><font color='#4f0000'>" +
                        accountFrom + "</font></b> to <b><font color='#4f0000'>" +
                        accountTo + "</font></b> <br /><br /> </font>"
//                       + "<font color='#595959'>You will be charged KES 0</font>");
                );
                break;
            case "AIRTIME-SAF-OWN":
                message = Html.fromHtml("<font color='black'>Purchase  <b> <font color='#4f0000'>KES " +
                        Money.format(amount) + "</font></b> Safaricom airtime from <b><font color='#4f0000'>" +
                        accountFrom + "</font></b> for <b><font color='#4f0000'>" +
                        accountTo + "</font></b> <br /><br /> </font>"
//                        +"<font color='#595959'>You will be charged KES 0</font>");
                );
                break;
            case "AIRTIME-SAF-OTHER":
                message = Html.fromHtml("<font color='black'>Purchase <b><font color='#4f0000'> KES " +
                        Money.format(amount) + "</font></b> Safaricom airtime from <b><font color='#4f0000'>" +
                        accountFrom + "</font></b> for <b><font color='#4f0000'>" +
                        accountTo + "</font></b> <br /><br /> </font>"
//                        +"<font color='#595959'>You will be charged KES 0</font>");
                );
                break;

            case "RESULT":
                message = Html.fromHtml(result + "<br /><br /><font color='#595959'>" + resultExtra + "</font>");
                break;
            default:

                break;
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setTitle(title);
        builder.setMessage(message);

        builder.setPositiveButton(R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int buttonID) {

                        ConfirmTransactionDialogFragment.this.dismiss();
                        if (resultComp.contains("RESULT"))
                            listener.onConfirmTransactionListener(false, action);
                        else
                            listener.onConfirmTransactionListener(true, action);
                    }
                }
        );
        builder.setNegativeButton(R.string.cancel,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int buttonID) {
                        ConfirmTransactionDialogFragment.this.dismiss();
                        listener.onConfirmTransactionListener(false, action);
                    }
                }
        );

        if (action.contains("RESULT")) {
            builder.setNeutralButton("Home",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int buttonID) {
                            ConfirmTransactionDialogFragment.this.dismiss();

                            Intent i = new Intent(getContext(), MainActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);

                            ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                        }
                    }
            );
        }

        return builder.create();
    }

}

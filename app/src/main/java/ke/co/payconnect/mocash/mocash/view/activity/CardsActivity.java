package ke.co.payconnect.mocash.mocash.view.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.mocash.util.Access;

public class CardsActivity extends AppCompatActivity {
    private final Context context = this;
    private final Activity activity = this;

    // Views
    private Toolbar tbCards;
    private CardView cvRequestCards;
    private CardView cvDeactivateCards;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cards);

        tbCards = findViewById(R.id.tbCards);
        setSupportActionBar(tbCards);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbCards.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvRequestCards = findViewById(R.id.cvRequestCards);
        cvDeactivateCards = findViewById(R.id.cvDeactivateCards);

        setListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void setListeners() {
        cvRequestCards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, CardsRequestActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvDeactivateCards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, CardsDeactivateActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });
    }
}

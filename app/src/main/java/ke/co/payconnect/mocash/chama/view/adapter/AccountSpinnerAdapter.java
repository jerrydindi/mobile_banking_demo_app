package ke.co.payconnect.mocash.chama.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.chama.data.entity.AccountEntity;


public class AccountSpinnerAdapter extends ArrayAdapter<String> {
    private Context context;

    private LayoutInflater inflater;
    private List<AccountEntity> accounts;


    public AccountSpinnerAdapter(@NonNull Context context, @NonNull List accounts) {
        super(context, 0, accounts);

        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.accounts = accounts;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        return getImageForPosition(position, convertView, parent);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        return getImageForPosition(position, convertView, parent);
    }

    private View getImageForPosition(int position, View convertView, ViewGroup parent) {

        AccountEntity acc = accounts.get(position);

        View row = inflater.inflate(R.layout.struct_sp_accounts, parent, false);

        final TextView tvAccountNumberSpin = row.findViewById(R.id.tvAccountNumberSpin);
        tvAccountNumberSpin.setText(acc.getAccountNumber());

        final TextView tvAccountNameSpin = row.findViewById(R.id.tvAccountNameSpin);
        tvAccountNameSpin.setText(acc.getAccountName());


        return row;
    }
}

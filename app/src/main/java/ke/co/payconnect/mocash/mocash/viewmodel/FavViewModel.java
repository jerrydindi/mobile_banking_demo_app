package ke.co.payconnect.mocash.mocash.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

import ke.co.payconnect.mocash.mocash.data.MocashDatabase;
import ke.co.payconnect.mocash.mocash.data.entity.FavEntity;

public class FavViewModel extends AndroidViewModel {

    private final LiveData<List<FavEntity>> favList;
    private MocashDatabase db;

    public FavViewModel(Application application) {
        super(application);

        db = MocashDatabase.getMocashDatabaseInstance(this.getApplication());
        favList = db.favDao().getAllFav();
    }


    public LiveData<List<FavEntity>> getFavList() {
        return favList;
    }

    public void insertFav(FavEntity fav){
        new InsertAsyncTask(db).execute(fav);
    }

    public void updateFav(FavEntity fav){
        new UpdateAsyncTask(db).execute(fav);
    }

    public void deleteFav(FavEntity fav) {
        new DeleteAsyncTask(db).execute(fav);
    }


    private static class InsertAsyncTask extends AsyncTask<FavEntity, Void, Void> {

        private MocashDatabase db;

        InsertAsyncTask(MocashDatabase mocashDatabase) {
            db = mocashDatabase;
        }

        @Override
        protected Void doInBackground(final FavEntity... favEntities) {
            db.favDao().insertFav(favEntities[0]);
            return null;
        }
    }

    private static class UpdateAsyncTask extends AsyncTask<FavEntity, Void, Void> {

        private MocashDatabase db;

        UpdateAsyncTask(MocashDatabase mocashDatabase) {
            db = mocashDatabase;
        }

        @Override
        protected Void doInBackground(final FavEntity... favEntities) {
            db.favDao().updateFav(favEntities[0]);
            return null;
        }
    }

    private static class DeleteAsyncTask extends AsyncTask<FavEntity, Void, Void> {

        private MocashDatabase db;

        DeleteAsyncTask(MocashDatabase mocashDatabase) {
            db = mocashDatabase;
        }

        @Override
        protected Void doInBackground(final FavEntity... favEntities) {
            db.favDao().deleteFav(favEntities[0]);
            return null;
        }
    }
}
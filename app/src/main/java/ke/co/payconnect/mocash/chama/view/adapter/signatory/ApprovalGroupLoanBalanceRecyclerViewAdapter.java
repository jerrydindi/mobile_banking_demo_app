package ke.co.payconnect.mocash.chama.view.adapter.signatory;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalGroupLoanBalanceEntity;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;
import ke.co.payconnect.mocash.chama.util.Auth;
import ke.co.payconnect.mocash.chama.util.Utility;

import ke.co.payconnect.mocash.chama.view.activity.signatory.ApprovalGroupLoanBalanceActivity;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;

public class ApprovalGroupLoanBalanceRecyclerViewAdapter extends RecyclerView.Adapter<ApprovalGroupLoanBalanceRecyclerViewAdapter.ApprovalViewHolder>  {

    private final Context context;
    private List<ApprovalGroupLoanBalanceEntity> approvalGroupLoanBalanceEntities;

    ApprovalGroupLoanBalanceRecyclerViewAdapter(Context context, List<ApprovalGroupLoanBalanceEntity> approvalGroupLoanBalanceEntities) {
        this.context = context;
        this.approvalGroupLoanBalanceEntities = approvalGroupLoanBalanceEntities;
    }
    @NonNull
    @Override
    public ApprovalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_approval_group_loan_balance, parent, false);
        return new ApprovalViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ApprovalViewHolder holder, int position) {
        try {
            final ApprovalGroupLoanBalanceEntity approvalEntity = approvalGroupLoanBalanceEntities.get(position);

            MemberViewModel memberViewModel = ViewModelProviders.of((FragmentActivity) context).get(MemberViewModel.class);

            MemberEntity memberEntity = memberViewModel.findMemberByPhoneNumber(approvalEntity.getMemberPhoneNumber());

            holder.tvNoStructApprovalGroupLoanBalance.setText(String.valueOf(position + 1));
            holder.tvMemberStructApprovalGroupLoanBalance.setText(memberEntity.getFirstName() + " " + memberEntity.getLastName());

            String requestDate = approvalEntity.getRequestDate();

            try {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                Date date = format.parse(requestDate);
                Date dateToCompare = format.parse("2000-01-01");

                if ((date != null ? date.compareTo(dateToCompare) : 0) == 0) {
                    requestDate = "NA";
                }
            } catch (ParseException ignored) {
            }

            if (!requestDate.equals("NA")) {
                Calendar calendar;
                String date;
                try {
                    calendar = Utility.getDateTime(requestDate, "yyyy-MM-dd");
                    @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    date = dateFormat.format(calendar.getTime());
                    holder.tvDateStructApprovalGroupLoanBalance.setText(date);
                } catch (ParseException e) {
                    holder.tvDateStructApprovalGroupLoanBalance.setText(approvalEntity.getRequestDate());
                }
            } else {
                holder.tvDateStructApprovalGroupLoanBalance.setText(requestDate);
            }

            if (!approvalEntity.getApprovalStatus().toLowerCase().equals("pending"))
                holder.clStructApprovalGroupLoanBalance.setBackgroundColor(context.getResources().getColor(R.color.cG6));

            holder.clStructApprovalGroupLoanBalance.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!Auth.isAuthorized("signatory")) {
                        Toasty.info(context, R.string.action_not_permitted, Toast.LENGTH_SHORT, true).show();
                        return;
                    }

                    Intent i = new Intent(context, ApprovalGroupLoanBalanceActivity.class);
                    i.putExtra("approval_id", approvalEntity.getApprovalID());
                    context.startActivity(i);

                    ((Activity) context).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                }
            });
        } catch (Exception ignored) {
        }
    }

    @Override
    public int getItemCount() {
        return this.approvalGroupLoanBalanceEntities.size();
    }

    class ApprovalViewHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout clStructApprovalGroupLoanBalance;
        private TextView tvNoStructApprovalGroupLoanBalance;
        private TextView tvDateStructApprovalGroupLoanBalance;
        private TextView tvMemberStructApprovalGroupLoanBalance;

        ApprovalViewHolder(View view) {
            super(view);

            clStructApprovalGroupLoanBalance = view.findViewById(R.id.clStructApprovalGroupLoanBalance);
            tvNoStructApprovalGroupLoanBalance = view.findViewById(R.id.tvNoStructApprovalGroupLoanBalance);
            tvDateStructApprovalGroupLoanBalance = view.findViewById(R.id.tvDateStructApprovalGroupLoanBalance);
            tvMemberStructApprovalGroupLoanBalance = view.findViewById(R.id.tvMemberStructApprovalGroupLoanBalance);
        }

    }
}

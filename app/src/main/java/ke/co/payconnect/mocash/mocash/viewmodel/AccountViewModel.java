package ke.co.payconnect.mocash.mocash.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import android.os.AsyncTask;
import androidx.annotation.NonNull;

import java.util.List;

import ke.co.payconnect.mocash.mocash.data.MocashDatabase;
import ke.co.payconnect.mocash.mocash.data.entity.AccountEntity;

public class AccountViewModel extends AndroidViewModel {

    private final LiveData<List<AccountEntity>> accountList;
    private final List<AccountEntity> accountListList;
    private MocashDatabase db;

    public AccountViewModel(@NonNull Application application) {
        super(application);

        db = MocashDatabase.getMocashDatabaseInstance(this.getApplication());
        accountList = db.accountDao().getAllAccounts();
        accountListList = db.accountDao().getAllAccountsList();
    }

    public LiveData<List<AccountEntity>> getAccountLiveList() {
        return accountList;
    }

    public List<AccountEntity> getAccountList(){
        return accountListList;
    }

    public boolean accountExists(String accountNumber) {
        return db.accountDao().accountExists(accountNumber);
    }

    public void insertAccount(AccountEntity account) {
        new InsertAsyncTask(db).execute(account);
    }

    public void updateAccount(AccountEntity account) {
        new UpdateAsyncTask(db).execute(account);
    }

    public void deleteAccount(AccountEntity account) {
        new DeleteAsyncTask(db).execute(account);
    }

    public void deleteAllAccounts() {
        new DeleteAllAsyncTask(db).execute();
    }


    private static class InsertAsyncTask extends AsyncTask<AccountEntity, Void, Void> {

        private MocashDatabase db;

        InsertAsyncTask(MocashDatabase mocashDatabase) {
            db = mocashDatabase;
        }

        @Override
        protected Void doInBackground(final AccountEntity... accountEntities) {
            db.accountDao().insertAccount(accountEntities[0]);
            return null;
        }
    }

    private static class UpdateAsyncTask extends AsyncTask<AccountEntity, Void, Void> {

        private MocashDatabase db;

        UpdateAsyncTask(MocashDatabase mocashDatabase) {
            db = mocashDatabase;
        }

        @Override
        protected Void doInBackground(final AccountEntity... accountEntities) {
            db.accountDao().updateAccount(accountEntities[0]);
            return null;
        }
    }

    private static class DeleteAsyncTask extends AsyncTask<AccountEntity, Void, Void> {

        private MocashDatabase db;

        DeleteAsyncTask(MocashDatabase mocashDatabase) {
            db = mocashDatabase;
        }

        @Override
        protected Void doInBackground(final AccountEntity... accountEntities) {
            db.accountDao().deleteAccount(accountEntities[0]);
            return null;
        }
    }

    private static class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {

        private MocashDatabase db;

        DeleteAllAsyncTask(MocashDatabase mocashDatabase) {
            db = mocashDatabase;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            db.accountDao().deleteAllAccounts();
            return null;
        }
    }
}
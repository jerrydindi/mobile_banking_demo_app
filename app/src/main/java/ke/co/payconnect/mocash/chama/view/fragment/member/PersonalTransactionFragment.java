package ke.co.payconnect.mocash.chama.view.fragment.member;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.view.activity.member.TransactionActivity;
import ke.co.payconnect.mocash.chama.view.adapter.member.PersonalTransactionsDatesRecyclerAdapter;
import ke.co.payconnect.mocash.chama.viewmodel.TransactionViewModel;

public class PersonalTransactionFragment extends Fragment {
    private TransactionViewModel transactionViewModel;

    private PersonalTransactionsDatesRecyclerAdapter adapter;

    private OnPersonalInteractionListener listener;

    public PersonalTransactionFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        String phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();
        View view = inflater.inflate(R.layout.fragment_personal_transaction, container, false);

        final String txnCategory = TransactionActivity.txnCategory;

        final RecyclerView rvPersonalTransactionsDates = view.findViewById(R.id.rvPersonalTransactionsDates);
        final TextView tvNoDataPersonalTransactionsDates = view.findViewById(R.id.tvNoDataPersonalTransactionsDates);

        transactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel.class);
        transactionViewModel.getSummaryMemberTransactionsList(phoneNumber, txnCategory).observe((LifecycleOwner) getContext(), new Observer<List<String>>() {
            @Override
            public void onChanged(List<String> transactionDates) {

                if (transactionDates.size() > 0) {
                    toggleDataViews(tvNoDataPersonalTransactionsDates, true);

                    adapter = new PersonalTransactionsDatesRecyclerAdapter(getContext(), transactionDates, txnCategory);
                    adapter.notifyDataSetChanged();

                    LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                    rvPersonalTransactionsDates.setAdapter(adapter);
                    rvPersonalTransactionsDates.setLayoutManager(layoutManager);
                } else {
                    toggleDataViews(tvNoDataPersonalTransactionsDates, false);
                }

            }
        });

        return view;
    }

    private void toggleDataViews(TextView tvNoData, boolean hasItems) {
        if (hasItems)
            tvNoData.setVisibility(View.GONE);
        else
            tvNoData.setVisibility(View.VISIBLE);
    }


    public void onButtonPressed(Uri uri) {
        if (listener != null) {
            listener.OnPersonalInteraction(uri);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnPersonalInteractionListener) {
            listener = (OnPersonalInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnPersonalInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public interface OnPersonalInteractionListener {
        void OnPersonalInteraction(Uri uri);
    }
}

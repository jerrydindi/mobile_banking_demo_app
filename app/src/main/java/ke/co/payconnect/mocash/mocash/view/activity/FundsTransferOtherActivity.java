package ke.co.payconnect.mocash.mocash.view.activity;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.mocash.util.Access;
import ke.co.payconnect.mocash.mocash.view.fragment.FundsTransferBaseFragment;
import ke.co.payconnect.mocash.mocash.view.fragment.FundsTransferBaseFragment.OnFundsTransferBaseInteractionListener;
import ke.co.payconnect.mocash.mocash.view.fragment.FundsTransferOtherFragment.OnFundsTransferOtherInteractionListener;
import ke.co.payconnect.mocash.mocash.view.fragment.FundsTransferOwnFragment;
import ke.co.payconnect.mocash.mocash.view.fragment.PasswordInputFragment;

public class FundsTransferOtherActivity extends AppCompatActivity implements
        OnFundsTransferBaseInteractionListener,
        OnFundsTransferOtherInteractionListener,
        FundsTransferOwnFragment.OnFundsTransferOwnInteractionListener,
        PasswordInputFragment.PasswordInputListener {
    private final Activity activity = this;

    // Views
    private Toolbar toolbar;

    private FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_funds_transfer);

        // Toolbar
        toolbar = findViewById(R.id.tbFT);
        setSupportActionBar(toolbar);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.flFT, new FundsTransferBaseFragment());
        fragmentTransaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    @Override
    public void onFundsTransferBaseInteraction(Uri uri) {

    }

    @Override
    public void onFundsTransferOtherInteraction(Uri uri) {

    }

    @Override
    public void onPasswordInputDialog(boolean isSuccessful, String tag) {
    }

    @Override
    public void onFundsTransferOwnInteraction(Uri uri) {

    }
}

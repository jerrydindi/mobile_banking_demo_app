package ke.co.payconnect.mocash.chama.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.ChamaDatabase;
import ke.co.payconnect.mocash.chama.data.entity.AccountEntity;

public class AccountViewModel extends AndroidViewModel {

    private ChamaDatabase db;

    public AccountViewModel(@NonNull Application application) {
        super(application);

        db = ChamaDatabase.getAppDatabaseInstance(this.getApplication());
    }

    public LiveData<List<AccountEntity>> getAllAccountsForMemberLive(String memberPhoneNumber){
        return db.accountDao().getAllAccountsForMemberLive(memberPhoneNumber);
    }

    public List<AccountEntity> getAllAccountsForMember(String memberPhoneNumber){
        return db.accountDao().getAllAccountsForMember(memberPhoneNumber);
    }

    public List<AccountEntity> getAllAccountsForGroup(String groupNumber){
        return db.accountDao().getAllAccountsForGroup(groupNumber);
    }

    public int getAccountCountForMember(String memberPhoneNubmer){
        return db.accountDao().getAccountCountForMember(memberPhoneNubmer);
    }

    public void insertAccount(AccountEntity accountEntity){
        db.accountDao().insertAccount(accountEntity);
    }

    public void updateAccount(AccountEntity accountEntity){
        db.accountDao().updateAccount(accountEntity);
    }

    public void deleteAccount(AccountEntity accountEntity){
        db.accountDao().deleteAccount(accountEntity);
    }

    public void deleteAllMemberAccounts(String memberPhoneNumber){
        db.accountDao().deleteAllMemberAccounts(memberPhoneNumber);
    }

    public void deleteAllGroupAccounts(String groupNumber){
        db.accountDao().deleteAllGroupAccounts(groupNumber);
    }
}

package ke.co.payconnect.mocash.chama.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.ChamaDatabase;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalWithdrawEntity;

public class ApprovalWithdrawViewModel extends AndroidViewModel {

    private ChamaDatabase db;

    public ApprovalWithdrawViewModel(@NonNull Application application) {
        super(application);

        db = ChamaDatabase.getAppDatabaseInstance(this.getApplication());
    }

    public ApprovalWithdrawEntity findWithdrawApprovalById(String approvalId){
        return db.approvalWithdrawDao().findWithdrawApprovalById(approvalId);
    }

    public boolean doesWithdrawApprovalExistWithApprovalId(String approvalId){
        return db.approvalWithdrawDao().doesWithdrawApprovalExistWithApprovalId(approvalId);
    }

    public  void updateApprovalWithdrawSetData(String approvalID, String approvalStatus, String approvalNarration, String approvalDate) {
        db.approvalWithdrawDao().updateApprovalWithdrawSetData(approvalID, approvalStatus, approvalNarration, approvalDate);
    }

    public  LiveData<List<ApprovalWithdrawEntity>> getApprovalWithdrawsByTypeLive(String approvalType) {
        return  db.approvalWithdrawDao().getApprovalWithdrawsByTypeLive(approvalType);
    }

    public void insertApprovalWithdraw(ApprovalWithdrawEntity approvalTransactionEntity) {
        db.approvalWithdrawDao().insertApprovalWithdraw(approvalTransactionEntity);
    }

    public void deleteAllApprovalWithdraws() {
        db.approvalWithdrawDao().deleteAllApprovalWithdraws();
    }
}

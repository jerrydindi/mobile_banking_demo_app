package ke.co.payconnect.mocash.mocash.view.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.mocash.util.Access;

public class AirtimeActivity extends AppCompatActivity {
    private final Context context = this;
    private final Activity activity = this;

    // Views
    private Toolbar tbAirtime;
    private CardView cvSafaricomAirtime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_airtime);

        tbAirtime = findViewById(R.id.tbAirtime);
        setSupportActionBar(tbAirtime);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbAirtime.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvSafaricomAirtime = findViewById(R.id.cvSafaricomAirtime);

        setListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void setListeners() {
        cvSafaricomAirtime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, AirtimeBaseActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });
    }


}

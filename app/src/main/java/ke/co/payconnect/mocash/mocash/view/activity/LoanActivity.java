package ke.co.payconnect.mocash.mocash.view.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;

import android.view.View;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.mocash.data.entity.AccountEntity;
import ke.co.payconnect.mocash.mocash.data.object.LoanAccount;
import ke.co.payconnect.mocash.mocash.util.Access;
import ke.co.payconnect.mocash.mocash.util.Config;
import ke.co.payconnect.mocash.mocash.view.activity.loan.CheckoffLoanActivity;
import ke.co.payconnect.mocash.mocash.view.activity.loan.InukaLoanActivity;
import ke.co.payconnect.mocash.mocash.view.activity.loan.MocashLoanActivity;
import ke.co.payconnect.mocash.mocash.viewmodel.AccountViewModel;

import static ke.co.payconnect.mocash.mocash.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.mocash.util.Config.RETRIES;

public class LoanActivity extends AppCompatActivity {
    private Context context = this;
    private final Activity activity = this;

    // Views
    private Toolbar tbLoan;
    private CardView cvCheckoffAdvanceLoan;
    private CardView cvInukaAdvanceLoan;
    private CardView cvMocashLoan;

    private AccountViewModel accountViewModel;

    private ProgressDialog loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loan);

        tbLoan = findViewById(R.id.tbLoan);
        setSupportActionBar(tbLoan);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        accountViewModel = ViewModelProviders.of(this).get(AccountViewModel.class);

        tbLoan.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvCheckoffAdvanceLoan = findViewById(R.id.cvCheckoffAdvanceLoan);
        cvInukaAdvanceLoan = findViewById(R.id.cvInukaAdvanceLoan);
        cvMocashLoan = findViewById(R.id.cvMocashLoan);

      //  loading = ProgressDialog.show(context, null, "Checking account status", false, false);
      //  loading.dismiss();

        setListeners();

        fetchLoanAccounts();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void setListeners() {
        cvCheckoffAdvanceLoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cvCheckoffAdvanceLoan.isEnabled())
                    actionCheckoff();
            }
        });

        cvInukaAdvanceLoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(cvInukaAdvanceLoan.isEnabled()) {
                    loading.show();
                    fetchInukaLimit();
                }
            }
        });

        cvMocashLoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(cvMocashLoan.isEnabled()) {
              //      loading.show();
                    fetchMocashCheckLimit();
                }
            }
        });
    }

    private void fetchLoanAccounts() {
        final ProgressDialog loading = ProgressDialog.show(this, null, "Loading accounts...", false, false);

        String requestTag = "loan_accounts_request";
        String url = Config.GET_LOAN_ACCOUNTS;
        String phoneNumber = AppController.getInstance().getMocashPreferenceManager().getUser().getPhone();

        final Map<String, String> map = new HashMap<>();
        map.put("phone_number", phoneNumber);
        map.put("account", phoneNumber);

        JsonObjectRequest loanAccountsReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(final JSONObject response) {
                        loading.dismiss();

                        System.out.println("Loan Response: " + response);

                        try {
                            switch (response.getString("status")) {
                                case "success":
//                                    LoanAccount loanAccount = new LoanAccount();
                                    JSONObject loanObj = new JSONObject(response.getString("loan_account"));

                                 //   loanAccount.setAccountNumber(response.getString("loan_account"));
                                    System.out.println("Loan Object: " + loanObj);
//                                    loanAccount.setAccountNumber(loanObj.getString("account"));
//                                    loanAccount.setAccountName(loanObj.getString("accountType"));

                                    AccountEntity accountEntity = new AccountEntity();

                                    accountEntity.setAccountNumber(loanObj.getString("account"));
                                    accountEntity.setAccountType(loanObj.getString("accountType"));

                                    accountViewModel.insertAccount(accountEntity);

                                    System.out.println("account: " + loanObj.getString("account") + "\naccountType" + loanObj.getString("accountType"));

//                                    AppController.getInstance().getMocashPreferenceManager().setLoanAccount(loanAccount);

                                    cvCheckoffAdvanceLoan.setEnabled(true);
                                    cvInukaAdvanceLoan.setEnabled(true);
                                    cvMocashLoan.setEnabled(true);
                                    break;
                                default:
                                    Toasty.info(context, "Your loan accounts could not be loaded", Toast.LENGTH_SHORT, true).show();
                                    cvCheckoffAdvanceLoan.setEnabled(false);
                                    cvInukaAdvanceLoan.setEnabled(false);
                                    cvMocashLoan.setEnabled(false);
                                    break;
                            }

                        } catch (JSONException ignored) {
                            Toasty.info(context, "Your loan accounts could not be loaded", Toast.LENGTH_SHORT, true).show();
                            cvCheckoffAdvanceLoan.setEnabled(false);
                            cvInukaAdvanceLoan.setEnabled(false);
                            cvMocashLoan.setEnabled(false);
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                Toasty.info(context, "Your loan accounts could not be loaded", Toast.LENGTH_SHORT, true).show();
                cvCheckoffAdvanceLoan.setEnabled(false);
                cvInukaAdvanceLoan.setEnabled(false);
                cvMocashLoan.setEnabled(false);
            }
        });

        loanAccountsReq.setRetryPolicy(new DefaultRetryPolicy(20000, RETRIES, BACKOFF_MULTIPLIER));

        // Adding request to queue
        AppController.getInstance().addToRequestQueue(loanAccountsReq, requestTag);
    }

    private void actionCheckoff() {
        if (AppController.getInstance().getMocashPreferenceManager().getInCheckoff()) {

            Intent i = new Intent(context, CheckoffLoanActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(i);

            overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
        } else {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
            builder.setCancelable(true);
            builder.setTitle(getString(R.string.notification));
            builder.setIcon(R.drawable.ic_app_logo);
            builder.setMessage("You are not registered for this service. Visit Unaitas at any branch to access interesting features.");
            builder.setPositiveButton(getString(R.string.ok), dialogClickListener);
            builder.show();
        }

    }

    private void actionInuka() {
        Intent i = new Intent(context, InukaLoanActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(i);

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void fetchInukaLimit() {
        // Tag used to cancel the request
        String requestTag = "inuka_check_limit_request_1";
        String url = Config.INUKA_CHECK_LIMIT_URL;
        String phoneNumber = AppController.getInstance().getMocashPreferenceManager().getUser().getPhone();

        final Map<String, String> map = new HashMap<>();
        map.put("phone_number", phoneNumber);
        map.put("account", phoneNumber);

        JsonObjectRequest mInukaCheckLimitReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(final JSONObject response) {
                        try {
                            switch (response.getString("status")) {
                                case "success":
                                    String loanLimit = response.getString("loan_limit").replace(",", "");
                                    AppController.getInstance().getMocashPreferenceManager().setInukaLoanLimit(loanLimit);
                                    break;
                                case "failed":
                                    AppController.getInstance().getMocashPreferenceManager().setInukaLoanLimit("Not Available");
                                    break;
                                default:

                                    break;
                            }

                        } catch (JSONException ignored) {
                        }

                        fetchInukaLoanBalance();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                fetchInukaLoanBalance();
            }
        });

        mInukaCheckLimitReq.setRetryPolicy(new DefaultRetryPolicy(20000, RETRIES, BACKOFF_MULTIPLIER));

        // Adding request to queue
        AppController.getInstance().addToRequestQueue(mInukaCheckLimitReq, requestTag);
    }

    private void fetchInukaLoanBalance() {
        // Tag used to cancel the request
        String requestTag = "inuka_check_loan_balance_request_1";
        String url = Config.INUKA_CHECK_LOAN_BALANCE_URL;
        String phoneNumber = AppController.getInstance().getMocashPreferenceManager().getUser().getPhone();

        final Map<String, String> map = new HashMap<>();
        map.put("phone_number", phoneNumber);
        map.put("account", phoneNumber);

        JsonObjectRequest mInukaCheckLoanBalanceReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(final JSONObject response) {
                        try {
                            switch (response.getString("status")) {
                                case "success":
                                    String loanBalance = response.getString("loan_balance").replace(",", "");
                                    AppController.getInstance().getMocashPreferenceManager().setInukaLoanBalance(loanBalance);
                                    break;
                                case "failed":
                                    AppController.getInstance().getMocashPreferenceManager().setInukaLoanBalance("Not Available");
                                    break;
                                default:
                                    break;
                            }

                        } catch (JSONException ignored) {
                            loading.dismiss();
                        }

                        loading.dismiss();
                        actionInuka();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                actionInuka();
            }
        });

        mInukaCheckLoanBalanceReq.setRetryPolicy(new DefaultRetryPolicy(20000, RETRIES, BACKOFF_MULTIPLIER));

        // Adding request to queue
        AppController.getInstance().addToRequestQueue(mInukaCheckLoanBalanceReq, requestTag);
    }

    private void actionMocashLoan() {
        Intent i = new Intent(context, MocashLoanActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(i);

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void fetchMocashCheckLimit() {
        // Tag used to cancel the request
        String requestTag = "m_advance_check_limit_request_1";
        String url = Config.MOCASH_CHECK_LIMIT_URL;
        String phoneNumber = AppController.getInstance().getMocashPreferenceManager().getUser().getPhone();

        final Map<String, String> map = new HashMap<>();
        map.put("phone_number", phoneNumber);
        map.put("account", phoneNumber);

        JsonObjectRequest mocashCheckLimitReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(final JSONObject response) {
                        try {
                            switch (response.getString("status")) {
                                case "success":
                                    String loanLimit = response.getString("loan_limit").replace(",", "");
                                    AppController.getInstance().getMocashPreferenceManager().setMocashLoanLimit(loanLimit);
                                    break;
                                case "failed":
                                    AppController.getInstance().getMocashPreferenceManager().setMocashLoanLimit("Not Available");
                                    break;
                                default:
                                    break;
                            }

                        } catch (JSONException ignored) {
                        }

                        fetchMocashCheckLoanBalance();
                       // actionMocashLoan();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
              //  fetchMocashCheckLoanBalance();
            }
        });//4246

        mocashCheckLimitReq.setRetryPolicy(new DefaultRetryPolicy(20000, RETRIES, BACKOFF_MULTIPLIER));

        // Adding request to queue
        AppController.getInstance().addToRequestQueue(mocashCheckLimitReq, requestTag);
    }

    private void fetchMocashCheckLoanBalance() {
        // Tag used to cancel the request
        String requestTag = "m_advance_check_loan_balance_request_1";
        String url = Config.MOCASH_CHECK_LOAN_BALANCE_URL;
        String phoneNumber = AppController.getInstance().getMocashPreferenceManager().getUser().getPhone();

        final Map<String, String> map = new HashMap<>();
        map.put("phone_number", phoneNumber);
        map.put("account", phoneNumber);

        JsonObjectRequest mocashCheckLoanBalanceReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(final JSONObject response) {
                        System.out.println("Loan Balance: " + response);
                        try {

                            switch (response.getString("status")) {
                                case "success":
                                    String loanBalance = response.getString("loan_balance");
                                    AppController.getInstance().getMocashPreferenceManager().setMocashLoanBalance(loanBalance);
                                    break;
                                case "failed":
                                    AppController.getInstance().getMocashPreferenceManager().setMocashLoanBalance("Not Available");
                                    break;
                                default:
                                    break;
                            }

                        } catch (JSONException ignored) {
                        }

                     //   loading.dismiss();
                        actionMocashLoan();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
              //  loading.dismiss();
                actionMocashLoan();
            }
        });

        mocashCheckLoanBalanceReq.setRetryPolicy(new DefaultRetryPolicy(20000, RETRIES, BACKOFF_MULTIPLIER));

        // Adding request to queue
        AppController.getInstance().addToRequestQueue(mocashCheckLoanBalanceReq, requestTag);
    }
}

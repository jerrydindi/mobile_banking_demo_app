package ke.co.payconnect.mocash.chama.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import android.os.Handler;
import android.os.HandlerThread;
import androidx.annotation.NonNull;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.ChamaDatabase;
import ke.co.payconnect.mocash.chama.data.entity.TransactionEntity;

public class TransactionViewModel extends AndroidViewModel {

    private final LiveData<List<TransactionEntity>> transactionLiveList;
    private final List<TransactionEntity> transactionList;
    private ChamaDatabase db;

    public TransactionViewModel(@NonNull Application application) {
        super(application);

        db = ChamaDatabase.getAppDatabaseInstance(this.getApplication());
        transactionLiveList = db.transactionDao().getAllTransactionsLive();
        transactionList = db.transactionDao().getTransactionList();

    }

    public LiveData<List<TransactionEntity>> getSummaryTransactionsLive(String txnCategory){
        return db.transactionDao().getSummaryTransactionsLive(txnCategory);
    }

    public List<TransactionEntity> getTransactionsByTimeStamp(String sessionTimeStamp) {
        return db.transactionDao().getSummaryTransactionsLiveByTime(sessionTimeStamp);
    }

    public LiveData<List<TransactionEntity>> getSummaryTransactionsLiveByTimestamp(String sessionTimeStamp) {
        return db.transactionDao().getSummaryTransactionsLiveByTimestamp(sessionTimeStamp);
    }

    public LiveData<List<TransactionEntity>> getSummaryLimitTransactionsLive(int limit){
        String txnProcessingStatus = "new";
        return db.transactionDao().getSummaryLimitTransactionsLive(limit, txnProcessingStatus);
    }

    public List<TransactionEntity> getBatchTransactions(String batchNumber){
        String txnProcessingStatus = "new";
        return db.transactionDao().getBatchTransactions(batchNumber, txnProcessingStatus);
    }

    public LiveData<List<TransactionEntity>> getBatchTransactionsLive(String batchNumber){
        String txnProcessingStatus = "new";
        return db.transactionDao().getBatchTransactionsLive(batchNumber, txnProcessingStatus);
    }

    public List<String> getTransactionsSum(String sessionTimeStamp) {

        return db.transactionDao().getTransactionsLists(sessionTimeStamp);
    }

    public TransactionEntity findTransactionByTxnID(String txnID) {

        return db.transactionDao().findTransactionByTxnID(txnID);
    }

    public LiveData<List<TransactionEntity>> getTransactionListByNumberAndTranDate(String memberPhoneNumber,String txnTruncDate) {

        return db.transactionDao().getTransactionListByNumberAndTranDate(memberPhoneNumber,txnTruncDate);
    }

    public LiveData<List<String>> getSummaryMemberTransactionsList(String memberPhoneNumber, String txnCategory) {
        return db.transactionDao().getSummaryMemberTransactionsList(memberPhoneNumber, txnCategory);
    }

    public LiveData<List<String>> getSummaryGroupTransactionsList(String txnCategory) {
        return db.transactionDao().getSummaryGroupTransactionsList(txnCategory);
    }

    public boolean doesTransactionExistWithTxnID(String txnID){
        return db.transactionDao().doesTransactionExistWithTxnID(txnID);
    }

    public void insertTransaction(TransactionEntity transaction){
        db.transactionDao().insertTransaction(transaction);
    }

    public void updateTransaction(TransactionEntity transaction){
        new UpdateTransactionEntity(db.getClass().getSimpleName()).onLooperPrepared(transaction);
    }

    public void deleteTransaction(TransactionEntity transaction){
        new DeleteTransactionEntity(db.getClass().getSimpleName()).onLooperPrepared(transaction);
    }

    public void deleteAllTransactions() {
        db.transactionDao().deleteAllTransactions();
    }

    private class InsertTransactionEntity extends HandlerThread {

        Handler handler;

        public InsertTransactionEntity(String name) {
            super(name);
        }

        public void onLooperPrepared(TransactionEntity transaction) {
            handler =new Handler(getLooper()){

                public void handleMessage(TransactionEntity ...transactionEntities){
                    db.transactionDao().insertTransaction(transactionEntities[0]);
                }
            };
        }
    }

    private class UpdateTransactionEntity extends HandlerThread{

        Handler handler;

        public UpdateTransactionEntity(String name) {
            super(name);
        }

        public void onLooperPrepared(TransactionEntity transaction) {
            handler =new Handler(getLooper()){

                public void handleMessage(TransactionEntity ...transactionEntities){
                    db.transactionDao().updateTransaction(transactionEntities[0]);
                }
            };
        }
    }

    private class DeleteTransactionEntity extends HandlerThread{

        Handler handler;

        public DeleteTransactionEntity(String name) {
            super(name);
        }

        public void onLooperPrepared(TransactionEntity transaction) {
            handler =new Handler(getLooper()){

                public void handleMessage(TransactionEntity ...transactionEntities){
                    db.transactionDao().deleteTransaction(transactionEntities[0]);
                }
            };
        }
    }
}

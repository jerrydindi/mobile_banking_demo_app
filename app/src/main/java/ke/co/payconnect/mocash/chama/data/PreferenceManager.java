package ke.co.payconnect.mocash.chama.data;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class PreferenceManager {

    // Shared Preferences
    private SharedPreferences pref;

    // Editor for Shared preferences
    private SharedPreferences.Editor editor;

    // SharedPref file name
    private static final String PREF_NAME = "chamaPREF";

    // Shared Preference Keys
    private static final String KEY_GROUP_NUMBER = "aa";
    private static final String KEY_GROUP_NAME = "bb";
    private static final String KEY_GROUP_TYPE = "cc"; // 1 - structured, 2 - unstructured
    private static final String KEY_PHONE_NUMBER = "dd";
    private static final String KEY_PIN = "ee";
    private static final String KEY_ROLE_SIGNATORY = "ff";
    private static final String KEY_ROLE_SECRETARY = "gg";
    private static final String KEY_REMEMBER_PHONE = "hh";
    private static final String KEY_IMEI = "ii";
    private static final String KEY_SESSION_TOKEN = "jj";
    private static final String KEY_LOGON = "kk";
    private static final String KEY_SESSION_START = "ll";
    private static final String KEY_DEVICE_FCM_TOKEN = "mm";

    public PreferenceManager(Context context) {
        pref = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        editor = pref.edit();
        editor.apply();
    }

    public void setGroupNumber(String groupNumber) {
        editor.putString(KEY_GROUP_NUMBER, groupNumber);
        editor.commit();
    }

    public String getGroupNumber() {
        return pref.getString(KEY_GROUP_NUMBER, null);
    }

    public void setGroupName(String groupName) {
        editor.putString(KEY_GROUP_NAME, groupName);
        editor.commit();
    }

    public String getGroupName() {
        return pref.getString(KEY_GROUP_NAME, null);
    }

    public void setGroupType(int groupType) {
        editor.putInt(KEY_GROUP_TYPE, groupType);
        editor.commit();
    }

    public int getGroupType() {
        return pref.getInt(KEY_GROUP_TYPE, 0);
    }

    public void setPhoneNumber(String phoneNumber) {
        editor.putString(KEY_PHONE_NUMBER, phoneNumber);
        editor.commit();
    }

    public String getPhoneNumber() {
        return pref.getString(KEY_PHONE_NUMBER, null);
    }

    public void setPin(String pin) {
        editor.putString(KEY_PIN, pin);
        editor.commit();
    }

    public String getPin() {
        return pref.getString(KEY_PIN, null);
    }

    public void setSignatoryRole(String role) {
        editor.putString(KEY_ROLE_SIGNATORY, role);
        editor.commit();
    }

    public String getSignatoryRole() {
        return pref.getString(KEY_ROLE_SIGNATORY, null);
    }

    public void setSecretaryRole(String role) {
        editor.putString(KEY_ROLE_SECRETARY, role);
        editor.commit();
    }

    public String getSecretaryRole() {
        return pref.getString(KEY_ROLE_SECRETARY, null);
    }

    public void setRememberPhone(boolean rememberPhone) {
        editor.putBoolean(KEY_REMEMBER_PHONE, rememberPhone);
        editor.commit();
    }

    public boolean getRememberPhone() {
        return pref.getBoolean(KEY_REMEMBER_PHONE, false);
    }

    public void setIMEI(String imei) {
        editor.putString(KEY_IMEI, imei);
        editor.commit();
    }

    public String getIMEI() {
        return pref.getString(KEY_IMEI, null);
    }

    public void setSessionToken(String sessionToken) {
        editor.putString(KEY_SESSION_TOKEN, sessionToken);
        editor.commit();
    }

    public String getSessionToken() {
        return pref.getString(KEY_SESSION_TOKEN, null);
    }

    public void setLogon(boolean logon) {
        editor.putBoolean(KEY_LOGON, logon);
        editor.commit();
    }

    public boolean getLogon() {
        return pref.getBoolean(KEY_LOGON, false);
    }

    public void setSessionStart(long startTime) {
        editor.putLong(KEY_SESSION_START, startTime);
        editor.commit();
    }

    public long getSessionStart() {
        return pref.getLong(KEY_SESSION_START, 0);
    }

    public String getDeviceFCMToken() {
        return pref.getString(KEY_DEVICE_FCM_TOKEN, "INVALID");
    }

    public void setDeviceFCMToken(String token) {
        editor.putString(KEY_DEVICE_FCM_TOKEN, token);
        editor.commit();
    }

    public void clear() {
        editor.clear();
        editor.commit();
    }
}

package ke.co.payconnect.mocash.chama.view.activity.member;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;

import java.text.ParseException;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;
import ke.co.payconnect.mocash.chama.data.entity.TransactionEntity;
import ke.co.payconnect.mocash.chama.util.Access;
import ke.co.payconnect.mocash.chama.util.Utility;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.TransactionViewModel;

public class TransactionDetailsActivity extends AppCompatActivity {
    private final Context context = this;
    private final Activity activity = this;

    private Toolbar tbTxnDetails;
    private TextView tvMemberTxnDetails;
    private TextView tvBatchNoTxnDetails;
    private TextView tvTxnIDTxnDetails;
    private TextView tvAmountTxnDetails;
    private TextView tvTxnDateTxnDetails;
    private TextView tvTxnTypeTxnDetails;
    private TextView tvDestinationAccountTxnDetails;
    private TextView tvTxnProcessingStatusTxnDetails;

    private TransactionViewModel transactionViewModel;
    private MemberViewModel memberViewModel;

    private String member;
    private String batchNo;
    private String txnID;
    private String amount;
    private String txnDate;
    private String txnType;
    private String destinationAccount;
    private String processingStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_details);

        tbTxnDetails = findViewById(R.id.tbTxnDetails);
        setSupportActionBar(tbTxnDetails);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbTxnDetails.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        tvMemberTxnDetails = findViewById(R.id.tvMemberTxnDetails);
        tvBatchNoTxnDetails = findViewById(R.id.tvBatchNoTxnDetails);
        tvTxnIDTxnDetails = findViewById(R.id.tvTxnIDTxnDetails);
        tvAmountTxnDetails = findViewById(R.id.tvAmountTxnDetails);
        tvTxnDateTxnDetails = findViewById(R.id.tvTxnDateTxnDetails);
        tvTxnTypeTxnDetails = findViewById(R.id.tvTxnTypeTxnDetails);
        tvDestinationAccountTxnDetails = findViewById(R.id.tvDestinationAccountTxnDetails);
        tvTxnProcessingStatusTxnDetails = findViewById(R.id.tvTxnProcessingStatusTxnDetails);

        transactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel.class);
        memberViewModel = ViewModelProviders.of(this).get(MemberViewModel.class);

        getData();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void getData() {
        Intent i = new Intent(getIntent());
        txnID = i.getStringExtra("txn_id");

        TransactionEntity transactionEntity = transactionViewModel.findTransactionByTxnID(txnID);

        String groupNumber = AppController.getInstance().getChamaPreferenceManager().getGroupNumber();
        if (transactionEntity.getMemberPhoneNumber().length() > 2) {
            // Structured
            if (transactionEntity.getMemberPhoneNumber().substring(0, 4).equals("2547")) {
                if (memberViewModel.doesMemberExistWithPhoneNumber(transactionEntity.getMemberPhoneNumber())) {
                    MemberEntity memberEntity = memberViewModel.findMemberByPhoneNumber(transactionEntity.getMemberPhoneNumber());
                    member = memberEntity.getFirstName() + " " + memberEntity.getLastName();
                } else {
                    member = "No data";
                }
            } else {
                member = AppController.getInstance().getChamaPreferenceManager().getGroupName();
            }
        } else {
            // Unstructured
            // This type uses the group number as memberPhoneNumber
            if (transactionEntity.getMemberPhoneNumber().equals(groupNumber)) {
                member = AppController.getInstance().getChamaPreferenceManager().getGroupName();
            } else {
                member = AppController.getInstance().getChamaPreferenceManager().getGroupName();
            }
        }
        batchNo = transactionEntity.getBatchNumber();
        amount = transactionEntity.getAmount();
        txnDate = transactionEntity.getTxnDate();
        txnType = transactionEntity.getTxnType();
        destinationAccount = transactionEntity.getTxnAccount();
        processingStatus = transactionEntity.getTxnProcessingStatus().toUpperCase();

        setData();
    }

    private void setData() {
        tvMemberTxnDetails.setText(member);
        tvBatchNoTxnDetails.setText(batchNo);
        tvTxnIDTxnDetails.setText(txnID);
        tvAmountTxnDetails.setText(amount);
        try {
            String formattedDate = Utility.getDate(txnDate, "yyyy-MM-dd HH:mm:ss");
            tvTxnDateTxnDetails.setText(formattedDate);
        } catch (ParseException e) {
            tvTxnDateTxnDetails.setText(txnDate);
        }
        tvTxnTypeTxnDetails.setText(txnType);
        tvDestinationAccountTxnDetails.setText(destinationAccount);
        tvTxnProcessingStatusTxnDetails.setText(processingStatus);
        switch (processingStatus) {
            case "processed":
                tvTxnProcessingStatusTxnDetails.setTextColor(getResources().getColor(R.color.cBL0));
                break;
            case "authorised":
            case "unprocessed":
                tvTxnProcessingStatusTxnDetails.setTextColor(getResources().getColor(R.color.cO1));
                break;
            case "rejected":
                tvTxnProcessingStatusTxnDetails.setTextColor(getResources().getColor(R.color.cR2));
                break;
        }
    }
}

package ke.co.payconnect.mocash.chama.view.adapter.signatory;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Money;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalTransactionEntity;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;
import ke.co.payconnect.mocash.chama.util.Auth;
import ke.co.payconnect.mocash.chama.util.Utility;
import ke.co.payconnect.mocash.chama.view.activity.signatory.ApproveTransactionActivity;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;

public class ApprovalTransactionsRecyclerViewAdapter extends RecyclerView.Adapter<ApprovalTransactionsRecyclerViewAdapter.ApprovalViewHolder> {
    private final Context context;
    private List<ApprovalTransactionEntity> approvalTransactionEntities;

    ApprovalTransactionsRecyclerViewAdapter(Context context, List<ApprovalTransactionEntity> approvalTransactionEntities) {
        this.context = context;
        this.approvalTransactionEntities = approvalTransactionEntities;
    }

    @NonNull
    @Override
    public ApprovalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_approval_txn, parent, false);
        return new ApprovalViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ApprovalViewHolder holder, int position) {
        try {
            final ApprovalTransactionEntity approvalEntity = approvalTransactionEntities.get(position);

            MemberViewModel memberViewModel = ViewModelProviders.of((FragmentActivity) context).get(MemberViewModel.class);

            String memberName;

            if (approvalEntity.getMemberPhoneNumber().length() > 2) {
                if (approvalEntity.getMemberPhoneNumber().substring(0, 4).equals("2547")) {
                    final MemberEntity memberEntity = memberViewModel.findMemberByPhoneNumber(approvalEntity.getMemberPhoneNumber());
                    memberName = memberEntity.getFirstName() + " " + memberEntity.getLastName();
                } else {
                    memberName = AppController.getInstance().getChamaPreferenceManager().getGroupName();
                }
            } else {
                memberName = AppController.getInstance().getChamaPreferenceManager().getGroupName();
            }

            holder.tvNoStructApprovalTxn.setText(String.valueOf(position + 1));
            holder.tvDateStructApprovalTxn.setText(approvalEntity.getApprovalDate());
            holder.tvBatchNoStructApprovalTxn.setText(approvalEntity.getBatchNumber());
            holder.tvMemberStructApprovalTxn.setText(memberName);
            holder.tvAmountStructApprovalTxn.setText(Money.format(Double.parseDouble(approvalEntity.getAmount())));

            String approvalDate = approvalEntity.getApprovalDate();

            try {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                Date date = format.parse(approvalDate);
                Date dateToCompare = format.parse("2000-01-01");

                if ((date != null ? date.compareTo(dateToCompare) : 0) == 0) {
                    approvalDate = "NA";
                }
            } catch (ParseException ignored) {
            }

            if (!approvalDate.equals("NA")) {
                Calendar calendar;
                String date;
                try {
                    calendar = Utility.getDateTime(approvalDate, "yyyy-MM-dd");
                    @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    date = dateFormat.format(calendar.getTime());
                    holder.tvDateStructApprovalTxn.setText(date);
                } catch (ParseException e) {
                    holder.tvDateStructApprovalTxn.setText(approvalEntity.getApprovalDate());
                }
            } else {
                holder.tvDateStructApprovalTxn.setText(approvalDate);
            }

            if (!approvalEntity.getApprovalStatus().toLowerCase().equals("pending"))
                holder.clStructApprovalTxn.setBackgroundColor(context.getResources().getColor(R.color.cG6));

            holder.clStructApprovalTxn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!Auth.isAuthorized("signatory")) {
                        Toasty.info(context, R.string.action_not_permitted, Toast.LENGTH_SHORT, true).show();
                        return;
                    }

                    Intent i = new Intent(context, ApproveTransactionActivity.class);
                    i.putExtra("approval_id", approvalEntity.getApprovalID());
                    context.startActivity(i);

                    ((Activity) context).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                }
            });
        } catch (Exception ignored) {
        }
    }

    @Override
    public int getItemCount() {
        return this.approvalTransactionEntities.size();
    }

    class ApprovalViewHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout clStructApprovalTxn;
        private TextView tvNoStructApprovalTxn;
        private TextView tvDateStructApprovalTxn;
        private TextView tvBatchNoStructApprovalTxn;
        private TextView tvMemberStructApprovalTxn;
        private TextView tvAmountStructApprovalTxn;

        ApprovalViewHolder(View view) {
            super(view);

            clStructApprovalTxn = view.findViewById(R.id.clStructApprovalTxn);
            tvNoStructApprovalTxn = view.findViewById(R.id.tvNoStructApprovalTxn);
            tvDateStructApprovalTxn = view.findViewById(R.id.tvDateStructApprovalTxn);
            tvBatchNoStructApprovalTxn = view.findViewById(R.id.tvBatchNoStructApprovalTxn);
            tvMemberStructApprovalTxn = view.findViewById(R.id.tvMemberStructApprovalTxn);
            tvAmountStructApprovalTxn = view.findViewById(R.id.tvAmountStructApprovalTxn);
        }

    }
}

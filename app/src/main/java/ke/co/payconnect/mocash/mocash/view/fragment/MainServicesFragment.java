package ke.co.payconnect.mocash.mocash.view.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.Objects;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Connection;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.mocash.view.activity.CardsActivity;
import ke.co.payconnect.mocash.mocash.view.activity.ChangePinActivity;
import ke.co.payconnect.mocash.mocash.view.activity.FaqActivity;
import ke.co.payconnect.mocash.mocash.view.activity.FavouritesActivity;
import ke.co.payconnect.mocash.mocash.view.activity.FeedbackActivity;
import ke.co.payconnect.mocash.mocash.view.activity.InfoActivity;
import ke.co.payconnect.mocash.mocash.view.activity.LocationsActivity;
import ke.co.payconnect.mocash.mocash.view.activity.ProfileActivity;


public class MainServicesFragment extends Fragment {

    // Views
    private CardView cvCardsServices;
    private CardView cvChangePinServices;
    private CardView cvProfileServices;
    private CardView cvAlertsServices;
    private CardView cvFavouritesServices;
    private CardView cvShareServices;
    private CardView cvFeedbackServices;
    private CardView cvInfoServices;
    private CardView cvLocateServices;
    private CardView cvFaqServices;

    private ProgressDialog loading;

    private OnMainServicesInteractionListener mListener;

    public MainServicesFragment() {
        // Required empty public constructor
    }


    public static MainServicesFragment newInstance() {
        return new MainServicesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_services, container, false);

        cvCardsServices = view.findViewById(R.id.cvCardsServices);
        cvChangePinServices = view.findViewById(R.id.cvChangePinServices);
        cvProfileServices = view.findViewById(R.id.cvProfileServices);
        cvAlertsServices = view.findViewById(R.id.cvAlertsServices);
        cvFavouritesServices = view.findViewById(R.id.cvFavouritesServices);
        cvShareServices = view.findViewById(R.id.cvShareServices);
        cvFeedbackServices = view.findViewById(R.id.cvFeedbackServices);
        cvInfoServices = view.findViewById(R.id.cvInfoServices);
        cvLocateServices = view.findViewById(R.id.cvLocateServices);
        cvFaqServices = view.findViewById(R.id.cvFaqServices);

        loading = ProgressDialog.show(getContext(), null, "Loading", false, true);

        setListeners();

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onMainOtherServicesInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMainServicesInteractionListener) {
            mListener = (OnMainServicesInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnMainServicesInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();

        loading.dismiss();
    }

    public interface OnMainServicesInteractionListener {
        // TODO: Update argument type and name
        void onMainOtherServicesInteraction(Uri uri);
    }

    private void setListeners() {
        cvCardsServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), CardsActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvChangePinServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), ChangePinActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvProfileServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), ProfileActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvAlertsServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), ProfileActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvFavouritesServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), FavouritesActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvShareServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loading = ProgressDialog.show(getContext(), null, "Loading", false, true);

                Intent i = new Intent(android.content.Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.share_subject));
                i.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.share_text));
                startActivity(Intent.createChooser(i, getString(R.string.share_via)));
            }

        });

        cvFeedbackServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), FeedbackActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvInfoServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), InfoActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvLocateServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new Connection(getContext()).IsInternetConnected()) {
                    loading = ProgressDialog.show(getContext(), null, "Loading", false, true);

                    startActivity(new Intent(getContext(), LocationsActivity.class));
                } else {
                    if (getContext() != null)
                        Toasty.info(getContext(), R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                }
            }
        });

        cvFaqServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), FaqActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                ((Activity) Objects.requireNonNull(getContext())).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });
    }
}

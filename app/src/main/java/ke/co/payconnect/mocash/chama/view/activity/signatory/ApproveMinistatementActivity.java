package ke.co.payconnect.mocash.chama.view.activity.signatory;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.ViewModelProviders;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Time;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalMinistatementEntity;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;
import ke.co.payconnect.mocash.chama.util.Access;
import ke.co.payconnect.mocash.chama.util.Config;
import ke.co.payconnect.mocash.chama.util.SendTo;
import ke.co.payconnect.mocash.chama.util.Utility;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalMinistatementViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;

import static ke.co.payconnect.mocash.chama.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.chama.util.Config.RETRIES;
import static ke.co.payconnect.mocash.chama.util.Config.TIMEOUT_MS;

public class ApproveMinistatementActivity extends AppCompatActivity {
    private final Context context = this;
    private final Activity activity = this;

    private Toolbar tbApproveMinistatement;
    private TextView tvConnectivityStatusApproveMinistatement;
    private TextView tvMemberNameApproveMinistatement;
    private TextView tvDateAppliedApproveMinistatement;
    private TextView tvReasonApproveMinistatement;
    private TextView tvApprovalStatusApproveMinistatement;
    private TextView tvApprovalIDApproveMinistatement;
    private TextView tvApprovalTypeApproveMinistatement;
    private TextView tvApprovalDateApproveMinistatement;
    private TextView tvApprovalNarrationApproveMinistatement;
    private CardView cvRejectApproveMinistatement;
    private CardView cvAcceptApproveMinistatement;

    private String memberName;
    private String requestDate;
    private String narration;
    private String approvalStatus;
    private String approvalID;
    private String approvalType;
    private String approvalDate;
    private String approvalNarration;

    private ApprovalMinistatementViewModel approvalMinistatementViewModel;
    private MemberViewModel memberViewModel;


    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if (isConnected)
                tvConnectivityStatusApproveMinistatement.setVisibility(View.GONE);
            else
                tvConnectivityStatusApproveMinistatement.setVisibility(View.VISIBLE);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approve_ministatement);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        else
            registerReceiver(connectionReceiver, intentFilter);

        tvConnectivityStatusApproveMinistatement = findViewById(R.id.tvConnectivityStatusApproveMinistatement);
        tbApproveMinistatement = findViewById(R.id.tbApproveMinistatement);
        setSupportActionBar(tbApproveMinistatement);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbApproveMinistatement.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        tvMemberNameApproveMinistatement = findViewById(R.id.tvMemberNameApproveMinistatement);
        tvDateAppliedApproveMinistatement = findViewById(R.id.tvDateAppliedApproveMinistatement);
        tvReasonApproveMinistatement = findViewById(R.id.tvReasonApproveMinistatement);
        tvApprovalStatusApproveMinistatement = findViewById(R.id.tvApprovalStatusApproveMinistatement);
        tvApprovalIDApproveMinistatement = findViewById(R.id.tvApprovalIDApproveMinistatement);
        tvApprovalTypeApproveMinistatement = findViewById(R.id.tvApprovalTypeApproveMinistatement);
        tvApprovalDateApproveMinistatement = findViewById(R.id.tvApprovalDateApproveMinistatement);
        tvApprovalNarrationApproveMinistatement = findViewById(R.id.tvApprovalNarrationApproveMinistatement);
        cvRejectApproveMinistatement = findViewById(R.id.cvRejectApproveMinistatement);
        cvAcceptApproveMinistatement = findViewById(R.id.cvAcceptApproveMinistatement);

        approvalMinistatementViewModel = ViewModelProviders.of(this).get(ApprovalMinistatementViewModel.class);
        memberViewModel = ViewModelProviders.of(this).get(MemberViewModel.class);

        setListeners();

    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);

        getData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void getData() {
        Intent intent = getIntent();
        approvalID = intent.getStringExtra("approval_id");

        ApprovalMinistatementEntity approvalMinistatementEntity = approvalMinistatementViewModel.findMinistatementApprovalById(approvalID);
        approvalType = approvalMinistatementEntity.getApprovalType().toUpperCase();

        approvalStatus = approvalMinistatementEntity.getApprovalStatus().toUpperCase();

        if (approvalMinistatementEntity.getApprovalDate() != null) {
            approvalDate = approvalMinistatementEntity.getApprovalDate();

            try {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                Date date = format.parse(approvalDate);
                Date dateToCompare = format.parse("2000-01-01");

                if ((date != null ? date.compareTo(dateToCompare) : 0) == 0) {
                    approvalDate = "";
                }
            } catch (ParseException ignored) {
            }
        }

        MemberEntity memberEntity = memberViewModel.findMemberByPhoneNumber(approvalMinistatementEntity.getMemberPhoneNumber());

        memberName = memberEntity.getFirstName() + " " + memberEntity.getLastName();
        if (approvalMinistatementEntity.getRequestDate() != null) {
            requestDate = approvalMinistatementEntity.getRequestDate();

            try {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                Date date = format.parse(requestDate);
                Date dateToCompare = format.parse("2000-01-01");

                if ((date != null ? date.compareTo(dateToCompare) : 0) == 0) {
                    requestDate = "";
                }
            } catch (ParseException ignored) {
            }

        }

        narration = approvalMinistatementEntity.getNarration();
        if (narration.equals("0"))
            narration = "No data";

        approvalNarration = approvalMinistatementEntity.getApprovalNarration();
        if (approvalNarration.equals("0"))
            approvalNarration = "No data";

        toggleAcceptView();

        setData();
    }

    private void setData() {
        tvMemberNameApproveMinistatement.setText(memberName);
        if (!requestDate.isEmpty()) {
            try {
                String formattedDate = Utility.getDate(requestDate, "yyyy-MM-dd HH:mm:ss");
                tvDateAppliedApproveMinistatement.setText(formattedDate);
            } catch (ParseException e) {
                tvDateAppliedApproveMinistatement.setText(requestDate);
            }
        } else {
            tvDateAppliedApproveMinistatement.setText("NA");
        }

        if (narration != null) {
            if (!narration.isEmpty())
                tvReasonApproveMinistatement.setText(narration);
        }
        tvApprovalStatusApproveMinistatement.setText(approvalStatus);
        switch (approvalStatus) {
            case "PENDING":
                tvApprovalStatusApproveMinistatement.setTextColor(getResources().getColor(R.color.cO1));
                break;
            case "ACCEPTED":
                tvApprovalStatusApproveMinistatement.setTextColor(getResources().getColor(R.color.cBL0));
                break;
            case "REJECTED":
            case "FAILED":
                tvApprovalStatusApproveMinistatement.setTextColor(getResources().getColor(R.color.cR2));
                break;
        }

        tvApprovalIDApproveMinistatement.setText(approvalID);
        tvApprovalTypeApproveMinistatement.setText(approvalType);

        if (!approvalDate.isEmpty()) {
            try {
                String formattedDate = Utility.getDate(approvalDate, "yyyy-MM-dd HH:mm:ss");
                tvApprovalDateApproveMinistatement.setText(formattedDate);
            } catch (ParseException e) {
                tvApprovalDateApproveMinistatement.setText(approvalDate);
            }
        } else {
            tvApprovalDateApproveMinistatement.setText("NA");
        }

        if (approvalNarration != null) {
            if (!approvalNarration.isEmpty() && !approvalNarration.equals("NA"))
                tvApprovalNarrationApproveMinistatement.setText(approvalNarration);
        }
    }

    private void setListeners() {
        cvRejectApproveMinistatement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onApprovalApproval("rejected");
            }
        });

        cvAcceptApproveMinistatement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onApprovalApproval("accepted");
            }
        });
    }

    private void onApprovalApproval(final String action) {
        LayoutInflater li = LayoutInflater.from(context);
        @SuppressLint("InflateParams") View promptsView = li.inflate(R.layout.struct_accept_approval_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptsView);

        final EditText etStructAcceptApproval = promptsView.findViewById(R.id.etStructAcceptApproval);


        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setPositiveButton(getString(R.string.send),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        String narration = etStructAcceptApproval.getText().toString().trim();

                        if (narration.isEmpty())
                            narration = "NA";

                        String phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();
                        String groupNumber = AppController.getInstance().getChamaPreferenceManager().getGroupNumber();

                        final HashMap<String, String> map = new HashMap<>();
                        map.put("approval", action);
                        map.put("approval_id", approvalID);
                        map.put("approval_narration", narration);
                        map.put("phone_number", phoneNumber);
                        map.put("group_number", groupNumber);
                        map.put("tag", "A-TMINISTAT");

                        final String sessionToken = AppController.getInstance().getChamaPreferenceManager().getSessionToken();
                        processRequest(map, sessionToken);

                    }
                })
                .setNeutralButton(getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void toggleAcceptView() {
        switch (approvalStatus) {
            case "PENDING":
            case "FAILED":
                cvRejectApproveMinistatement.setVisibility(View.VISIBLE);
                cvRejectApproveMinistatement.setEnabled(true);

                cvAcceptApproveMinistatement.setVisibility(View.VISIBLE);
                cvAcceptApproveMinistatement.setEnabled(true);
                break;
            case "ACCEPTED":
            case "REJECTED":
                cvRejectApproveMinistatement.setVisibility(View.GONE);
                cvRejectApproveMinistatement.setEnabled(false);

                cvAcceptApproveMinistatement.setVisibility(View.GONE);
                cvAcceptApproveMinistatement.setEnabled(false);
                break;
        }
    }

    SendTo st = new SendTo(context);
    HurlStack hurlStack = new HurlStack() {
        @Override
        protected HttpURLConnection createConnection(URL url) throws IOException {
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
            try {
                httpsURLConnection.setSSLSocketFactory(st.getSSLSocketFactory());
                httpsURLConnection.setHostnameVerifier(st.getHostnameVerifier());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return httpsURLConnection;
        }
    };


    private void processRequest(final HashMap<String, String> map, final String sessionToken) {

        final ProgressDialog loading = ProgressDialog.show(this, null, "Processing approval", false, false);


        final String requestTag = "chama_approve_exit_approval_request";
        final String url = Config.APPROVE_APPROVAL;
        RequestQueue requestQueue = Volley.newRequestQueue(ApproveMinistatementActivity.this);

        JsonObjectRequest addApprovalReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(final JSONObject response) {
                        try {

                            loading.dismiss();

                            switch (response.getString("status")) {
                                case "success":
                                    updateApprovalStatus(approvalID, map.get("approval"), map.get("approval_narration"));
                                    onComplete("Action submitted successfully", true);
                                    break;

                                case "failed":
                                case "fatal":
                                default:
                                    updateApprovalStatus(approvalID, "failed", map.get("approval_narration"));
                                    onComplete("Action could not be committed at the moment", false);
                                    break;
                            }

                        } catch (JSONException e) {
                            loading.dismiss();
                            Toasty.info(context, "An error occurred", Toast.LENGTH_LONG, true).show();

                            updateApprovalStatus(approvalID, "failed", map.get("approval_narration"));
                            onComplete("Action could not be committed at the moment", false);
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                loading.dismiss();

                updateApprovalStatus(approvalID, "failed", map.get("approval_narration"));
                onComplete("Action could not be committed at the moment", false);

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error")) {
                                    Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":

                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();

                                        break;

                                    default:
                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } else {
                    if (error instanceof NoConnectionError) {
                        Toasty.info(context, "No connection to the server", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof TimeoutError) {
                        Toasty.info(context, "The request has timed out", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof AuthFailureError) {
                        Toasty.info(context, "Authentication failed", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof ServerError) {
                        Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof NetworkError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof ParseError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else {
                        Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    }
                }

            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionToken);

                return headers;
            }

        };

        addApprovalReq.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));
        requestQueue.add(addApprovalReq);
    }

    private void updateApprovalStatus(String approvalID, String approvalStatus, String approvalNarration) {
        String approvalDate = Time.getDateTimeNow("yyyy-MM-dd HH:mm:ss");
        approvalMinistatementViewModel.updateApprovalMinistatementSetData(approvalID, approvalStatus, approvalNarration, approvalDate);

        getData();
    }

    private void onComplete(String message, final boolean isSuccessful) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    if (isSuccessful) {
                        finish();

                        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                    }
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(true);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.ok), dialogClickListener);
        builder.show();
    }
}
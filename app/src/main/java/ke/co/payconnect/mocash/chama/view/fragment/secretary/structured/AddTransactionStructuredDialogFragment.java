package ke.co.payconnect.mocash.chama.view.fragment.secretary.structured;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

import java.text.ParseException;
import java.util.List;

import es.dmoral.toasty.Toasty;
import fr.ganfra.materialspinner.MaterialSpinner;
import io.captano.utility.Time;
import io.captano.utility.ui.UI;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.AccountEntity;
import ke.co.payconnect.mocash.chama.data.entity.TransactionEntity;
import ke.co.payconnect.mocash.chama.util.Config;
import ke.co.payconnect.mocash.chama.util.Utility;
import ke.co.payconnect.mocash.chama.view.adapter.AccountSpinnerAdapter;
import ke.co.payconnect.mocash.chama.viewmodel.AccountViewModel;

public class AddTransactionStructuredDialogFragment extends DialogFragment {

    private MaterialSpinner msSelectAccountDialogAddTxnStructured;
    private EditText etAmountDialogAddTxnStructured;
    private Button btCancelDialogAddTxnStructured;
    private Button btSaveDialogAddTxnStructured;

    private String customerType;
    private String memberPhoneNumber;
    private String groupNumber;
    private String txnType;
    private String accountNumber;

    public static AddTransactionStructuredListener listener;

    public AddTransactionStructuredDialogFragment() {

    }

    public static AddTransactionStructuredDialogFragment newInstance(String customerType, String memberPhoneNumber) {
        AddTransactionStructuredDialogFragment fragment = new AddTransactionStructuredDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("customer_type", customerType);
        bundle.putString("member_phone_number", memberPhoneNumber);
        bundle.putString("group_number", AppController.getInstance().getChamaPreferenceManager().getGroupNumber());
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (AddTransactionStructuredListener) context;

        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement AddTransactionStructuredListener");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_transaction_structured_dialog, container);

        msSelectAccountDialogAddTxnStructured = view.findViewById(R.id.msSelectAccountDialogAddTxnStructured);
        etAmountDialogAddTxnStructured = view.findViewById(R.id.etAmountDialogAddTxnStructured);
        btCancelDialogAddTxnStructured = view.findViewById(R.id.btCancelDialogAddTxnStructured);
        btSaveDialogAddTxnStructured = view.findViewById(R.id.btSaveDialogAddTxnStructured);

        if (getArguments() != null) {
            customerType = getArguments().getString("customer_type");
            memberPhoneNumber = getArguments().getString("member_phone_number");
            groupNumber = getArguments().getString("group_number");

            AccountViewModel accountViewModel = ViewModelProviders.of((FragmentActivity) getContext()).get(AccountViewModel.class);

            switch (customerType) {
                case "group":
                    List<AccountEntity> groupAccountEntities = accountViewModel.getAllAccountsForGroup(groupNumber);
                    if (groupAccountEntities != null) {

                        AccountSpinnerAdapter adapter = new AccountSpinnerAdapter(getContext(), groupAccountEntities);
                        msSelectAccountDialogAddTxnStructured.setAdapter(adapter);

                    } else {
                        Toasty.info(getContext(), "Accounts could not be loaded", Toast.LENGTH_SHORT, true).show();
                    }
                    break;
                case "member":
                    List<AccountEntity> accountEntities = accountViewModel.getAllAccountsForMember(memberPhoneNumber);
                    if (accountEntities != null) {

                        AccountSpinnerAdapter adapter = new AccountSpinnerAdapter(getContext(), accountEntities);
                        msSelectAccountDialogAddTxnStructured.setAdapter(adapter);

                    } else {
                        Toasty.info(getContext(), "Accounts could not be loaded", Toast.LENGTH_SHORT, true).show();
                    }
                    break;
                default:
                    break;
            }


            msSelectAccountDialogAddTxnStructured.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {

                    try {
                        accountNumber = ((TextView) v.findViewById(R.id.tvAccountNumberSpin)).getText().toString();
                        txnType = ((TextView) v.findViewById(R.id.tvAccountNameSpin)).getText().toString();
                    } catch (Exception ignored) {

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {

                }

            });

            btCancelDialogAddTxnStructured.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDialog().cancel();
                }
            });

            btSaveDialogAddTxnStructured.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Validation
                    if (etAmountDialogAddTxnStructured.getText().toString().isEmpty()) {
                        new UI(getActivity()).hideSoftInput();
                        Toasty.info(getContext(), "Amount is required", Toast.LENGTH_SHORT, true).show();
                        return;
                    }

                    if (Double.parseDouble(etAmountDialogAddTxnStructured.getText().toString()) == 0 || Double.parseDouble(etAmountDialogAddTxnStructured.getText().toString()) > 1000000) {
                        new UI(getActivity()).hideSoftInput();
                        Toasty.info(getContext(), "Invalid amount", Toast.LENGTH_SHORT, true).show();
                        return;
                    }

                    if (accountNumber == null) {
                        new UI(getActivity()).hideSoftInput();
                        Toasty.info(getContext(), "Validation failed", Toast.LENGTH_SHORT, true).show();
                        return;
                    }

                    if (accountNumber.toLowerCase().contains("select")) {
                        new UI(getActivity()).hideSoftInput();
                        Toasty.info(getContext(), "Invalid selection for transaction account", Toast.LENGTH_SHORT, true).show();
                        return;
                    }

                    if (txnType == null) {
                        new UI(getActivity()).hideSoftInput();
                        Toasty.info(getContext(), "Validation failed", Toast.LENGTH_SHORT, true).show();
                        return;
                    }

                    if (txnType.toLowerCase().contains("select")) {
                        new UI(getActivity()).hideSoftInput();
                        Toasty.info(getContext(), "Invalid selection for transaction account", Toast.LENGTH_SHORT, true).show();
                        return;
                    }

                    String txnDate = Time.getDateTimeNow("yyyy-MM-dd HH:mm:ss");
                    String truncDate = txnDate;

                    try {
                        truncDate = Utility.getDate(txnDate, "yyyy-MM-dd");
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    String destination;

                    switch (customerType) {
                        case "group":
                            destination = accountNumber;
                            break;
                        case "member":
                            destination = memberPhoneNumber;
                            break;
                        default:
                            Toasty.info(getContext(), getContext().getString(R.string.action_not_permitted), Toast.LENGTH_SHORT, true).show();
                            return;
                    }

                    String phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();

                    TransactionEntity transactionEntity = new TransactionEntity();
                    transactionEntity.setMemberPhoneNumber(destination);
                    transactionEntity.setAmount(etAmountDialogAddTxnStructured.getText().toString().trim());
                    transactionEntity.setSessionTimeStamp(Config.sessionStamp);
                    transactionEntity.setTxnID(Utility.getTxnID());
                    transactionEntity.setTxnDate(txnDate);
                    transactionEntity.setTxnTruncDate(truncDate);
                    transactionEntity.setTxnType(txnType);
                    transactionEntity.setTxnCategory("TXN-GRP-POSTING");
                    transactionEntity.setTxnAccount(accountNumber);
                    transactionEntity.setTxnNarration("Financial Transaction");
                    transactionEntity.setMakerPhoneNumber(phoneNumber);
                    transactionEntity.setTxnStatus("Pending upload");
                    transactionEntity.setTxnProcessingStatus("Unprocessed");

                    listener.onAddTransactionDialog(transactionEntity);

                    getDialog().cancel();
                }
            });
        }

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = 1200;
            dialog.getWindow().setLayout(width, height);
        }
    }

    public interface AddTransactionStructuredListener {
        void onAddTransactionDialog(TransactionEntity transactionEntity);
    }
}

package ke.co.payconnect.mocash.chama.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.ChamaDatabase;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalGroupLoanBalanceEntity;

public class ApprovalGroupLoanBalanceViewModel extends AndroidViewModel {

    private ChamaDatabase db;


    public ApprovalGroupLoanBalanceViewModel(@NonNull Application application) {
        super(application);

        db = ChamaDatabase.getAppDatabaseInstance(this.getApplication());
    }

    public ApprovalGroupLoanBalanceEntity findGroupLoanBalanceApprovalById(String approvalId){
        return db.approvalGroupLoanBalanceDao().findGroupLoanBalanceApprovalById(approvalId);
    }

    public boolean doesGroupLoanBalanceApprovalExistWithApprovalId(String approvalId){
        return db.approvalGroupLoanBalanceDao().doesGroupLoanBalanceApprovalExistWithApprovalId(approvalId);
    }

    public  void updateApprovalGroupLoanBalanceSetData(String approvalID, String approvalStatus, String approvalNarration, String approvalDate) {
        db.approvalGroupLoanBalanceDao().updateApprovalGroupLoanBalanceSetData(approvalID, approvalStatus, approvalNarration, approvalDate);
    }

    public LiveData<List<ApprovalGroupLoanBalanceEntity>> getApprovalGroupLoanBalanceByTypeLive(String approvalType) {
        return  db.approvalGroupLoanBalanceDao().getApprovalGroupLoanBalanceByTypeLive(approvalType);
    }

    public void insertApprovalGroupLoanBalance(ApprovalGroupLoanBalanceEntity approvalTransactionEntity) {
        db.approvalGroupLoanBalanceDao().insertApprovalGroupLoanBalance(approvalTransactionEntity);
    }

    public void deleteAllGroupLoanBalances() {
        db.approvalGroupLoanBalanceDao().deleteAllApprovalGroupLoanBalances();
    }
}

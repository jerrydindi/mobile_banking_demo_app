package ke.co.payconnect.mocash.chama.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.entity.ApprovalTransactionEntity;

@Dao
public interface ApprovalTransactionDao {

    @Query("SELECT uid, member_phone_number, amount, txn_id, batch_number, maker_phone_number, txn_date, txn_type, approval_id, approval_status, approval_type, approval_narration, approval_date FROM ApprovalTransactionEntity ORDER BY approval_id ASC")
    LiveData<List<ApprovalTransactionEntity>> getApprovalTransactionsLive();

    @Query("SELECT  uid, member_phone_number, amount, txn_id, batch_number, maker_phone_number, txn_date, txn_type, approval_id, approval_status, approval_type, approval_narration, approval_date  FROM ApprovalTransactionEntity WHERE approval_id == :approvalID ORDER BY approval_id ASC")
    LiveData<List<ApprovalTransactionEntity>> getApprovalTransactionsByApprovalIDLive(final String approvalID);

    @Query("SELECT  uid, member_phone_number, amount, txn_id, batch_number, maker_phone_number, txn_date, txn_type, approval_id, approval_status, approval_type, approval_narration, approval_date  FROM ApprovalTransactionEntity WHERE approval_type == :approvalType ORDER BY uid DESC")
    LiveData<List<ApprovalTransactionEntity>> getApprovalTransactionsByTypeLive(final String approvalType);

    @Query("SELECT  uid, member_phone_number, amount, txn_id, batch_number, maker_phone_number, txn_date, txn_type, approval_id, approval_status, approval_type, approval_narration, approval_date  FROM ApprovalTransactionEntity WHERE approval_id == :approvalID")
   ApprovalTransactionEntity findApprovalTransactionsByApprovalID(final String approvalID);

    @Query("SELECT EXISTS(SELECT 1 FROM ApprovalTransactionEntity WHERE approval_id == :approvalId LIMIT 1)")
    boolean doesTransactionApprovalExistWithApprovalId(String approvalId);

    @Query("UPDATE ApprovalTransactionEntity SET approval_status = :approvalStatus, approval_narration = :approvalNarration, approval_date = :approvalDate WHERE approval_id == :approvalID")
    void updateApprovalTransactionSetData(final String approvalID, final String approvalStatus, final String approvalNarration, final String approvalDate);

    @Query("SELECT COUNT(*) FROM ApprovalTransactionEntity")
    int getApprovalTransactionsCount();

    @Insert
    void insertApprovalTransaction(ApprovalTransactionEntity... approvalTransactionEntities);

    @Update
    void updateApprovalTransaction(ApprovalTransactionEntity... approvalTransactionEntities);

    @Delete
    void deleteApprovalTransaction(ApprovalTransactionEntity... approvalTransactionEntities);

    @Query("DELETE FROM ApprovalTransactionEntity")
    void deleteAllApprovalTransactions();
}

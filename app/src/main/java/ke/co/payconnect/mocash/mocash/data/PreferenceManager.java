package ke.co.payconnect.mocash.mocash.data;

import android.content.Context;
import android.content.SharedPreferences;

import ke.co.payconnect.mocash.mocash.data.object.LoanAccount;
import ke.co.payconnect.mocash.mocash.data.object.User;

import static android.content.Context.MODE_PRIVATE;

public class PreferenceManager {

    // Shared Preferences
    private SharedPreferences pref;

    // Editor for Shared preferences
    private SharedPreferences.Editor editor;

    // SharedPref file name
    private static final String PREF_NAME = "mocashPREF";

    // Shared Preference Keys
    private static final String KEY_PHONE = "a";
    private static final String KEY_ACCOUNT = "b";
    private static final String KEY_FIRSTNAME = "v";
    private static final String KEY_LASTNAME = "d";
    private static final String KEY_PHOTO = "d";
    private static final String KEY_NATIONAL_ID = "f";
    private static final String KEY_AUTHORISER = "g";
    private static final String KEY_REMEMBER_PHONE = "h";
    private static final String KEY_DEBIT_ALERTS = "i";
    private static final String KEY_SESSION_START = "j";
    private static final String KEY_SESSION_TOKEN = "k";
    private static final String KEY_CHECKOFF_LOAN_LIMIT = "l";
    private static final String KEY_CHECKOFF_LOAN_BALANCE = "m";
    private static final String KEY_M_ADVANCE_LOAN_LIMIT = "n";
    private static final String KEY_MOCASH_LOAN_BALANCE = "o";
    private static final String KEY_INUKA_LOAN_LIMIT = "p";
    private static final String KEY_INUKA_LOAN_BALANCE = "q";
    private static final String KEY_LOGON = "r";
    private static final String KEY_IMEI = "s";
    private static final String KEY_IN_CHECKOFF = "t";
    private static final String KEY_LOAN_ACC_NUMBER = "u";
    private static final String KEY_LOAN_ACC_NAME = "v";

    public PreferenceManager(Context context) {
        pref = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        editor = pref.edit();
        editor.apply();
    }

    public void setUser(User user) {
        editor.putString(KEY_PHONE, user.getPhone());
        editor.putString(KEY_ACCOUNT, user.getAccount());
        editor.putString(KEY_FIRSTNAME, user.getFirstname());
        editor.putString(KEY_LASTNAME, user.getLastname());
        editor.putString(KEY_PHOTO, user.getPhoto());
        editor.putString(KEY_NATIONAL_ID, user.getNationalID());
        editor.putString(KEY_AUTHORISER, user.getPin());

        editor.commit();
    }

    public User getUser() {
        User user = new User();

        if (pref.getString(KEY_PHONE, null) != null)
            user.setPhone(pref.getString(KEY_PHONE, null));

        if (pref.getString(KEY_ACCOUNT, null) != null)
            user.setAccount(pref.getString(KEY_ACCOUNT, null));

        if (pref.getString(KEY_FIRSTNAME, null) != null)
            user.setFirstname(pref.getString(KEY_FIRSTNAME, null));

        if (pref.getString(KEY_LASTNAME, null) != null)
            user.setLastname(pref.getString(KEY_LASTNAME, null));

        if (pref.getString(KEY_PHOTO, null) != null)
            user.setPhoto(pref.getString(KEY_PHOTO, null));

        if (pref.getString(KEY_NATIONAL_ID, null) != null)
            user.setNationalID(pref.getString(KEY_NATIONAL_ID, null));

        if (pref.getString(KEY_AUTHORISER, null) != null)
            user.setPin(pref.getString(KEY_AUTHORISER, null));

        return user;
    }

    public void setLoanAccount(LoanAccount loanAccount){
        editor.putString(KEY_LOAN_ACC_NUMBER, loanAccount.getAccountNumber());
        editor.putString(KEY_LOAN_ACC_NAME, loanAccount.getAccountName());

        editor.commit();
    }

    public LoanAccount getLoanAccount(){
        LoanAccount loanAccount = new LoanAccount();

        if (pref.getString(KEY_LOAN_ACC_NUMBER, null) != null)
            loanAccount.setAccountNumber(pref.getString(KEY_LOAN_ACC_NUMBER, null));

        if (pref.getString(KEY_LOAN_ACC_NAME, null) != null)
            loanAccount.setAccountName(pref.getString(KEY_LOAN_ACC_NAME, null));

        return loanAccount;
    }

    public void setRememberPhone(boolean rememberPhone) {
        editor.putBoolean(KEY_REMEMBER_PHONE, rememberPhone);
        editor.commit();
    }

    public boolean getRememberPhone() {
        return pref.getBoolean(KEY_REMEMBER_PHONE, false);
    }

    public void setDebiAlerts(boolean debitAlert) {
        editor.putBoolean(KEY_DEBIT_ALERTS, debitAlert);
        editor.commit();
    }

    public boolean getDebitAlert() {
        return pref.getBoolean(KEY_DEBIT_ALERTS, true);
    }

    public void setSessionStart(long startTime) {
        editor.putLong(KEY_SESSION_START, startTime);
        editor.commit();
    }

    public long getSessionStart() {
        return pref.getLong(KEY_SESSION_START, 0);
    }

    public void setSessionToken(String sessionToken) {
        editor.putString(KEY_SESSION_TOKEN, sessionToken);
        editor.commit();
    }

    public String getSessionToken() {
        return pref.getString(KEY_SESSION_TOKEN, null);
    }

    public void setCheckoffLoanLimit(String limit) {
        editor.putString(KEY_CHECKOFF_LOAN_LIMIT, limit);
        editor.commit();
    }

    public String getCheckoffLoanLimit() {
        return pref.getString(KEY_CHECKOFF_LOAN_LIMIT, null);
    }

    public void setCheckoffLoanBalance(String loanBalance) {
        editor.putString(KEY_CHECKOFF_LOAN_BALANCE, loanBalance);
        editor.commit();
    }

    public String getCheckoffLoanBalance() {
        return pref.getString(KEY_CHECKOFF_LOAN_BALANCE, null);
    }

    public void setMocashLoanLimit(String limit) {
        editor.putString(KEY_M_ADVANCE_LOAN_LIMIT, limit);
        editor.commit();
    }

    public String getMocashLoanLimit() {
        return pref.getString(KEY_M_ADVANCE_LOAN_LIMIT, null);
    }

    public void setMocashLoanBalance(String loanBalance) {
        editor.putString(KEY_MOCASH_LOAN_BALANCE, loanBalance);
        editor.commit();
    }

    public String getMocashLoanBalance() {
        return pref.getString(KEY_MOCASH_LOAN_BALANCE, null);
    }

    public void setInukaLoanLimit(String limit) {
        editor.putString(KEY_INUKA_LOAN_LIMIT, limit);
        editor.commit();
    }

    public String getInukaLoanLimit() {
        return pref.getString(KEY_INUKA_LOAN_LIMIT, null);
    }

    public void setInukaLoanBalance(String loanBalance) {
        editor.putString(KEY_INUKA_LOAN_BALANCE, loanBalance);
        editor.commit();
    }

    public String getInukaLoanBalance() {
        return pref.getString(KEY_INUKA_LOAN_BALANCE, null);
    }

    public void setLogon(boolean logon) {
        editor.putBoolean(KEY_LOGON, logon);
        editor.commit();
    }

    public boolean getLogon() {
        return pref.getBoolean(KEY_LOGON, false);
    }

    public void setIMEI(String imei) {
        editor.putString(KEY_IMEI, imei);
        editor.commit();
    }

    public String getIMEI() {
        return pref.getString(KEY_IMEI, null);
    }

    public void setInCheckoff(boolean inCheckoff) {
        editor.putBoolean(KEY_IN_CHECKOFF, inCheckoff);
        editor.commit();
    }

    public boolean getInCheckoff() {
        return pref.getBoolean(KEY_IN_CHECKOFF, false);
    }

    public void clear() {
        editor.clear();
        editor.commit();
    }
}

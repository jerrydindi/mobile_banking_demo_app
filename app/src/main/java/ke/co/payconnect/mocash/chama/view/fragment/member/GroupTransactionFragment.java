package ke.co.payconnect.mocash.chama.view.fragment.member;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.LinkedList;
import java.util.List;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.chama.data.entity.TransactionEntity;
import ke.co.payconnect.mocash.chama.view.activity.member.TransactionActivity;
import ke.co.payconnect.mocash.chama.view.adapter.member.GroupTransactionsRecyclerViewAdapter;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.TransactionViewModel;

public class GroupTransactionFragment extends Fragment {
    private TransactionViewModel transactionViewModel;
    private GroupTransactionsRecyclerViewAdapter groupTransactionsRecyclerViewAdapter;

    private RecyclerView rvGroupTxns;
    private TextView tvNoDataGroupTxns;

    private OnGroupInteractionListener listener;

    public GroupTransactionFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_group_transaction, container, false);

        final String txnCategory = TransactionActivity.txnCategory;

        rvGroupTxns = view.findViewById(R.id.rvGroupTxns);
        tvNoDataGroupTxns = view.findViewById(R.id.tvNoDataGroupTxns);

        transactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel.class);
        transactionViewModel.getSummaryTransactionsLive(txnCategory).observe((LifecycleOwner) getContext(), new Observer<List<TransactionEntity>>() {
            @Override
            public void onChanged(List<TransactionEntity> transactionEntities) {

                if (transactionEntities.size() > 0) {
                    toggleDataViews(true);

                    List<TransactionEntity> filteredTransactionEntities = new LinkedList<>();

                    for (TransactionEntity transactionEntity : transactionEntities) {
                        if (transactionEntity.getTxnCategory().equals(txnCategory)) {
                            // Group posting
                            // First check if the transactions match the specified category
                            // Then filter the result depending on the txn category
                            if (txnCategory.equals("TXN-GRP-POSTING")) {
                                List<TransactionEntity> batchTransactions = transactionViewModel.getBatchTransactions(transactionEntity.getBatchNumber());
                                double amount = 0;

                                for (TransactionEntity transaction : batchTransactions)
                                    amount = amount + Double.parseDouble(transaction.getAmount());

                                if (transactionEntity.getMemberPhoneNumber().length() > 2) {
                                    // Structured
                                    if (transactionEntity.getMemberPhoneNumber().substring(0, 4).equals("2547")) {
                                        MemberViewModel memberViewModel = ViewModelProviders.of((FragmentActivity) getContext()).get(MemberViewModel.class);
                                        if (!memberViewModel.doesMemberExistWithPhoneNumber(transactionEntity.getMemberPhoneNumber()))
                                            continue;
                                    }
                                }

                                if (transactionEntity.getTxnProcessingStatus().toLowerCase().trim().equals("processed")) {
                                    TransactionEntity txn = new TransactionEntity();
                                    txn.setTxnID(transactionEntity.getTxnID());
                                    txn.setTxnDate(transactionEntity.getTxnDate());
                                    txn.setBatchNumber(transactionEntity.getBatchNumber());
                                    txn.setMemberPhoneNumber(transactionEntity.getMemberPhoneNumber());
                                    txn.setAmount(String.valueOf(amount));

                                    filteredTransactionEntities.add(txn);
                                }
                            } else if (txnCategory.equals("TXN-TBL-BANKING")) {
                                // Table banking
                                List<TransactionEntity> batchTransactions = transactionViewModel.getBatchTransactions(transactionEntity.getBatchNumber());
                                double amount = 0;

                                for (TransactionEntity transaction : batchTransactions)
                                    amount = amount + Double.parseDouble(transaction.getAmount());

                                if (transactionEntity.getMemberPhoneNumber().length() > 2) {
                                    // Structured
                                    if (transactionEntity.getMemberPhoneNumber().substring(0, 4).equals("2547")) {
                                        MemberViewModel memberViewModel = ViewModelProviders.of((FragmentActivity) getContext()).get(MemberViewModel.class);
                                        if (!memberViewModel.doesMemberExistWithPhoneNumber(transactionEntity.getMemberPhoneNumber()))
                                            continue;
                                    }
                                }

                                if (transactionEntity.getTxnProcessingStatus().toLowerCase().trim().equals("authorised")) {
                                    TransactionEntity txn = new TransactionEntity();
                                    txn.setTxnID(transactionEntity.getTxnID());
                                    txn.setTxnDate(transactionEntity.getTxnDate());
                                    txn.setBatchNumber(transactionEntity.getBatchNumber());
                                    txn.setMemberPhoneNumber(transactionEntity.getMemberPhoneNumber());
                                    txn.setAmount(String.valueOf(amount));

                                    filteredTransactionEntities.add(txn);
                                }
                            } else {
                                // None | Won't execute
                                List<TransactionEntity> batchTransactions = transactionViewModel.getBatchTransactions(transactionEntity.getBatchNumber());
                                double amount = 0;

                                for (TransactionEntity transaction : batchTransactions)
                                    amount = amount + Double.parseDouble(transaction.getAmount());

                                if (transactionEntity.getMemberPhoneNumber().length() > 2) {
                                    // Structured
                                    if (transactionEntity.getMemberPhoneNumber().substring(0, 4).equals("2547")) {
                                        MemberViewModel memberViewModel = ViewModelProviders.of((FragmentActivity) getContext()).get(MemberViewModel.class);
                                        if (!memberViewModel.doesMemberExistWithPhoneNumber(transactionEntity.getMemberPhoneNumber()))
                                            continue;
                                    }
                                }

                                TransactionEntity txn = new TransactionEntity();
                                txn.setTxnID(transactionEntity.getTxnID());
                                txn.setTxnDate(transactionEntity.getTxnDate());
                                txn.setBatchNumber(transactionEntity.getBatchNumber());
                                txn.setMemberPhoneNumber(transactionEntity.getMemberPhoneNumber());
                                txn.setAmount(String.valueOf(amount));

                                filteredTransactionEntities.add(txn);
                            }
                        }

                    }

                    if (filteredTransactionEntities.size() > 0) {
                        groupTransactionsRecyclerViewAdapter = new GroupTransactionsRecyclerViewAdapter(getContext(), filteredTransactionEntities);
                        groupTransactionsRecyclerViewAdapter.notifyDataSetChanged();

                        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                        rvGroupTxns.setAdapter(groupTransactionsRecyclerViewAdapter);
                        rvGroupTxns.setLayoutManager(layoutManager);
                    } else {
                        toggleDataViews(false);
                    }
                } else {
                    toggleDataViews(false);
                }
            }
        });

        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (listener != null) {
            listener.OnGroupInteraction(uri);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnGroupInteractionListener) {
            listener = (OnGroupInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnGroupInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public interface OnGroupInteractionListener {
        void OnGroupInteraction(Uri uri);
    }

    private void toggleDataViews(boolean hasItems) {
        if (hasItems)
            tvNoDataGroupTxns.setVisibility(View.GONE);
        else
            tvNoDataGroupTxns.setVisibility(View.VISIBLE);
    }
}

package ke.co.payconnect.mocash.chama.view.activity.member;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalEntity;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;
import ke.co.payconnect.mocash.chama.data.entity.TransactionEntity;
import ke.co.payconnect.mocash.chama.util.Access;
import ke.co.payconnect.mocash.chama.view.adapter.member.PersonalTransactionsRecyclerViewAdapter;
import ke.co.payconnect.mocash.chama.view.adapter.signatory.ApprovalsRecyclerViewAdapter;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.TransactionViewModel;

public class TransactionBatchDetailsActivity extends AppCompatActivity {
    private final Context context = this;
    private final Activity activity = this;

    private Toolbar tbTxnBatchDetails;
    private TextView tvMemberTxnBatchDetails;
    private TextView tvBatchNoTxnBatchDetails;
    private RecyclerView rvTransactionsTxnBatchDetails;
    private TextView tvNoDataTxnBatchDetails;

    private MemberViewModel memberViewModel;
    private TransactionViewModel transactionViewModel;

    private PersonalTransactionsRecyclerViewAdapter adapter;
    private LinearLayoutManager layoutManager;

    private String member;
    private String batchNo;
    private String txnID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_batch_details);

        tbTxnBatchDetails = findViewById(R.id.tbTxnBatchDetails);
        setSupportActionBar(tbTxnBatchDetails);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbTxnBatchDetails.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        tvMemberTxnBatchDetails = findViewById(R.id.tvMemberTxnBatchDetails);
        tvBatchNoTxnBatchDetails = findViewById(R.id.tvBatchNoTxnBatchDetails);
        rvTransactionsTxnBatchDetails = findViewById(R.id.rvTransactionsTxnBatchDetails);
        tvNoDataTxnBatchDetails = findViewById(R.id.tvNoDataTxnBatchDetails);

        transactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel.class);
        memberViewModel = ViewModelProviders.of(this).get(MemberViewModel.class);

        layoutManager = new LinearLayoutManager(context);
        rvTransactionsTxnBatchDetails.setLayoutManager(layoutManager);

        getData();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);

        setupRecyclerView();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void getData() {
        Intent i = new Intent(getIntent());
        txnID = i.getStringExtra("txn_id");

        TransactionEntity transactionEntity = transactionViewModel.findTransactionByTxnID(txnID);
        if (transactionEntity.getMemberPhoneNumber().substring(3).equals("254")) {
            if (memberViewModel.doesMemberExistWithPhoneNumber(transactionEntity.getMemberPhoneNumber())) {
                MemberEntity memberEntity = memberViewModel.findMemberByPhoneNumber(transactionEntity.getMemberPhoneNumber());
                member = memberEntity.getFirstName() + " " + memberEntity.getLastName();
            } else {
                member = "No data";
            }
        } else {
            member = AppController.getInstance().getChamaPreferenceManager().getGroupName();
        }
        batchNo = transactionEntity.getBatchNumber();

        setData();
    }

    private void setData() {
        tvMemberTxnBatchDetails.setText(member);
        tvBatchNoTxnBatchDetails.setText(batchNo);
    }

    private void setupRecyclerView() {
        transactionViewModel.getBatchTransactionsLive(batchNo).observe((LifecycleOwner) context, new Observer<List<TransactionEntity>>() {
            @Override
            public void onChanged(List<TransactionEntity> transactionEntities) {

                if (transactionEntities.size() > 0) {
                    toggleDataViews(true);

                    adapter = new PersonalTransactionsRecyclerViewAdapter(context, transactionEntities);
                    adapter.notifyDataSetChanged();

                    rvTransactionsTxnBatchDetails.setAdapter(adapter);
                    rvTransactionsTxnBatchDetails.setLayoutManager(layoutManager);
                } else
                    toggleDataViews(false);

            }
        });
    }

    private void toggleDataViews(boolean hasItems) {
        if (hasItems)
            tvNoDataTxnBatchDetails.setVisibility(View.GONE);
        else
            tvNoDataTxnBatchDetails.setVisibility(View.VISIBLE);
    }
}

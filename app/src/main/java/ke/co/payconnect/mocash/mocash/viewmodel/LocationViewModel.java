package ke.co.payconnect.mocash.mocash.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import android.os.AsyncTask;

import java.util.List;

import ke.co.payconnect.mocash.mocash.data.MocashDatabase;
import ke.co.payconnect.mocash.mocash.data.entity.LocationEntity;

public class LocationViewModel extends AndroidViewModel {

    private final List<LocationEntity> locationList;
    private MocashDatabase db;

    public LocationViewModel(Application application) {
        super(application);

        db = MocashDatabase.getMocashDatabaseInstance(this.getApplication());
        locationList = db.locationDao().getAllLocations();
    }


    public List<LocationEntity> getLocationList() {
        return locationList;
    }

    public void insertLocation(LocationEntity location) {
        new InsertAsyncTask(db).execute(location);
    }

    public void deleteAllLocations() {
        new DeleteAllAsyncTask(db).execute();
    }


    private static class InsertAsyncTask extends AsyncTask<LocationEntity, Void, Void> {

        private MocashDatabase db;

        InsertAsyncTask(MocashDatabase mocashDatabase) {
            db = mocashDatabase;
        }

        @Override
        protected Void doInBackground(final LocationEntity... locationEntities) {
            db.locationDao().insertLocation(locationEntities[0]);
            return null;
        }
    }

    private static class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {

        private MocashDatabase db;

        DeleteAllAsyncTask(MocashDatabase mocashDatabase) {
            db = mocashDatabase;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            db.accountDao().deleteAllAccounts();
            return null;
        }
    }
}
package ke.co.payconnect.mocash.mocash.view.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import ke.co.payconnect.mocash.application.AppController;

public class EntryActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= 21) {
            Window w = getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        Intent launchIntent = new Intent();
        Class<?> launchActivity;
        try {
            String className = getScreenClassName();
            launchActivity = Class.forName(className);
        } catch (ClassNotFoundException e) {
            launchActivity = LoginActivity.class;
        }

        launchIntent.setClass(getApplicationContext(), launchActivity);
        startActivity(launchIntent);

        finish();
    }



    // Return Class name of Activity to show
    private String getScreenClassName() {
        if (AppController.getInstance().getMocashPreferenceManager().getRememberPhone() && AppController.getInstance().getMocashPreferenceManager().getUser().getPhone() != null)
            return LoginPinActivity.class.getName();
        else
            return LoginActivity.class.getName();
    }
}
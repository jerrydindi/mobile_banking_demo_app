package ke.co.payconnect.mocash.chama.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.entity.ApprovalMinistatementEntity;

@Dao
public interface ApprovalMinistatementDao {
    @Query("SELECT uid, member_phone_number, narration, request_date, approval_id, approval_status, approval_type, approval_narration, approval_date FROM ApprovalMinistatementEntity WHERE approval_type == :approvalType ORDER BY uid DESC")
    LiveData<List<ApprovalMinistatementEntity>> getApprovalMinistatementsByTypeLive(final String approvalType);

    @Query("SELECT uid, member_phone_number, narration, request_date, approval_id, approval_status, approval_type, approval_narration, approval_date FROM ApprovalMinistatementEntity WHERE approval_id == :approvalId ORDER BY uid DESC")
    ApprovalMinistatementEntity findMinistatementApprovalById(String approvalId);

    @Query("SELECT EXISTS(SELECT 1 FROM ApprovalMinistatementEntity WHERE approval_id == :approvalId LIMIT 1)")
    boolean doesMinistatementApprovalExistWithApprovalId(String approvalId);

    @Query("UPDATE ApprovalMinistatementEntity SET approval_status = :approvalStatus, approval_narration = :approvalNarration, approval_date = :approvalDate WHERE approval_id == :approvalID")
    void updateApprovalMinistatementSetData(final String approvalID, final String approvalStatus, final String approvalNarration, final String approvalDate);

    @Insert
    void insertApprovalMinistatement(ApprovalMinistatementEntity... approvalMinistatementEntities);

    @Query("DELETE FROM ApprovalMinistatementEntity")
    void deleteAllApprovalMinistatements();
}

package ke.co.payconnect.mocash.mocash.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;

import java.util.List;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.mocash.data.entity.FavEntity;
import ke.co.payconnect.mocash.mocash.view.fragment.ConfirmActionDialogFragment;
import ke.co.payconnect.mocash.mocash.view.fragment.FavManageAccountsDialogFragment;
import ke.co.payconnect.mocash.mocash.view.fragment.FavManageDialogFragment;

public class FavListViewAdapter extends BaseAdapter {
    private final Context context;
    private List<FavEntity> favList;
    private LayoutInflater layoutInflater;
    private String tag;

    public FavListViewAdapter(Context context, List<FavEntity> favList, String tag) {
        this.context = context;
        this.favList = favList;
        this.tag = tag;
//        layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater = LayoutInflater.from(context);
    }


    private static class ViewHolder {
        private TextView tvFavAccountName;
        private TextView tvFavAccountNumber;
        private TextView tvFavPhoneNumber;
        private TextView btFavTag;
        private ImageView ivFavIcon, ivFavEdit, ivFavRemove;
        private TextDrawable tdRoundIcon;
    }

    @Override
    public int getCount() {
        if (favList != null)
            return favList.size();
        else
            return 0;
    }

    @Override
    public Object getItem(int position) {
        return favList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FavEntity fav = (FavEntity) getItem(position);
        ViewHolder holder = new ViewHolder();

        try {
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.struct_fav, parent);

                holder.tvFavAccountName = convertView.findViewById(R.id.tvFavAccountName);
                holder.tvFavAccountNumber = convertView.findViewById(R.id.tvFavAccountNumber);
                holder.tvFavPhoneNumber = convertView.findViewById(R.id.tvFavPhoneNumber);
                holder.ivFavIcon = convertView.findViewById(R.id.ivFavIcon);
                holder.ivFavEdit = convertView.findViewById(R.id.ivFavEdit);
                holder.ivFavRemove = convertView.findViewById(R.id.ivFavRemove);
                holder.btFavTag = convertView.findViewById(R.id.btFavTag);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.tvFavAccountName.setText(fav.getAccountName());
            holder.tvFavAccountNumber.setText(fav.getAccountNumber());

            if (fav.getPhoneNumber().equals("-")) {
                // Account
                holder.btFavTag.setVisibility(View.VISIBLE);
            } else {
                // Contact
                holder.btFavTag.setVisibility(View.GONE);
                holder.tvFavPhoneNumber.setText(fav.getPhoneNumber());
            }

            int colour = context.getResources().getColor(R.color.colorPrimary);

            holder.tdRoundIcon = TextDrawable.builder()
                    .beginConfig()
                    .endConfig()
                    .buildRoundRect(fav.getAccountName().substring(0, 1).toUpperCase(), colour, 100);

            holder.ivFavIcon.setImageDrawable(holder.tdRoundIcon);

            final int uid = fav.getUid();
            final String accountName = fav.getAccountName();
            final String accountNumber = fav.getAccountNumber();
            final String phoneNumber = fav.getPhoneNumber();

            if (tag.equals("select")) {
                holder.ivFavEdit.setEnabled(false);
                holder.ivFavEdit.setVisibility(View.GONE);

                holder.ivFavRemove.setEnabled(false);
                holder.ivFavRemove.setVisibility(View.GONE);


                // Listeners
                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Pass the selected device to the active trip activity
                        Intent intent = new Intent("INTENT_FAV");
                        intent.putExtra("accountName", accountName);
                        intent.putExtra("accountNumber", accountNumber);
                        intent.putExtra("phoneNumber", phoneNumber);

                        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

                    }
                });
            } else if (tag.equals("manage")) {

                holder.ivFavEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (phoneNumber.equals("-")) {
                            FragmentManager fm = ((FragmentActivity) context).getSupportFragmentManager();
                            FavManageAccountsDialogFragment favManageAccountsDialogFragment = FavManageAccountsDialogFragment.newInstance("edit_acc", uid, accountName, accountNumber);
                            favManageAccountsDialogFragment.show(fm, "dialog");
                        } else {
                            FragmentManager fm = ((FragmentActivity) context).getSupportFragmentManager();
                            FavManageDialogFragment favManageDialogFragment = FavManageDialogFragment.newInstance("edit", uid, accountName, accountNumber, phoneNumber);
                            favManageDialogFragment.show(fm, "dialog");
                        }
                    }
                });

                holder.ivFavRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Activity activity = (Activity) context;
                        FragmentManager fm = ((FragmentActivity) activity).getSupportFragmentManager();
                        ConfirmActionDialogFragment editNameDialogFragment = new ConfirmActionDialogFragment().newInstance(R.string.confirm_action, "FAV_DELETE", uid, accountName, accountNumber, phoneNumber, "");
                        editNameDialogFragment.show(fm, "fragment_edit_name");

                    }
                });
            }

        } catch (Exception | OutOfMemoryError ex) {
            ex.printStackTrace();
        }
        return convertView;
    }
}


package ke.co.payconnect.mocash.chama.view.activity.signatory;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import es.dmoral.toasty.Toasty;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalExitEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalFullStatementEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalGroupBalanceEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalGroupLoanBalanceEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalMemberAddEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalMinistatementEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalSecretaryEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalTransactionEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalWithdrawEntity;
import ke.co.payconnect.mocash.chama.util.Access;
import ke.co.payconnect.mocash.chama.util.Config;
import ke.co.payconnect.mocash.chama.util.SendTo;
import ke.co.payconnect.mocash.chama.view.adapter.signatory.ApprovalsRecyclerViewAdapter;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalExitViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalFullStatementViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalGroupBalanceViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalGroupLoanBalanceViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalMemberAddViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalMinistatementViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalSecretaryViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalTransactionViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalWithdrawViewModel;

import static ke.co.payconnect.mocash.chama.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.chama.util.Config.RETRIES;

public class ApprovalActivity extends AppCompatActivity {
    private final Context context = this;
    private final Activity activity = this;

    private Toolbar tbApproval;
    private TextView tvConnectivityStatusApproval;
    private CoordinatorLayout cdlRefreshApproval;
    private RecyclerView rvApproval;
    private TextView tvNoDataApproval;

    private LinearLayoutManager layoutManager;

    public ApprovalsRecyclerViewAdapter adapter;

    private ApprovalViewModel approvalViewModel;
    private ApprovalTransactionViewModel approvalTransactionViewModel;
    private ApprovalSecretaryViewModel approvalSecretaryViewModel;
    private ApprovalExitViewModel approvalExitViewModel;
    private ApprovalGroupBalanceViewModel approvalGroupBalanceViewModel;
    private ApprovalMinistatementViewModel approvalMinistatementViewModel;
    private ApprovalFullStatementViewModel approvalFullStatementViewModel;
    private ApprovalWithdrawViewModel approvalWithdrawViewModel;
    private ApprovalMemberAddViewModel approvalMemberAddViewModel;
    private ApprovalGroupLoanBalanceViewModel approvalGroupLoanBalanceViewModel;

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if (isConnected)
                tvConnectivityStatusApproval.setVisibility(View.GONE);
            else
                tvConnectivityStatusApproval.setVisibility(View.VISIBLE);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approval);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        else
            registerReceiver(connectionReceiver, intentFilter);

        tvConnectivityStatusApproval = findViewById(R.id.tvConnectivityStatusApproval);
        tbApproval = findViewById(R.id.tbApproval);
        setSupportActionBar(tbApproval);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbApproval.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        tvConnectivityStatusApproval = findViewById(R.id.tvConnectivityStatusApproval);
        cdlRefreshApproval = findViewById(R.id.cdlRefreshApproval);
        rvApproval = findViewById(R.id.rvApproval);
        tvNoDataApproval = findViewById(R.id.tvNoDataApproval);

        layoutManager = new LinearLayoutManager(context);
        rvApproval.setLayoutManager(layoutManager);

        approvalViewModel = ViewModelProviders.of(this).get(ApprovalViewModel.class);
        approvalTransactionViewModel = ViewModelProviders.of(this).get(ApprovalTransactionViewModel.class);
        approvalTransactionViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalTransactionViewModel.class);
        approvalSecretaryViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalSecretaryViewModel.class);
        approvalExitViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalExitViewModel.class);
        approvalGroupBalanceViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalGroupBalanceViewModel.class);
        approvalMinistatementViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalMinistatementViewModel.class);
        approvalFullStatementViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalFullStatementViewModel.class);
        approvalWithdrawViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalWithdrawViewModel.class);
        approvalMemberAddViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalMemberAddViewModel.class);
        approvalGroupLoanBalanceViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalGroupLoanBalanceViewModel.class);

        setListeners();
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);

        setupRecyclerView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void setupRecyclerView() {
        approvalViewModel.getSummaryApprovalsLive().observe((LifecycleOwner) context, new Observer<List<ApprovalEntity>>() {
            @Override
            public void onChanged(List<ApprovalEntity> approvalEntities) {

                if (approvalEntities.size() > 0) {
                    toggleDataViews(true);

                    adapter = new ApprovalsRecyclerViewAdapter(context, approvalEntities);
                    adapter.notifyDataSetChanged();

                    rvApproval.setAdapter(adapter);
                    rvApproval.setLayoutManager(layoutManager);
                } else
                    toggleDataViews(false);

            }
        });
    }

    private void setListeners() {

        cdlRefreshApproval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();
                String groupNumber = AppController.getInstance().getChamaPreferenceManager().getGroupNumber();
                final int groupType = AppController.getInstance().getChamaPreferenceManager().getGroupType();
                final String sessionToken = AppController.getInstance().getChamaPreferenceManager().getSessionToken();

                final HashMap<String, String> map = new HashMap<>();
                map.put("phone_number", phoneNumber);
                map.put("group_number", groupNumber);
                fetchApprovals(map, String.valueOf(groupType), sessionToken);
            }
        });

    }

    private void toggleDataViews(boolean hasItems) {
        if (hasItems)
            tvNoDataApproval.setVisibility(View.GONE);
        else
            tvNoDataApproval.setVisibility(View.VISIBLE);
    }

    SendTo st = new SendTo(context);
    HurlStack hurlStack = new HurlStack() {
        @Override
        protected HttpURLConnection createConnection(URL url) throws IOException {
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
            try {
                httpsURLConnection.setSSLSocketFactory(st.getSSLSocketFactory());
                httpsURLConnection.setHostnameVerifier(st.getHostnameVerifier());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return httpsURLConnection;
        }
    };

    private void fetchApprovals(HashMap<String, String> map, final String groupType, final String sessionToken) {
        final ProgressDialog loading = ProgressDialog.show(context, null, "Synchronising approvals", false, false);

        final String requestTag = "fetch_approvals_request";
        final String url = Config.FETCH_GROUP_DATA_URL;
        RequestQueue requestQueue = Volley.newRequestQueue(ApprovalActivity.this);

        JsonObjectRequest fetchApprovalsReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(final JSONObject response) {
                        try {
                            loading.dismiss();

                            switch (response.getString("status")) {
                                case "success":
                                    loading.show();
                                    loading.setMessage("Setting up data");

                                    approvalViewModel.deleteAllApprovalsWhere("assignSecretary");
                                    approvalViewModel.deleteAllApprovalsWhere("assign-secretary");
                                    approvalSecretaryViewModel.deleteAllSecretaries();
                                    approvalViewModel.deleteAllApprovalsWhere("memberExit");
                                    approvalViewModel.deleteAllApprovalsWhere("exit");
                                    approvalExitViewModel.deleteAllExits();
                                    approvalViewModel.deleteAllApprovalsWhere("groupBalance");
                                    approvalViewModel.deleteAllApprovalsWhere("group-balance");
                                    approvalGroupBalanceViewModel.deleteAllGroupBalances();
                                    approvalViewModel.deleteAllApprovalsWhere("ministatement");
                                    approvalMinistatementViewModel.deleteAllMinistatements();
                                    approvalViewModel.deleteAllApprovalsWhere("full-statement");
                                    approvalFullStatementViewModel.deleteAllFullStatements();
                                    approvalViewModel.deleteAllApprovalsWhere("withdraw");
                                    approvalWithdrawViewModel.deleteAllApprovalWithdraws();
                                    approvalViewModel.deleteAllApprovalsWhere("member-add");
                                    approvalMemberAddViewModel.deleteAllMemberAddApprovals();
                                    approvalViewModel.deleteAllApprovalsWhere("group-balance");
                                    approvalGroupLoanBalanceViewModel.deleteAllGroupLoanBalances();

                                    AppController.getInstance().getChamaPreferenceManager().setGroupType(Integer.valueOf(groupType));

                                    JSONObject approvalsObject = response.getJSONObject("approvals");
                                    JSONArray approvalTransactionsArray = approvalsObject.getJSONArray("transactionApproval");
                                    addApprovalTransactions(approvalTransactionsArray);

                                    JSONArray addApprovalSecretaries = approvalsObject.getJSONArray("assignSecretaryApproval");
                                    addApprovalSecretaries(addApprovalSecretaries);

                                    JSONArray addApprovalExits = approvalsObject.getJSONArray("memberExitApproval");
                                    addApprovalExits(addApprovalExits);

                                    JSONArray addApprovalGroupBalances = approvalsObject.getJSONArray("accountBalanceApproval");
                                    addApprovalGroupBalances(addApprovalGroupBalances);

                                    JSONArray addApprovalMinistatements = approvalsObject.getJSONArray("miniStatementApproval");
                                    addApprovalMinistatements(addApprovalMinistatements);

                                    JSONArray addApprovalFullStatements = approvalsObject.getJSONArray("fullStatementApproval");
                                    addApprovalFullStatements(addApprovalFullStatements);

                                    JSONArray addApprovalWithdraws = approvalsObject.getJSONArray("withdrawalApproval");
                                    addApprovalWithdraws(addApprovalWithdraws);

                                    JSONArray addApprovalMembersAdd = approvalsObject.getJSONArray("addMemberApproval");
                                    addApprovalMembersAdd(addApprovalMembersAdd);

                                    JSONArray addApprovalGroupLoanBalances = approvalsObject.getJSONArray("loanBalanceApproval");
                                    addApprovalGroupLoanBalances(addApprovalGroupLoanBalances);


                                    loading.dismiss();

                                    break;

                                case "failed":
                                case "fatal":
                                default:
                                    Toasty.info(context, "Failed to synchronise approvals", Toast.LENGTH_LONG, true).show();

                                    break;
                            }

                        } catch (JSONException e) {
                            loading.dismiss();
                            Toasty.info(context, "Failed to synchronise approvals", Toast.LENGTH_LONG, true).show();

                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                loading.dismiss();

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error")) {
                                    Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":
                                        Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();

                                        break;

                                    default:
                                        Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + context.getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } else {
                    if (error instanceof NoConnectionError) {
                        Toasty.info(context, "No connection to the server", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof TimeoutError) {
                        Toasty.info(context, "The request has timed out", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof AuthFailureError) {
                        Toasty.info(context, "Authentication failed", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof ServerError) {
                        Toasty.info(context, context.getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof NetworkError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof ParseError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else {
                        Toasty.info(context, context.getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    }
                }

            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionToken);

                return headers;
            }

        };

        fetchApprovalsReq.setRetryPolicy(new DefaultRetryPolicy(20000, RETRIES, BACKOFF_MULTIPLIER));
        requestQueue.add(fetchApprovalsReq);
    }

    private void addApprovalTransactions(JSONArray approvalTransactionsArray) throws JSONException {
        if (approvalTransactionsArray.length() > 0) {

            // Clear persistent transactions
            approvalViewModel.deleteAllApprovalsWhere("transaction");
            approvalTransactionViewModel.deleteAllApprovalTransactions();

            for (int i = 0; i < approvalTransactionsArray.length(); i++) {

                JSONObject approvalsObject = approvalTransactionsArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("transaction");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalTransactionEntity approvalTransactionEntity = new ApprovalTransactionEntity();

                approvalTransactionEntity.setAmount(approvalsObject.getString("amount"));
                approvalTransactionEntity.setTxnID(approvalsObject.getString("txnID"));
                approvalTransactionEntity.setTxnDate(approvalsObject.getString("txnDate"));
                approvalTransactionEntity.setTxnType(approvalsObject.getString("txnType"));
                approvalTransactionEntity.setBatchNumber(approvalsObject.getString("batchNumber"));
                approvalTransactionEntity.setMemberPhoneNumber(approvalsObject.getString("memberPhoneNumber"));
                approvalTransactionEntity.setMakerPhoneNumber(approvalsObject.getString("makerPhoneNumber"));
                approvalTransactionEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalTransactionEntity.setApprovalType("transaction");
                approvalTransactionEntity.setApprovalStatus("pending");
                approvalTransactionEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalTransactionEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalTransactionViewModel.insertApproval(approvalTransactionEntity);

            }
        }
    }

    private void addApprovalSecretaries(JSONArray approvalSecretariesArray) throws JSONException {
        if (approvalSecretariesArray.length() > 0) {

            // Clear persistent transactions
            approvalViewModel.deleteAllApprovalsWhere("assignSecretary");
            approvalSecretaryViewModel.deleteAllSecretaries();

            for (int i = 0; i < approvalSecretariesArray.length(); i++) {

                JSONObject approvalsObject = approvalSecretariesArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("secretary");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalSecretaryEntity approvalSecretaryEntity = new ApprovalSecretaryEntity();

                approvalSecretaryEntity.setCurrentSecretaryPhoneNumber(approvalsObject.getString("currentSecretaryPhoneNumber"));
                approvalSecretaryEntity.setProposedSecretaryPhoneNumber(approvalsObject.getString("proposedSecretaryPhoneNumber"));
                approvalSecretaryEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalSecretaryEntity.setAssignDate(approvalsObject.getString("assignDate"));
                approvalSecretaryEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalSecretaryEntity.setApprovalType("secretary");
                approvalSecretaryEntity.setApprovalStatus("pending");
                approvalSecretaryEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalSecretaryEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalSecretaryViewModel.insertApprovalSecretary(approvalSecretaryEntity);

            }
        }
    }

    private void addApprovalExits(JSONArray approvalExitssArray) throws JSONException {
        if (approvalExitssArray.length() > 0) {

            // Clear persistent transactions
            approvalViewModel.deleteAllApprovalsWhere("memberExit");
            approvalExitViewModel.deleteAllExits();

            for (int i = 0; i < approvalExitssArray.length(); i++) {

                JSONObject approvalsObject = approvalExitssArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("exit");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalExitEntity approvalExitEntity = new ApprovalExitEntity();

                approvalExitEntity.setMakerPhoneNumber(approvalsObject.getString("makerPhoneNumber"));
                approvalExitEntity.setMemberPhoneNumber(approvalsObject.getString("memberPhoneNumber"));
                approvalExitEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalExitEntity.setExitDate(approvalsObject.getString("exitDate"));
                approvalExitEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalExitEntity.setApprovalType("exit");
                approvalExitEntity.setApprovalStatus("pending");
                approvalExitEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalExitEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalExitViewModel.insertApprovalExit(approvalExitEntity);

            }
        }
    }

    private void addApprovalGroupBalances(JSONArray approvalGroupBalancesArray) throws JSONException {
        if (approvalGroupBalancesArray.length() > 0) {

            for (int i = 0; i < approvalGroupBalancesArray.length(); i++) {

                JSONObject approvalsObject = approvalGroupBalancesArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("group-balance");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalGroupBalanceEntity approvalGroupBalanceEntity = new ApprovalGroupBalanceEntity();

                approvalGroupBalanceEntity.setMemberPhoneNumber(approvalsObject.getString("memberPhoneNumber"));
                approvalGroupBalanceEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalGroupBalanceEntity.setRequestDate(approvalsObject.getString("transactionDate"));
                approvalGroupBalanceEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalGroupBalanceEntity.setApprovalType("group-balance");
                approvalGroupBalanceEntity.setApprovalStatus("pending");
                approvalGroupBalanceEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalGroupBalanceEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalGroupBalanceViewModel.insertApprovalGroupBalance(approvalGroupBalanceEntity);

            }
        }
    }

    private void addApprovalMinistatements(JSONArray approvalMinistatementsArray) throws JSONException {
        if (approvalMinistatementsArray.length() > 0) {

            for (int i = 0; i < approvalMinistatementsArray.length(); i++) {

                JSONObject approvalsObject = approvalMinistatementsArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("ministatement");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalMinistatementEntity approvalMinistatementEntity = new ApprovalMinistatementEntity();

                approvalMinistatementEntity.setMemberPhoneNumber(approvalsObject.getString("memberPhoneNumber"));
                approvalMinistatementEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalMinistatementEntity.setRequestDate(approvalsObject.getString("transactionDate"));
                approvalMinistatementEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalMinistatementEntity.setApprovalType("ministatement");
                approvalMinistatementEntity.setApprovalStatus("pending");
                approvalMinistatementEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalMinistatementEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalMinistatementViewModel.insertApprovalMinistatement(approvalMinistatementEntity);

            }
        }
    }

    private void addApprovalFullStatements(JSONArray approvalFullStatementsArray) throws JSONException {
        if (approvalFullStatementsArray.length() > 0) {

            for (int i = 0; i < approvalFullStatementsArray.length(); i++) {

                JSONObject approvalsObject = approvalFullStatementsArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("full-statement");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalFullStatementEntity approvalFullStatementEntity = new ApprovalFullStatementEntity();

                approvalFullStatementEntity.setMemberPhoneNumber(approvalsObject.getString("memberPhoneNumber"));
                approvalFullStatementEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalFullStatementEntity.setRequestDate(approvalsObject.getString("transactionDate"));
                approvalFullStatementEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalFullStatementEntity.setApprovalType("full-statement");
                approvalFullStatementEntity.setApprovalStatus("pending");
                approvalFullStatementEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalFullStatementEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalFullStatementViewModel.insertApprovalFullStatement(approvalFullStatementEntity);

            }
        }
    }

    private void addApprovalWithdraws(JSONArray approvalWithdrawsArray) throws JSONException {
        if (approvalWithdrawsArray.length() > 0) {

            // Clear persistent withdraws
            approvalViewModel.deleteAllApprovalsWhere("withdraw");
            approvalWithdrawViewModel.deleteAllApprovalWithdraws();

            for (int i = 0; i < approvalWithdrawsArray.length(); i++) {

                JSONObject approvalsObject = approvalWithdrawsArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("withdraw");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval withdraws
                ApprovalWithdrawEntity approvalWithdrawEntity = new ApprovalWithdrawEntity();

                approvalWithdrawEntity.setTxnID(approvalsObject.getString("txnID"));
                approvalWithdrawEntity.setAmount(approvalsObject.getString("withdrawalAmount"));
                approvalWithdrawEntity.setWithdrawDate(approvalsObject.getString("txnDate"));
                approvalWithdrawEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalWithdrawEntity.setMakerPhoneNumber(approvalsObject.getString("makerPhoneNumber"));
                approvalWithdrawEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalWithdrawEntity.setApprovalType("withdraw");
                approvalWithdrawEntity.setDestination(approvalsObject.getString("destinationAccount"));
                approvalWithdrawEntity.setApprovalStatus("pending");
                approvalWithdrawEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalWithdrawEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalWithdrawViewModel.insertApprovalWithdraw(approvalWithdrawEntity);

            }
        }
    }

    private void addApprovalMembersAdd(JSONArray addApprovalMembersAddArray) throws JSONException {
        if (addApprovalMembersAddArray.length() > 0) {

            // Clear persistent withdraws
            approvalViewModel.deleteAllApprovalsWhere("member-add");
            approvalMemberAddViewModel.deleteAllMemberAddApprovals();

            for (int i = 0; i < addApprovalMembersAddArray.length(); i++) {

                JSONObject approvalsObject = addApprovalMembersAddArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("member-add");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval member add
                ApprovalMemberAddEntity approvalMemberAddEntity = new ApprovalMemberAddEntity();

                approvalMemberAddEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalMemberAddEntity.setMakerPhonenumber(approvalsObject.getString("makerPhoneNumber"));
                approvalMemberAddEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalMemberAddEntity.setApprovalType("member-add");
                approvalMemberAddEntity.setApprovalStatus("pending");
                approvalMemberAddEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalMemberAddEntity.setApprovalDate(approvalsObject.getString("approvalDate"));

                approvalMemberAddViewModel.insertApprovalMemberAdd(approvalMemberAddEntity);

            }
        }
    }

    private void addApprovalGroupLoanBalances(JSONArray approvalGroupLoanBalancesArray) throws JSONException {
        if (approvalGroupLoanBalancesArray.length() > 0) {

            for (int i = 0; i < approvalGroupLoanBalancesArray.length(); i++) {

                JSONObject approvalsObject = approvalGroupLoanBalancesArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("group-balance");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalGroupLoanBalanceEntity approvalGroupLoanBalanceEntity = new ApprovalGroupLoanBalanceEntity();

                approvalGroupLoanBalanceEntity.setMemberPhoneNumber(approvalsObject.getString("memberPhoneNumber"));
                approvalGroupLoanBalanceEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalGroupLoanBalanceEntity.setRequestDate(approvalsObject.getString("transactionDate"));
                approvalGroupLoanBalanceEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalGroupLoanBalanceEntity.setApprovalType("group-balance");
                approvalGroupLoanBalanceEntity.setApprovalStatus("pending");
                approvalGroupLoanBalanceEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalGroupLoanBalanceEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalGroupLoanBalanceViewModel.insertApprovalGroupLoanBalance(approvalGroupLoanBalanceEntity);

            }
        }
    }

}

package ke.co.payconnect.mocash.chama.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import android.os.Handler;
import android.os.HandlerThread;
import androidx.annotation.NonNull;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.ChamaDatabase;
import ke.co.payconnect.mocash.chama.data.entity.PermissionEntity;

public class PermissionViewModel extends AndroidViewModel {

    private final LiveData<List<PermissionEntity>> permissionLiveList;
    private ChamaDatabase db;

    public PermissionViewModel(@NonNull Application application) {
        super(application);

        db = ChamaDatabase.getAppDatabaseInstance(this.getApplication());
        permissionLiveList = db.permissionDao().getAllPermissions();
    }

    public LiveData<List<PermissionEntity>> getPermissionLiveList(){
        return permissionLiveList;
    }

    public void insertPermision(PermissionEntity permission){
        new InsertPermissionEntity(db.getClass().getSimpleName()).onLooperPrepared(permission);
    }

    public void updatePermission(PermissionEntity permission){
        new UpdatePermissionEntity(db.getClass().getSimpleName()).onLooperPrepared(permission);
    }

    public void deletePermission(PermissionEntity permission){
        new DeletePermissionEntity(db.getClass().getSimpleName()).onLooperPrepared(permission);
    }

    private class InsertPermissionEntity extends HandlerThread {

        Handler handler;

        public InsertPermissionEntity(String name) {
            super(name);
        }

        public void onLooperPrepared(PermissionEntity permission) {
            handler =new Handler(getLooper()){

                public void handleMessage(PermissionEntity ...permissionEntities){
                    db.permissionDao().insertPermission(permissionEntities[0]);
                }
            };
        }
    }

    private class UpdatePermissionEntity extends HandlerThread {

        Handler handler;

        public UpdatePermissionEntity(String name) {
            super(name);
        }

        public void onLooperPrepared(PermissionEntity permission) {
            handler =new Handler(getLooper()){

                public void handleMessage(PermissionEntity ...permissionEntities){
                    db.permissionDao().updatePermission(permissionEntities[0]);
                }
            };
        }
    }

    private class DeletePermissionEntity extends HandlerThread {

        Handler handler;

        public DeletePermissionEntity(String name) {
            super(name);
        }

        public void onLooperPrepared(PermissionEntity permission) {
            handler =new Handler(getLooper()){

                public void handleMessage(PermissionEntity ...permissionEntities){
                    db.permissionDao().deletePermission(permissionEntities[0]);
                }
            };
        }
    }
}

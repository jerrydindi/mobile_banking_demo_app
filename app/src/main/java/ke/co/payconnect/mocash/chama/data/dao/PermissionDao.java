package ke.co.payconnect.mocash.chama.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.entity.PermissionEntity;

@Dao
public interface PermissionDao {
    //LiveData List
    @Query("SELECT DISTINCT uid, category, name FROM PermissionEntity ORDER BY name ASC")
    LiveData<List<PermissionEntity>> getAllPermissions();

    @Query("SELECT uid, category, name FROM PermissionEntity WHERE uid == :uid")
    PermissionEntity findPermissionByUID(int uid);

    @Query("SELECT COUNT(*) FROM PermissionEntity")
    int getPermissionCount();

    @Insert
    void insertPermission(PermissionEntity ... permissionEntities);
    @Update
    void updatePermission(PermissionEntity ... permissionEntities);
    @Delete
    void deletePermission(PermissionEntity ... permissionEntities);
}

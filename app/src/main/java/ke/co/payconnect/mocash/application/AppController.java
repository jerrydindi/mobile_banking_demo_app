package ke.co.payconnect.mocash.application;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;
import ke.co.payconnect.mocash.mocash.data.entity.AccountDestinationEntity;
import ke.co.payconnect.mocash.mocash.data.entity.AccountEntity;
import ke.co.payconnect.mocash.mocash.data.entity.EmployerEntity;

public class AppController extends Application {

    public static final String TAG = AppController.class.getSimpleName();

    private static AppController instance;
    private RequestQueue requestQueue;

    private ke.co.payconnect.mocash.mocash.data.PreferenceManager mocashPreferenceManager;
    private ke.co.payconnect.mocash.chama.data.PreferenceManager chamaPreferennceManager;
    private List<AccountEntity> accounts;
    private List<AccountDestinationEntity> accountsDestination;
    private List<EmployerEntity> employers;
    private List<MemberEntity> members;

    public static synchronized AppController getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null)
            requestQueue = Volley.newRequestQueue(getApplicationContext());

        return requestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // Set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req,HurlStack hurlstack, String tag) {
        // Set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (requestQueue != null) {
            requestQueue.cancelAll(tag);
        }
    }

    public List<AccountEntity> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<AccountEntity> accounts) {
        this.accounts = accounts;
    }

    public List<AccountDestinationEntity> getAccountsDestination() {
        return accountsDestination;
    }

    public void setAccountsDestination(List<AccountDestinationEntity> accountsDestination) {
        this.accountsDestination = accountsDestination;
    }

    public List<EmployerEntity> getEmployers() {
        return employers;
    }

    public void setEmployers(List<EmployerEntity> employers) {
        this.employers = employers;
    }

    public ke.co.payconnect.mocash.mocash.data.PreferenceManager getMocashPreferenceManager() {
        if (mocashPreferenceManager == null)
            mocashPreferenceManager = new ke.co.payconnect.mocash.mocash.data.PreferenceManager(this);

        return mocashPreferenceManager;
    }

    public ke.co.payconnect.mocash.chama.data.PreferenceManager getChamaPreferenceManager() {
        if (chamaPreferennceManager == null)
            chamaPreferennceManager = new ke.co.payconnect.mocash.chama.data.PreferenceManager(this);

        return chamaPreferennceManager;
    }

    public List<MemberEntity> getMembers() {
        return members;
    }

    public void setMembers(List<MemberEntity> members) {
        this.members = members;
    }
}
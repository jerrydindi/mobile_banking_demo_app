package ke.co.payconnect.mocash.mocash.view.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Connection;
import io.captano.utility.Version;
import io.captano.utility.ui.UI;
import io.captano.utility.ui.UIHandler;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.mocash.data.entity.AccountDestinationEntity;
import ke.co.payconnect.mocash.mocash.data.entity.AccountEntity;
import ke.co.payconnect.mocash.mocash.data.object.User;
import ke.co.payconnect.mocash.mocash.util.Access;
import ke.co.payconnect.mocash.mocash.util.Config;
import ke.co.payconnect.mocash.mocash.viewmodel.AccountDestinationViewModel;
import ke.co.payconnect.mocash.mocash.viewmodel.AccountViewModel;

import static ke.co.payconnect.mocash.mocash.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.mocash.util.Config.RETRIES;
import static ke.co.payconnect.mocash.mocash.util.Config.TIMEOUT_MS;

public class LoginActivity extends AppCompatActivity {
    private final Context context = this;
    private final Activity activity = this;

    // Views
    private TextView tvAttemptsLogin;
    private Button btLogin;
    private AutoCompleteTextView actvPhoneLogin;
    private AutoCompleteTextView actvIdNumberLogin;
    private AutoCompleteTextView actvPinLogin;
    private SwitchCompat swSavePhoneLogin;
    private TextView tvForgotPinLogin;
    private TextView tvConnectivityStatusLogin;
    private static ProgressDialog loading;
    private TextView tvPoweredLogin;

    private double appVersion = 0;

    private final String permission = Manifest.permission.READ_PHONE_STATE;
    private static final int PERMISSION_REQUEST_CODE = 1;

    private AccountViewModel accountViewModel;
    private AccountDestinationViewModel accountDestinationViewModel;

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if (isConnected)
                tvConnectivityStatusLogin.setVisibility(View.GONE);
            else
                tvConnectivityStatusLogin.setVisibility(View.VISIBLE);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (Build.VERSION.SDK_INT >= 21)
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        } else {
            registerReceiver(connectionReceiver, intentFilter);
        }

        AppController.getInstance().getMocashPreferenceManager().setRememberPhone(false);

        accountViewModel = ViewModelProviders.of(this).get(AccountViewModel.class);
        accountDestinationViewModel = ViewModelProviders.of(this).get(AccountDestinationViewModel.class);

        tvAttemptsLogin = findViewById(R.id.tvAttemptsLogin);
        btLogin = findViewById(R.id.btLogin);
        actvPhoneLogin = findViewById(R.id.actvPhoneLogin);
        actvIdNumberLogin = findViewById(R.id.actvIdNumberLogin);
        actvPinLogin = findViewById(R.id.actvPinLogin);
        swSavePhoneLogin = findViewById(R.id.swSavePhoneLogin);
        tvForgotPinLogin = findViewById(R.id.tvForgotPinLogin);
        tvConnectivityStatusLogin = findViewById(R.id.tvConnectivityStatusLogin);
        tvPoweredLogin = findViewById(R.id.tvPoweredLogin);

        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            appVersion = Double.parseDouble(pInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        setListeners();

//        checkPermission();

        if (AppController.getInstance().getMocashPreferenceManager().getUser().getPhone() != null) {
            String phoneNumber = AppController.getInstance().getMocashPreferenceManager().getUser().getPhone();

            actvPhoneLogin.setText(phoneNumber);
            actvPhoneLogin.setEnabled(false);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void checkPermission() {
        if (!isPermissionGranted(permission)) {
            requestPermission(permission);
        } else {
            if (getPhone() != null) {
                String imei = getPhone().get(0);
                if (imei != null)
                    AppController.getInstance().getMocashPreferenceManager().setIMEI(imei);
            }
        }
    }

    private boolean isPermissionGranted(String permission) {
        if (Build.VERSION.SDK_INT >= 23) {
            int result = ContextCompat.checkSelfPermission(activity, permission);
            return result == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }

    private void requestPermission(String permission) {
        ActivityCompat.requestPermissions(activity, new String[]{permission}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (getPhone() != null) {
                    String imei = getPhone().get(0);
                    if (imei != null)
                        AppController.getInstance().getChamaPreferenceManager().setIMEI(imei);
                }
                Toasty.info(context, "Permission granted", Toast.LENGTH_SHORT, true).show();
            } else {
                Toasty.info(context, "Permission denied", Toast.LENGTH_SHORT, true).show();
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    private ArrayList<String> getPhone() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        if (ActivityCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED)
            return null;


        ArrayList<String> phoneList = new ArrayList<>();
//        phoneList.add(telephonyManager != null ? telephonyManager.getImei() : null);
        return phoneList;
    }

    private void setListeners() {
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Internet connectivity check
                if (tvConnectivityStatusLogin.getVisibility() == View.VISIBLE) {
                    Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                } else {
                    if (new Connection(context).IsInternetConnected())
                        validation(actvPhoneLogin.getText().toString().trim(), actvIdNumberLogin.getText().toString().trim(), actvPinLogin.getText().toString().trim());
                    else
                        Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                }

            }
        });

        swSavePhoneLogin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    AppController.getInstance().getMocashPreferenceManager().setRememberPhone(true);
                else
                    AppController.getInstance().getMocashPreferenceManager().setRememberPhone(false);

            }
        });

        tvForgotPinLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotPinAlert();
            }
        });

        tvPoweredLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent webIntent = new Intent(Intent.ACTION_VIEW);
                String websiteUrl = "http://payconnect.co.ke/";
                webIntent.setData(Uri.parse(websiteUrl));
                startActivity(webIntent);
            }
        });
    }

    private void validation(String phoneNumber, String idNumber, String pin) {
        if (phoneNumber.isEmpty()) {
            Toasty.info(context, getString(R.string.phone_required), Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (idNumber.isEmpty()) {
            Toasty.info(context, getString(R.string.id_number_required), Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (pin.isEmpty()) {
            Toasty.info(context, getString(R.string.pin_required), Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (phoneNumber.substring(0, 3).equals("254"))
            phoneNumber = phoneNumber.substring(3);

        if (phoneNumber.substring(0, 1).equals("0"))
            phoneNumber = phoneNumber.substring(1);

        new UI(activity).hideSoftInput();

        if (isPermissionGranted(permission))
            processLogin("254" + phoneNumber, idNumber, pin, Double.toString(appVersion));
        else
            notifyPermissionRequired();


    }

    private void processLogin(String phone, String idNumber, String pin, String version) {
        loading = ProgressDialog.show(context, null, "Authenticating", false, false);

        final String imei = AppController.getInstance().getMocashPreferenceManager().getIMEI();

        final String requestTag = "login_request";
        final String url = Config.LOGIN_URL;

        final Map<String, String> map = new HashMap<>();
        map.put("phone_number", phone);
        map.put("id_number", idNumber);
        map.put("pin", pin);
        map.put("account", phone);
        map.put("version", version);
        map.put("imei", imei);

        JsonObjectRequest loginReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(final JSONObject response) {
                        try {
                            System.out.println("response: " + response);
                            loading.dismiss();
                            tvAttemptsLogin.setVisibility(View.INVISIBLE);


                            switch (response.getString("status")) {
                                case "success":

                                    // Check if the system is under maintenance
                                    if (response.getBoolean("shutdown_mocash_android")) {
                                        notifyMaintenance(response.getString("shutdown_message"));
                                        return;
                                    }

                                    // Save session token
                                    Config.sessionExpiry = response.getInt("device_session_expiry");
                                    String sessionToken = response.getString("token");
                                    if (!sessionToken.isEmpty())
                                        AppController.getInstance().getMocashPreferenceManager().setSessionToken(sessionToken);

                                    // Save user data
                                    User user = new User();
                                    user.setFirstname(response.getString("first_name"));
                                    user.setLastname(response.getString("last_name"));
                                    user.setPhone(response.getString("phone_number"));
                                    user.setPin(map.get("pin"));
                                    user.setAccount(response.getString("phone_number")); // The phone number is also the account value
                                    user.setNationalID(response.getString("id_number"));

                                    AppController.getInstance().getMocashPreferenceManager().setUser(user);

                                    // Add accounts
                                    JSONArray accountsArray = response.getJSONArray("accounts");

                                    // Validate accounts
                                    if (accountsArray.length() <= 0) {
                                        Toasty.info(context, "Could not fetch accounts.", Toast.LENGTH_LONG, true).show();

                                        return;
                                    }

                                    // Clear persistent accounts
                                    accountViewModel.deleteAllAccounts();

                                    if (accountsArray.length() > 0) {

                                        for (int i = 0; i < accountsArray.length(); i++) {
                                            AccountEntity accountEntity = new AccountEntity();

                                            JSONObject accountsObject = accountsArray.getJSONObject(i);

                                            accountEntity.setAccountNumber(accountsObject.getString("account"));
                                            accountEntity.setAccountType(accountsObject.getString("accountType"));

                                            accountViewModel.insertAccount(accountEntity);
                                        }
                                    }


                                    // Add accountsDestination
                                    JSONArray accountsDestinationArray = response.getJSONArray("accounts");

                                    // Clear persistent destination accounts
                                    accountDestinationViewModel.deleteAllAccounts();

                                    if (accountsDestinationArray.length() > 0) {

                                        for (int i = 0; i < accountsDestinationArray.length(); i++) {
                                            AccountDestinationEntity accountDestinationEntity = new AccountDestinationEntity();

                                            JSONObject accountsObject = accountsDestinationArray.getJSONObject(i);

                                            accountDestinationEntity.setAccountNumber(accountsObject.getString("account"));
                                            accountDestinationEntity.setAccountType(accountsObject.getString("accountType"));

                                            accountDestinationViewModel.insertAccount(accountDestinationEntity);
                                        }
                                    }

                                    switch (response.getString("change_pin")) {
                                        case "0":
                                            // Check update status
                                            if (response.getBoolean("is_update_due")) {
                                                versionCheck(response.getInt("version"), response.getString("update_message"), response.getBoolean("force_update"));

                                                tvAttemptsLogin.setVisibility(View.INVISIBLE);
                                            } else {
                                                Toasty.info(context, "Login successful", Toast.LENGTH_SHORT, true).show();

                                                tvAttemptsLogin.setVisibility(View.INVISIBLE);

                                                actionLogin();
                                            }
                                            break;
                                        case "1":
                                            Toasty.info(context, "Kindly verify your login details", Toast.LENGTH_LONG, true).show();

                                            openChangePinActivity();
                                            break;
                                        default:
                                            Toasty.info(context, "This request could not be verified at the moment", Toast.LENGTH_LONG, true).show();
                                            break;
                                    }

                                    break;
                                case "failed":
                                    actvPinLogin.getText().clear();

                                    tvAttemptsLogin.setVisibility(View.VISIBLE);

                                    try {
                                        if (response.getString("response_code").equals("009")) {
                                            if (response.getString("attempts").equals("0")) {

                                                tvAttemptsLogin.setText("Your account has been locked");
                                                accountBlockedDialog(false);

                                            } else {

                                                if (response.getString("attempts").equals("1")) {
                                                    tvAttemptsLogin.setText(response.getString("attempts") + " attempt remaining");
                                                } else {
                                                    tvAttemptsLogin.setText(response.getString("attempts") + " attempts remaining");
                                                }
                                            }

                                            Toasty.info(context, "Login failed. Invalid credentials", Toast.LENGTH_SHORT, true).show();

                                        } else if (response.getString("response_code").equals("012")) {

                                            tvAttemptsLogin.setText("Your account has been locked");
                                            accountBlockedDialog(false);

                                        } else {
                                            Toasty.info(context, "Login failed. Invalid credentials", Toast.LENGTH_SHORT, true).show();
                                        }

                                    } catch (JSONException e) {
                                        UIHandler.runOnUI(new Runnable() {
                                            public void run() {
                                                actvPinLogin.getText().clear();

                                                loading.dismiss();
                                                Toasty.info(context, "An error occurred", Toast.LENGTH_LONG, true).show();
                                            }
                                        });
                                    }

                                    break;
                                default:
                                    actvPinLogin.getText().clear();

                                    Toasty.info(context, "Login failed", Toast.LENGTH_SHORT, true).show();

                                    break;
                            }

                        } catch (JSONException e) {
                            actvPinLogin.getText().clear();

                            loading.dismiss();
                            Toasty.info(context, "An error occurred", Toast.LENGTH_LONG, true).show();

                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                loading.dismiss();

                actvPinLogin.getText().clear();

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error")) {
                                    Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":
                                /* Validation errors - Sample response
                                  {
                                      "status": "fatal",
                                        "errors": [{
                                          "location": "body",
                                          "param": "agent_number",
                                          "msg": "Validation failed | CO::00-1"
                                        }]
                                 }
                                 */

                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();

                                        break;

                                    default:
                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } else {
                    if (error instanceof NoConnectionError) {
                        Toasty.info(context, "No connection to the server", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof TimeoutError) {
                        Toasty.info(context, "The request has timed out", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof AuthFailureError) {
                        Toasty.info(context, "Authentication failed", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof ServerError) {
                        Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof NetworkError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof ParseError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else {
                        Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    }
                }
            }
        });

        loginReq.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));
        AppController.getInstance().addToRequestQueue(loginReq, requestTag);

    }

    private void actionLogin() {
        Access.startSession();

        Intent i = new Intent(context, MainActivity.class);
        startActivity(i);

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void actionUpdateApp() {
        /*
        Re-directs user to app listing on PlayStore
         */

        final String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    private void openChangePinActivity() {
        Intent i = new Intent(context, LoginNewActivity.class);
        startActivity(i);

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void versionCheck(int newVersion, String versionUpdateMessage, boolean forceUpdate) {
        try {
            int currentVersion = Version.getVersionCode(getApplication());
            if (currentVersion < newVersion) {
                notifyUpdateVersion(versionUpdateMessage, forceUpdate);
            } else {
                Toasty.info(context, "Login successful", Toast.LENGTH_SHORT, true).show();
                actionLogin();
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void notifyUpdateVersion(String message, boolean forceUpdate) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        actionUpdateApp();
                        break;
                    case DialogInterface.BUTTON_NEUTRAL:
                        Toasty.info(context, "Login successful", Toast.LENGTH_SHORT, true).show();
                        actionLogin();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(true);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.update), dialogClickListener);
        if (!forceUpdate)
            builder.setNeutralButton(getString(R.string.action_login), dialogClickListener);
        builder.show();
    }

    private void notifyMaintenance(String message) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(true);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.ok), dialogClickListener);
        builder.show();
    }

    private void notifyPermissionRequired() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                checkPermission();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(true);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage("Phone state permission allows the app to get the device identifier. Please allow it to be able to proceed.");
        builder.setPositiveButton(getString(R.string.ok), dialogClickListener);
        builder.show();
    }

    private void accountBlockedDialog(final boolean goBack) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:

                        if (goBack) {
                            Intent i = new Intent(context, LandingActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);

                            overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                        }

                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        // Do nothing
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.warning));
        builder.setIcon(R.drawable.ic_fyi);
        builder.setMessage(getString(R.string.locked_message));
        builder.setPositiveButton(getString(R.string.ok), dialogClickListener);
        builder.show();
    }

    private void forgotPinAlert() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);

                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:

                        call();
                        break;
                }
            }
        };


        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.confirm_action));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage("Kindly contact customer care for assistance. \n\nContact: 0709253000");
        builder.setPositiveButton("call", dialogClickListener);
        builder.show();
    }

    private void call() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + "0709253000"));
        startActivity(intent);
    }
}
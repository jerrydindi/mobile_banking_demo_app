package ke.co.payconnect.mocash.chama.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.ChamaDatabase;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalGroupBalanceEntity;

public class ApprovalGroupBalanceViewModel extends AndroidViewModel {

    private ChamaDatabase db;

    public ApprovalGroupBalanceViewModel(@NonNull Application application) {
        super(application);

        db = ChamaDatabase.getAppDatabaseInstance(this.getApplication());
    }

    public ApprovalGroupBalanceEntity findGroupBalanceApprovalById(String approvalId){
        return db.approvalGroupBalanceDao().findGroupBalanceApprovalById(approvalId);
    }

    public boolean doesGroupBalanceApprovalExistWithApprovalId(String approvalId){
        return db.approvalGroupBalanceDao().doesGroupBalanceApprovalExistWithApprovalId(approvalId);
    }

    public  void updateApprovalGroupBalanceSetData(String approvalID, String approvalStatus, String approvalNarration, String approvalDate) {
        db.approvalGroupBalanceDao().updateApprovalGroupBalanceSetData(approvalID, approvalStatus, approvalNarration, approvalDate);
    }

    public  LiveData<List<ApprovalGroupBalanceEntity>> getApprovalGroupBalancesByTypeLive(String approvalType) {
        return  db.approvalGroupBalanceDao().getApprovalGroupBalancesByTypeLive(approvalType);
    }

    public void insertApprovalGroupBalance(ApprovalGroupBalanceEntity approvalTransactionEntity) {
        db.approvalGroupBalanceDao().insertApprovalGroupBalance(approvalTransactionEntity);
    }

    public void deleteAllGroupBalances() {
        db.approvalGroupBalanceDao().deleteAllApprovalGroupBalances();
    }
}

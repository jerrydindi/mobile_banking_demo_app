package ke.co.payconnect.mocash.mocash.view.activity;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.mocash.util.Access;
import ke.co.payconnect.mocash.mocash.view.adapter.ViewPagerAdapter;
import ke.co.payconnect.mocash.mocash.view.fragment.MobileMoneyMpesaOwnFragment;
import ke.co.payconnect.mocash.mocash.view.fragment.MobileMoneyMpesaOtherFragment;
import ke.co.payconnect.mocash.mocash.view.fragment.MobileMoneyMpesaOtherFragment.OnMobileMoneyMpesaOtherInteractionListener;

public class MobileMoneyBaseActivity extends AppCompatActivity
        implements
        MobileMoneyMpesaOwnFragment.OnMobileMoneyMpesaOwnInteractionListener,
        OnMobileMoneyMpesaOtherInteractionListener {
    private final Activity activity = this;
    private final Context context = this;

    // Views
    private Toolbar toolbar;
    private ViewPager viewPager;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_money_base);

        toolbar = findViewById(R.id.tbMMB);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        viewPager = findViewById(R.id.vpMpesaMMB);
        setupViewPager(viewPager);

        // Set Tabs inside Toolbar
        tabLayout = findViewById(R.id.tlMpesaMMB);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    // Add fragments to tabs
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new MobileMoneyMpesaOwnFragment(), getString(R.string.my_num));
        viewPagerAdapter.addFragment(new MobileMoneyMpesaOtherFragment(), getString(R.string.other_num));

        viewPager.setAdapter(viewPagerAdapter);
    }

    @Override
    public void onMobileMoneyMpesaOwnInteraction(Uri uri) {

    }

    @Override
    public void onMobileMoneyMpesaOtherInteraction(Uri uri) {

    }
}

package ke.co.payconnect.mocash.mocash.view.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.transition.Slide;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Connection;
import io.captano.utility.ui.UIController;
import io.captano.utility.ui.UIHandler;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.mocash.data.entity.AccountEntity;
import ke.co.payconnect.mocash.mocash.util.Access;
import ke.co.payconnect.mocash.mocash.util.Config;
import ke.co.payconnect.mocash.mocash.view.adapter.AccountSpinnerAdapter;
import ke.co.payconnect.mocash.mocash.view.fragment.ConfirmTransactionActivitiesDialogFragment;
import ke.co.payconnect.mocash.mocash.view.fragment.PasswordInputActivitiesFragment;

import static ke.co.payconnect.mocash.mocash.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.mocash.util.Config.RETRIES;
import static ke.co.payconnect.mocash.mocash.util.Config.TIMEOUT_MS;

public class AgentWithdrawActivity extends AppCompatActivity implements
        ConfirmTransactionActivitiesDialogFragment.ConfirmTransactionActivitiesListener,
        PasswordInputActivitiesFragment.PasswordInputActivitiesListener {

    private final Context context = this;
    private final Activity activity = this;

    // Views
    private Toolbar tbAgency;
    private Spinner spCurrencyAgency;
    private Spinner spAccountSourceAgency;
    private EditText etAmountAgency;
    private EditText etAgentNumberAgency;
    private Button btSubmitAgency;

    private String accountFromSelected = null;

    private String validatedAccountFrom = "";
    private String validatedAmount = "0";
    private String validatedAgentNo = "0";
    private ConstraintLayout clContainerAgency;
    private TextView tvConnectivityStatusAgency;

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            // With animated visibility transitions
            if (isConnected) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    Slide slideVisible = new Slide();
                    slideVisible.setSlideEdge(Gravity.TOP);
                    TransitionManager.beginDelayedTransition(clContainerAgency, slideVisible);
                    tvConnectivityStatusAgency.setVisibility(View.GONE);
                } else {
                    tvConnectivityStatusAgency.setVisibility(View.GONE);
                }
            } else {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    Slide slideInvisible = new Slide();
                    slideInvisible.setSlideEdge(Gravity.TOP);
                    TransitionManager.beginDelayedTransition(clContainerAgency, slideInvisible);
                    tvConnectivityStatusAgency.setVisibility(View.VISIBLE);
                } else {
                    tvConnectivityStatusAgency.setVisibility(View.VISIBLE);
                }
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_withdraw);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        } else {
            registerReceiver(connectionReceiver, intentFilter);
        }

        tbAgency = findViewById(R.id.tbAgency);
        setSupportActionBar(tbAgency);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbAgency.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        spCurrencyAgency = findViewById(R.id.spCurrencyAgency);
        spAccountSourceAgency = findViewById(R.id.spAccountSourceAgency);
        etAmountAgency = findViewById(R.id.etAmountAgency);
        etAgentNumberAgency = findViewById(R.id.etAgentNumberAgency);
        btSubmitAgency = findViewById(R.id.btSubmitAgency);
        clContainerAgency = findViewById(R.id.clContainerAgency);
        tvConnectivityStatusAgency = findViewById(R.id.tvConnectivityStatusAgency);

        if (context != null) {

            String[] currency = {"KES"};
            ArrayAdapter<String> currencyAdapter = new ArrayAdapter<>(this.context, android.R.layout.simple_spinner_item, currency);
            currencyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spCurrencyAgency.setAdapter(currencyAdapter);


            List<AccountEntity> accounts = AppController.getInstance().getAccounts();

            if (accounts != null) {

                AccountSpinnerAdapter adapter = new AccountSpinnerAdapter(context, accounts);
                spAccountSourceAgency.setAdapter(adapter);

                setListeners();

            } else {
                new UIController().redirect(activity, LoginActivity.class);
            }

        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    @Override
    public void onPasswordInputActivitiesDialog(boolean isSuccessful, String tag) {
        if (isSuccessful)
            if (tag.equals("AGENT"))
                processTransaction();
            else
                Toasty.info(context, "Incorrect pin", Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onConfirmTransactionActivitiesListener(boolean isConfirmed, String action) {
        if (isConfirmed) {
            if (action.equals("AGENT")) {
                PasswordInputActivitiesFragment fragment = PasswordInputActivitiesFragment.newInstance("AGENT");
                fragment.show(getSupportFragmentManager(), null);

            } else
                Toasty.info(context, "Action could not be completed at the moment", Toast.LENGTH_SHORT, true).show();
        }
    }

    public void setListeners() {
        spAccountSourceAgency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {

                try {
                    accountFromSelected = ((TextView) v.findViewById(R.id.tvAccountNumberSpin)).getText().toString();
                } catch (Exception e) {
                    Toasty.info(context, "Your session has timed out", Toast.LENGTH_SHORT, true).show();
                    new UIController().redirect(activity, LoginActivity.class);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });

        btSubmitAgency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (tvConnectivityStatusAgency.getVisibility() == View.VISIBLE) {
                    Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                } else {
                    if (new Connection(context).IsInternetConnected())
                        validation(accountFromSelected, etAmountAgency.getText().toString(), etAgentNumberAgency.getText().toString());
                    else
                        Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                }

            }
        });
    }

    private void validation(String accountFrom, String amount, String agentNo) {
        if (amount == null || amount.isEmpty()) {
            Toasty.info(context, "Please enter a valid amount", Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (accountFrom.equals(agentNo)) {
            Toasty.info(context, "Please enter a valid agent number", Toast.LENGTH_SHORT, true).show();
            return;
        }

        validatedAccountFrom = accountFrom;
        validatedAmount = amount;
        validatedAgentNo = agentNo;

        double amountFormat = Double.parseDouble(amount);
        ConfirmTransactionActivitiesDialogFragment fragment = ConfirmTransactionActivitiesDialogFragment.newInstance(R.string.confirm_transaction, validatedAgentNo, validatedAccountFrom, amountFormat, "AGENT", "", "");
        fragment.show(getSupportFragmentManager(), null);
    }

    private void notifySessionExpired(String message) {
        Access.forceSessionExpiry();

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        boolean stateRemember = AppController.getInstance().getMocashPreferenceManager().getRememberPhone();
                        Access.logout(activity, stateRemember, false);

                        Toasty.info(context, R.string.logout_message_post, Toast.LENGTH_SHORT, true).show();
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(false);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.sign_in), dialogClickListener);
        builder.show();
    }

    public void processTransaction() {
        final ProgressDialog loading = ProgressDialog.show(context, null, "Processing transaction", false, false);

        final String phoneNumber = AppController.getInstance().getMocashPreferenceManager().getUser().getPhone();
        final String idNumber = AppController.getInstance().getMocashPreferenceManager().getUser().getNationalID();
        final String sessionToken = AppController.getInstance().getMocashPreferenceManager().getSessionToken();

        final String requestTag = "agency_withdraw_request";
        final String url = Config.AGENT_WITHDRAW_URL;

        Map<String, String> map = new HashMap<>();
        map.put("phonenumber", phoneNumber);
        map.put("idnumber", idNumber);
        map.put("account", validatedAccountFrom);
        map.put("agentNumber", validatedAgentNo);
        map.put("amount", validatedAmount);

        JsonObjectRequest agentWithdrawReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            loading.dismiss();
                            ConfirmTransactionActivitiesDialogFragment fragment = ConfirmTransactionActivitiesDialogFragment.newInstance(R.string.response, "", "", 0, "RESULT",
                                    response.getString("message"),
                                    "");


                            switch (response.getString("status")) {
                                case "success":
                                    etAmountAgency.getText().clear();
                                    etAgentNumberAgency.getText().clear();

                                    // Show dialog
                                    fragment.show(getSupportFragmentManager(), null);
                                    break;

                                case "failed":
                                    etAmountAgency.getText().clear();
                                    etAgentNumberAgency.getText().clear();

                                    // Show dialog
                                    fragment.show(getSupportFragmentManager(), null);
                                    break;

                                default:
                                    etAmountAgency.getText().clear();
                                    etAgentNumberAgency.getText().clear();

                                    Toasty.info(context, "Failed to process the transaction", Toast.LENGTH_LONG, true).show();
                                    break;
                            }

                        } catch (JSONException e) {
                            loading.dismiss();

                            etAmountAgency.getText().clear();
                            etAgentNumberAgency.getText().clear();

                            Toasty.info(context, "An error occurred", Toast.LENGTH_LONG, true).show();

                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();

                etAmountAgency.getText().clear();
                etAgentNumberAgency.getText().clear();

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 403:
                            notifySessionExpired("Authorization failed: Your session has expired. Kindly sign-in to access this option");
                            break;
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error.")) {
                                    Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":
                                /* validation errors - Sample response
                                  {
                                      "status": "fatal",
                                        "errors": [{
                                          "location": "body",
                                          "param": "agent_number",
                                          "msg": "validation failed | CO::00-1"
                                        }]
                                 }
                                 */

                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();

                                        break;

                                    default:
                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } if (error instanceof NoConnectionError) {
                    UIHandler.runOnUI(new Runnable() {
                        public void run() {
                            Toasty.info(context, "The service is currently unavailable.", Toast.LENGTH_SHORT, true).show();
                        }
                    });
                } else if (error instanceof TimeoutError) {
                    UIHandler.runOnUI(new Runnable() {
                        public void run() {
                            Toasty.info(context, "The request has timed out.", Toast.LENGTH_SHORT, true).show();
                        }
                    });
                } else if (error instanceof AuthFailureError) {
                    UIHandler.runOnUI(new Runnable() {
                        public void run() {
                            Toasty.info(context, "Authentication failed", Toast.LENGTH_SHORT, true).show();
                        }
                    });
                } else if (error instanceof ServerError) {
                    UIHandler.runOnUI(new Runnable() {
                        public void run() {
                            Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                        }
                    });
                } else if (error instanceof NetworkError) {
                    UIHandler.runOnUI(new Runnable() {
                        public void run() {
                            Toasty.info(context, "Network error.", Toast.LENGTH_LONG, true).show();
                        }
                    });
                } else if (error instanceof ParseError) {
                    UIHandler.runOnUI(new Runnable() {
                        public void run() {
                            Toasty.info(context, "Network error.", Toast.LENGTH_LONG, true).show();
                        }
                    });
                } else {
                    UIHandler.runOnUI(new Runnable() {
                        public void run() {
                            Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                        }
                    });
                }
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionToken);

                return headers;
            }
        };

        agentWithdrawReq.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));

        // Adding request to queue
        AppController.getInstance().addToRequestQueue(agentWithdrawReq, requestTag);
    }
}

package ke.co.payconnect.mocash.chama.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.ChamaDatabase;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalTransactionEntity;

public class ApprovalTransactionViewModel extends AndroidViewModel {

    private final LiveData<List<ApprovalTransactionEntity>> approvalTransactionList;
    private ChamaDatabase db;

    public ApprovalTransactionViewModel(@NonNull Application application) {
        super(application);

        db = ChamaDatabase.getAppDatabaseInstance(this.getApplication());
        approvalTransactionList = db.approvalTransactionDao().getApprovalTransactionsLive();
    }

    public LiveData<List<ApprovalTransactionEntity>> getApprovalTransactionsLive() {
        return approvalTransactionList;
    }

    public LiveData<List<ApprovalTransactionEntity>> getApprovalTransactionsByApprovalIDLive(String approvalID) {
        return  db.approvalTransactionDao().getApprovalTransactionsByApprovalIDLive(approvalID);
    }

    public  LiveData<List<ApprovalTransactionEntity>> getApprovalTransactionsByTypeLive(String approvalType) {
        return  db.approvalTransactionDao().getApprovalTransactionsByTypeLive(approvalType);
    }

    public  ApprovalTransactionEntity findApprovalTransactionsByApprovalID(String approvalID) {
        return  db.approvalTransactionDao().findApprovalTransactionsByApprovalID(approvalID);
    }

    public boolean doesTransactionApprovalExistWithApprovalId(String approvalId){
        return db.approvalTransactionDao().doesTransactionApprovalExistWithApprovalId(approvalId);
    }

    public  void updateApprovalTransactionSetData(String approvalID, String approvalStatus, String approvalNarration, String approvalDate) {
         db.approvalTransactionDao().updateApprovalTransactionSetData(approvalID, approvalStatus, approvalNarration, approvalDate);
    }

    public void insertApproval(ApprovalTransactionEntity approvalTransactionEntity) {
        db.approvalTransactionDao().insertApprovalTransaction(approvalTransactionEntity);
    }

    public void updateApprovalTransaction(ApprovalTransactionEntity approvalTransactionEntity) {
        db.approvalTransactionDao().updateApprovalTransaction(approvalTransactionEntity);
    }

    public void deleteApprovalTransaction(ApprovalTransactionEntity approvalTransactionEntity) {
        db.approvalTransactionDao().deleteApprovalTransaction(approvalTransactionEntity);
    }

    public void deleteAllApprovalTransactions() {
        db.approvalTransactionDao().deleteAllApprovalTransactions();
    }
}

package ke.co.payconnect.mocash.chama.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.ChamaDatabase;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalExitEntity;

public class ApprovalExitViewModel extends AndroidViewModel {

    private ChamaDatabase db;

    public ApprovalExitViewModel(@NonNull Application application) {
        super(application);

        db = ChamaDatabase.getAppDatabaseInstance(this.getApplication());
    }

    public ApprovalExitEntity findExitApprovalById(String approvalId){
        return db.approvalExitDao().findExitApprovalById(approvalId);
    }

    public boolean doesExitApprovalExistWithApprovalId(String approvalId){
        return db.approvalExitDao().doesExitApprovalExistWithApprovalId(approvalId);
    }

    public  void updateApprovalExitSetData(String approvalID, String approvalStatus, String approvalNarration, String approvalDate) {
        db.approvalExitDao().updateApprovalExitSetData(approvalID, approvalStatus, approvalNarration, approvalDate);
    }

    public  LiveData<List<ApprovalExitEntity>> getApprovalExitsByTypeLive(String approvalType) {
        return  db.approvalExitDao().getApprovalExitsByTypeLive(approvalType);
    }

    public void insertApprovalExit(ApprovalExitEntity approvalTransactionEntity) {
        db.approvalExitDao().insertApprovalExit(approvalTransactionEntity);
    }

    public void deleteAllExits() {
        db.approvalExitDao().deleteAllApprovalExits();
    }
}

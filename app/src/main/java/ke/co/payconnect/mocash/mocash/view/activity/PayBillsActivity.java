package ke.co.payconnect.mocash.mocash.view.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.mocash.util.Access;

public class PayBillsActivity extends AppCompatActivity {
    private final Context context = this;
    private final Activity activity = this;

    // Views
    private Toolbar toolbar;
    private CardView cvKplcPrepaid;
    private CardView cvKplcPostpaid;
    private CardView cvDstv;
    private CardView cvGotv;
    private CardView cvZuku;
    private CardView cvJamboJet;
    private CardView cvNairobiWater;
    private CardView cvStartimes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_bills);

        toolbar = findViewById(R.id.tbPB);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvKplcPrepaid = findViewById(R.id.cvKplcPrepaid);
        cvKplcPostpaid = findViewById(R.id.cvKplcPostpaid);
        cvDstv = findViewById(R.id.cvDstv);
        cvGotv = findViewById(R.id.cvGotv);
        cvZuku = findViewById(R.id.cvZuku);
        cvJamboJet = findViewById(R.id.cvJamboJet);
        cvNairobiWater = findViewById(R.id.cvNairobiWater);
        cvStartimes = findViewById(R.id.cvStartimes);

        setListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void setListeners() {
        cvKplcPrepaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, PaybillsBaseActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("tag", "kplc_prepaid");
                startActivity(i);

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvKplcPostpaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, PaybillsBaseActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("tag", "kplc_postpaid");
                startActivity(i);

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvDstv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, PaybillsBaseActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("tag", "dstv");
                startActivity(i);

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvGotv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, PaybillsBaseActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("tag", "gotv");
                startActivity(i);

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvZuku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, PaybillsBaseActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("tag", "zuku");
                startActivity(i);

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvJamboJet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, PaybillsBaseActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("tag", "jambojet");
                startActivity(i);

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvNairobiWater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, PaybillsBaseActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("tag", "nairobiwater");
                startActivity(i);

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvStartimes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, PaybillsBaseActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("tag", "startimes");
                startActivity(i);

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });
    }
}

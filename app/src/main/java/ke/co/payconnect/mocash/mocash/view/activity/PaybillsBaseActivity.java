package ke.co.payconnect.mocash.mocash.view.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.transition.Slide;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Connection;
import io.captano.utility.ui.UIController;
import io.captano.utility.ui.UIHandler;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.mocash.data.entity.AccountEntity;
import ke.co.payconnect.mocash.mocash.data.entity.FavEntity;
import ke.co.payconnect.mocash.mocash.util.Access;
import ke.co.payconnect.mocash.mocash.util.Config;
import ke.co.payconnect.mocash.mocash.view.adapter.AccountSpinnerAdapter;
import ke.co.payconnect.mocash.mocash.view.fragment.ConfirmTransactionActivitiesDialogFragment;
import ke.co.payconnect.mocash.mocash.view.fragment.FavSelectDialogFragment;
import ke.co.payconnect.mocash.mocash.view.fragment.PasswordInputActivitiesFragment;
import ke.co.payconnect.mocash.mocash.viewmodel.FavViewModel;

import static ke.co.payconnect.mocash.mocash.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.mocash.util.Config.RETRIES;
import static ke.co.payconnect.mocash.mocash.util.Config.TIMEOUT_MS;

public class PaybillsBaseActivity extends AppCompatActivity implements
        ConfirmTransactionActivitiesDialogFragment.ConfirmTransactionActivitiesListener,
        PasswordInputActivitiesFragment.PasswordInputActivitiesListener {

    private final Activity activity = this;
    private final Context context = this;

    // Views
    private Toolbar toolbar;
    private TextView tvPaybillName;
    private ImageView ivPaybill;

    private Spinner spAccountSourceOther, spCurrencyOther;
    private EditText etAccountDestinationOtherPBase;
    private EditText etAmountOther;
    private ImageView ivFavContactsPBase;
    private ImageView ivFavContactsRemovePBase;
    private TextView tvAccountNameDestinationPBase, tvAccountNumberDestinationPBase;
    private Button btSubmitOther;
    private ConstraintLayout clContainerPBBase;
    private TextView tvConnectivityStatusPBBase;

    private FavViewModel favViewModel;

    private String accountName = "";
    private String accountNumber = "";
    private String phoneNumber = "";
    private String accountFromSelected = null;

    private String validatedAccountFrom = "";
    private String validatedAccountTo = "";
    private String validatedAmount = "0";

    private boolean isFavAvailable = false;

    private String paybillName = "";
    private String paybill = "";

    private final Map<String, String> paybillParams = new HashMap<>();

    private BroadcastReceiver mFavReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            accountName = intent.getStringExtra("accountName");
            accountNumber = intent.getStringExtra("accountNumber");
            phoneNumber = intent.getStringExtra("phoneNumber");

            if (!phoneNumber.equals("-")) {
                Toasty.info(context, getString(R.string.invalid_cr_pb), Toast.LENGTH_SHORT, true).show();
                return;
            }

            tvAccountNameDestinationPBase.setText(accountName);
            tvAccountNumberDestinationPBase.setText(accountNumber);
            etAccountDestinationOtherPBase.setText(accountNumber);

            etAccountDestinationOtherPBase.setVisibility(View.GONE);
            ivFavContactsPBase.setVisibility(View.GONE);

            ivFavContactsRemovePBase.setVisibility(View.VISIBLE);
            tvAccountNameDestinationPBase.setVisibility(View.VISIBLE);
            tvAccountNumberDestinationPBase.setVisibility(View.VISIBLE);
        }
    };

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            // With animated visibility transitions
            if (isConnected) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    Slide slideVisible = new Slide();
                    slideVisible.setSlideEdge(Gravity.TOP);
                    TransitionManager.beginDelayedTransition(clContainerPBBase, slideVisible);
                    tvConnectivityStatusPBBase.setVisibility(View.GONE);
                } else {
                    tvConnectivityStatusPBBase.setVisibility(View.GONE);
                }
            } else {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    Slide slideInvisible = new Slide();
                    slideInvisible.setSlideEdge(Gravity.TOP);
                    TransitionManager.beginDelayedTransition(clContainerPBBase, slideInvisible);
                    tvConnectivityStatusPBBase.setVisibility(View.VISIBLE);
                } else {
                    tvConnectivityStatusPBBase.setVisibility(View.VISIBLE);
                }
            }

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paybills_base);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        } else {
            registerReceiver(connectionReceiver, intentFilter);
        }

        toolbar = findViewById(R.id.tbPBBase);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        LocalBroadcastManager.getInstance(activity).registerReceiver(mFavReceiver, new IntentFilter("INTENT_FAV_PB"));

        favViewModel = ViewModelProviders.of(this).get(FavViewModel.class);

        favViewModel.getFavList().observe(PaybillsBaseActivity.this, new Observer<List<FavEntity>>() {
            @Override
            public void onChanged(@Nullable List<FavEntity> fav) {

                if (fav == null || fav.isEmpty())
                    isFavAvailable = false;
                else
                    isFavAvailable = true;

            }
        });

        tvPaybillName = findViewById(R.id.tvPaybillName);
        ivPaybill = findViewById(R.id.ivPaybill);

        spAccountSourceOther = findViewById(R.id.spAccountSourceOther);
        spCurrencyOther = findViewById(R.id.spCurrencyOther);
        etAccountDestinationOtherPBase = findViewById(R.id.etAccountDestinationOtherPBase);
        etAmountOther = findViewById(R.id.etAmountOther);
        ivFavContactsPBase = findViewById(R.id.ivFavContactsPBase);
        ivFavContactsRemovePBase = findViewById(R.id.ivFavContactsRemovePBase);
        tvAccountNameDestinationPBase = findViewById(R.id.tvAccountNameDestinationPBase);
        tvAccountNumberDestinationPBase = findViewById(R.id.tvAccountNumberDestinationPBase);
        btSubmitOther = findViewById(R.id.btSubmitOther);
        clContainerPBBase = findViewById(R.id.clContainerPBBase);
        tvConnectivityStatusPBBase = findViewById(R.id.tvConnectivityStatusPBBase);


        Intent intent = getIntent();

        switch (intent.getStringExtra("tag")) {
            case "kplc_prepaid":
                paybillName = "kplcprepaid";
                ivPaybill.setImageResource(R.drawable.ic_kplc_prepaid);
                tvPaybillName.setText("Kenya Power - Prepaid (Buy Tokens)");
                break;
            case "kplc_postpaid":
                paybillName = "kplcpostpaid";
                ivPaybill.setImageResource(R.drawable.ic_kplc_postpaid);
                tvPaybillName.setText("Kenya Power - Postpaid");
                break;
            case "dstv":
                paybillName = "dstv";
                ivPaybill.setImageResource(R.drawable.dstv);
                tvPaybillName.setText("DSTV");
                break;
            case "gotv":
                paybillName = "gotv";
                ivPaybill.setImageResource(R.drawable.gotv);
                tvPaybillName.setText("GOTV");
                break;
            case "zuku":
                paybillName = "zuku";
                ivPaybill.setImageResource(R.drawable.zuku);
                tvPaybillName.setText("ZUKU");
                break;
            case "jambojet":
                paybillName = "jambojet";
                ivPaybill.setImageResource(R.drawable.jambo_jet);
                tvPaybillName.setText("JAMBOJET");
                break;
            case "nairobiwater":
                paybillName = "nairobiwater";
                ivPaybill.setImageResource(R.drawable.nairobi_water);
                tvPaybillName.setText("NAIROBI WATER");
                break;
            case "startimes":
                paybillName = "startimes";
                ivPaybill.setImageResource(R.drawable.startimes);
                tvPaybillName.setText("STARTIMES");
                break;
        }


        String[] currency = {"KES"};
        ArrayAdapter<String> currencyAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, currency);
        currencyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCurrencyOther.setAdapter(currencyAdapter);


        List<AccountEntity> accounts = AppController.getInstance().getAccounts();

        if (accounts != null) {

            AccountSpinnerAdapter adapter = new AccountSpinnerAdapter(this.context, accounts);
            spAccountSourceOther.setAdapter(adapter);

            setListeners();

        } else {
            new UIController().redirect(activity, LoginActivity.class);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    public void setListeners() {
        btSubmitOther.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (tvConnectivityStatusPBBase.getVisibility() == View.VISIBLE) {
                    Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                } else {
                    if (new Connection(context).IsInternetConnected())
                        validation(etAccountDestinationOtherPBase.getText().toString(), paybill, etAmountOther.getText().toString(), accountFromSelected);
                    else
                        Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                }
            }
        });

        ivFavContactsPBase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFavAvailable) {
                    FavSelectDialogFragment dialog;

                    FavSelectDialogFragment fragment = FavSelectDialogFragment.newInstance("Saved Contacts", "PB");
                    fragment.show(getSupportFragmentManager(), null);

                } else {
                    showSnackBar(v);
                }
            }
        });

        ivFavContactsRemovePBase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etAccountDestinationOtherPBase.setVisibility(View.VISIBLE);
                ivFavContactsPBase.setVisibility(View.VISIBLE);
                ivFavContactsRemovePBase.setVisibility(View.GONE);
                tvAccountNameDestinationPBase.setVisibility(View.GONE);
                tvAccountNumberDestinationPBase.setVisibility(View.GONE);

                etAccountDestinationOtherPBase.getText().clear();
                accountName = "";
                accountNumber = "";
                phoneNumber = "";
            }
        });
        spAccountSourceOther.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {

                try {
                    accountFromSelected = ((TextView) v.findViewById(R.id.tvAccountNumberSpin)).getText().toString();
                } catch (Exception e) {
                    Toasty.info(context, "Your session has timed out", Toast.LENGTH_SHORT, true).show();
                    new UIController().redirect(activity, LoginActivity.class);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });


    }

    public void showSnackBar(View view) {
        final Snackbar snackbar = Snackbar
                .make(view, "You do not have any saved contacts", Snackbar.LENGTH_LONG)
                .setAction("ADD", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent i = new Intent(context, FavouritesActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(i);
                    }
                });

        snackbar.setActionTextColor(getResources().getColor(R.color.colorPrimary));
        snackbar.show();
    }

    @Override
    public void onPasswordInputActivitiesDialog(boolean isSuccessful, String tag) {
        if (isSuccessful)
            if (tag.equals("PB"))
                processTransaction();
            else
                Toasty.info(context, "Incorrect pin", Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onConfirmTransactionActivitiesListener(boolean isConfirmed, String action) {
        if (isConfirmed) {
            if (action.equals("PB")) {
                PasswordInputActivitiesFragment fragment = PasswordInputActivitiesFragment.newInstance("PB");
                fragment.show(getSupportFragmentManager(), null);

            } else
                Toasty.info(context, "Action could not be completed at the moment", Toast.LENGTH_LONG, true).show();
        }
    }

    private void validation(String accountNumber, String businessNumber, String amount, String accountFrom) {
        if (accountNumber == null || accountNumber.isEmpty()) {
            Toasty.info(context, "Please enter a valid account number", Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (amount == null || amount.isEmpty()) {
            Toasty.info(context, "Please enter a valid amount", Toast.LENGTH_SHORT, true).show();
            return;
        }

        validatedAccountFrom = accountFrom;
        validatedAccountTo = accountNumber;
        validatedAmount = amount;

        double amountFormat = Double.parseDouble(amount);

        ConfirmTransactionActivitiesDialogFragment fragment = ConfirmTransactionActivitiesDialogFragment.newInstance(R.string.confirm_transaction, accountNumber, paybillName.toUpperCase(), amountFormat, "PB", "", "");
        fragment.show(getSupportFragmentManager(), null);

    }

    private void notifySessionExpired(String message) {
        Access.forceSessionExpiry();

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        boolean stateRemember = AppController.getInstance().getMocashPreferenceManager().getRememberPhone();
                        Access.logout(activity, stateRemember, false);

                        Toasty.info(context, R.string.logout_message_post, Toast.LENGTH_SHORT, true).show();
                        break;
                }
            }
        };
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(false);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.sign_in), dialogClickListener);
        builder.show();
    }

    public void processTransaction() {
        final ProgressDialog loading = ProgressDialog.show(context, null, "Processing transaction", false, false);

        String phone = AppController.getInstance().getMocashPreferenceManager().getUser().getPhone();
        String idnumber = AppController.getInstance().getMocashPreferenceManager().getUser().getNationalID();
        final String sessionToken = AppController.getInstance().getMocashPreferenceManager().getSessionToken();

        String requestTag = "pb_request";
        String url = Config.PAY_BILL_URL;

        Map<String, String> map = new HashMap<>();
        map.put("phone_number", phone);
        map.put("id_number", idnumber);
        map.put("account", validatedAccountFrom);
        map.put("biller", paybillName.toUpperCase());
        map.put("utility_account", validatedAccountTo);
        map.put("amount", validatedAmount);

        JsonObjectRequest pbReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            loading.dismiss();
                            ConfirmTransactionActivitiesDialogFragment fragment = ConfirmTransactionActivitiesDialogFragment.newInstance(R.string.response, "", "", 0, "RESULT",
                                    response.getString("message"),
                                    "");


                            switch (response.getString("status")) {
                                case "success":
                                    etAmountOther.getText().clear();
                                    etAccountDestinationOtherPBase.getText().clear();

                                    etAccountDestinationOtherPBase.setVisibility(View.VISIBLE);
                                    ivFavContactsPBase.setVisibility(View.VISIBLE);
                                    ivFavContactsRemovePBase.setVisibility(View.GONE);
                                    tvAccountNameDestinationPBase.setVisibility(View.GONE);
                                    tvAccountNumberDestinationPBase.setVisibility(View.GONE);

                                    // Show dialog
                                    fragment.show(getSupportFragmentManager(), null);
                                    break;

                                case "failed":
                                    etAmountOther.getText().clear();
                                    etAccountDestinationOtherPBase.getText().clear();

                                    etAccountDestinationOtherPBase.setVisibility(View.VISIBLE);
                                    ivFavContactsPBase.setVisibility(View.VISIBLE);
                                    ivFavContactsRemovePBase.setVisibility(View.GONE);
                                    tvAccountNameDestinationPBase.setVisibility(View.GONE);
                                    tvAccountNumberDestinationPBase.setVisibility(View.GONE);

                                    // Show dialog
                                    fragment.show(getSupportFragmentManager(), null);
                                    break;

                                default:
                                    etAmountOther.getText().clear();
                                    etAccountDestinationOtherPBase.getText().clear();

                                    etAccountDestinationOtherPBase.setVisibility(View.VISIBLE);
                                    ivFavContactsPBase.setVisibility(View.VISIBLE);
                                    ivFavContactsRemovePBase.setVisibility(View.GONE);
                                    tvAccountNameDestinationPBase.setVisibility(View.GONE);
                                    tvAccountNumberDestinationPBase.setVisibility(View.GONE);

                                    Toasty.info(context, "Failed to process the transaction", Toast.LENGTH_LONG, true).show();
                                    break;
                            }

                        } catch (JSONException e) {
                            loading.dismiss();


                            etAmountOther.getText().clear();
                            etAccountDestinationOtherPBase.getText().clear();

                            etAccountDestinationOtherPBase.setVisibility(View.VISIBLE);
                            ivFavContactsPBase.setVisibility(View.VISIBLE);
                            ivFavContactsRemovePBase.setVisibility(View.GONE);
                            tvAccountNameDestinationPBase.setVisibility(View.GONE);
                            tvAccountNumberDestinationPBase.setVisibility(View.GONE);

                            Toasty.info(context, "An error occurred", Toast.LENGTH_LONG, true).show();

                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();


                etAmountOther.getText().clear();
                etAccountDestinationOtherPBase.getText().clear();

                etAccountDestinationOtherPBase.setVisibility(View.VISIBLE);
                ivFavContactsPBase.setVisibility(View.VISIBLE);
                ivFavContactsRemovePBase.setVisibility(View.GONE);
                tvAccountNameDestinationPBase.setVisibility(View.GONE);
                tvAccountNumberDestinationPBase.setVisibility(View.GONE);

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 403:
                            notifySessionExpired("Authorization failed: Your session has expired. Kindly sign-in to access this option");
                            break;
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error.")) {
                                    Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":
                                /* validation errors - Sample response
                                  {
                                      "status": "fatal",
                                        "errors": [{
                                          "location": "body",
                                          "param": "agent_number",
                                          "msg": "validation failed | CO::00-1"
                                        }]
                                 }
                                 */

                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();

                                        break;

                                    default:
                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } if (error instanceof NoConnectionError) {
                    UIHandler.runOnUI(new Runnable() {
                        public void run() {
                            Toasty.info(context, "The service is currently unavailable.", Toast.LENGTH_SHORT, true).show();
                        }
                    });
                } else if (error instanceof TimeoutError) {
                    UIHandler.runOnUI(new Runnable() {
                        public void run() {
                            Toasty.info(context, "The request has timed out.", Toast.LENGTH_SHORT, true).show();
                        }
                    });
                } else if (error instanceof AuthFailureError) {
                    UIHandler.runOnUI(new Runnable() {
                        public void run() {
                            Toasty.info(context, "Authentication failed", Toast.LENGTH_SHORT, true).show();
                        }
                    });
                } else if (error instanceof ServerError) {
                    UIHandler.runOnUI(new Runnable() {
                        public void run() {
                            Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                        }
                    });
                } else if (error instanceof NetworkError) {
                    UIHandler.runOnUI(new Runnable() {
                        public void run() {
                            Toasty.info(context, "Network error.", Toast.LENGTH_LONG, true).show();
                        }
                    });
                } else if (error instanceof ParseError) {
                    UIHandler.runOnUI(new Runnable() {
                        public void run() {
                            Toasty.info(context, "Network error.", Toast.LENGTH_LONG, true).show();
                        }
                    });
                } else {
                    UIHandler.runOnUI(new Runnable() {
                        public void run() {
                            Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                        }
                    });
                }
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionToken);

                return headers;
            }
        };

        pbReq.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));

        // Adding request to queue
        AppController.getInstance().addToRequestQueue(pbReq, requestTag);
    }
}

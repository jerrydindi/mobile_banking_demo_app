package ke.co.payconnect.mocash.chama.view.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import es.dmoral.toasty.Toasty;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalExitEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalFullStatementEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalGroupBalanceEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalMemberAddEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalMinistatementEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalSecretaryEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalTransactionEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalWithdrawEntity;
import ke.co.payconnect.mocash.chama.data.entity.GroupEntity;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;
import ke.co.payconnect.mocash.chama.data.entity.MessageEntity;
import ke.co.payconnect.mocash.chama.data.entity.TransactionEntity;
import ke.co.payconnect.mocash.chama.util.Access;
import ke.co.payconnect.mocash.chama.util.Config;
import ke.co.payconnect.mocash.chama.util.FireSync;
import ke.co.payconnect.mocash.chama.util.SendTo;
import ke.co.payconnect.mocash.chama.util.Utility;
import ke.co.payconnect.mocash.chama.view.activity.MainChamaActivity;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalExitViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalFullStatementViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalGroupBalanceViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalMemberAddViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalMinistatementViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalSecretaryViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalTransactionViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalWithdrawViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.MessageViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.TransactionViewModel;

import static ke.co.payconnect.mocash.chama.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.chama.util.Config.RETRIES;
import static ke.co.payconnect.mocash.chama.util.Config.TIMEOUT_MS;

public class LoginGroupRecyclerViewAdapter extends RecyclerView.Adapter<LoginGroupRecyclerViewAdapter.GroupViewHolder> {
    private Context context;
    private List<GroupEntity> groups;

    private MemberViewModel memberViewModel;
    private TransactionViewModel transactionViewModel;
    private ApprovalViewModel approvalViewModel;
    private ApprovalTransactionViewModel approvalTransactionViewModel;
    private ApprovalSecretaryViewModel approvalSecretaryViewModel;
    private ApprovalExitViewModel approvalExitViewModel;
    private ApprovalGroupBalanceViewModel approvalGroupBalanceViewModel;
    private ApprovalMinistatementViewModel approvalMinistatementViewModel;
    private ApprovalFullStatementViewModel approvalFullStatementViewModel;
    private ApprovalWithdrawViewModel approvalWithdrawViewModel;
    private ApprovalMemberAddViewModel approvalMemberAddViewModel;
    private MessageViewModel messageViewModel;

    private FirebaseFirestore firebaseFirestore;
    private FireSync fireSync;
    private SendTo st;

    private ProgressDialog loading;

    public LoginGroupRecyclerViewAdapter(Context context, List<GroupEntity> groups) {
        this.context = context;
        this.groups = groups;

        memberViewModel = ViewModelProviders.of((FragmentActivity) context).get(MemberViewModel.class);
        transactionViewModel = ViewModelProviders.of((FragmentActivity) context).get(TransactionViewModel.class);
        approvalViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalViewModel.class);
        approvalTransactionViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalTransactionViewModel.class);
        approvalSecretaryViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalSecretaryViewModel.class);
        approvalExitViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalExitViewModel.class);
        approvalGroupBalanceViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalGroupBalanceViewModel.class);
        approvalMinistatementViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalMinistatementViewModel.class);
        approvalFullStatementViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalFullStatementViewModel.class);
        messageViewModel = ViewModelProviders.of((FragmentActivity) context).get(MessageViewModel.class);
        approvalWithdrawViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalWithdrawViewModel.class);
        approvalMemberAddViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalMemberAddViewModel.class);

        firebaseFirestore = FirebaseFirestore.getInstance();
        fireSync = new FireSync((Activity) context, firebaseFirestore);
        st = new SendTo(context);

        loading = ProgressDialog.show(context, null, "Synchronising", false, false);
        loading.dismiss();
    }

    @NonNull
    @Override
    public GroupViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_list_groups, parent, false);
        return new GroupViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull GroupViewHolder holder, int position) {
        try {
            final GroupEntity groupEntity = groups.get(position);
            final int colour = context.getResources().getColor(R.color.colorPrimary);

            holder.tdRoundIcon = TextDrawable.builder()
                    .beginConfig()
                    .endConfig()
                    .buildRoundRect(groupEntity.getName().substring(0, 1).toUpperCase(), colour, 100);

            holder.ivNameStructGroup.setImageDrawable(holder.tdRoundIcon);
            holder.tvNameStructGroup.setText(groupEntity.getName().toUpperCase());
            holder.cvStructGroup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AppController.getInstance().getChamaPreferenceManager().setGroupName(groupEntity.getName());
                    AppController.getInstance().getChamaPreferenceManager().setGroupNumber(groupEntity.getNumber());

                    String phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();

                    final HashMap<String, String> map = new HashMap<>();
                    map.put("phone_number", phoneNumber);
                    map.put("group_number", groupEntity.getNumber());

                    final String sessionToken = AppController.getInstance().getChamaPreferenceManager().getSessionToken();

                            fetchGroupData(map, groupEntity.getType(), sessionToken);

                }
            });
        } catch (Exception ignored) {

        }
    }

    @Override
    public int getItemCount() {
        return this.groups.size();
    }

    class GroupViewHolder extends RecyclerView.ViewHolder {
        private CardView cvStructGroup;
        private TextView tvNameStructGroup;
        private ImageView ivNameStructGroup;
        private TextDrawable tdRoundIcon;

        GroupViewHolder(View view) {
            super(view);

            cvStructGroup = view.findViewById(R.id.cvStructGroup);
            tvNameStructGroup = view.findViewById(R.id.tvNameStructGroup);
            ivNameStructGroup = view.findViewById(R.id.ivNameStructGroup);
        }

    }


    HurlStack hurlStack = new HurlStack() {
        @Override
        protected HttpURLConnection createConnection(URL url) throws IOException {
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
            try {
                httpsURLConnection.setSSLSocketFactory(st.getSSLSocketFactory());
                httpsURLConnection.setHostnameVerifier(st.getHostnameVerifier());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return httpsURLConnection;
        }
    };

    private void fetchGroupData(HashMap<String, String> map, final String groupType, final String sessionToken) {
        loading = ProgressDialog.show(context, null, "Fetching group data", false, false);

        final String requestTag = "fetch_group_txn_request";
        final String url = Config.FETCH_GROUP_DATA_URL;
        RequestQueue requestQueue = Volley.newRequestQueue(context);


        JsonObjectRequest fetchGroupTxnReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(final JSONObject response) {
                        Log.d("CONRES", String.valueOf(response));

                        System.out.println("::::::::::::::CONRES2" + response);
                        try {
                            switch (response.getString("status")) {
                                case "success":
                                    loading.setMessage("Synchronising");

                                    AppController.getInstance().getChamaPreferenceManager().setSignatoryRole(response.getString("signatory")); // 1/ 0
                                    AppController.getInstance().getChamaPreferenceManager().setSecretaryRole(response.getString("secretary")); // 1/ 0

                                    AppController.getInstance().getChamaPreferenceManager().setGroupType(Integer.valueOf(groupType));

                                    JSONArray membersArray = response.getJSONArray("members");
                                    addMembers(membersArray);

                                    JSONArray transactionsArray = response.getJSONArray("transactions");
                                    addTransactions(transactionsArray);

                                    if (response.has("approvals")){
                                        JSONObject approvalsObject = response.getJSONObject("approvals");

                                        JSONArray approvalTransactionsArray = approvalsObject.getJSONArray("transactionApproval");
                                        addApprovalTransactions(approvalTransactionsArray);

                                        JSONArray addApprovalSecretaries = approvalsObject.getJSONArray("assignSecretaryApproval");
                                        addApprovalSecretaries(addApprovalSecretaries);

                                        JSONArray addApprovalExits = approvalsObject.getJSONArray("memberExitApproval");
                                        addApprovalExits(addApprovalExits);

                                        JSONArray addApprovalGroupBalances = approvalsObject.getJSONArray("accountBalanceApproval");
                                        addApprovalGroupBalances(addApprovalGroupBalances);

                                        JSONArray addApprovalMinistatements = approvalsObject.getJSONArray("miniStatementApproval");
                                        addApprovalMinistatements(addApprovalMinistatements);

                                        JSONArray addApprovalFullStatements = approvalsObject.getJSONArray("fullStatementApproval");
                                        addApprovalFullStatements(addApprovalFullStatements);

                                        JSONArray addApprovalWithdraws = approvalsObject.getJSONArray("withdrawalApproval");
                                        addApprovalWithdraws(addApprovalWithdraws);

                                        JSONArray addApprovalMembersAdd = approvalsObject.getJSONArray("addMemberApproval");
                                        addApprovalMembersAdd(addApprovalMembersAdd);
                                    }

                                    initFirestoreMessage();

                                    break;

                                case "failed":
                                case "fatal":
                                default:
                                    System.out.println("::::::::::::::CONRES3" + response);
                                    loading.dismiss();
                                    Toasty.info(context, "An error occurred", Toast.LENGTH_LONG, true).show();

                                    break;
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            System.out.println("::::::::::::::CONRES4" + response);
                            loading.dismiss();
                            Toasty.info(context, "An error occurred", Toast.LENGTH_LONG, true).show();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                loading.dismiss();

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error")) {
                                    Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":

                                        Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();

                                        break;

                                    default:
                                        Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + context.getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } else {
                    if (error instanceof NoConnectionError) {
                        Toasty.info(context, "No connection to the server", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof TimeoutError) {
                        Toasty.info(context, "The request has timed out", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof AuthFailureError) {
                        Toasty.info(context, "Authentication failed", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof ServerError) {
                        Toasty.info(context, context.getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof NetworkError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof ParseError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else {
                        Toasty.info(context, context.getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    }
                }

            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionToken);

                return headers;
            }

        };

//        fetchGroupTxnReq.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));
        requestQueue.add(fetchGroupTxnReq);
    }

    private void addMembers(JSONArray membersArray) throws JSONException {
        if (membersArray.length() > 0) {

            for (int i = 0; i < membersArray.length(); i++) {
                MemberEntity memberEntity = new MemberEntity();

                JSONObject membersObject = membersArray.getJSONObject(i);

                memberEntity.setFirstName(membersObject.getString("firstName"));
                memberEntity.setLastName(membersObject.getString("lastName"));
                memberEntity.setPhoneNumber(membersObject.getString("memberPhoneNumber"));
                memberEntity.setIdNumber(membersObject.getString("idNumber"));
                memberEntity.setMakerPhoneNumber(membersObject.getString("makerPhoneNumber"));
                memberEntity.setDateRegistered(membersObject.getString("dateRegistered"));
                memberEntity.setSignatory(membersObject.getString("signatory"));
                memberEntity.setSecretary(membersObject.getString("secretary"));
                if (membersObject.getString("status").equals("1"))
                    memberEntity.setActive(true);
                else
                    memberEntity.setActive(false);

                memberViewModel.insertMember(memberEntity);

            }
        }
    }

    private void addTransactions(JSONArray transactionsArray) throws JSONException {
        if (transactionsArray.length() > 0) {

            for (int i = 0; i < transactionsArray.length(); i++) {
                TransactionEntity transactionEntity = new TransactionEntity();

                JSONObject transactionsObject = transactionsArray.getJSONObject(i);

                transactionEntity.setAmount(transactionsObject.getString("amount"));
                transactionEntity.setTxnID(transactionsObject.getString("txnID"));
                String txnDate = transactionsObject.getString("txnDate");
                transactionEntity.setTxnDate(txnDate);
                String truncDate = txnDate;

                try {
                    truncDate = Utility.getDate(txnDate, "yyyy-MM-dd");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                transactionEntity.setTxnTruncDate(truncDate);
                transactionEntity.setBatchNumber(transactionsObject.getString("batchNumber"));
                transactionEntity.setMemberPhoneNumber(transactionsObject.getString("memberPhoneNumber"));
                transactionEntity.setMakerPhoneNumber(transactionsObject.getString("makerPhoneNumber"));
                transactionEntity.setTxnType(transactionsObject.getString("txnType"));
                transactionEntity.setTxnCategory(transactionsObject.getString("txnCategory"));
                transactionEntity.setTxnAccount(transactionsObject.getString("txnAccount"));
                transactionEntity.setTxnNarration(transactionsObject.getString("txnNarration"));
                transactionEntity.setTxnStatus("Uploaded");
                transactionEntity.setTxnProcessingStatus(transactionsObject.getString("transactionStatus").toLowerCase());
                transactionEntity.setSessionTimeStamp(transactionsObject.getString("session_timestamp"));

                transactionViewModel.insertTransaction(transactionEntity);

            }
        }
    }

    private void addApprovalTransactions(JSONArray approvalTransactionsArray) throws JSONException {
        if (approvalTransactionsArray.length() > 0) {

            for (int i = 0; i < approvalTransactionsArray.length(); i++) {

                JSONObject approvalsObject = approvalTransactionsArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("transaction");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalTransactionEntity approvalTransactionEntity = new ApprovalTransactionEntity();

                approvalTransactionEntity.setAmount(approvalsObject.getString("amount"));
                approvalTransactionEntity.setTxnID(approvalsObject.getString("txnID"));
                approvalTransactionEntity.setTxnDate(approvalsObject.getString("txnDate"));
                approvalTransactionEntity.setTxnType(approvalsObject.getString("txnType"));
                approvalTransactionEntity.setBatchNumber(approvalsObject.getString("batchNumber"));
                approvalTransactionEntity.setMemberPhoneNumber(approvalsObject.getString("memberPhoneNumber"));
                approvalTransactionEntity.setMakerPhoneNumber(approvalsObject.getString("makerPhoneNumber"));
                approvalTransactionEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalTransactionEntity.setApprovalType("transaction");
                approvalTransactionEntity.setApprovalStatus("pending");
                approvalTransactionEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalTransactionEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalTransactionViewModel.insertApproval(approvalTransactionEntity);

            }
        }
    }

    private void addApprovalSecretaries(JSONArray approvalSecretariesArray) throws JSONException {
        if (approvalSecretariesArray.length() > 0) {

            for (int i = 0; i < approvalSecretariesArray.length(); i++) {

                JSONObject approvalsObject = approvalSecretariesArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("secretary");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalSecretaryEntity approvalSecretaryEntity = new ApprovalSecretaryEntity();

                approvalSecretaryEntity.setCurrentSecretaryPhoneNumber(approvalsObject.getString("currentSecretaryPhoneNumber"));
                approvalSecretaryEntity.setProposedSecretaryPhoneNumber(approvalsObject.getString("proposedSecretaryPhoneNumber"));
                approvalSecretaryEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalSecretaryEntity.setAssignDate(approvalsObject.getString("assignDate"));
                approvalSecretaryEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalSecretaryEntity.setApprovalType("secretary");
                approvalSecretaryEntity.setApprovalStatus("pending");
                approvalSecretaryEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalSecretaryEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalSecretaryViewModel.insertApprovalSecretary(approvalSecretaryEntity);

            }
        }
    }

    private void addApprovalExits(JSONArray approvalExitsArray) throws JSONException {
        if (approvalExitsArray.length() > 0) {

            for (int i = 0; i < approvalExitsArray.length(); i++) {

                JSONObject approvalsObject = approvalExitsArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("exit");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalExitEntity approvalExitEntity = new ApprovalExitEntity();

                approvalExitEntity.setMakerPhoneNumber(approvalsObject.getString("makerPhoneNumber"));
                approvalExitEntity.setMemberPhoneNumber(approvalsObject.getString("memberPhoneNumber"));
                approvalExitEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalExitEntity.setExitDate(approvalsObject.getString("exitDate"));
                approvalExitEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalExitEntity.setApprovalType("exit");
                approvalExitEntity.setApprovalStatus("pending");
                approvalExitEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalExitEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalExitViewModel.insertApprovalExit(approvalExitEntity);

            }
        }
    }

    private void addApprovalGroupBalances(JSONArray approvalGroupBalancesArray) throws JSONException {
        if (approvalGroupBalancesArray.length() > 0) {

            for (int i = 0; i < approvalGroupBalancesArray.length(); i++) {

                JSONObject approvalsObject = approvalGroupBalancesArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("group-balance");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalGroupBalanceEntity approvalGroupBalanceEntity = new ApprovalGroupBalanceEntity();

                approvalGroupBalanceEntity.setMemberPhoneNumber(approvalsObject.getString("memberPhoneNumber"));
                approvalGroupBalanceEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalGroupBalanceEntity.setRequestDate(approvalsObject.getString("transactionDate"));
                approvalGroupBalanceEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalGroupBalanceEntity.setApprovalType("group-balance");
                approvalGroupBalanceEntity.setApprovalStatus("pending");
                approvalGroupBalanceEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalGroupBalanceEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalGroupBalanceViewModel.insertApprovalGroupBalance(approvalGroupBalanceEntity);

            }
        }
    }

    private void addApprovalMinistatements(JSONArray approvalMinistatementsArray) throws JSONException {
        if (approvalMinistatementsArray.length() > 0) {

            for (int i = 0; i < approvalMinistatementsArray.length(); i++) {

                JSONObject approvalsObject = approvalMinistatementsArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("ministatement");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalMinistatementEntity approvalMinistatementEntity = new ApprovalMinistatementEntity();

                approvalMinistatementEntity.setMemberPhoneNumber(approvalsObject.getString("memberPhoneNumber"));
                approvalMinistatementEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalMinistatementEntity.setRequestDate(approvalsObject.getString("transactionDate"));
                approvalMinistatementEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalMinistatementEntity.setApprovalType("ministatement");
                approvalMinistatementEntity.setApprovalStatus("pending");
                approvalMinistatementEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalMinistatementEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalMinistatementViewModel.insertApprovalMinistatement(approvalMinistatementEntity);

            }
        }
    }

    private void addApprovalFullStatements(JSONArray approvalFullStatementsArray) throws JSONException {
        if (approvalFullStatementsArray.length() > 0) {

            for (int i = 0; i < approvalFullStatementsArray.length(); i++) {

                JSONObject approvalsObject = approvalFullStatementsArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("full-statement");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalFullStatementEntity approvalFullStatementEntity = new ApprovalFullStatementEntity();

                approvalFullStatementEntity.setMemberPhoneNumber(approvalsObject.getString("memberPhoneNumber"));
                approvalFullStatementEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalFullStatementEntity.setRequestDate(approvalsObject.getString("transactionDate"));
                approvalFullStatementEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalFullStatementEntity.setApprovalType("full-statement");
                approvalFullStatementEntity.setApprovalStatus("pending");
                approvalFullStatementEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalFullStatementEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalFullStatementViewModel.insertApprovalFullStatement(approvalFullStatementEntity);

            }
        }
    }

    private void addApprovalWithdraws(JSONArray approvalWithdrawsArray) throws JSONException {
        if (approvalWithdrawsArray.length() > 0) {

            // Clear persistent withdraws
            approvalViewModel.deleteAllApprovalsWhere("withdraw");
            approvalWithdrawViewModel.deleteAllApprovalWithdraws();

            for (int i = 0; i < approvalWithdrawsArray.length(); i++) {

                JSONObject approvalsObject = approvalWithdrawsArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("withdraw");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval withdraws
                ApprovalWithdrawEntity approvalWithdrawEntity = new ApprovalWithdrawEntity();

                approvalWithdrawEntity.setTxnID(approvalsObject.getString("txnID"));
                approvalWithdrawEntity.setAmount(approvalsObject.getString("withdrawalAmount"));
                approvalWithdrawEntity.setWithdrawDate(approvalsObject.getString("txnDate"));
                approvalWithdrawEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalWithdrawEntity.setMakerPhoneNumber(approvalsObject.getString("makerPhoneNumber"));
                approvalWithdrawEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalWithdrawEntity.setApprovalType("withdraw");
                approvalWithdrawEntity.setDestination(approvalsObject.getString("destinationAccount"));
                approvalWithdrawEntity.setApprovalStatus("pending");
                approvalWithdrawEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalWithdrawEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalWithdrawViewModel.insertApprovalWithdraw(approvalWithdrawEntity);

            }
        }
    }

    private void addApprovalMembersAdd(JSONArray addApprovalMembersAddArray) throws JSONException {
        if (addApprovalMembersAddArray.length() > 0) {

            // Clear persistent withdraws
            approvalViewModel.deleteAllApprovalsWhere("member-add");
            approvalMemberAddViewModel.deleteAllMemberAddApprovals();

            for (int i = 0; i < addApprovalMembersAddArray.length(); i++) {

                JSONObject approvalsObject = addApprovalMembersAddArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("member-add");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval member add
                ApprovalMemberAddEntity approvalMemberAddEntity = new ApprovalMemberAddEntity();

                approvalMemberAddEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalMemberAddEntity.setMakerPhonenumber(approvalsObject.getString("makerPhoneNumber"));
                approvalMemberAddEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalMemberAddEntity.setApprovalType("member-add");
                approvalMemberAddEntity.setApprovalStatus("pending");
                approvalMemberAddEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalMemberAddEntity.setApprovalDate(approvalsObject.getString("approvalDate"));

                approvalMemberAddViewModel.insertApprovalMemberAdd(approvalMemberAddEntity);

            }
        }
    }

    private void initFirestoreMessage() {
        final String groupNumber = AppController.getInstance().getChamaPreferenceManager().getGroupNumber();

        firebaseFirestore.collection("message")
                .whereEqualTo("groupNumber", groupNumber)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot querySnapshot) {
                        List<MessageEntity> messageEntities = querySnapshot.toObjects(MessageEntity.class);

                        if (messageEntities.size() > 0) {
                            for (MessageEntity messageEntity : messageEntities) {
                                if (messageEntity.getGroupNumber().equals(groupNumber)) {
                                    if (!messageViewModel.doesMessageExistWithUID(messageEntity.getUid()))
                                        messageViewModel.insertMessage(messageEntity);
                                    else
                                        messageViewModel.updateMessage(messageEntity);
                                }
                            }
                        }

                        loading.dismiss();
                        actionLogin();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toasty.info(context, "Messages could not be synced at the moment", Toast.LENGTH_SHORT, true).show();

                        loading.dismiss();
                        actionLogin();
                    }
                });
    }

    private void actionLogin() {
        Access.startSession();
        fireSync.registerMessageListener(messageViewModel);

        Intent i = new Intent(context, MainChamaActivity.class);
        context.startActivity(i);

        ((Activity) context).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void notifySessionExpired(String message) {
        Access.forceSessionExpiry();

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        boolean stateRemember = AppController.getInstance().getChamaPreferenceManager().getRememberPhone();
                        Access.logout((Activity) context, stateRemember, false);

                        Toasty.info(context, R.string.logout_message_post, Toast.LENGTH_SHORT, true).show();
                        break;
                }
            }
        };
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(false);
        builder.setTitle(R.string.notification);
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage(message);
        builder.setPositiveButton(R.string.sign_in, dialogClickListener);
        builder.show();
    }
}
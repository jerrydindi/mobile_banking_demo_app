package ke.co.payconnect.mocash.chama.view.activity.secretary.unstructured;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Connection;
import io.captano.utility.Money;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalExitEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalFullStatementEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalGroupBalanceEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalMemberAddEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalMinistatementEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalSecretaryEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalTransactionEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalWithdrawEntity;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;
import ke.co.payconnect.mocash.chama.data.entity.TransactionEntity;
import ke.co.payconnect.mocash.chama.util.Access;
import ke.co.payconnect.mocash.chama.util.Auth;
import ke.co.payconnect.mocash.chama.util.Config;
import ke.co.payconnect.mocash.chama.util.SendTo;
import ke.co.payconnect.mocash.chama.util.Utility;
import ke.co.payconnect.mocash.chama.view.adapter.secretary.MemberTransactionRecyclerViewAdapter;
import ke.co.payconnect.mocash.chama.view.fragment.secretary.unstructured.AddTransactionDialogFragment;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalExitViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalFullStatementViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalGroupBalanceViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalMemberAddViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalMinistatementViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalSecretaryViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalTransactionViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalWithdrawViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.TransactionViewModel;

import static ke.co.payconnect.mocash.chama.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.chama.util.Config.RETRIES;
import static ke.co.payconnect.mocash.chama.util.Config.TIMEOUT_MS;

public class AddTransactionActivity extends AppCompatActivity
        implements AddTransactionDialogFragment.AddTransactionListener,
        MemberTransactionRecyclerViewAdapter.AddTransactionListener {

    private final Context context = this;
    private final Activity activity = this;

    private Toolbar tbAddTxn;
    private TextView tvConnectivityStatusAddTxn;
    private TextView tvMemberAddTxn;
    private CoordinatorLayout cdlAddTxn;
    private RecyclerView rvAddTxn;
    private TextView tvNoDataAddTxn;
    private TextView tvTotalAddTxn;
    private CardView cvSubmitChamaAddTxn;

    private LinearLayoutManager layoutManager;
    public MemberTransactionRecyclerViewAdapter adapter;

    private MemberViewModel memberViewModel;
    private TransactionViewModel transactionViewModel;
    private ApprovalViewModel approvalViewModel;
    private ApprovalTransactionViewModel approvalTransactionViewModel;
    private ApprovalSecretaryViewModel approvalSecretaryViewModel;
    private ApprovalExitViewModel approvalExitViewModel;
    private ApprovalGroupBalanceViewModel approvalGroupBalanceViewModel;
    private ApprovalMinistatementViewModel approvalMinistatementViewModel;
    private ApprovalFullStatementViewModel approvalFullStatementViewModel;
    private ApprovalWithdrawViewModel approvalWithdrawViewModel;
    private ApprovalMemberAddViewModel approvalMemberAddViewModel;

    private boolean isSubmitted = false;
    public static List<TransactionEntity> transactionList;
    private String memberPhoneNumber;
    private double total = 0;
    private String batchNumber;
    private String groupNumber;
    private String customerType;
    private String txnCategory;

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if (isConnected)
                tvConnectivityStatusAddTxn.setVisibility(View.GONE);
            else
                tvConnectivityStatusAddTxn.setVisibility(View.VISIBLE);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_transaction);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        else
            registerReceiver(connectionReceiver, intentFilter);

        tvConnectivityStatusAddTxn = findViewById(R.id.tvConnectivityStatusAddTxn);
        tbAddTxn = findViewById(R.id.tbAddTxn);
        setSupportActionBar(tbAddTxn);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbAddTxn.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        tvMemberAddTxn = findViewById(R.id.tvMemberAddTxn);
        cdlAddTxn = findViewById(R.id.cdlAddTxn);
        rvAddTxn = findViewById(R.id.rvAddTxn);
        tvNoDataAddTxn = findViewById(R.id.tvNoDataGroupTxns);
        tvTotalAddTxn = findViewById(R.id.tvTotalAddTxn);
        cvSubmitChamaAddTxn = findViewById(R.id.cvSubmitChamaAddTxn);

        layoutManager = new LinearLayoutManager(context);

        memberViewModel = ViewModelProviders.of(this).get(MemberViewModel.class);
        transactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel.class);
        approvalViewModel = ViewModelProviders.of(this).get(ApprovalViewModel.class);
        approvalTransactionViewModel = ViewModelProviders.of(this).get(ApprovalTransactionViewModel.class);
        approvalTransactionViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalTransactionViewModel.class);
        approvalSecretaryViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalSecretaryViewModel.class);
        approvalExitViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalExitViewModel.class);
        approvalGroupBalanceViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalGroupBalanceViewModel.class);
        approvalMinistatementViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalMinistatementViewModel.class);
        approvalFullStatementViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalFullStatementViewModel.class);
        approvalWithdrawViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalWithdrawViewModel.class);
        approvalMemberAddViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalMemberAddViewModel.class);

        transactionList = new LinkedList<>();

        getHeaderData();

        setupRecyclerView();

        setListeners();

        toggleDataViews();

        setTotal();
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void getHeaderData() {
        Intent i = new Intent(getIntent());
        memberPhoneNumber = i.getStringExtra("selected_member_phone");
        txnCategory = i.getStringExtra("txn_category");
        groupNumber = i.getStringExtra("group_number");
        customerType = i.getStringExtra("customer_type");

        if (memberPhoneNumber != null) {
            setHeaderData(memberPhoneNumber);
        }

    }

    @SuppressLint("SetTextI18n")
    private void setHeaderData(String memberPhoneNumber) {
        MemberEntity memberEntity = memberViewModel.findMemberByPhoneNumber(memberPhoneNumber);
        tvMemberAddTxn.setText(memberEntity.getFirstName() + " " + memberEntity.getLastName());
    }

    private void setupRecyclerView() {
        if (transactionList != null) {
            adapter = new MemberTransactionRecyclerViewAdapter(context, transactionList);
            adapter.notifyDataSetChanged();

            rvAddTxn.setAdapter(adapter);
            rvAddTxn.setLayoutManager(layoutManager);
        }
    }

    private void setListeners() {
        cdlAddTxn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Auth.isAuthorized("secretary")) {
                    Toasty.info(context, R.string.action_not_permitted, Toast.LENGTH_SHORT, true).show();
                    return;
                }

                switch (customerType) {
                    case "group":
                        if (groupNumber != null) {
                            FragmentManager fm = ((FragmentActivity) context).getSupportFragmentManager();
                            AddTransactionDialogFragment dialogFragment = AddTransactionDialogFragment.newInstance(memberPhoneNumber, txnCategory, "group");
                            dialogFragment.show(fm, "dialog");
                        }
                        break;
                    case "member":
                        if (memberPhoneNumber != null) {
                            FragmentManager fm = ((FragmentActivity) context).getSupportFragmentManager();
                            AddTransactionDialogFragment dialogFragment = AddTransactionDialogFragment.newInstance(memberPhoneNumber, txnCategory, "member");
                            dialogFragment.show(fm, "dialog");
                        }
                        break;
                    default:
                        Toasty.info(context, "Validation failed. Invalid selection", Toast.LENGTH_SHORT, true).show();
                        break;
                }

            }
        });

        cvSubmitChamaAddTxn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Auth.isAuthorized("secretary")) {
                    Toasty.info(context, R.string.action_not_permitted, Toast.LENGTH_SHORT, true).show();
                    return;
                }

                if (tvConnectivityStatusAddTxn.getVisibility() == View.VISIBLE) {
                    Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                } else {
                    if (new Connection(context).IsInternetConnected()) {
                        if (!isSubmitted) {
                            addTransactionBatchNumber();

                            String phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();
                            int groupType = AppController.getInstance().getChamaPreferenceManager().getGroupType();
                            String groupNumber = AppController.getInstance().getChamaPreferenceManager().getGroupNumber();
                            String sessionToken = AppController.getInstance().getChamaPreferenceManager().getSessionToken();

                            String transactionListJSON = new Gson().toJson(transactionList);

                            HashMap<String, Object> map = new HashMap<>();
                            map.put("phone_number", phoneNumber);
                            map.put("group_type", String.valueOf(groupType));
                            map.put("group_number", groupNumber);
                            map.put("batch_number", batchNumber);
                            map.put("transactions", transactionListJSON);
                            map.put("tag", "TFIN");


                            processTransactions(map, sessionToken);


                        } else {
                            Toasty.info(context, R.string.already_submitted, Toast.LENGTH_SHORT, true).show();
                        }

                    } else {
                        Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                    }
                }

            }
        });

    }

    @SuppressLint("SetTextI18n")
    private void setTotal() {
        tvTotalAddTxn.setText("TOTAL: " + Money.format(total));
    }

    @Override
    public void onAddTransactionDialog(TransactionEntity transactionEntity) {
        transactionList.add(transactionEntity);
        adapter.notifyDataSetChanged();

        total = total + Double.parseDouble(transactionEntity.getAmount());
        setTotal();
    }

    @Override
    public void onAddTransactionTotalAdapter(double newTotal) {
        total = total - newTotal;
        setTotal();
    }

    @Override
    public void onAddTransactionListAdapter() {
        toggleDataViews();
    }

    private void toggleDataViews() {
        if (transactionList.size() == 0) {
            cvSubmitChamaAddTxn.setVisibility(View.INVISIBLE);
            cvSubmitChamaAddTxn.setEnabled(false);
            tvNoDataAddTxn.setVisibility(View.VISIBLE);
            tvTotalAddTxn.setVisibility(View.INVISIBLE);
        } else {
            cvSubmitChamaAddTxn.setVisibility(View.VISIBLE);
            cvSubmitChamaAddTxn.setEnabled(true);
            tvNoDataAddTxn.setVisibility(View.GONE);
            tvTotalAddTxn.setVisibility(View.VISIBLE);
        }
    }

    private void toggleCompleteView() {
        isSubmitted = true;
        cvSubmitChamaAddTxn.setVisibility(View.INVISIBLE);
        cvSubmitChamaAddTxn.setEnabled(false);
        cdlAddTxn.setVisibility(View.INVISIBLE);
        cdlAddTxn.setEnabled(false);
    }

    private void addTransactionBatchNumber() {
        batchNumber = Utility.getBatchNumber();
        for (TransactionEntity transactionEntity : transactionList) {
            transactionEntity.setBatchNumber(batchNumber);
        }
    }

    SendTo st = new SendTo(context);
    HurlStack hurlStack = new HurlStack() {
        @Override
        protected HttpURLConnection createConnection(URL url) throws IOException {
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
            try {
                httpsURLConnection.setSSLSocketFactory(st.getSSLSocketFactory());
                httpsURLConnection.setHostnameVerifier(st.getHostnameVerifier());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return httpsURLConnection;
        }
    };

    private void processTransactions(final HashMap<String, Object> map, final String sessionToken) {
        final ProgressDialog loading = ProgressDialog.show(this, null, "Processing transactions", false, false);

        final String requestTag = "chama_add_txn_request";
        final String url = Config.ADD_TRANSACTION;
        RequestQueue requestQueue = Volley.newRequestQueue(AddTransactionActivity.this);

        JsonObjectRequest addTxnReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(final JSONObject response) {
                        try {

                            loading.dismiss();

                            switch (response.getString("status")) {
                                case "success":
                                    loading.show();
                                    loading.setMessage("Please wait");

                                    updateTransactionStatus();
                                    adapter.notifyDataSetChanged();

                                    insertToDatabase();

                                    toggleCompleteView();

                                    loading.dismiss();

                                    String signatory = AppController.getInstance().getChamaPreferenceManager().getSignatoryRole();

                                    if(signatory.equals("1")) {
                                        HashMap<String, String> approvalMap = new HashMap<>();
                                        approvalMap.put("phone_number", String.valueOf(map.get("phone_number")));
                                        approvalMap.put("group_number", String.valueOf(map.get("group_number")));

                                        fetchApprovals(approvalMap, sessionToken);
                                    }

                                    break;

                                case "failed":
                                case "fatal":
                                default:

                                    break;
                            }

                        } catch (JSONException e) {
                            loading.dismiss();
                            Toasty.info(context, "An error occurred", Toast.LENGTH_LONG, true).show();

                        }
                    }


                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                loading.dismiss();

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error")) {
                                    Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":

                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();

                                        break;

                                    default:
                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } else {
                    if (error instanceof NoConnectionError) {
                        Toasty.info(context, "No connection to the server", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof TimeoutError) {
                        Toasty.info(context, "The request has timed out", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof AuthFailureError) {
                        Toasty.info(context, "Authentication failed", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof ServerError) {
                        Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof NetworkError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof ParseError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else {
                        Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    }
                }

            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionToken);

                return headers;
            }

        };

        addTxnReq.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));
        requestQueue.add(addTxnReq);
    }

    private void updateTransactionStatus() {
        for (TransactionEntity transactionEntity : transactionList) {
            transactionEntity.setTxnStatus("Uploaded");
        }
    }

    private void insertToDatabase() {
        for (TransactionEntity transactionEntity : transactionList) {
            transactionViewModel.insertTransaction(transactionEntity);
        }

    }

    private void fetchApprovals(final HashMap<String, String> map, final String sessionToken) {
        final ProgressDialog loading = ProgressDialog.show(context, null, "Synchronising approvals", false, false);

        final String requestTag = "fetch_approvals_post_txn_request";
        final String url = Config.FETCH_GROUP_DATA_URL;
        RequestQueue requestQueue = Volley.newRequestQueue(AddTransactionActivity.this);

        JsonObjectRequest fetchApprovalsReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(final JSONObject response) {
                        try {
                            switch (response.getString("status")) {
                                case "success":
                                    approvalViewModel.deleteAllApprovalsWhere("assignSecretary");
                                    approvalViewModel.deleteAllApprovalsWhere("assign-secretary");
                                    approvalSecretaryViewModel.deleteAllSecretaries();
                                    approvalViewModel.deleteAllApprovalsWhere("memberExit");
                                    approvalViewModel.deleteAllApprovalsWhere("exit");
                                    approvalExitViewModel.deleteAllExits();
                                    approvalViewModel.deleteAllApprovalsWhere("groupBalance");
                                    approvalViewModel.deleteAllApprovalsWhere("group-balance");
                                    approvalGroupBalanceViewModel.deleteAllGroupBalances();
                                    approvalViewModel.deleteAllApprovalsWhere("ministatement");
                                    approvalMinistatementViewModel.deleteAllMinistatements();
                                    approvalViewModel.deleteAllApprovalsWhere("full-statement");
                                    approvalFullStatementViewModel.deleteAllFullStatements();
                                    approvalViewModel.deleteAllApprovalsWhere("withdraw");
                                    approvalWithdrawViewModel.deleteAllApprovalWithdraws();
                                    approvalViewModel.deleteAllApprovalsWhere("member-add");
                                    approvalMemberAddViewModel.deleteAllMemberAddApprovals();

                                    JSONObject approvalsObject = response.getJSONObject("approvals");
                                    JSONArray approvalTransactionsArray = approvalsObject.getJSONArray("transactionApproval");
                                    addApprovalTransactions(approvalTransactionsArray);

                                    JSONArray addApprovalSecretaries = approvalsObject.getJSONArray("assignSecretaryApproval");
                                    addApprovalSecretaries(addApprovalSecretaries);

                                    JSONArray addApprovalExits = approvalsObject.getJSONArray("memberExitApproval");
                                    addApprovalExits(addApprovalExits);

                                    int groupType = AppController.getInstance().getChamaPreferenceManager().getGroupType();
                                    if (groupType == 1) {
                                        JSONArray addApprovalGroupBalances = approvalsObject.getJSONArray("accountBalanceApproval");
                                        addApprovalGroupBalances(addApprovalGroupBalances);

                                        JSONArray addApprovalMinistatements = approvalsObject.getJSONArray("miniStatementApproval");
                                        addApprovalMinistatements(addApprovalMinistatements);

                                        JSONArray addApprovalFullStatements = approvalsObject.getJSONArray("fullStatementApproval");
                                        addApprovalFullStatements(addApprovalFullStatements);

                                        JSONArray addApprovalWithdraws = approvalsObject.getJSONArray("withdrawalApproval");
                                        addApprovalWithdraws(addApprovalWithdraws);
                                    }

                                    JSONArray addApprovalMembersAdd = approvalsObject.getJSONArray("addMemberApproval");
                                    addApprovalMembersAdd(addApprovalMembersAdd);

                                    break;

                                case "failed":
                                case "fatal":
                                default:
                                    Toasty.info(context, "Failed to synchronise approvals", Toast.LENGTH_LONG, true).show();

                                    break;
                            }

                        } catch (JSONException e) {
                            Toasty.info(context, "Failed to synchronise approvals", Toast.LENGTH_LONG, true).show();

                        } finally {
                            loading.dismiss();
                            finish();

                            overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                loading.dismiss();

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error")) {
                                    Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":
                                        Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();

                                        break;

                                    default:
                                        Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + context.getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } else {
                    if (error instanceof NoConnectionError) {
                        Toasty.info(context, "No connection to the server", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof TimeoutError) {
                        Toasty.info(context, "The request has timed out", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof AuthFailureError) {
                        Toasty.info(context, "Authentication failed", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof ServerError) {
                        Toasty.info(context, context.getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof NetworkError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof ParseError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else {
                        Toasty.info(context, context.getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    }
                }

            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionToken);

                return headers;
            }

        };

        fetchApprovalsReq.setRetryPolicy(new DefaultRetryPolicy(20000, RETRIES, BACKOFF_MULTIPLIER));
        requestQueue.add(fetchApprovalsReq);
    }

    private void addApprovalTransactions(JSONArray approvalTransactionsArray) throws JSONException {
        if (approvalTransactionsArray.length() > 0) {

            // Clear persistent transactions
            approvalViewModel.deleteAllApprovalsWhere("transaction");
            approvalTransactionViewModel.deleteAllApprovalTransactions();

            for (int i = 0; i < approvalTransactionsArray.length(); i++) {

                JSONObject approvalsObject = approvalTransactionsArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("transaction");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalTransactionEntity approvalTransactionEntity = new ApprovalTransactionEntity();

                approvalTransactionEntity.setAmount(approvalsObject.getString("amount"));
                approvalTransactionEntity.setTxnID(approvalsObject.getString("txnID"));
                approvalTransactionEntity.setTxnDate(approvalsObject.getString("txnDate"));
                approvalTransactionEntity.setTxnType(approvalsObject.getString("txnType"));
                approvalTransactionEntity.setBatchNumber(approvalsObject.getString("batchNumber"));
                approvalTransactionEntity.setMemberPhoneNumber(approvalsObject.getString("memberPhoneNumber"));
                approvalTransactionEntity.setMakerPhoneNumber(approvalsObject.getString("makerPhoneNumber"));
                approvalTransactionEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalTransactionEntity.setApprovalType("transaction");
                approvalTransactionEntity.setApprovalStatus("pending");
                approvalTransactionEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalTransactionEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalTransactionViewModel.insertApproval(approvalTransactionEntity);

            }
        }
    }

    private void addApprovalSecretaries(JSONArray approvalSecretariesArray) throws JSONException {
        if (approvalSecretariesArray.length() > 0) {

            // Clear persistent transactions
            approvalViewModel.deleteAllApprovalsWhere("assignSecretary");
            approvalSecretaryViewModel.deleteAllSecretaries();

            for (int i = 0; i < approvalSecretariesArray.length(); i++) {

                JSONObject approvalsObject = approvalSecretariesArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("secretary");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalSecretaryEntity approvalSecretaryEntity = new ApprovalSecretaryEntity();

                approvalSecretaryEntity.setCurrentSecretaryPhoneNumber(approvalsObject.getString("currentSecretaryPhoneNumber"));
                approvalSecretaryEntity.setProposedSecretaryPhoneNumber(approvalsObject.getString("proposedSecretaryPhoneNumber"));
                approvalSecretaryEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalSecretaryEntity.setAssignDate(approvalsObject.getString("assignDate"));
                approvalSecretaryEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalSecretaryEntity.setApprovalType("secretary");
                approvalSecretaryEntity.setApprovalStatus("pending");
                approvalSecretaryEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalSecretaryEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalSecretaryViewModel.insertApprovalSecretary(approvalSecretaryEntity);

            }
        }
    }

    private void addApprovalExits(JSONArray approvalExitssArray) throws JSONException {
        if (approvalExitssArray.length() > 0) {

            // Clear persistent transactions
            approvalViewModel.deleteAllApprovalsWhere("memberExit");
            approvalExitViewModel.deleteAllExits();

            for (int i = 0; i < approvalExitssArray.length(); i++) {

                JSONObject approvalsObject = approvalExitssArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("exit");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalExitEntity approvalExitEntity = new ApprovalExitEntity();

                approvalExitEntity.setMakerPhoneNumber(approvalsObject.getString("makerPhoneNumber"));
                approvalExitEntity.setMemberPhoneNumber(approvalsObject.getString("memberPhoneNumber"));
                approvalExitEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalExitEntity.setExitDate(approvalsObject.getString("exitDate"));
                approvalExitEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalExitEntity.setApprovalType("exit");
                approvalExitEntity.setApprovalStatus("pending");
                approvalExitEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalExitEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalExitViewModel.insertApprovalExit(approvalExitEntity);

            }
        }
    }

    private void addApprovalGroupBalances(JSONArray approvalGroupBalancesArray) throws JSONException {
        if (approvalGroupBalancesArray.length() > 0) {

            for (int i = 0; i < approvalGroupBalancesArray.length(); i++) {

                JSONObject approvalsObject = approvalGroupBalancesArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("group-balance");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalGroupBalanceEntity approvalGroupBalanceEntity = new ApprovalGroupBalanceEntity();

                approvalGroupBalanceEntity.setMemberPhoneNumber(approvalsObject.getString("memberPhoneNumber"));
                approvalGroupBalanceEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalGroupBalanceEntity.setRequestDate(approvalsObject.getString("transactionDate"));
                approvalGroupBalanceEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalGroupBalanceEntity.setApprovalType("group-balance");
                approvalGroupBalanceEntity.setApprovalStatus("pending");
                approvalGroupBalanceEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalGroupBalanceEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalGroupBalanceViewModel.insertApprovalGroupBalance(approvalGroupBalanceEntity);

            }
        }
    }

    private void addApprovalMinistatements(JSONArray approvalMinistatementsArray) throws JSONException {
        if (approvalMinistatementsArray.length() > 0) {

            for (int i = 0; i < approvalMinistatementsArray.length(); i++) {

                JSONObject approvalsObject = approvalMinistatementsArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("ministatement");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalMinistatementEntity approvalMinistatementEntity = new ApprovalMinistatementEntity();

                approvalMinistatementEntity.setMemberPhoneNumber(approvalsObject.getString("memberPhoneNumber"));
                approvalMinistatementEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalMinistatementEntity.setRequestDate(approvalsObject.getString("transactionDate"));
                approvalMinistatementEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalMinistatementEntity.setApprovalType("ministatement");
                approvalMinistatementEntity.setApprovalStatus("pending");
                approvalMinistatementEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalMinistatementEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalMinistatementViewModel.insertApprovalMinistatement(approvalMinistatementEntity);

            }
        }
    }

    private void addApprovalFullStatements(JSONArray approvalFullStatementsArray) throws JSONException {
        if (approvalFullStatementsArray.length() > 0) {

            for (int i = 0; i < approvalFullStatementsArray.length(); i++) {

                JSONObject approvalsObject = approvalFullStatementsArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("full-statement");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalFullStatementEntity approvalFullStatementEntity = new ApprovalFullStatementEntity();

                approvalFullStatementEntity.setMemberPhoneNumber(approvalsObject.getString("memberPhoneNumber"));
                approvalFullStatementEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalFullStatementEntity.setRequestDate(approvalsObject.getString("transactionDate"));
                approvalFullStatementEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalFullStatementEntity.setApprovalType("full-statement");
                approvalFullStatementEntity.setApprovalStatus("pending");
                approvalFullStatementEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalFullStatementEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalFullStatementViewModel.insertApprovalFullStatement(approvalFullStatementEntity);

            }
        }
    }

    private void addApprovalWithdraws(JSONArray approvalWithdrawsArray) throws JSONException {
        if (approvalWithdrawsArray.length() > 0) {

            // Clear persistent withdraws
            approvalViewModel.deleteAllApprovalsWhere("withdraw");
            approvalWithdrawViewModel.deleteAllApprovalWithdraws();

            for (int i = 0; i < approvalWithdrawsArray.length(); i++) {

                JSONObject approvalsObject = approvalWithdrawsArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("withdraw");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval withdraws
                ApprovalWithdrawEntity approvalWithdrawEntity = new ApprovalWithdrawEntity();

                approvalWithdrawEntity.setTxnID(approvalsObject.getString("txnID"));
                approvalWithdrawEntity.setAmount(approvalsObject.getString("withdrawalAmount"));
                approvalWithdrawEntity.setWithdrawDate(approvalsObject.getString("txnDate"));
                approvalWithdrawEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalWithdrawEntity.setMakerPhoneNumber(approvalsObject.getString("makerPhoneNumber"));
                approvalWithdrawEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalWithdrawEntity.setApprovalType("withdraw");
                approvalWithdrawEntity.setDestination(approvalsObject.getString("destinationAccount"));
                approvalWithdrawEntity.setApprovalStatus("pending");
                approvalWithdrawEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalWithdrawEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalWithdrawViewModel.insertApprovalWithdraw(approvalWithdrawEntity);

            }
        }
    }

    private void addApprovalMembersAdd(JSONArray addApprovalMembersAddArray) throws JSONException {
        if (addApprovalMembersAddArray.length() > 0) {

            // Clear persistent withdraws
            approvalViewModel.deleteAllApprovalsWhere("member-add");
            approvalWithdrawViewModel.deleteAllApprovalWithdraws();

            for (int i = 0; i < addApprovalMembersAddArray.length(); i++) {

                JSONObject approvalsObject = addApprovalMembersAddArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("member-add");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval member add
                ApprovalMemberAddEntity approvalMemberAddEntity = new ApprovalMemberAddEntity();

                approvalMemberAddEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalMemberAddEntity.setMakerPhonenumber(approvalsObject.getString("makerPhoneNumber"));
                approvalMemberAddEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalMemberAddEntity.setApprovalType("withdraw");
                approvalMemberAddEntity.setApprovalStatus("pending");
                approvalMemberAddEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalMemberAddEntity.setApprovalDate(approvalsObject.getString("approvalDate"));

                approvalMemberAddViewModel.insertApprovalMemberAdd(approvalMemberAddEntity);

            }
        }
    }
}

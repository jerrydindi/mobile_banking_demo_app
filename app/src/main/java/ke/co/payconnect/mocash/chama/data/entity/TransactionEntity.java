package ke.co.payconnect.mocash.chama.data.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity
public class TransactionEntity {
    @PrimaryKey(autoGenerate = true)
    private int uid;

    @ColumnInfo(name = "member_phone_number")
    @SerializedName(value = "memberPhoneNumber")
    private String memberPhoneNumber;

    @ColumnInfo(name = "amount")
    @SerializedName(value = "amount")
    private String amount;

    @ColumnInfo(name = "txn_id")
    @SerializedName(value = "txnId")
    private String txnID;

    @ColumnInfo(name = "batch_number")
    @SerializedName(value = "batchNumber")
    private String batchNumber;

    @ColumnInfo(name = "maker_phone_number")
    @SerializedName(value = "makerPhoneNumber")
    private String makerPhoneNumber;

    @ColumnInfo(name = "txn_date")
    @SerializedName(value = "txnDate")
    private String txnDate;

    @ColumnInfo(name = "txn_trunc_date")
    private transient String txnTruncDate;
    @ColumnInfo(name = "txn_type")
    @SerializedName(value = "txnType")
    private String txnType;

    @ColumnInfo(name = "txn_category")
    @SerializedName(value = "txnCategory")
    private String txnCategory;

    @ColumnInfo(name = "txn_account")
    @SerializedName(value = "destinationAccount")
    private String txnAccount;

    @ColumnInfo(name = "txn_narration")
    @SerializedName(value = "txnNarration")
    private String txnNarration;

    @ColumnInfo(name = "txn_status")
    private transient String txnStatus;

    @ColumnInfo(name = "txn_processing_status")
    private transient String txnProcessingStatus;

    @ColumnInfo(name = "session_time_stamp")
    private transient String sessionTimeStamp;


    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getMemberPhoneNumber() {
        return memberPhoneNumber;
    }

    public void setMemberPhoneNumber(String memberPhoneNumber) {
        this.memberPhoneNumber = memberPhoneNumber;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTxnID() {
        return txnID;
    }

    public void setTxnID(String txnID) {
        this.txnID = txnID;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getMakerPhoneNumber() {
        return makerPhoneNumber;
    }

    public void setMakerPhoneNumber(String makerPhoneNumber) {
        this.makerPhoneNumber = makerPhoneNumber;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    public String getTxnTruncDate() {
        return txnTruncDate;
    }

    public void setTxnTruncDate(String txnTruncDate) {
        this.txnTruncDate = txnTruncDate;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getTxnCategory() {
        return txnCategory;
    }

    public void setTxnCategory(String txnCategory) {
        this.txnCategory = txnCategory;
    }

    public String getTxnAccount() {
        return txnAccount;
    }

    public void setTxnAccount(String txnAccount) {
        this.txnAccount = txnAccount;
    }

    public String getTxnNarration() {
        return txnNarration;
    }

    public void setTxnNarration(String txnNarration) {
        this.txnNarration = txnNarration;
    }

    public String getTxnStatus() {
        return txnStatus;
    }

    public void setTxnStatus(String txnStatus) {
        this.txnStatus = txnStatus;
    }

    public String getTxnProcessingStatus() {
        return txnProcessingStatus;
    }

    public void setTxnProcessingStatus(String txnProcessingStatus) {
        this.txnProcessingStatus = txnProcessingStatus;
    }

    public String getSessionTimeStamp() {
        return sessionTimeStamp;
    }

    public void setSessionTimeStamp(String sessionTimeStamp) {
        this.sessionTimeStamp = sessionTimeStamp;
    }
}

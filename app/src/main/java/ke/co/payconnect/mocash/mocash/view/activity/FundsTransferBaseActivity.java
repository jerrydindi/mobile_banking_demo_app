package ke.co.payconnect.mocash.mocash.view.activity;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.mocash.util.Access;
import ke.co.payconnect.mocash.mocash.view.adapter.ViewPagerAdapter;
import ke.co.payconnect.mocash.mocash.view.fragment.FundsTransferOtherFragment;
import ke.co.payconnect.mocash.mocash.view.fragment.FundsTransferOtherFragment.OnFundsTransferOtherInteractionListener;
import ke.co.payconnect.mocash.mocash.view.fragment.FundsTransferOwnFragment;
import ke.co.payconnect.mocash.mocash.view.fragment.FundsTransferOwnFragment.OnFundsTransferOwnInteractionListener;

public class FundsTransferBaseActivity extends AppCompatActivity
        implements
        OnFundsTransferOwnInteractionListener,
        OnFundsTransferOtherInteractionListener {
    private final Activity activity = this;
    private final Context context = this;

    // Views
    private Toolbar toolbar;
    private ViewPager viewPager;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_funds_transfer_base);

        toolbar = findViewById(R.id.tbFTB);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        viewPager = findViewById(R.id.vpFTB);
        setupViewPager(viewPager);

        // Set Tabs inside Toolbar
        tabLayout = findViewById(R.id.tlFTB);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    // Add fragments to tabs
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new FundsTransferOwnFragment(), "My Accounts");
        viewPagerAdapter.addFragment(new FundsTransferOtherFragment(), "Other Accounts");

        viewPager.setAdapter(viewPagerAdapter);
    }

    @Override
    public void onFundsTransferOtherInteraction(Uri uri) {

    }

    @Override
    public void onFundsTransferOwnInteraction(Uri uri) {

    }
}

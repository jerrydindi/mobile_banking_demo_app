package ke.co.payconnect.mocash.chama.viewmodel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.ChamaDatabase;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalEntity;

public class ApprovalViewModel extends AndroidViewModel {

    private final LiveData<List<ApprovalEntity>> approvalList;
    private ChamaDatabase db;

    public ApprovalViewModel(@NonNull Application application) {
        super(application);

        db = ChamaDatabase.getAppDatabaseInstance(this.getApplication());
        approvalList = db.approvalDao().getApprovalsLive("pending");
    }

    public LiveData<List<ApprovalEntity>> getApprovalsLive() {
        return approvalList;
    }

    public LiveData<List<ApprovalEntity>> getSummaryApprovalsLive(){
        return db.approvalDao().getSummaryApprovalsLive("pending");
    }

    public List<ApprovalEntity> getTypeApprovals(String approvalType){
        return db.approvalDao().getTypeApprovals("pending", approvalType);
    }

    public boolean doesApprovalExistWithApprovalId(String approvalId){
        return db.approvalDao().doesApprovalExistWithApprovalId(approvalId);
    }

    public void insertApproval(ApprovalEntity approvalEntity) {
        db.approvalDao().insertApproval(approvalEntity);
    }

    public void updateApproval(ApprovalEntity approvalEntity) {
        db.approvalDao().updateApproval(approvalEntity);
    }

    public void deleteApproval(ApprovalEntity approvalEntity) {
        db.approvalDao().deleteApproval(approvalEntity);
    }

    public void deleteAllApprovals() {
        db.approvalDao().deleteAllApprovals();
    }

    public void deleteAllApprovalsWhere(String approvalType) {
        db.approvalDao().deleteAllApprovalsWhere(approvalType);
    }
}

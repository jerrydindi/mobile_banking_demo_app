package ke.co.payconnect.mocash.chama.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.ChamaDatabase;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalExitEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalSecretaryEntity;

public class ApprovalSecretaryViewModel extends AndroidViewModel {

    private final LiveData<List<ApprovalSecretaryEntity>> approvalSecretaryList;
    private ChamaDatabase db;

    public ApprovalSecretaryViewModel(@NonNull Application application) {
        super(application);

        db = ChamaDatabase.getAppDatabaseInstance(this.getApplication());
        approvalSecretaryList = db.approvalSecretaryDao().getApprovalSecretariesLive();
    }

    public LiveData<List<ApprovalSecretaryEntity>> getApprovalSecretariesLive() {
        return approvalSecretaryList;
    }

    public LiveData<List<ApprovalSecretaryEntity>> getApprovalSecretariesByApprovalIDLive(String approvalID) {
        return  db.approvalSecretaryDao().getApprovalSecretariesByApprovalIDLive(approvalID);
    }

    public  LiveData<List<ApprovalSecretaryEntity>> getApprovalSecretariesByTypeLive(String approvalType) {
        return  db.approvalSecretaryDao().getApprovalSecretariesByTypeLive(approvalType);
    }

    public ApprovalSecretaryEntity findSecretaryApprovalById(String approvalId){
        return db.approvalSecretaryDao().findSecretaryApprovalById(approvalId);
    }

    public boolean doesSecretaryApprovalExistWithApprovalId(String approvalId){
        return db.approvalSecretaryDao().doesSecretaryApprovalExistWithApprovalId(approvalId);
    }

    public  void updateApprovalSecretarySetData(String approvalID, String approvalStatus, String approvalNarration, String approvalDate) {
        db.approvalSecretaryDao().updateApprovalSecretarySetData(approvalID, approvalStatus, approvalNarration, approvalDate);
    }

    public void insertApprovalSecretary(ApprovalSecretaryEntity approvalTransactionEntity) {
        db.approvalSecretaryDao().insertApprovalSecretary(approvalTransactionEntity);
    }

    public void updateApprovalSecretary(ApprovalSecretaryEntity approvalTransactionEntity) {
        db.approvalSecretaryDao().updateApprovalSecretary(approvalTransactionEntity);
    }

    public void deleteApprovalSecretary(ApprovalSecretaryEntity approvalTransactionEntity) {
        db.approvalSecretaryDao().deleteApprovalSecretary(approvalTransactionEntity);
    }

    public void deleteAllSecretaries() {
        db.approvalSecretaryDao().deleteAllApprovalSecretaries();
    }
}

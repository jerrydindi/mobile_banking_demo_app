package ke.co.payconnect.mocash.chama.view.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Connection;
import io.captano.utility.ui.UIController;
import io.captano.utility.ui.UIHandler;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.util.Access;
import ke.co.payconnect.mocash.chama.util.Config;
import ke.co.payconnect.mocash.chama.util.SendTo;

import javax.net.ssl.HttpsURLConnection;

import static ke.co.payconnect.mocash.chama.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.chama.util.Config.RETRIES;
import static ke.co.payconnect.mocash.chama.util.Config.TIMEOUT_MS;


public class LoginNewChamaActivity extends AppCompatActivity {
    private final Context context = this;
    private final Activity activity = this;

    // Views
    private EditText etOtp;
    private TextView tvOTPNotReceived;
    private TextView tvOTPReceived;
    private TextView tvPhoneSetNewPin;
    private EditText etPinOld;
    private EditText etPinNew;
    private EditText etPinNewConfirm;
    private Button btResendOTP;
    private Button btLoginSetNewPin;
    private LinearLayout llDidntreceive;
    private TextView tvConnectivityStatusLoginNew;

    private String phoneNumber;

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if (isConnected)
                tvConnectivityStatusLoginNew.setVisibility(View.GONE);
            else
                tvConnectivityStatusLoginNew.setVisibility(View.VISIBLE);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_new);

        if (Build.VERSION.SDK_INT >= 21)
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        } else {
            registerReceiver(connectionReceiver, intentFilter);
        }


        etOtp = findViewById(R.id.etOtp);
        tvOTPNotReceived = findViewById(R.id.tvOTPNotReceived);
        tvPhoneSetNewPin = findViewById(R.id.tvPhoneSetNewPin);
        etPinOld = findViewById(R.id.etPinCurrent);
        etPinNew = findViewById(R.id.etPinNew);
        etPinNewConfirm = findViewById(R.id.etPinNewConfirm);
        btResendOTP = findViewById(R.id.btResendOTP);
        btLoginSetNewPin = findViewById(R.id.btLoginSetNewPin);
        llDidntreceive = findViewById(R.id.llDidntreceive);
        tvConnectivityStatusLoginNew = findViewById(R.id.tvConnectivityStatusLoginNew);

        phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();

        setListeners();

        if (phoneNumber != null)
            tvPhoneSetNewPin.setText("0" + AppController.getInstance().getChamaPreferenceManager().getPhoneNumber().substring(3));

        Toasty.info(context, getString(R.string.change_pin_prompt), Toast.LENGTH_SHORT, true).show();
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        ke.co.payconnect.mocash.mocash.util.Access.sessionStatusCheck(activity);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void setListeners() {
        btResendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        llDidntreceive.setVisibility(View.GONE);
                    }
                }, 3000);

                btResendOTP.setEnabled(false);
                btResendOTP.setText(getString(R.string.resending));
                btResendOTP.setAllCaps(false);
                btResendOTP.setTextColor(getResources().getColor(R.color.cG2));

            }
        });

        btLoginSetNewPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validation(etOtp.getText().toString(), etPinNew.getText().toString(), etPinNewConfirm.getText().toString());
            }
        });

        tvOTPNotReceived.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toasty.info(context, "Not available", Toast.LENGTH_SHORT, true).show();
            }
        });
    }

    private void validation(final String otp, final String newPin, String newPinConfirm) {
        if (otp == null || otp.isEmpty()) {
            Toasty.info(context, "Please enter a valid pin", Toast.LENGTH_SHORT, true).show();

            etPinOld.getText().clear();
            etPinNew.getText().clear();
            etPinNewConfirm.getText().clear();
            return;
        } else {

            // New pin validation
            if (newPin == null || newPin.isEmpty()) {
                Toasty.info(context, "Please enter a valid pin", Toast.LENGTH_SHORT, true).show();

                etPinOld.getText().clear();
                etPinNew.getText().clear();
                etPinNewConfirm.getText().clear();
                return;
            } else {
                if (!(newPin.length() == 4)) {
                    Toasty.info(context, "Please enter a valid new pin", Toast.LENGTH_SHORT, true).show();

                    etPinOld.getText().clear();
                    etPinNew.getText().clear();
                    etPinNewConfirm.getText().clear();
                    return;
                }
            }

            // New pin confirmation validation
            if (newPinConfirm == null || newPinConfirm.isEmpty()) {
                Toasty.info(context, "Please enter a valid new pin confirmation", Toast.LENGTH_SHORT, true).show();

                etPinOld.getText().clear();
                etPinNew.getText().clear();
                etPinNewConfirm.getText().clear();
                return;
            } else {
                if (!(newPinConfirm.length() == 4)) {
                    Toasty.info(context, "Please enter a valid new pin confirmation", Toast.LENGTH_SHORT, true).show();

                    etPinOld.getText().clear();
                    etPinNew.getText().clear();
                    etPinNewConfirm.getText().clear();
                    return;
                }
            }

            // New pin & confirmation pin validation
            if (!newPin.equals(newPinConfirm)) {
                Toasty.info(context, "Your new pin does not match the confirmation entry", Toast.LENGTH_SHORT, true).show();

                etPinOld.getText().clear();
                etPinNew.getText().clear();
                etPinNewConfirm.getText().clear();
                return;
            } else {
                if (newPin.trim().length() != 4) {
                    Toasty.info(context, "Please enter a valid pin", Toast.LENGTH_SHORT, true).show();

                    etPinOld.getText().clear();
                    etPinNew.getText().clear();
                    etPinNewConfirm.getText().clear();
                    return;
                }
            }
        }

        // Validate current pin. Final step
        if (phoneNumber == null) {
            etPinOld.getText().clear();
            etPinNew.getText().clear();
            etPinNewConfirm.getText().clear();

            // user not found
            exitAlert();
            return;
        }

        String savedPin = AppController.getInstance().getChamaPreferenceManager().getPin();
        if (savedPin != null) {
            if (!savedPin.equals(otp)) {
                Toasty.info(context, "Please enter a valid pin", Toast.LENGTH_SHORT, true).show();

                etPinOld.getText().clear();
                etPinNew.getText().clear();
                etPinNewConfirm.getText().clear();

                return;
            }
        } else {
            etPinOld.getText().clear();
            etPinNew.getText().clear();
            etPinNewConfirm.getText().clear();

            // Pin not found
            exitAlert();
            return;
        }


        // Internet connectivity check
        if (tvConnectivityStatusLoginNew.getVisibility() == View.VISIBLE) {
            Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
        } else {
            if (new Connection(context).IsInternetConnected())
                processRequest(otp, newPin);

            else
                Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
        }

    }

    private void exitAlert() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        new UIController().redirect(activity, LoginChamaActivity.class);
                        break;

                }

            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.confirm_action));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.proceed), dialogClickListener);
        builder.setMessage("Your session has timed out. Kindly sign-in again to be able to change your pin");
        builder.show();

    }

    private void responseAlert(String message) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        new UIController().redirect(activity, LoginChamaActivity.class);
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.ok), dialogClickListener);
        builder.show();
    }

    private void notifySessionExpired(String message) {
        Access.forceSessionExpiry();

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        boolean stateRemember = AppController.getInstance().getChamaPreferenceManager().getRememberPhone();
                        Access.logout(activity, stateRemember, false);

                        Toasty.info(context, R.string.logout_message_post, Toast.LENGTH_SHORT, true).show();
                        break;
                }
            }
        };
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(false);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.sign_in), dialogClickListener);
        builder.show();
    }

    SendTo st = new SendTo(context);
    HurlStack hurlStack = new HurlStack() {
        @Override
        protected HttpURLConnection createConnection(URL url) throws IOException {
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
            try {
                httpsURLConnection.setSSLSocketFactory(st.getSSLSocketFactory());
                httpsURLConnection.setHostnameVerifier(st.getHostnameVerifier());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return httpsURLConnection;
        }
    };

    private void processRequest(String oldPin, String newPin) {

        final ProgressDialog loading = ProgressDialog.show(this, null, "Processing request...", false, false);

        final String sessionToken = AppController.getInstance().getChamaPreferenceManager().getSessionToken();

        String requestTag = "change_pin_new_request";
        String url = Config.CHANGE_PIN_URL;

        Map<String, String> map = new HashMap<>();
        map.put("phone_number", phoneNumber);
        map.put("account", phoneNumber);
        map.put("current_pin", oldPin);
        map.put("new_pin", newPin);
        RequestQueue requestQueue = Volley.newRequestQueue(context);

        JsonObjectRequest changePinReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        System.out.println("::::::::::::::::::::::::::::CHANGEPINRES:::::::::::::::" + response);
                        try {
                            loading.dismiss();

                            switch (response.getString("status")) {
                                case "success":
                                    responseAlert(response.getString("message") + ".\nKindly sign-in again");
                                    break;

                                case "failed":
                                    Toasty.info(context, "Change pin request failed", Toast.LENGTH_LONG, true).show();
                                    break;

                                default:
                                    Toasty.info(context, "The request could not be completed at this moment", Toast.LENGTH_LONG, true).show();
                                    break;
                            }

                        } catch (JSONException e) {
                            loading.dismiss();
                            Toasty.info(context, "An error occurred", Toast.LENGTH_LONG, true).show();

                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 403:
                            notifySessionExpired("Authorization failed: Your session has expired. Kindly sign-in to access this option");
                            break;
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error.")) {
                                    Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":
                                    default:
                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } else {
                    if (error instanceof NoConnectionError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, "The service is currently unavailable.", Toast.LENGTH_SHORT, true).show();
                            }
                        });
                    } else if (error instanceof TimeoutError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, "The request has timed out.", Toast.LENGTH_SHORT, true).show();
                            }
                        });
                    } else if (error instanceof AuthFailureError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, "Authentication failed", Toast.LENGTH_SHORT, true).show();
                            }
                        });
                    } else if (error instanceof ServerError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            }
                        });
                    } else if (error instanceof NetworkError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, "Network error.", Toast.LENGTH_LONG, true).show();
                            }
                        });
                    } else if (error instanceof ParseError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, "Network error.", Toast.LENGTH_LONG, true).show();
                            }
                        });
                    } else {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            }
                        });
                    }
                }
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionToken);

                return headers;
            }
        };

        changePinReq.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));

        requestQueue.add(changePinReq);
    }
}

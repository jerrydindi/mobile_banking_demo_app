package ke.co.payconnect.mocash.mocash.view.fragment;

import android.app.Dialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;
import java.util.Objects;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.mocash.data.MocashDatabase;
import ke.co.payconnect.mocash.mocash.data.entity.FavEntity;
import ke.co.payconnect.mocash.mocash.view.adapter.FavListViewAdapter;
import ke.co.payconnect.mocash.mocash.viewmodel.FavViewModel;

public class FavSelectDialogFragment extends DialogFragment {

    public static final String TAG = "Fav";

    // Views
    private ListView lvSavedContacts;
    private TextView tvFavCancel;

    private List<FavEntity> favList;
    private FavViewModel favViewModel;
    // Adapters
    public FavListViewAdapter adapter;

    // Database
    private static MocashDatabase db;

    public static String mTag = "";

    public static FavSelectDialogFragment newInstance(String title, String tag) {
        FavSelectDialogFragment frag = new FavSelectDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        frag.setArguments(bundle);

        mTag = tag;
        return frag;

    }

    private BroadcastReceiver mSelectedFavReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String accountName = intent.getStringExtra("accountName");
            String accountNumber = intent.getStringExtra("accountNumber");
            String phoneNumber = intent.getStringExtra("phoneNumber");

            returnSelectedFav(accountName, accountNumber, phoneNumber);

            if (getDialog() != null)
                getDialog().cancel();

        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);

        LocalBroadcastManager.getInstance(Objects.requireNonNull(getActivity())).registerReceiver(mSelectedFavReceiver, new IntentFilter("INTENT_FAV"));
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup parent, Bundle state) {
        super.onCreateView(inflater, parent, state);

        db = MocashDatabase.getMocashDatabaseInstance(getContext());

        // Initialize adapter
        adapter = new FavListViewAdapter(getContext(), favList, "select");

        View view = Objects.requireNonNull(getActivity()).getLayoutInflater().inflate(R.layout.fragment_fav_select_dialog, parent, false);

        lvSavedContacts = view.findViewById(R.id.lvSavedContacts);
        lvSavedContacts.setAdapter(adapter);

        favViewModel = ViewModelProviders.of(this).get(FavViewModel.class);

        favViewModel.getFavList().observe(getActivity(), new Observer<List<FavEntity>>() {
            @Override
            public void onChanged(@Nullable List<FavEntity> fav) {
                // Update UI
                favList = fav;
                adapter = new FavListViewAdapter(getActivity(), favList, "select");
                adapter.notifyDataSetChanged();
                lvSavedContacts.setAdapter(adapter);
            }
        });


        tvFavCancel = view.findViewById(R.id.tvFavCancel);
        tvFavCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getDialog() != null)
                    getDialog().cancel();
            }
        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void returnSelectedFav(String accountName, String accountNumber, String phoneNumber) {
        Intent i;
        switch (mTag) {
            case "AIRTIME-SAF":
                i = new Intent("INTENT_FAV_AIRTIME");
                i.putExtra("accountName", accountName);
                i.putExtra("accountNumber", accountNumber);
                i.putExtra("phoneNumber", phoneNumber);
                LocalBroadcastManager.getInstance(getContext()).sendBroadcast(i);
                break;
            case "MPESA":
                i = new Intent("INTENT_FAV_MPESA");
                i.putExtra("accountName", accountName);
                i.putExtra("accountNumber", accountNumber);
                i.putExtra("phoneNumber", phoneNumber);
                LocalBroadcastManager.getInstance(getContext()).sendBroadcast(i);
                break;
            case "FT":
                i = new Intent("INTENT_FAV_FT");
                i.putExtra("accountName", accountName);
                i.putExtra("accountNumber", accountNumber);
                i.putExtra("phoneNumber", phoneNumber);
                LocalBroadcastManager.getInstance(getContext()).sendBroadcast(i);
                break;
            case "PB":
                i = new Intent("INTENT_FAV_PB");
                i.putExtra("accountName", accountName);
                i.putExtra("accountNumber", accountNumber);
                i.putExtra("phoneNumber", phoneNumber);
                LocalBroadcastManager.getInstance(getContext()).sendBroadcast(i);
                break;
        }
    }

}


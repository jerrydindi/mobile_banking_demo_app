package ke.co.payconnect.mocash.mocash.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import ke.co.payconnect.mocash.mocash.data.entity.FavEntity;

@Dao
public interface FavDao {
    @Query("SELECT EXISTS(SELECT 1 FROM FavEntity WHERE account_number == :accountNumber LIMIT 1)")
    boolean favExists(String accountNumber);

    @Query("SELECT * FROM FavEntity ORDER BY account_name ASC")
    LiveData<List<FavEntity>> getAllFav();

    @Query("SELECT * FROM FavEntity WHERE uid == :uid")
    FavEntity findFavByUID(int uid);

    @Query("SELECT COUNT(*) FROM FavEntity")
    int favCount();

    @Query("SELECT uid FROM FavEntity WHERE account_number == :accountNumber")
    int favUID(String accountNumber);

    @Insert
    void insertFav(FavEntity... favEntities);

    @Update
    void updateFav(FavEntity... favEntities);

    @Delete
    void deleteFav(FavEntity favModels);

    @Query("DELETE FROM FavEntity")
    void deleteAllFavs();
}

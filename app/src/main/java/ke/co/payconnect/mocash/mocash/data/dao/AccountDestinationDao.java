package ke.co.payconnect.mocash.mocash.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import ke.co.payconnect.mocash.mocash.data.entity.AccountDestinationEntity;

@Dao
public interface AccountDestinationDao {

    @Query("SELECT EXISTS(SELECT 1 FROM AccountDestinationEntity WHERE account_number == :accountNumber LIMIT 1)")
    boolean accountDestinationExists(String accountNumber);

    // LiveData List
    @Query("SELECT DISTINCT uid, account_number, account_type FROM AccountDestinationEntity ORDER BY account_number ASC")
    LiveData<List<AccountDestinationEntity>> getAllAccountsDestination();

    // Normal List
    @Query("SELECT DISTINCT uid, account_number, account_type FROM AccountDestinationEntity ORDER BY account_number ASC")
    List<AccountDestinationEntity> getAllAccountsDestinationList();

    @Query("SELECT uid, account_number, account_type FROM AccountDestinationEntity WHERE uid == :uid")
    AccountDestinationEntity findAccountDestinationByUID(int uid);

    @Query("SELECT uid, account_number, account_type FROM AccountDestinationEntity WHERE account_number == :accountNumber")
    AccountDestinationEntity findAccountDestinationByAccountNo(String accountNumber);

    @Query("SELECT COUNT(*) FROM AccountDestinationEntity")
    int getAccountsDestinationCount();

    @Query("SELECT uid FROM AccountDestinationEntity WHERE account_number == :accountNumber")
    int getAccountDestinationUID(String accountNumber);

    @Insert
    void insertAccountDestination(AccountDestinationEntity... accountDestinationEntities);

    @Update
    void updateAccountDestination(AccountDestinationEntity... accountDestinationEntities);

    @Delete
    void deleteAccountDestination(AccountDestinationEntity accountDestinationModels);

    @Query("DELETE FROM AccountDestinationEntity")
    void deleteAllAccountsDestination();
}

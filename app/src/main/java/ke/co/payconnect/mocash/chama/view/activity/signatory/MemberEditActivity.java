package ke.co.payconnect.mocash.chama.view.activity.signatory;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.ViewModelProviders;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Time;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;
import ke.co.payconnect.mocash.chama.util.Access;
import ke.co.payconnect.mocash.chama.util.Auth;
import ke.co.payconnect.mocash.chama.util.Config;
import ke.co.payconnect.mocash.chama.util.SendTo;
import ke.co.payconnect.mocash.chama.util.Utility;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;

import static ke.co.payconnect.mocash.chama.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.chama.util.Config.RETRIES;
import static ke.co.payconnect.mocash.chama.util.Config.TIMEOUT_MS;

public class MemberEditActivity extends AppCompatActivity {

    private final Context context = this;
    private final Activity activity = this;

    private Toolbar tbEditMember;
    private TextView tvConnectivityStatusEditMember;
    private EditText etFirstNameEditMember;
    private EditText etLastNameEditMember;
    private EditText etIdNumberEditMember;
    private CardView cvDateOfBirthEditMember;
    private TextView tvDateOfBirthEditMember;
    private EditText etPhoneNumberEditMember;
    private CardView cvSaveEditMember;
    private TextView tvIdNumberEditMember;

    private int year;
    private int month;
    private int day;

    private MemberViewModel memberViewModel;

    private String memberPhoneNumber;
    private boolean isActive;
    private String first_name;
    private String last_name;
    private String idNumber;
    private String address;
    private String dateOfBirth;

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if (isConnected)
                tvConnectivityStatusEditMember.setVisibility(View.GONE);
            else
                tvConnectivityStatusEditMember.setVisibility(View.VISIBLE);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_edit);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        else
            registerReceiver(connectionReceiver, intentFilter);

        tvConnectivityStatusEditMember = findViewById(R.id.tvConnectivityStatusEditMember);
        tbEditMember = findViewById(R.id.tbEditMember);
        setSupportActionBar(tbEditMember);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbEditMember.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        tvConnectivityStatusEditMember = findViewById(R.id.tvConnectivityStatusEditMember);
        etFirstNameEditMember = findViewById(R.id.etFirstNameEditMember);
        etLastNameEditMember = findViewById(R.id.etLastNameEditMember);
        etIdNumberEditMember = findViewById(R.id.etIdNumberEditMember);
        cvDateOfBirthEditMember = findViewById(R.id.cvDateOfBirthEditMember);
        tvDateOfBirthEditMember = findViewById(R.id.tvDateOfBirthEditMember);
        etPhoneNumberEditMember = findViewById(R.id.etPhoneNumberEditMember);
        cvSaveEditMember = findViewById(R.id.cvSaveEditMember);
        tvIdNumberEditMember = findViewById(R.id.tvIdNumberEditMember);

        memberViewModel = ViewModelProviders.of(this).get(MemberViewModel.class);
        toggleGroupTypeViews();

        getData();

        setListeners();
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void getData() {
        try {
            Intent i = new Intent(getIntent());
            memberPhoneNumber = i.getStringExtra("member_phone_number");

            MemberEntity memberEntity = memberViewModel.findMemberByPhoneNumber(memberPhoneNumber);
            isActive = memberEntity.isActive();
            first_name = memberEntity.getFirstName();
            last_name =  memberEntity.getLastName();
            idNumber = memberEntity.getIdNumber();
            dateOfBirth = memberEntity.getDateOfBirth();


            setData();
        } catch (Exception e) {
            e.printStackTrace();
            finish();

            overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
        }

    }

    private void setData() {
        etFirstNameEditMember.setText(first_name);
        etLastNameEditMember.setText(last_name);
        etIdNumberEditMember.setText(idNumber);
        tvDateOfBirthEditMember.setText(dateOfBirth);
        etPhoneNumberEditMember.setText(memberPhoneNumber);
    }

    private void setListeners() {

        cvDateOfBirthEditMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();

                year = c.get(Calendar.YEAR) - 18;
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("DefaultLocale")
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                        try {
                            tvDateOfBirthEditMember.setText(String.format("%d-%d-%d", year, monthOfYear + 1, dayOfMonth));
                        } catch (Exception e) {
                            Toasty.info(context, "Date of birth selection is required", Toast.LENGTH_SHORT, true).show();
                        }
                    }
                }, year, month, day);

                datePickerDialog.getDatePicker();
                datePickerDialog.show();
            }
        });

                cvSaveEditMember.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!Auth.isAuthorized("signatory")) {
                            Toasty.info(context, R.string.action_not_permitted, Toast.LENGTH_SHORT, true).show();
                            return;
                        }
                        validation();
                    }
                });



    }

    private void validation() {
        String firstName = etFirstNameEditMember.getText().toString();
        String lastName = etLastNameEditMember.getText().toString();
        String idNumber = etIdNumberEditMember.getText().toString();
        String dateOfBirth = tvDateOfBirthEditMember.getText().toString();
        String memberPhoneNumber = etPhoneNumberEditMember.getText().toString();

        if (firstName.isEmpty()) {
            Toasty.info(context, "Name cannot be blank", Toast.LENGTH_SHORT, true).show();
            return;
        }
        if (lastName.isEmpty()) {
            Toasty.info(context, "Name cannot be blank", Toast.LENGTH_SHORT, true).show();
            return;
        }
        if (idNumber.isEmpty()) {
            Toasty.info(context, "ID number cannot be blank", Toast.LENGTH_SHORT, true).show();
            return;
        }
        if (dateOfBirth.isEmpty()) {
            Toasty.info(context, "Date of birth cannot be blank", Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (memberPhoneNumber.isEmpty()) {
            Toasty.info(context, "Phone number cannot be blank", Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (memberPhoneNumber.substring(0, 4).equals("+254"))
            memberPhoneNumber = memberPhoneNumber.substring(4);

        if (memberPhoneNumber.substring(0, 3).equals("254"))
            memberPhoneNumber = memberPhoneNumber.substring(3);

        if (memberPhoneNumber.substring(0, 1).equals("0"))
            memberPhoneNumber = memberPhoneNumber.substring(1);

        String phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();
        String dateRegistered = Time.getDateTimeNow("yyyy-MM-dd HH:mm:ss");
        String groupNumber = AppController.getInstance().getChamaPreferenceManager().getGroupNumber();

        HashMap<String, String> map = new HashMap<>();
        map.put("group_number", groupNumber);
        map.put("first_name", firstName);
        map.put("last_name", lastName);
        map.put("member_phone_number", "254" + memberPhoneNumber);
        map.put("id_number", idNumber);
        map.put("date_of_birth", dateOfBirth);
        map.put("phone_number", phoneNumber);

        MemberEntity memberEntity = new MemberEntity();
        memberEntity.setFirstName(map.get("first_name"));
        memberEntity.setLastName(map.get("last_name"));
        memberEntity.setPhoneNumber(map.get("member_phone_number"));
        memberEntity.setIdNumber(map.get("id_number"));
        memberEntity.setDateOfBirth(map.get("date_of_birth"));
        memberEntity.setSignatory("0");
        memberEntity.setSecretary("0");
        memberEntity.setMakerPhoneNumber("phone_number");
        memberEntity.setActive(true);

        final String sessionToken = AppController.getInstance().getChamaPreferenceManager().getSessionToken();

        processRequest(map, sessionToken);

    }

    SendTo st = new SendTo(context);
    HurlStack hurlStack = new HurlStack() {
        @Override
        protected HttpURLConnection createConnection(URL url) throws IOException {
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
            try {
                httpsURLConnection.setSSLSocketFactory(st.getSSLSocketFactory());
                httpsURLConnection.setHostnameVerifier(st.getHostnameVerifier());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return httpsURLConnection;
        }
    };


    private void processRequest(final HashMap<String, String> map, final String sessionToken) {
        final ProgressDialog loading = ProgressDialog.show(this, null, "Processing request", false, false);

        final String requestTag = "chama_edit_member_request";
        final String url = Config.ADD_MEMBER;
        RequestQueue requestQueue = Volley.newRequestQueue(MemberEditActivity.this);

        JsonObjectRequest editMemberReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(final JSONObject response) {
                        try {

                            loading.dismiss();

                            switch (response.getString("status")) {
                                case "success":
                                    loading.show();
                                    loading.setMessage("Please wait");

                                    insertToDatabase(map);
                                    onCompleteAlertDialog("Member details submitted");

                                    loading.dismiss();

                                    break;

                                case "failed":
                                case "fatal":
                                default:
                                    onCompleteAlertDialog("Could not save member details");
                                    break;
                            }

                        } catch (JSONException e) {
                            loading.dismiss();
                            onCompleteAlertDialog("An error occurred");

                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                loading.dismiss();
                onCompleteAlertDialog("An error occurred");

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error")) {
                                    Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":
                                    default:
                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } else {
                    if (error instanceof NoConnectionError) {
                        Toasty.info(context, "No connection to the server", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof TimeoutError) {
                        Toasty.info(context, "The request has timed out", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof AuthFailureError) {
                        Toasty.info(context, "Authentication failed", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof ServerError) {
                        Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof NetworkError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof ParseError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else {
                        Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    }
                }

            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionToken);

                return headers;
            }

        };

        editMemberReq.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));
        requestQueue.add(editMemberReq);
    }

    private void insertToDatabase(HashMap<String, String> map) {
        MemberEntity memberEntity = new MemberEntity();
        memberEntity.setFirstName(map.get("first_name"));
        memberEntity.setLastName(map.get("last_name"));
        memberEntity.setPhoneNumber(map.get("member_phone_number"));
        memberEntity.setIdNumber(map.get("id_number"));
        memberEntity.setMakerPhoneNumber("phone_number");
        memberEntity.setSignatory(map.get("signatory"));
        memberEntity.setSecretary(map.get("secretary"));
        memberEntity.setDateOfBirth(map.get("date_of_birth"));

        memberViewModel.updateMember(memberEntity);
    }

    private void onCompleteAlertDialog(String message) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(false);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_unaitas);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.ok), dialogClickListener);
        builder.show();
    }

    private void toggleGroupTypeViews() {
        int groupType = AppController.getInstance().getChamaPreferenceManager().getGroupType();

        if (groupType == 1)
            tvIdNumberEditMember.setText(getString(R.string.prompt_customer_number));
        else
            tvIdNumberEditMember.setText(getString(R.string.prompt_id_number));

    }
}
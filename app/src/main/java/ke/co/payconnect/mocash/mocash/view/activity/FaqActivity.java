package ke.co.payconnect.mocash.mocash.view.activity;

import android.app.Activity;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.mocash.util.Access;

public class FaqActivity extends AppCompatActivity {
    private Activity activity = this;

    // Views
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);

        toolbar = findViewById(R.id.tbFaq);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }
}

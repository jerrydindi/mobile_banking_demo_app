package ke.co.payconnect.mocash.chama.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.entity.GroupEntity;


@Dao
public interface GroupDao {

    @Query("SELECT uid, name, number, type FROM GroupEntity ORDER BY name ASC")
    List<GroupEntity> getAllGroups();

    @Query("SELECT uid, name, number, type FROM GroupEntity ORDER BY name ASC")
    LiveData<List<GroupEntity>> getAllGroupsLive();

    @Query("SELECT COUNT(*) FROM GroupEntity")
    int getGroupCount();

    @Insert
    void insertGroup(GroupEntity... groupEntities);

    @Update
    void updateGroup(GroupEntity... groupEntities);

    @Delete
    void deleteGroup(GroupEntity... groupEntities);

    @Query("DELETE FROM GroupEntity")
    void deleteAllGroups();
}

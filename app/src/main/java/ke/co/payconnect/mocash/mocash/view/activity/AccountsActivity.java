package ke.co.payconnect.mocash.mocash.view.activity;

import android.app.Activity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import java.util.List;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.mocash.data.entity.AccountEntity;
import ke.co.payconnect.mocash.mocash.util.Access;
import ke.co.payconnect.mocash.mocash.view.adapter.AccountsRecyclerViewAdapter;
import ke.co.payconnect.mocash.mocash.viewmodel.AccountViewModel;

public class AccountsActivity extends AppCompatActivity {
    private final Context context = this;
    private final Activity activity = this;

    // Views
    private Toolbar tbAccounts;
    private RecyclerView rvAccounts;
    private LinearLayoutManager layoutManager;

    public AccountsRecyclerViewAdapter adapter;

    private AccountViewModel accountViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accounts);

        tbAccounts = findViewById(R.id.tbAccounts);
        setSupportActionBar(tbAccounts);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbAccounts.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        rvAccounts = findViewById(R.id.rvAccounts);

        accountViewModel = ViewModelProviders.of(this).get(AccountViewModel.class);

        accountViewModel.getAccountLiveList().observe(AccountsActivity.this, new Observer<List<AccountEntity>>() {
            @Override
            public void onChanged(@Nullable List<AccountEntity> accounts) {
                // Update UI

                if (accounts != null) {
                    adapter = new AccountsRecyclerViewAdapter(context, accounts);
                    adapter.notifyDataSetChanged();

                    layoutManager = new LinearLayoutManager(context);
                    rvAccounts.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
                    rvAccounts.setAdapter(adapter);
                    rvAccounts.setLayoutManager(layoutManager);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }
}

package ke.co.payconnect.mocash.mocash.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import ke.co.payconnect.mocash.R;

public class SplashActivity extends AppCompatActivity {
    private final Context context = this;

    private ImageView ivLogo;
    private TextView ivName;
    private TextView ivNameSub;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (Build.VERSION.SDK_INT >= 21) {
            Window w = getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        ivLogo = findViewById(R.id.ivLogo);
        ivName = findViewById(R.id.ivName);
        ivNameSub = findViewById(R.id.ivNameSub);

        Animation ivAnimation = AnimationUtils.loadAnimation(context, R.anim.splash_transition);
        ivLogo.startAnimation(ivAnimation);

        Animation ivAnimationTwo = AnimationUtils.loadAnimation(context, R.anim.splash_two);
        ivName.startAnimation(ivAnimationTwo);

        Animation ivAnimationThree = AnimationUtils.loadAnimation(context, R.anim.splash_three);
        ivNameSub.startAnimation(ivAnimationThree);

        final Intent i = new Intent(context, EntryActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

        Thread thread = new Thread() {
            public void run() {
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    startActivity(i);

                    overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                }
            }
        };

        thread.start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);

        System.exit(0);
    }
}

package ke.co.payconnect.mocash.chama.view.adapter.member;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import io.captano.utility.Money;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.chama.data.entity.TransactionEntity;
import ke.co.payconnect.mocash.chama.view.activity.member.TransactionDetailsActivity;

public class PersonalTransactionsRecyclerViewAdapter extends RecyclerView.Adapter<PersonalTransactionsRecyclerViewAdapter.PersonalTransactionViewHolder> {
    private List<TransactionEntity> transactions;
    private final Context context;

    public PersonalTransactionsRecyclerViewAdapter(Context context, List<TransactionEntity> transactions) {
        this.context = context;
        this.transactions = transactions;
    }

    @NonNull
    @Override
    public PersonalTransactionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_personal_transactions, parent, false);
        return new PersonalTransactionViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull PersonalTransactionViewHolder holder, int position) {
        try {
            final TransactionEntity transactionEntity = transactions.get(position);

            holder.tvNoStructPersonalTransaction.setText(String.valueOf(position + 1));
            holder.tvTxnIDStructPersonalTransaction.setText(transactionEntity.getTxnID());
            holder.tvTypeStructPersonalTransaction.setText(transactionEntity.getTxnType());
            holder.tvAmountStructPersonalTransaction.setText(Money.format(Double.parseDouble(transactionEntity.getAmount())));

            holder.clStructApprovalTxn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, TransactionDetailsActivity.class);
                    i.putExtra("txn_id", transactionEntity.getTxnID());
                    context.startActivity(i);

                    ((Activity) context).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                }
            });
        } catch (Exception ignored) {
        }
    }

    @Override
    public int getItemCount() {
        return this.transactions.size();
    }

    class PersonalTransactionViewHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout clStructApprovalTxn;
        private TextView tvNoStructPersonalTransaction;
        private TextView tvTxnIDStructPersonalTransaction;
        private TextView tvTypeStructPersonalTransaction;
        private TextView tvAmountStructPersonalTransaction;

        PersonalTransactionViewHolder(@NonNull View view) {
            super(view);

            clStructApprovalTxn = view.findViewById(R.id.clStructApprovalTxn);
            tvNoStructPersonalTransaction = view.findViewById(R.id.tvNoStructPersonalTxn);
            tvTxnIDStructPersonalTransaction = view.findViewById(R.id.tvTxnIDStructPersonalTxn);
            tvTypeStructPersonalTransaction = view.findViewById(R.id.tvTypeStructPersonalTxn);
            tvAmountStructPersonalTransaction = view.findViewById(R.id.tvAmountStructPersonalTxn);
        }
    }
}

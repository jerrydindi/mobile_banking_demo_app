package ke.co.payconnect.mocash.chama.view.activity.signatory;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.ViewModelProviders;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Money;
import io.captano.utility.Time;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalTransactionEntity;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;
import ke.co.payconnect.mocash.chama.data.entity.TransactionEntity;
import ke.co.payconnect.mocash.chama.util.Access;
import ke.co.payconnect.mocash.chama.util.Config;
import ke.co.payconnect.mocash.chama.util.SendTo;
import ke.co.payconnect.mocash.chama.util.Utility;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalTransactionViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.TransactionViewModel;

import static ke.co.payconnect.mocash.chama.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.chama.util.Config.RETRIES;
import static ke.co.payconnect.mocash.chama.util.Config.TIMEOUT_MS;

public class ApproveTransactionActivity extends AppCompatActivity {
    private final Context context = this;
    private final Activity activity = this;

    private Toolbar tbApproveTxn;
    private TextView tvConnectivityStatusApproveTxn;
    private TextView tvBatchNoApproveTxn;
    private TextView tvTxnIDApproveTxn;
    private TextView tvMemberNameApproveTxn;
    private TextView tvAmountApproveTxn;
    private TextView tvMakerNameApproveTxn;
    private TextView tvTxnTypeApproveTxn;
    private TextView tvTxnDateApproveTxn;
    private TextView tvApprovalIDApproveTxn;
    private TextView tvApprovalTypeApproveTxn;
    private TextView tvApprovalStatusApproveTxn;
    private TextView tvApprovalDateApproveTxn;
    private TextView tvApprovalNarrationApproveTxn;
    private CardView cvRejectApproveTxn;
    private CardView cvAcceptApproveTxn;

    private String batchNo;
    private String txnID;
    private String memberName;
    private String amount;
    private String makerName;
    private String txnType;
    private String txnDate;
    private String approvalID;
    private String approvalType;
    private String approvalStatus;
    private String approvalDate;
    private String approvalNarration;

    private ApprovalTransactionViewModel approvalTransactionViewModel;
    private TransactionViewModel transactionViewModel;
    private MemberViewModel memberViewModel;


    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if (isConnected)
                tvConnectivityStatusApproveTxn.setVisibility(View.GONE);
            else
                tvConnectivityStatusApproveTxn.setVisibility(View.VISIBLE);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approve_transaction);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        else
            registerReceiver(connectionReceiver, intentFilter);
        tbApproveTxn = findViewById(R.id.tbApproveTxn);
        setSupportActionBar(tbApproveTxn);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbApproveTxn.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        tvConnectivityStatusApproveTxn = findViewById(R.id.tvConnectivityStatusApproveTxn);
        tvBatchNoApproveTxn = findViewById(R.id.tvBatchNoApproveTxn);
        tvTxnIDApproveTxn = findViewById(R.id.tvTxnIDApproveTxn);
        tvMemberNameApproveTxn = findViewById(R.id.tvMemberNameApproveTxn);
        tvAmountApproveTxn = findViewById(R.id.tvAmountApproveTxn);
        tvMakerNameApproveTxn = findViewById(R.id.tvMakerNameApproveTxn);
        tvTxnTypeApproveTxn = findViewById(R.id.tvTxnTypeApproveTxn);
        tvTxnDateApproveTxn = findViewById(R.id.tvTxnDateApproveTxn);
        tvApprovalIDApproveTxn = findViewById(R.id.tvApprovalIDApproveTxn);
        tvApprovalTypeApproveTxn = findViewById(R.id.tvApprovalTypeApproveTxn);
        tvApprovalStatusApproveTxn = findViewById(R.id.tvApprovalStatusApproveTxn);
        tvApprovalDateApproveTxn = findViewById(R.id.tvApprovalDateApproveTxn);
        tvApprovalNarrationApproveTxn = findViewById(R.id.tvApprovalNarrationApproveTxn);
        cvRejectApproveTxn = findViewById(R.id.cvRejectApproveTxn);
        cvAcceptApproveTxn = findViewById(R.id.cvAcceptApproveTxn);

        approvalTransactionViewModel = ViewModelProviders.of(this).get(ApprovalTransactionViewModel.class);
        transactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel.class);
        memberViewModel = ViewModelProviders.of(this).get(MemberViewModel.class);

        setListeners();
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);

        getData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void setListeners() {
        cvRejectApproveTxn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onApprovalApproval("rejected");
            }
        });

        cvAcceptApproveTxn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onApprovalApproval("accepted");
            }
        });
    }

    private void getData() {
        Intent i = new Intent(getIntent());
        approvalID = i.getStringExtra("approval_id");

        ApprovalTransactionEntity approvalTransactionEntity = approvalTransactionViewModel.findApprovalTransactionsByApprovalID(approvalID);
        approvalType = approvalTransactionEntity.getApprovalType().toUpperCase();
        approvalStatus = approvalTransactionEntity.getApprovalStatus().toUpperCase();
        if (approvalTransactionEntity.getApprovalDate() != null) {
            approvalDate = approvalTransactionEntity.getApprovalDate();

            try {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                Date date = format.parse(approvalDate);
                Date dateToCompare = format.parse("2000-01-01");

                if ((date != null ? date.compareTo(dateToCompare) : 0) == 0) {
                    approvalDate = "";
                }
            } catch (ParseException ignored) {
            }
        }

        if (approvalTransactionEntity.getApprovalNarration() != null) {
            approvalNarration = approvalTransactionEntity.getApprovalNarration();
            if (approvalNarration.equals("0"))
                approvalNarration = "No data";
        }


        TransactionEntity transactionEntity = transactionViewModel.findTransactionByTxnID(approvalTransactionEntity.getTxnID());
        batchNo = transactionEntity.getBatchNumber();
        txnID = transactionEntity.getTxnID();
        amount = transactionEntity.getAmount();
        txnType = transactionEntity.getTxnType();
        if (transactionEntity.getTxnDate() != null) {
            txnDate = transactionEntity.getTxnDate();

            try {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                Date date = format.parse(txnDate);
                Date dateToCompare = format.parse("2000-01-01");

                if ((date != null ? date.compareTo(dateToCompare) : 0) == 0) {
                    txnDate = "";
                }
            } catch (ParseException ignored) {
            }

        }

        if (approvalTransactionEntity.getMemberPhoneNumber().length() > 2) {
            if (approvalTransactionEntity.getMemberPhoneNumber().substring(0, 4).equals("2547")) {
                MemberEntity member = memberViewModel.findMemberByPhoneNumber(approvalTransactionEntity.getMemberPhoneNumber());
                memberName = member.getFirstName() + " " + member.getLastName();
            } else {
                memberName = AppController.getInstance().getChamaPreferenceManager().getGroupName();
            }
        } else {
            memberName = AppController.getInstance().getChamaPreferenceManager().getGroupName();
        }

        MemberEntity maker = memberViewModel.findMemberByPhoneNumber(approvalTransactionEntity.getMakerPhoneNumber());
        makerName = maker.getFirstName() + " " + maker.getLastName();

        toggleAcceptView();

        setData();
    }

    private void setData() {
        tvBatchNoApproveTxn.setText(batchNo);
        tvTxnIDApproveTxn.setText(txnID);
        tvMemberNameApproveTxn.setText(memberName);
        tvAmountApproveTxn.setText(Money.format(Double.parseDouble(amount)));
        tvMakerNameApproveTxn.setText(makerName);
        tvTxnTypeApproveTxn.setText(txnType);

        if (!txnDate.isEmpty()) {
            try {
                String formattedDate = Utility.getDate(txnDate, "yyyy-MM-dd HH:mm:ss");
                tvTxnDateApproveTxn.setText(formattedDate);
            } catch (ParseException e) {
                tvTxnDateApproveTxn.setText(txnDate);
            }
        } else {
            tvTxnDateApproveTxn.setText("NA");
        }

        tvApprovalIDApproveTxn.setText(approvalID);
        tvApprovalTypeApproveTxn.setText(approvalType);
        tvApprovalStatusApproveTxn.setText(approvalStatus);

        switch (approvalStatus) {
            case "PENDING":
                tvApprovalStatusApproveTxn.setTextColor(getResources().getColor(R.color.cO1));
                break;
            case "ACCEPTED":
                tvApprovalStatusApproveTxn.setTextColor(getResources().getColor(R.color.cBL0));
                break;
            case "REJECTED":
            case "FAILED":
                tvApprovalStatusApproveTxn.setTextColor(getResources().getColor(R.color.cR2));
                break;
        }

        if (approvalDate != null) {
            if (!approvalDate.isEmpty()) {
                try {
                    String formattedDate = Utility.getDate(approvalDate, "yyyy-MM-dd HH:mm:ss");
                    tvApprovalDateApproveTxn.setText(formattedDate);
                } catch (ParseException e) {
                    tvApprovalDateApproveTxn.setText(approvalDate);
                }
            } else {
                tvApprovalDateApproveTxn.setText("NA");
            }
        }

        if (approvalNarration != null) {
            if (!approvalNarration.isEmpty() && !approvalNarration.equals("NA"))
                tvApprovalNarrationApproveTxn.setText(approvalNarration);
        }

    }

    private void onApprovalApproval(final String action) {
        LayoutInflater li = LayoutInflater.from(context);
        @SuppressLint("InflateParams") View promptsView = li.inflate(R.layout.struct_accept_approval_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptsView);

        final EditText etStructAcceptApproval = promptsView.findViewById(R.id.etStructAcceptApproval);


        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setPositiveButton(getString(R.string.send),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        String narration = etStructAcceptApproval.getText().toString().trim();

                        if (narration.isEmpty())
                            narration = "NA";

                        String phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();
                        String groupNumber = AppController.getInstance().getChamaPreferenceManager().getGroupNumber();

                        final HashMap<String, String> map = new HashMap<>();
                        map.put("approval", action);
                        map.put("approval_id", approvalID);
                        map.put("approval_narration", narration);
                        map.put("phone_number", phoneNumber);
                        map.put("group_number", groupNumber);
                        map.put("tag", "A-TFIN");

                        final String sessionToken = AppController.getInstance().getChamaPreferenceManager().getSessionToken();
                        processRequest(map, sessionToken);
                    }
                })
                .setNeutralButton(getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void toggleAcceptView() {
        switch (approvalStatus) {
            case "PENDING":
            case "FAILED":
                cvRejectApproveTxn.setVisibility(View.VISIBLE);
                cvRejectApproveTxn.setEnabled(true);

                cvAcceptApproveTxn.setVisibility(View.VISIBLE);
                cvAcceptApproveTxn.setEnabled(true);
                break;
            case "ACCEPTED":
            case "REJECTED":
                cvRejectApproveTxn.setVisibility(View.GONE);
                cvRejectApproveTxn.setEnabled(false);

                cvAcceptApproveTxn.setVisibility(View.GONE);
                cvAcceptApproveTxn.setEnabled(false);
                break;
        }
    }

    SendTo st = new SendTo(context);
    HurlStack hurlStack = new HurlStack() {
        @Override
        protected HttpURLConnection createConnection(URL url) throws IOException {
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
            try {
                httpsURLConnection.setSSLSocketFactory(st.getSSLSocketFactory());
                httpsURLConnection.setHostnameVerifier(st.getHostnameVerifier());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return httpsURLConnection;
        }
    };

    private void processRequest(final HashMap<String, String> map, final String sessionToken) {

        final ProgressDialog loading = ProgressDialog.show(this, null, "Processing approval", false, false);


        final String requestTag = "chama_approve_txn_approval_request";
        final String url = Config.APPROVE_APPROVAL;
        RequestQueue requestQueue = Volley.newRequestQueue(ApproveTransactionActivity.this);

        JsonObjectRequest addApprovalReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(final JSONObject response) {
                        try {

                            loading.dismiss();

                            switch (response.getString("status")) {
                                case "success":
                                    updateApprovalStatus(approvalID, map.get("approval"), map.get("approval_narration"));
                                    onComplete("Action submitted successfully", true);
                                    break;

                                case "failed":
                                case "fatal":
                                default:
                                    updateApprovalStatus(approvalID, "failed", map.get("approval_narration"));
                                    onComplete("Action could not be committed at the moment", false);
                                    break;
                            }

                        } catch (JSONException e) {
                            loading.dismiss();
                            Toasty.info(context, "An error occurred", Toast.LENGTH_LONG, true).show();

                            updateApprovalStatus(approvalID, "failed", map.get("approval_narration"));
                            onComplete("Action could not be committed at the moment", false);
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                loading.dismiss();

                updateApprovalStatus(approvalID, "failed", map.get("approval_narration"));
                onComplete("Action could not be committed at the moment", false);

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error")) {
                                    Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":

                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();

                                        break;

                                    default:
                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } else {
                    if (error instanceof NoConnectionError) {
                        Toasty.info(context, "No connection to the server", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof TimeoutError) {
                        Toasty.info(context, "The request has timed out", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof AuthFailureError) {
                        Toasty.info(context, "Authentication failed", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof ServerError) {
                        Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof NetworkError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof ParseError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else {
                        Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    }
                }

            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionToken);

                return headers;
            }

        };

        addApprovalReq.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));
        requestQueue.add(addApprovalReq);
    }

    private void updateApprovalStatus(String approvalID, String approvalStatus, String approvalNarration) {
        String approvalDate = Time.getDateTimeNow("yyyy-MM-dd HH:mm:ss");
        approvalTransactionViewModel.updateApprovalTransactionSetData(approvalID, approvalStatus, approvalNarration, approvalDate);

        getData();
    }

    private void onComplete(String message, final boolean isSuccessful) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    if (isSuccessful) {
                        finish();

                        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                    }
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(true);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.ok), dialogClickListener);
        builder.show();
    }
}

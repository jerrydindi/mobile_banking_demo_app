package ke.co.payconnect.mocash.chama.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.entity.ApprovalGroupLoanBalanceEntity;

@Dao
public interface ApprovalGroupLoanBalanceDao {

    @Query("SELECT uid, member_phone_number, narration, request_date, approval_id, approval_status, approval_type, approval_narration, approval_date FROM ApprovalGroupLoanBalanceEntity WHERE approval_type == :approvalType ORDER BY uid DESC")
    LiveData<List<ApprovalGroupLoanBalanceEntity>> getApprovalGroupLoanBalanceByTypeLive(final String approvalType);

    @Query("SELECT uid, member_phone_number, narration, request_date, approval_id, approval_status, approval_type, approval_narration, approval_date FROM ApprovalGroupLoanBalanceEntity WHERE approval_id == :approvalId ORDER BY uid DESC")
    ApprovalGroupLoanBalanceEntity findGroupLoanBalanceApprovalById(String approvalId);

    @Query("SELECT EXISTS(SELECT 1 FROM ApprovalGroupLoanBalanceEntity WHERE approval_id == :approvalId LIMIT 1)")
    boolean doesGroupLoanBalanceApprovalExistWithApprovalId(String approvalId);

    @Query("UPDATE ApprovalGroupLoanBalanceEntity SET approval_status = :approvalStatus, approval_narration = :approvalNarration, approval_date = :approvalDate WHERE approval_id == :approvalID")
    void updateApprovalGroupLoanBalanceSetData(final String approvalID, final String approvalStatus, final String approvalNarration, final String approvalDate);

    @Insert
    void insertApprovalGroupLoanBalance(ApprovalGroupLoanBalanceEntity... approvalGroupLoanBalanceEntities);

    @Query("DELETE FROM ApprovalGroupLoanBalanceEntity")
    void deleteAllApprovalGroupLoanBalances();
}

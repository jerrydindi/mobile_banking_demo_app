package ke.co.payconnect.mocash.chama.view.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.lifecycle.ViewModelProviders;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Connection;
import io.captano.utility.Version;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.GroupEntity;
import ke.co.payconnect.mocash.chama.util.Access;
import ke.co.payconnect.mocash.chama.util.Config;
import ke.co.payconnect.mocash.chama.util.SendTo;
import ke.co.payconnect.mocash.chama.viewmodel.GroupViewModel;
import ke.co.payconnect.mocash.mocash.view.activity.LandingActivity;

import static ke.co.payconnect.mocash.chama.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.chama.util.Config.RETRIES;
import static ke.co.payconnect.mocash.chama.util.Config.TIMEOUT_MS;


public class LoginPinChamaActivity extends AppCompatActivity {
    private final Context context = this;
    private final Activity activity = this;

    // Views
    private ImageView ivPin;
    private LinearLayout llPinLayoutLP;
    private LinearLayout llOneLP;
    private LinearLayout llTwoLP;
    private LinearLayout llThreeLP;
    private LinearLayout llFourLP;
    private LinearLayout llFiveLP;
    private LinearLayout llSixLP;
    private LinearLayout llSevenLP;
    private LinearLayout llEightLP;
    private LinearLayout llNineLP;
    private LinearLayout llZeroLP;
    private LinearLayout llDeletePinLP;
    private Button btLoginLP;
    private ImageView ivOptionsLP;
    private TextView tvForgotPinLP;
    private TextView tvAttemptsLP;
    private TextView tvConnectivityStatusLP;
    private TextView tvPoweredLP;
    private static ProgressDialog loading;

    private StringBuffer pinString = new StringBuffer();

    private double appVersion = 0;

    private boolean doubleBackToExitPressedOnce = false;

    private GroupViewModel groupViewModel;

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if (isConnected)
                tvConnectivityStatusLP.setVisibility(View.GONE);
            else
                tvConnectivityStatusLP.setVisibility(View.VISIBLE);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_pin);

        // Transparent status bar
        if (Build.VERSION.SDK_INT >= 21) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        } else {
            registerReceiver(connectionReceiver, intentFilter);
        }

        llPinLayoutLP = findViewById(R.id.llPinLayoutLP);
        llOneLP = findViewById(R.id.llOneLP);
        llTwoLP = findViewById(R.id.llTwoLP);
        llThreeLP = findViewById(R.id.llThreeLP);
        llFourLP = findViewById(R.id.llFourLP);
        llFiveLP = findViewById(R.id.llFiveLP);
        llSixLP = findViewById(R.id.llSixLP);
        llSevenLP = findViewById(R.id.llSevenLP);
        llEightLP = findViewById(R.id.llEightLP);
        llNineLP = findViewById(R.id.llNineLP);
        llZeroLP = findViewById(R.id.llZeroLP);
        llDeletePinLP = findViewById(R.id.llDeletePinLP);
        btLoginLP = findViewById(R.id.btLoginLP);
        ivOptionsLP = findViewById(R.id.ivOptionsLP);
        tvForgotPinLP = findViewById(R.id.tvForgotPinLP);
        tvAttemptsLP = findViewById(R.id.tvAttemptsLP);
        tvConnectivityStatusLP = findViewById(R.id.tvConnectivityStatusLP);
        tvPoweredLP = findViewById(R.id.tvPoweredLP);

        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            appVersion = Double.parseDouble(pInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        setListeners();

        groupViewModel = ViewModelProviders.of(this).get(GroupViewModel.class);
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();

            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();

            overridePendingTransition(R.xml.fade_in, R.xml.fade_out);

            System.exit(0);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toasty.info(context, "Tap again to exit", Toast.LENGTH_SHORT, true).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    private void setListeners() {
        ivOptionsLP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                optionsAlert();
            }
        });

        tvForgotPinLP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotPinAlert();
            }
        });

        llOneLP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (pinString.length() < 4) {
                    pinString.append("1");

                    ivPin = new ImageView(context);
                    ivPin.setImageResource(R.drawable.dot);
                    ivPin.setScaleX(0.2f);
                    ivPin.setScaleY(0.2f);
                    ivPin.setPadding(0, 0, 0, 0);
                    llPinLayoutLP.addView(ivPin);

                    ivPin.getLayoutParams().height = 100;
                    ivPin.getLayoutParams().width = 100;
                }

            }
        });

        llTwoLP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pinString.length() < 4) {
                    pinString.append("2");

                    ivPin = new ImageView(context);
                    ivPin.setImageResource(R.drawable.dot);
                    ivPin.setScaleX(0.2f);
                    ivPin.setScaleY(0.2f);
                    ivPin.setPadding(0, 0, 0, 0);
                    llPinLayoutLP.addView(ivPin);

                    ivPin.getLayoutParams().height = 100;
                    ivPin.getLayoutParams().width = 100;
                }
            }
        });

        llThreeLP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pinString.length() < 4) {
                    pinString.append("3");

                    ivPin = new ImageView(context);
                    ivPin.setImageResource(R.drawable.dot);
                    ivPin.setScaleX(0.2f);
                    ivPin.setScaleY(0.2f);
                    ivPin.setPadding(0, 0, 0, 0);
                    llPinLayoutLP.addView(ivPin);

                    ivPin.getLayoutParams().height = 100;
                    ivPin.getLayoutParams().width = 100;
                }
            }
        });

        llFourLP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pinString.length() < 4) {
                    pinString.append("4");

                    ivPin = new ImageView(context);
                    ivPin.setImageResource(R.drawable.dot);
                    ivPin.setScaleX(0.2f);
                    ivPin.setScaleY(0.2f);
                    ivPin.setPadding(0, 0, 0, 0);
                    llPinLayoutLP.addView(ivPin);

                    ivPin.getLayoutParams().height = 100;
                    ivPin.getLayoutParams().width = 100;
                }
            }
        });

        llFiveLP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pinString.length() < 4) {
                    pinString.append("5");

                    ivPin = new ImageView(context);
                    ivPin.setImageResource(R.drawable.dot);
                    ivPin.setScaleX(0.2f);
                    ivPin.setScaleY(0.2f);
                    ivPin.setPadding(0, 0, 0, 0);
                    llPinLayoutLP.addView(ivPin);

                    ivPin.getLayoutParams().height = 100;
                    ivPin.getLayoutParams().width = 100;
                }
            }
        });

        llSixLP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pinString.length() < 4) {
                    pinString.append("6");

                    ivPin = new ImageView(context);
                    ivPin.setImageResource(R.drawable.dot);
                    ivPin.setScaleX(0.2f);
                    ivPin.setScaleY(0.2f);
                    ivPin.setPadding(0, 0, 0, 0);
                    llPinLayoutLP.addView(ivPin);

                    ivPin.getLayoutParams().height = 100;
                    ivPin.getLayoutParams().width = 100;
                }
            }
        });

        llSevenLP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pinString.length() < 4) {
                    pinString.append("7");

                    ivPin = new ImageView(context);
                    ivPin.setImageResource(R.drawable.dot);
                    ivPin.setScaleX(0.2f);
                    ivPin.setScaleY(0.2f);
                    ivPin.setPadding(0, 0, 0, 0);
                    llPinLayoutLP.addView(ivPin);

                    ivPin.getLayoutParams().height = 100;
                    ivPin.getLayoutParams().width = 100;
                }
            }
        });

        llEightLP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pinString.length() < 4) {
                    pinString.append("8");

                    ivPin = new ImageView(context);
                    ivPin.setImageResource(R.drawable.dot);
                    ivPin.setScaleX(0.2f);
                    ivPin.setScaleY(0.2f);
                    ivPin.setPadding(0, 0, 0, 0);
                    llPinLayoutLP.addView(ivPin);

                    ivPin.getLayoutParams().height = 100;
                    ivPin.getLayoutParams().width = 100;
                }
            }
        });

        llNineLP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pinString.length() < 4) {
                    pinString.append("9");

                    ivPin = new ImageView(context);
                    ivPin.setImageResource(R.drawable.dot);
                    ivPin.setScaleX(0.2f);
                    ivPin.setScaleY(0.2f);
                    ivPin.setPadding(0, 0, 0, 0);
                    llPinLayoutLP.addView(ivPin);

                    ivPin.getLayoutParams().height = 100;
                    ivPin.getLayoutParams().width = 100;
                }
            }
        });

        llZeroLP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pinString.length() < 4) {
                    pinString.append("0");

                    ivPin = new ImageView(context);
                    ivPin.setImageResource(R.drawable.dot);
                    ivPin.setScaleX(0.2f);
                    ivPin.setScaleY(0.2f);
                    ivPin.setPadding(0, 0, 0, 0);
                    llPinLayoutLP.addView(ivPin);

                    ivPin.getLayoutParams().height = 100;
                    ivPin.getLayoutParams().width = 100;
                }
            }
        });

        llDeletePinLP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = llPinLayoutLP.getChildCount();
                if (count > 0) {
                    pinString.deleteCharAt(count - 1);
                    llPinLayoutLP.removeViewAt(count - 1);
                }
            }
        });

        llDeletePinLP.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                int count = llPinLayoutLP.getChildCount();
                if (count > 0) {
                    for (int i = 0; i < count; i++) {
                        pinString.deleteCharAt(count - (1 + i));
                        llPinLayoutLP.removeViewAt(count - (1 + i));
                    }
                    return false;
                }
                return false;
            }
        });

        btLoginLP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int count = llPinLayoutLP.getChildCount();

                if (count >= 4) {

                    // Internet connectivity check
                    if (tvConnectivityStatusLP.getVisibility() == View.VISIBLE) {
                        Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                    } else {

                        if (new Connection(context).IsInternetConnected())
                                    validation(count);
                        else
                            Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                    }

                } else {
                    Toasty.info(context, getString(R.string.valid_pin), Toast.LENGTH_SHORT, true).show();
                }

            }
        });

        tvPoweredLP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent webIntent = new Intent(Intent.ACTION_VIEW);
                String websiteUrl = "http://payconnect.co.ke/";
                webIntent.setData(Uri.parse(websiteUrl));
                startActivity(webIntent);
            }
        });
    }

    private void validation(final int count) {
        String pin = pinString.toString().trim();

        processLogin(pin, Double.toString(appVersion), count);


    }

    private void actionLogin() {
        Access.startSession();

        Intent i = new Intent(context, MainChamaActivity.class);
        i.putExtra("action", "login");
        startActivity(i);

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void actionUpdateApp() {
        /*
        Re-directs user to app listing on PlayStore
         */

        final String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    private void openChangePinActivity() {
        Intent i = new Intent(context, LoginNewChamaActivity.class);
        startActivity(i);

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void versionCheck(int newVersion, String versionUpdateMessage, boolean forceUpdate) {
        try {
            int currentVersion = Version.getVersionCode(getApplication());
            if (currentVersion < newVersion) {
                notifyUpdateVersion(versionUpdateMessage, forceUpdate);
            } else {
                Toasty.info(context, "Login successful", Toast.LENGTH_SHORT, true).show();
                actionLogin();
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void notifyUpdateVersion(String message, boolean forceUpdate) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        actionUpdateApp();
                        break;
                    case DialogInterface.BUTTON_NEUTRAL:
                        Toasty.info(context, "Login successful", Toast.LENGTH_SHORT, true).show();
                        actionLogin();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(true);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.update), dialogClickListener);
        if (!forceUpdate)
            builder.setNeutralButton(getString(R.string.action_login), dialogClickListener);
        builder.show();
    }

    private void accountBlockedDialog(final boolean goBack) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:

                        if (goBack) {
                            Intent i = new Intent(context, LandingActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);

                            overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                        }

                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        // Do nothing
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.warning));
        builder.setIcon(R.drawable.ic_fyi);
        builder.setMessage(getString(R.string.locked_message));
        builder.setPositiveButton(getString(R.string.ok), dialogClickListener);
        builder.show();
    }

    private void forgotPinAlert() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:

                        call("0709253000");
                        break;
                }
            }
        };


        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.confirm_action));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage("Kindly contact customer care for assistance. \n\nContact: 0709253000");
        builder.setPositiveButton("call", dialogClickListener);
        builder.show();
    }

    private void call(String phone) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phone));
        startActivity(intent);
    }

    private void optionsAlert() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:

                        Intent i = new Intent(context, LandingActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);

                        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);

                        Toasty.info(context, "Remember state changed", Toast.LENGTH_SHORT, true).show();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.confirm_action));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage("Reset the saved phone number");
        builder.setPositiveButton(getString(R.string.reset), dialogClickListener);
        builder.show();
    }

    private void notifyMaintenance(String message) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(true);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.ok), dialogClickListener);
        builder.show();
    }

    SendTo st = new SendTo(context);
    HurlStack hurlStack = new HurlStack() {
        @Override
        protected HttpURLConnection createConnection(URL url) throws IOException {
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
            try {
                httpsURLConnection.setSSLSocketFactory(st.getSSLSocketFactory());
                httpsURLConnection.setHostnameVerifier(st.getHostnameVerifier());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return httpsURLConnection;
        }
    };

    private void processLogin(String pin, String version, final int count) {
        loading = ProgressDialog.show(context, null, "Authenticating", false, false);

        String phone = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();
        final String imei = AppController.getInstance().getChamaPreferenceManager().getIMEI();

        final String requestTag = "login_pin_request";
        final String url = Config.LOGIN_URL;

        if (phone.substring(0, 3).equals("254"))
            phone = phone.substring(3);

        if (phone.substring(0, 1).equals("0"))
            phone = phone.substring(1);


        final Map<String, String> map = new HashMap<>();
        map.put("phone_number", "254" + phone);
        map.put("pin", pin);
        map.put("version", version);
        map.put("imei", imei);
        RequestQueue requestQueue = Volley.newRequestQueue(LoginPinChamaActivity.this);


        JsonObjectRequest loginReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(final JSONObject response) {
                        try {
                            loading.dismiss();
                            tvAttemptsLP.setVisibility(View.INVISIBLE);

                            if (count > 0) {
                                for (int i = 0; i < count; i++) {
                                    pinString.deleteCharAt(count - (1 + i));
                                    llPinLayoutLP.removeViewAt(count - (1 + i));
                                }
                            }

                            switch (response.getString("status")) {
                                case "success":

                                    // Check if the system is under maintenance
                                    if (response.getBoolean("shutdown_android")) {
                                        notifyMaintenance(response.getString("shutdown_message"));
                                        return;
                                    }

                                    // Save session token
                                    Config.sessionExpiry = response.getInt("device_session_expiry");
                                    String sessionToken = response.getString("token");
                                    if (!sessionToken.isEmpty())
                                        AppController.getInstance().getChamaPreferenceManager().setSessionToken(sessionToken);

                                    // Save user data
                                    AppController.getInstance().getChamaPreferenceManager().setPhoneNumber(map.get("phone_number"));
                                    AppController.getInstance().getChamaPreferenceManager().setPin(map.get("pin"));

                                    // Add groups
                                    JSONArray groupsArray = response.getJSONArray("groups");

                                    // Validate groups
                                    if (groupsArray.length() <= 0) {
                                        Toasty.info(context, "You do not exist in any active groups!", Toast.LENGTH_LONG, true).show();

                                        return;
                                    }

                                    if (groupsArray.length() > 0) {

                                        // Clear persistent groups
                                        groupViewModel.deleteAllGroups();

                                        for (int i = 0; i < groupsArray.length(); i++) {
                                            GroupEntity groupEntity = new GroupEntity();

                                            JSONObject groupObject = groupsArray.getJSONObject(i);

                                            groupEntity.setName(groupObject.getString("groupName"));
                                            groupEntity.setNumber(groupObject.getString("groupNumber"));
                                            groupEntity.setType(groupObject.getString("groupType"));

                                            groupViewModel.insertGroup(groupEntity);
                                        }
                                    }

                                    switch (response.getString("change_pin")) {
                                        case "0":

                                            // Check update status
                                            if (!response.getBoolean("is_update_due")) {
                                                versionCheck(response.getInt("version"), response.getString("update_message"), response.getBoolean("force_update"));
                                            } else {
                                                Toasty.info(context, "Login successful", Toast.LENGTH_SHORT, true).show();

                                                actionLogin();
                                            }
                                            break;
                                        case "1":
                                            Toasty.info(context, "Kindly verify your access details", Toast.LENGTH_LONG, true).show();

                                            if (count > 0) {
                                                for (int i = 0; i < count; i++) {
                                                    pinString.deleteCharAt(count - (1 + i));
                                                    llPinLayoutLP.removeViewAt(count - (1 + i));
                                                }
                                            }

                                            openChangePinActivity();
                                            break;
                                        default:
                                            Toasty.info(context, "This request could not be verified at the moment", Toast.LENGTH_LONG, true).show();
                                            break;
                                    }

                                    break;
                                case "failed":
                                    if (pinString.length() > 0) {
                                        for (int i = 0; i < count; i++) {
                                            pinString.deleteCharAt(count - (1 + i));
                                            llPinLayoutLP.removeViewAt(count - (1 + i));
                                        }
                                    }

                                    try {
                                        if (response.getString("response_code").equals("009")) {
                                            if (response.getString("attempts").equals("1")) {
                                                tvAttemptsLP.setText(response.getString("attempts") + " attempt remaining");
                                            } else {
                                                tvAttemptsLP.setText(response.getString("attempts") + " attempts remaining");
                                            }

                                            tvAttemptsLP.setVisibility(View.VISIBLE);
                                            Toasty.info(context, "Login failed. Invalid credentials", Toast.LENGTH_SHORT, true).show();

                                        } else if (response.getString("response_code").equals("012")) {

                                            tvAttemptsLP.setText("Your account has been locked");
                                            accountBlockedDialog(false);

                                        } else {
                                            Toasty.info(context, "Login failed. Invalid credentials", Toast.LENGTH_SHORT, true).show();
                                        }

                                    } catch (JSONException e) {
                                        Toasty.info(context, "An error occurred", Toast.LENGTH_LONG, true).show();

                                        if (pinString.length() > 0) {
                                            for (int i = 0; i < count; i++) {
                                                pinString.deleteCharAt(count - (1 + i));
                                                llPinLayoutLP.removeViewAt(count - (1 + i));
                                            }
                                        }
                                    }

                                    break;
                                default:
                                    Toasty.info(context, "Login failed", Toast.LENGTH_SHORT, true).show();

                                    if (pinString.length() > 0) {
                                        for (int i = 0; i < count; i++) {
                                            pinString.deleteCharAt(count - (1 + i));
                                            llPinLayoutLP.removeViewAt(count - (1 + i));
                                        }
                                    }

                                    break;
                            }

                        } catch (JSONException e) {
                            Toasty.info(context, "An error occurred", Toast.LENGTH_LONG, true).show();

                            if (pinString.length() > 0) {
                                for (int i = 0; i < count; i++) {
                                    pinString.deleteCharAt(count - (1 + i));
                                    llPinLayoutLP.removeViewAt(count - (1 + i));
                                }
                            }

                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                loading.dismiss();

                if (pinString.length() > 0) {
                    for (int i = 0; i < count; i++) {
                        pinString.deleteCharAt(count - (1 + i));
                        llPinLayoutLP.removeViewAt(count - (1 + i));
                    }
                }


                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error.")) {
                                    Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":
                                /* Validation errors - Sample response
                                  {
                                      "status": "fatal",
                                        "errors": [{
                                          "location": "body",
                                          "param": "agent_number",
                                          "msg": "Validation failed | CO::00-1"
                                        }]
                                 }
                                 */

                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();

                                        break;

                                    default:
                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } else {

                    if (error instanceof NoConnectionError) {
                        Toasty.info(context, "The service is currently unavailable.", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof TimeoutError) {
                        Toasty.info(context, "The request has timed out.", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof AuthFailureError) {
                        Toasty.info(context, "Authentication failed", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof ServerError) {
                        Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof NetworkError) {
                        Toasty.info(context, "Network error.", Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof ParseError) {
                        Toasty.info(context, "Network error.", Toast.LENGTH_LONG, true).show();
                    } else {
                        Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    }
                }
            }
        });

        loginReq.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));
        requestQueue.add(loginReq);

    }
}

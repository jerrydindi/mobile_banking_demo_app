package ke.co.payconnect.mocash.chama.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.entity.ApprovalFullStatementEntity;

@Dao
public interface ApprovalFullStatementDao {

    @Query("SELECT uid, member_phone_number, narration, request_date, approval_id, approval_status, approval_type, approval_narration, approval_date FROM ApprovalFullStatementEntity WHERE approval_type == :approvalType ORDER BY uid DESC")
    LiveData<List<ApprovalFullStatementEntity>> getApprovalFullStatementsByTypeLive(final String approvalType);

    @Query("SELECT uid, member_phone_number, narration, request_date, approval_id, approval_status, approval_type, approval_narration, approval_date FROM ApprovalFullStatementEntity WHERE approval_id == :approvalId ORDER BY uid DESC")
    ApprovalFullStatementEntity findFullStatementApprovalById(String approvalId);

    @Query("SELECT EXISTS(SELECT 1 FROM ApprovalFullStatementEntity WHERE approval_id == :approvalId LIMIT 1)")
    boolean doesFullStatementApprovalExistWithApprovalId(String approvalId);

    @Query("UPDATE ApprovalFullStatementEntity SET approval_status = :approvalStatus, approval_narration = :approvalNarration, approval_date = :approvalDate WHERE approval_id == :approvalID")
    void updateApprovalFullStatementSetData(final String approvalID, final String approvalStatus, final String approvalNarration, final String approvalDate);

    @Insert
    void insertApprovalFullStatement(ApprovalFullStatementEntity... approvalFullStatementEntities);

    @Query("DELETE FROM ApprovalFullStatementEntity")
    void deleteAllApprovalFullStatements();
}

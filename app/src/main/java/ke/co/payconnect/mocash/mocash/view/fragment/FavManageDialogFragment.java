package ke.co.payconnect.mocash.mocash.view.fragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

import es.dmoral.toasty.Toasty;
import ke.co.payconnect.mocash.R;

public class FavManageDialogFragment extends DialogFragment {

    // Views
    private TextView tvFavTitle;
    private EditText etFavAccountNameAdd, etFavAccountNumberAdd, etFavPhoneNumberAdd;
    private Button btFavSubmit;
    private Button btFavCancel;

    private FavManageListener listener;

    private String tag = "add";

    // Empty constructor (required)
    public FavManageDialogFragment() {

    }

    public static FavManageDialogFragment newInstance(String tag, int uid, String accountName, String accountNumber, String phoneNumber) {

        FavManageDialogFragment frag = new FavManageDialogFragment();
        Bundle args = new Bundle();
        args.putString("tag", tag);
        args.putInt("uid", uid);
        args.putString("accountName", accountName);
        args.putString("accountNumber", accountNumber);
        args.putString("phoneNumber", phoneNumber);
        frag.setArguments(args);

        return frag;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            // Verify that the host activity implements the callback interface
            listener = (FavManageListener) context;

        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString() + " must implement FavManageListener");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fav_manage_dialog, container);

        etFavAccountNameAdd = view.findViewById(R.id.etFavAccountNameAdd);
        etFavAccountNumberAdd = view.findViewById(R.id.etFavAccountNumberAdd);
        etFavPhoneNumberAdd = view.findViewById(R.id.etFavPhoneNumberAdd);
        btFavSubmit = view.findViewById(R.id.btFavSubmit);
        btFavCancel = view.findViewById(R.id.btFavCancel);
        tvFavTitle = view.findViewById(R.id.tvFavTitle);

        if (getArguments() != null) {
            tag = getArguments().getString("tag");
            if (Objects.equals(tag, "edit")) {

                tvFavTitle.setText(getString(R.string.fav_edit));
                etFavAccountNameAdd.setText(getArguments().getString("accountName"));
                etFavAccountNumberAdd.setText(getArguments().getString("accountNumber"));
                etFavPhoneNumberAdd.setText(getArguments().getString("phoneNumber"));
            }
        }

        btFavSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String accName = etFavAccountNameAdd.getText().toString().trim();
                String accNumber = etFavAccountNumberAdd.getText().toString().trim();
                String phone = etFavPhoneNumberAdd.getText().toString().trim();

                if (accName.isEmpty() || accNumber.isEmpty() || phone.isEmpty()) {
                    FavManageDialogFragment.this.dismiss();
                    return;
                }

                getDialog().cancel();
                if (tag.equals("add")) {
                    listener.onManageFavDialog(true, "add", getArguments().getInt("uid"), accName, accNumber, phone); // For add, the uid will not be used. Default 0

                    if(getContext() != null)
                        Toasty.info(getContext(), "A new contact has been added", Toast.LENGTH_SHORT, true).show();

                } else if (tag.equals("edit")) {
                    listener.onManageFavDialog(true, "edit", getArguments().getInt("uid"), accName, accNumber, phone);
                }
            }
        });

        btFavCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().cancel();
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public interface FavManageListener {
        void onManageFavDialog(boolean isSelected, String tag, int uid, String accountName, String accountNumber, String phoneNumber);
    }

}

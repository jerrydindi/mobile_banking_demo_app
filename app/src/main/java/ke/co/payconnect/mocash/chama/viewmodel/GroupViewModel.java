package ke.co.payconnect.mocash.chama.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.ChamaDatabase;
import ke.co.payconnect.mocash.chama.data.entity.GroupEntity;

public class GroupViewModel extends AndroidViewModel {

    private final LiveData<List<GroupEntity>> accountList;
    private ChamaDatabase db;

    public GroupViewModel(@NonNull Application application) {
        super(application);

        db = ChamaDatabase.getAppDatabaseInstance(this.getApplication());
        accountList = db.groupDao().getAllGroupsLive();
    }

    public LiveData<List<GroupEntity>> getaccountList(){
        return accountList;
    }

    public void insertGroup(GroupEntity accountEntity){
        db.groupDao().insertGroup(accountEntity);
    }

    public void updateGroup(GroupEntity accountEntity){
        db.groupDao().updateGroup(accountEntity);
    }

    public void deleteGroup(GroupEntity accountEntity){
        db.groupDao().deleteGroup(accountEntity);
    }

    public void deleteAllGroups(){
        db.groupDao().deleteAllGroups();
    }
}

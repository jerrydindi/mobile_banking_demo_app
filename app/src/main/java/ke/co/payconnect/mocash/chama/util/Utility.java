package ke.co.payconnect.mocash.chama.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;

import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.zip.CRC32;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import io.captano.utility.Time;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;

public class Utility {
    private static Context context;


    public static String getBatchNumber() {
        String phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();
        String time = Time.getDateTimeNow("yyyyMMddHHmmssSSS");

        return "B" + shrink(phoneNumber + time) + String.valueOf(randomize()).toUpperCase(); // B3122705853A
    }

    public static String getTxnID() {
        String phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();
        String time = Time.getDateTimeNow("yyyyMMddHHmmssSSS");

        return "T" + shrink(phoneNumber + time) + String.valueOf(randomize()).toUpperCase(); // T3122705853Z
    }

    private static long shrink(String input) {
        CRC32 crc = new CRC32();
        crc.update(input.getBytes());
        return crc.getValue();
    }

    private static char randomize() {
        Random rnd = new Random();
        return (char) (rnd.nextInt(26) + 'A');
    }

    public static Calendar getDateTime(String dateTimeRepresentation, String format) throws ParseException {
        @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat(format);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateFormat.parse(dateTimeRepresentation));

        return calendar;
    }

    public static String getDate(String dateTimeRepresentation, String format) throws ParseException {
        SimpleDateFormat sdf1 = new SimpleDateFormat(format);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd yyyy");
        Date date = sdf1.parse(dateTimeRepresentation);

        return simpleDateFormat.format(date);
    }

    private static boolean isDateSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }

    public static boolean isDateToday(Calendar cal) throws IllegalArgumentException {
        return isDateSameDay(cal, Calendar.getInstance());
    }

    public static boolean isDateYesterday(Calendar calendar) {
        Calendar nowTime = Calendar.getInstance();

        if (nowTime.get(Calendar.DATE) - calendar.get(Calendar.DATE) == 1)
            return true;
        else
            return false;
    }

    public static boolean isDeviceSupported(Context context) {
        // Check if 'google play services' is installed
        final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(context);

        if (resultCode != ConnectionResult.SUCCESS) {

            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog((Activity) context, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            }

            return false;
        }

        return true;
    }

    public static void registerNotificationChannel(NotificationManager notificationManager) {

        String NOTIFICATION_CHANNEL_ID = "101";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant")
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Notification", NotificationManager.IMPORTANCE_MAX);

            //Configure Notification Channel
            notificationChannel.setDescription("Unaitas Mocash");
            notificationChannel.enableLights(true);
            notificationChannel.setImportance(NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setVibrationPattern(new long[]{500, 500});
            notificationChannel.enableVibration(true);

            notificationManager.createNotificationChannel(notificationChannel);
        }
    }

    public static void getDeviceFCMTokens(final FirebaseFirestore firebaseFirestore, final JSONObject data) {
        final String groupNumber = AppController.getInstance().getChamaPreferenceManager().getGroupNumber();
        final String phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();

        firebaseFirestore.collection("token")
                .whereEqualTo("groupNumber", groupNumber)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot querySnapshot) {
                        final List<Token> tokens = querySnapshot.toObjects(Token.class);

                        if (tokens.size() > 0) {

                            new Thread() {
                                @Override
                                public void run() {

                                    for (Token token : tokens) {
                                        if (!token.getPhoneNumber().equals(phoneNumber))
                                            sendFCMNotification(data, token.getFcmToken());
                                    }
                                }
                            }.start();

                        }

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {


                    }
                });
    }

    private static void sendFCMNotification(final JSONObject data, final String destinationFCMKey) {
        final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json = new JSONObject();
                    json.put("priority", "high");
                    json.put("data", data);
                    json.put("to", destinationFCMKey);

                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization", "key=" + Config.FIREBASE_FCM_LEGACY_SERVER_KEY)
                            .url("https://fcm.googleapis.com/fcm/send")
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                } catch (Exception ignored) {
                }
                return null;
            }
        }.execute();

    }

    public static void sendDeviceFCMToken(final String deviceFCMToken, FirebaseFirestore firebaseFirestore) {
        String phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();
        String groupNumber = AppController.getInstance().getChamaPreferenceManager().getGroupNumber();

        String dateTime = Time.getDateTimeNow("yyyy-MM-dd HH:mm:ss");

        HashMap<String, String> map = new HashMap<>();
        map.put("uid", phoneNumber);
        map.put("fcmToken", deviceFCMToken);
        map.put("phoneNumber", phoneNumber);
        map.put("groupNumber", groupNumber);
        map.put("date", dateTime);

        CollectionReference collectionReference = firebaseFirestore.collection("token");
        collectionReference.document(phoneNumber)
                .set(map)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {


                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {


                    }
                });
    }

    public static void updateDeviceFCMToken(final String deviceFCMToken, FirebaseFirestore firebaseFirestore) {
        String phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();

        DocumentReference documentReference = firebaseFirestore.collection("token").document(phoneNumber);
        documentReference
                .update("fcm_token", deviceFCMToken)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {


                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {


                    }
                });
    }

class Token {
    private String uid;
    private String fcmToken;
    private String phoneNumber;
    private String groupNumber;
    private String date;

    public Token() {
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(String groupNumber) {
        this.groupNumber = groupNumber;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
}

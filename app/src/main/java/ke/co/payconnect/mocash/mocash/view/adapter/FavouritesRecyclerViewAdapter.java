package ke.co.payconnect.mocash.mocash.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;

import java.util.List;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.mocash.data.entity.FavEntity;
import ke.co.payconnect.mocash.mocash.view.fragment.ConfirmActionDialogFragment;
import ke.co.payconnect.mocash.mocash.view.fragment.FavManageAccountsDialogFragment;
import ke.co.payconnect.mocash.mocash.view.fragment.FavManageDialogFragment;

public class FavouritesRecyclerViewAdapter extends RecyclerView.Adapter<FavouritesRecyclerViewAdapter.FavsHolder> {
    private final Context context;
    private final List<FavEntity> favList;
    private final String tag;

    public FavouritesRecyclerViewAdapter(Context context, List<FavEntity> favList, String tag) {
        this.favList = favList;
        this.context = context;
        this.tag = tag;
    }

    @NonNull
    @Override
    public FavsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_fav, parent, false);
        return new FavsHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull FavsHolder holder, int position) {
        FavEntity fav = favList.get(position);

        holder.tvFavAccountName.setText(fav.getAccountName());
        holder.tvFavAccountNumber.setText(fav.getAccountNumber());

        if (fav.getPhoneNumber().equals("-")) {
            // Account
            holder.btFavTag.setVisibility(View.VISIBLE);
        } else {
            // Contact
            holder.btFavTag.setVisibility(View.GONE);
            holder.tvFavPhoneNumber.setText(fav.getPhoneNumber());
        }

        int colour = context.getResources().getColor(R.color.colorPrimary);

        holder.tdRoundIcon = TextDrawable.builder()
                .beginConfig()
                .endConfig()
                .buildRoundRect(fav.getAccountName().substring(0, 1).toUpperCase(), colour, 100);

        holder.ivFavIcon.setImageDrawable(holder.tdRoundIcon);

        final int uid = fav.getUid();
        final String accountName = fav.getAccountName();
        final String accountNumber = fav.getAccountNumber();
        final String phoneNumber = fav.getPhoneNumber();

        if (tag.equals("select")) {
            holder.ivFavEdit.setEnabled(false);
            holder.ivFavEdit.setVisibility(View.GONE);

            holder.ivFavRemove.setEnabled(false);
            holder.ivFavRemove.setVisibility(View.GONE);


            // Listeners
            holder.rlFav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Pass the selected device to the active trip activity
                    Intent intent = new Intent("INTENT_FAV");
                    intent.putExtra("accountName", accountName);
                    intent.putExtra("accountNumber", accountNumber);
                    intent.putExtra("phoneNumber", phoneNumber);

                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

                }
            });
        } else if (tag.equals("manage")) {

            holder.ivFavEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (phoneNumber.equals("-")) {
                        FragmentManager fm = ((FragmentActivity) context).getSupportFragmentManager();
                        FavManageAccountsDialogFragment favManageAccountsDialogFragment = FavManageAccountsDialogFragment.newInstance("edit_acc", uid, accountName, accountNumber);
                        favManageAccountsDialogFragment.show(fm, "dialog");
                    } else {
                        FragmentManager fm = ((FragmentActivity) context).getSupportFragmentManager();
                        FavManageDialogFragment favManageDialogFragment = FavManageDialogFragment.newInstance("edit", uid, accountName, accountNumber, phoneNumber);
                        favManageDialogFragment.show(fm, "dialog");
                    }
                }
            });

            holder.ivFavRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Activity activity = (Activity) context;
                    FragmentManager fm = ((FragmentActivity) activity).getSupportFragmentManager();
                    ConfirmActionDialogFragment editNameDialogFragment = new ConfirmActionDialogFragment().newInstance(R.string.confirm_action, "FAV_DELETE", uid, accountName, accountNumber, phoneNumber, "");
                    editNameDialogFragment.show(fm, "fragment_edit_name");

                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return favList.size();
    }

    class FavsHolder extends RecyclerView.ViewHolder {
        private RelativeLayout rlFav;
        private TextView tvFavAccountName;
        private TextView tvFavAccountNumber;
        private TextView tvFavPhoneNumber;
        private TextView btFavTag;
        private ImageView ivFavIcon, ivFavEdit, ivFavRemove;
        private TextDrawable tdRoundIcon;

        FavsHolder(View view) {
            super(view);

            rlFav = view.findViewById(R.id.rlFav);
            tvFavAccountName = view.findViewById(R.id.tvFavAccountName);
            tvFavAccountNumber = view.findViewById(R.id.tvFavAccountNumber);
            tvFavPhoneNumber = view.findViewById(R.id.tvFavPhoneNumber);
            ivFavIcon = view.findViewById(R.id.ivFavIcon);
            ivFavEdit = view.findViewById(R.id.ivFavEdit);
            ivFavRemove = view.findViewById(R.id.ivFavRemove);
            btFavTag = view.findViewById(R.id.btFavTag);

        }
    }
}

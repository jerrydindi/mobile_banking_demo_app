package ke.co.payconnect.mocash.mocash.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import es.dmoral.toasty.Toasty;
import ke.co.payconnect.mocash.R;

public class LoanRepaymentDialogFragment extends DialogFragment {

    // Views
    private EditText etAmountLoanRepayment;
    private Button btSendLoanRepayment;
    private Button btCancelLoanRepayment;

    private loanRepaymentListener listener;

    private String tag = "";

    // Empty constructor (required)
    public LoanRepaymentDialogFragment() {

    }

    public static LoanRepaymentDialogFragment newInstance(String tag, String amount) {

        LoanRepaymentDialogFragment fragment = new LoanRepaymentDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("tag", tag);
        bundle.putString("amount", amount);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (loanRepaymentListener) context;

        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement loanRepaymentListener");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fav_manage_accounts_dialog, container);

        etAmountLoanRepayment = view.findViewById(R.id.etAmountLoanRepayment);
        btSendLoanRepayment = view.findViewById(R.id.btSendLoanRepayment);
        btCancelLoanRepayment = view.findViewById(R.id.btCancelLoanRepayment);

        if (getArguments() != null) {
            tag = getArguments().getString("tag");
        }

        btSendLoanRepayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!tag.isEmpty()) {
                    String amount = etAmountLoanRepayment.getText().toString().trim();

                    if (amount.isEmpty()) {
                        LoanRepaymentDialogFragment.this.dismiss();
                        return;
                    }

                    getDialog().cancel();
                    listener.onLoanRepaymentDialog(true, tag, amount);
                }else{
                    Toasty.info(getContext(), "Could not process your request at the moment", Toast.LENGTH_SHORT, true).show();
                }

            }
        });

        btCancelLoanRepayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().cancel();
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public interface loanRepaymentListener {
        void onLoanRepaymentDialog(boolean isSuccessful, String tag, String amount);
    }

}

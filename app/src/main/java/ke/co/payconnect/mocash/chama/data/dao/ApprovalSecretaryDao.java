package ke.co.payconnect.mocash.chama.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.entity.ApprovalSecretaryEntity;

@Dao
public interface ApprovalSecretaryDao {

    @Query("SELECT uid, current_secretary_phone_number, proposed_secretary_phone_number, narration, assign_date, approval_id, approval_status, approval_type, approval_narration, approval_date FROM ApprovalSecretaryEntity ORDER BY approval_id ASC")
    LiveData<List<ApprovalSecretaryEntity>> getApprovalSecretariesLive();

    @Query("SELECT uid, current_secretary_phone_number, proposed_secretary_phone_number, narration, assign_date, approval_id, approval_status, approval_type, approval_narration, approval_date FROM ApprovalSecretaryEntity WHERE approval_id == :approvalID ORDER BY approval_id ASC")
    LiveData<List<ApprovalSecretaryEntity>> getApprovalSecretariesByApprovalIDLive(final String approvalID);

    @Query("SELECT uid, current_secretary_phone_number, proposed_secretary_phone_number, narration, assign_date, approval_id, approval_status, approval_type, approval_narration, approval_date FROM ApprovalSecretaryEntity WHERE approval_type == :approvalType ORDER BY uid DESC")
    LiveData<List<ApprovalSecretaryEntity>> getApprovalSecretariesByTypeLive(final String approvalType);

    @Query("SELECT uid, current_secretary_phone_number, proposed_secretary_phone_number, narration, assign_date, approval_id, approval_status, approval_type, approval_narration, approval_date FROM ApprovalSecretaryEntity WHERE approval_id == :approvalId ORDER BY uid DESC")
    ApprovalSecretaryEntity findSecretaryApprovalById(String approvalId);

    @Query("SELECT EXISTS(SELECT 1 FROM ApprovalSecretaryEntity WHERE approval_id == :approvalId LIMIT 1)")
    boolean doesSecretaryApprovalExistWithApprovalId(String approvalId);

    @Query("UPDATE ApprovalSecretaryEntity SET approval_status = :approvalStatus, approval_narration = :approvalNarration, approval_date = :approvalDate WHERE approval_id == :approvalID")
    void updateApprovalSecretarySetData(final String approvalID, final String approvalStatus, final String approvalNarration, final String approvalDate);

    @Query("SELECT COUNT(*) FROM ApprovalSecretaryEntity")
    int getApprovalSecretariesCount();

    @Insert
    void insertApprovalSecretary(ApprovalSecretaryEntity... approvalSecretaryEntities);

    @Update
    void updateApprovalSecretary(ApprovalSecretaryEntity... approvalSecretaryEntities);

    @Delete
    void deleteApprovalSecretary(ApprovalSecretaryEntity... approvalSecretaryEntities);

    @Query("DELETE FROM ApprovalSecretaryEntity")
    void deleteAllApprovalSecretaries();
}

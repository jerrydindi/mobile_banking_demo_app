package ke.co.payconnect.mocash.mocash.view.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.mocash.data.entity.EmployerEntity;


public class EmployerSpinnerAdapter extends ArrayAdapter<String> {
    private Context context;

    private LayoutInflater inflater;
    private List<EmployerEntity> employers;

    public EmployerSpinnerAdapter(@NonNull Context context, @NonNull List employers) {
        super(context, 0, employers);

        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.employers = employers;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        return getImageForPosition(position, convertView, parent);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        return getImageForPosition(position, convertView, parent);
    }

    private View getImageForPosition(int position, View convertView, ViewGroup parent) {

        EmployerEntity emplyer = employers.get(position);

        View row = inflater.inflate(R.layout.struct_sp_employers, parent, false);

        final TextView tvEmployerNameSpin = row.findViewById(R.id.tvEmployerNameSpin);
        tvEmployerNameSpin.setText(emplyer.getEmployerName());

        final TextView tvEmployerIDSpin = row.findViewById(R.id.tvEmployerIDSpin);
        tvEmployerIDSpin.setText(emplyer.getEmployerID());


        return row;
    }
}

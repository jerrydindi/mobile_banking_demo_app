package ke.co.payconnect.mocash.chama.view.adapter.member;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;
import ke.co.payconnect.mocash.chama.data.entity.MessageEntity;
import ke.co.payconnect.mocash.chama.util.Utility;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;


public class ChatRecyclerViewAdapter extends RecyclerView.Adapter<ChatRecyclerViewAdapter.MessagesHolder> {
    private static final int MESSAGE_TYPE_LEFT = 0;
    private static final int MESSAGE_TYPE_RIGHT = 1;

    private final Context context;
    private final List<MessageEntity> messageList;

    private MemberViewModel memberViewModel;

    public ChatRecyclerViewAdapter(Context context, List<MessageEntity> messageList) {
        this.context = context;
        this.messageList = messageList;

        memberViewModel = ViewModelProviders.of((FragmentActivity) context).get(MemberViewModel.class);
    }

    @NonNull
    @Override
    public MessagesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        if (viewType == MESSAGE_TYPE_RIGHT)
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_msg_right, parent, false);
        else
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_msg_left, parent, false);
        return new MessagesHolder(v);

    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    @Override
    public int getItemViewType(int position) {
        final MessageEntity message = messageList.get(position);
        String phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();

        if (message.getSenderPhoneNumber().equals(phoneNumber))
            return MESSAGE_TYPE_RIGHT;
        else
            return MESSAGE_TYPE_LEFT;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final MessagesHolder holder, final int position) {
        try {
            final MessageEntity messageEntity = messageList.get(position);

            int colour = context.getResources().getColor(R.color.cG4);
            int textColour = context.getResources().getColor(R.color.cG2);
            String senderName;

            // Validate sender
            if (!memberViewModel.doesMemberExistWithPhoneNumber(messageEntity.getSenderPhoneNumber())) {
                senderName = "Deleted Member";

                holder.tdRoundIcon = TextDrawable.builder()
                        .beginConfig()
                        .textColor(textColour)
                        .toUpperCase()
                        .endConfig()
                        .buildRound("DM", colour);
            } else {
                MemberEntity memberEntity = memberViewModel.findMemberByPhoneNumber(messageEntity.getSenderPhoneNumber());
                senderName = memberEntity.getFirstName() + " " + memberEntity.getLastName();

                holder.tdRoundIcon = TextDrawable.builder()
                        .beginConfig()
                        .textColor(textColour)
                        .toUpperCase()
                        .endConfig()
                        .buildRound(memberEntity.getFirstName().substring(0, 1).toUpperCase() + memberEntity.getLastName().substring(0, 1).toUpperCase(), colour);
            }

            holder.ivSenderStructMsg.setImageDrawable(holder.tdRoundIcon);

            holder.tvSenderStructMsg.setText(senderName);
            holder.tvMessageStructMsg.setText(messageEntity.getMessage());

            Calendar calendar;
            String time;
            try {
                calendar = Utility.getDateTime(messageEntity.getDateAdded(), "yyyy-MM-dd HH:mm");
                @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat("HH:mm");
                time = dateFormat.format(calendar.getTime());
                if (Utility.isDateToday(calendar)) {
                    holder.tvDateStructMsg.setText(time);
                } else if (Utility.isDateYesterday(calendar)) {
                    holder.tvDateStructMsg.setText("Yesterday, " + time);
                } else {
                    String formattedDate = Utility.getDate(messageEntity.getDateAdded(), "yyyy-MM-dd HH:mm");
                    holder.tvDateStructMsg.setText(formattedDate + "\n" + time);
                }
            } catch (ParseException e) {
                holder.tvDateStructMsg.setText(messageEntity.getDateAdded());
            }


            holder.cvStructMsg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                }
            });

            // Post recycle validation
            if (holder.tvMessageStructMsg.getText().equals("Waiting for message"))
                holder.tvMessageStructMsg.setTextColor(context.getResources().getColor(R.color.cR3));
            //holder.tvMessageStructMsg.setTextSize(12);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    class MessagesHolder extends RecyclerView.ViewHolder {
        private CardView cvStructMsg;
        private ImageView ivSenderStructMsg;
        private TextView tvSenderStructMsg;
        private TextView tvMessageStructMsg;
        private TextView tvDateStructMsg;
        private TextDrawable tdRoundIcon;

        MessagesHolder(View view) {
            super(view);

            cvStructMsg = view.findViewById(R.id.cvStructMsg);
            ivSenderStructMsg = view.findViewById(R.id.ivSenderStructMsg);
            tvSenderStructMsg = view.findViewById(R.id.tvSenderStructMsg);
            tvMessageStructMsg = view.findViewById(R.id.tvMessageStructMsg);
            tvDateStructMsg = view.findViewById(R.id.tvDateStructMsg);
        }
    }

}

package ke.co.payconnect.mocash.chama.view.activity.member;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Connection;
import io.captano.utility.ui.UIController;
import io.captano.utility.ui.UIHandler;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.util.Access;
import ke.co.payconnect.mocash.chama.util.Config;
import ke.co.payconnect.mocash.chama.util.SendTo;
import ke.co.payconnect.mocash.chama.view.activity.LoginChamaActivity;

import static ke.co.payconnect.mocash.chama.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.chama.util.Config.RETRIES;
import static ke.co.payconnect.mocash.chama.util.Config.TIMEOUT_MS;

@SuppressWarnings("FieldCanBeLocal")
public class ChangePinChamaActivity extends AppCompatActivity {
    private final Context context = this;
    private final Activity activity = this;

    // Views
    private Toolbar tbChangePinChama;
    private EditText etPinCurrentChangePinChama;
    private EditText etPinNewChangePinChama;
    private EditText etPinConfirmNewChangePinChama;
    private CardView cvSendChangePinChama;
    private TextView tvConnectivityStatusChangePinChama;

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if (isConnected)
                tvConnectivityStatusChangePinChama.setVisibility(View.GONE);
            else
                tvConnectivityStatusChangePinChama.setVisibility(View.VISIBLE);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pin_chama);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        } else {
            registerReceiver(connectionReceiver, intentFilter);
        }

        tbChangePinChama = findViewById(R.id.tbChangePinChama);
        setSupportActionBar(tbChangePinChama);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbChangePinChama.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        etPinCurrentChangePinChama = findViewById(R.id.etPinCurrentChangePinChama);
        etPinNewChangePinChama = findViewById(R.id.etPinNewChangePinChama);
        etPinConfirmNewChangePinChama = findViewById(R.id.etPinConfirmNewChangePinChama);
        tvConnectivityStatusChangePinChama = findViewById(R.id.tvConnectivityStatusChangePinChama);
        cvSendChangePinChama = findViewById(R.id.cvSendChangePinChama);

        setListeners();
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void setListeners() {
        cvSendChangePinChama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvConnectivityStatusChangePinChama.getVisibility() == View.VISIBLE) {
                    Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                } else {
                    if (new Connection(context).IsInternetConnected())
                        validation(etPinCurrentChangePinChama.getText().toString(), etPinNewChangePinChama.getText().toString(), etPinConfirmNewChangePinChama.getText().toString());
                    else
                        Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                }


            }
        });
    }

    private void validation(String currentPin, String newPin, String newPinConfirm) {
        if (currentPin == null || currentPin.isEmpty()) {
            Toasty.info(context, "Please enter a valid pin", Toast.LENGTH_SHORT, true).show();

            etPinCurrentChangePinChama.getText().clear();
            etPinNewChangePinChama.getText().clear();
            etPinConfirmNewChangePinChama.getText().clear();
            return;
        } else {

            // New pin validation
            if (newPin == null || newPin.isEmpty()) {
                Toasty.info(context, "Please enter a valid pin", Toast.LENGTH_SHORT, true).show();

                etPinCurrentChangePinChama.getText().clear();
                etPinNewChangePinChama.getText().clear();
                etPinConfirmNewChangePinChama.getText().clear();
                return;
            } else {
                if (!(newPin.length() == 4)) {
                    Toasty.info(context, "Please enter a valid new pin", Toast.LENGTH_SHORT, true).show();

                    etPinCurrentChangePinChama.getText().clear();
                    etPinNewChangePinChama.getText().clear();
                    etPinConfirmNewChangePinChama.getText().clear();
                    return;
                }
            }

            // New pin confirmation validation
            if (newPinConfirm == null || newPinConfirm.isEmpty()) {
                Toasty.info(context, "Please enter a valid new pin confirmation", Toast.LENGTH_SHORT, true).show();

                etPinCurrentChangePinChama.getText().clear();
                etPinNewChangePinChama.getText().clear();
                etPinConfirmNewChangePinChama.getText().clear();
                return;
            } else {
                if (!(newPinConfirm.length() == 4)) {
                    Toasty.info(context, "Please enter a valid new pin confirmation", Toast.LENGTH_SHORT, true).show();

                    etPinCurrentChangePinChama.getText().clear();
                    etPinNewChangePinChama.getText().clear();
                    etPinConfirmNewChangePinChama.getText().clear();
                    return;
                }
            }

            // New pin & confirmation pin validation
            if (!newPin.equals(newPinConfirm)) {
                Toasty.info(context, "Your new pin does not match the confirmation entry", Toast.LENGTH_SHORT, true).show();

                etPinCurrentChangePinChama.getText().clear();
                etPinNewChangePinChama.getText().clear();
                etPinConfirmNewChangePinChama.getText().clear();
                return;
            } else {
                if (newPin.trim().length() != 4) {
                    Toasty.info(context, "Please enter a valid pin", Toast.LENGTH_SHORT, true).show();

                    etPinCurrentChangePinChama.getText().clear();
                    etPinNewChangePinChama.getText().clear();
                    etPinConfirmNewChangePinChama.getText().clear();
                    return;
                }
            }
        }

        String pin = AppController.getInstance().getChamaPreferenceManager().getPin();
        if (pin == null) {
            etPinCurrentChangePinChama.getText().clear();
            etPinNewChangePinChama.getText().clear();
            etPinConfirmNewChangePinChama.getText().clear();

            // user not found
            exitAlert();
            return;
        }

        String savedPin = pin;
        if (savedPin != null) {
            if (!savedPin.equals(currentPin)) {
                Toasty.info(context, "Please enter a valid pin", Toast.LENGTH_SHORT, true).show();

                etPinCurrentChangePinChama.getText().clear();
                etPinNewChangePinChama.getText().clear();
                etPinConfirmNewChangePinChama.getText().clear();

                return;
            }
        } else {
            etPinCurrentChangePinChama.getText().clear();
            etPinNewChangePinChama.getText().clear();
            etPinConfirmNewChangePinChama.getText().clear();

            // Pin not found
            exitAlert();
            return;
        }

        // Proceed
        String phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();
        final String sessionToken = AppController.getInstance().getChamaPreferenceManager().getSessionToken();

        HashMap<String, String> map = new HashMap<>();
        map.put("phone_number", phoneNumber);
        map.put("current_pin", currentPin);
        map.put("new_pin", newPin);

        processRequest(map, sessionToken);
    }

    private void notifySessionExpired(String message) {
        Access.forceSessionExpiry();

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        boolean stateRemember = AppController.getInstance().getChamaPreferenceManager().getRememberPhone();
                        Access.logout(activity, stateRemember, false);

                        Toasty.info(context, R.string.logout_message_post, Toast.LENGTH_SHORT, true).show();
                        break;
                }
            }
        };
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(false);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_unaitas);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.sign_in), dialogClickListener);
        builder.show();
    }

    private void exitAlert() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        new UIController().redirect(activity, LoginChamaActivity.class);
                        break;

                }

            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_unaitas);
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.proceed), dialogClickListener);
        builder.setMessage("Your session has timed out. Kindly sign-in again to be able to change your pin");
        builder.show();

    }

    private void responseAlert(String message) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        new UIController().redirect(activity, LoginChamaActivity.class);
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_unaitas);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.ok), dialogClickListener);
        builder.show();
    }

    SendTo st = new SendTo(context);
    HurlStack hurlStack = new HurlStack() {
        @Override
        protected HttpURLConnection createConnection(URL url) throws IOException {
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
            try {
                httpsURLConnection.setSSLSocketFactory(st.getSSLSocketFactory());
                httpsURLConnection.setHostnameVerifier(st.getHostnameVerifier());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return httpsURLConnection;
        }
    };

    private void processRequest(final HashMap<String, String> map, final String sessionToken) {

        final ProgressDialog loading = ProgressDialog.show(this, null, "Processing request...", false, false);

        final String url = Config.CHANGE_PIN_URL;

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        JsonObjectRequest changePinReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            loading.dismiss();

                            switch (response.getString("status")) {
                                case "success":
                                    responseAlert(response.getString("message") + ".\nKindly sign-in again");
                                    break;

                                case "failed":
                                    Toasty.info(context, "Change pin request failed", Toast.LENGTH_LONG, true).show();
                                    break;

                                default:
                                    Toasty.info(context, "The request could not be completed at this moment", Toast.LENGTH_LONG, true).show();
                                    break;
                            }

                        } catch (JSONException e) {
                            loading.dismiss();
                            Toasty.info(context, "An error occurred", Toast.LENGTH_LONG, true).show();

                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 403:
                            notifySessionExpired("Authorization failed: Your session has expired. Kindly sign-in to access this option");
                            break;
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error.")) {
                                    Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":
                                    default:
                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException | UnsupportedEncodingException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } else {
                    if (error instanceof NoConnectionError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, "The service is currently unavailable.", Toast.LENGTH_SHORT, true).show();
                            }
                        });
                    } else if (error instanceof TimeoutError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, "The request has timed out.", Toast.LENGTH_SHORT, true).show();
                            }
                        });
                    } else if (error instanceof AuthFailureError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, "Authentication failed", Toast.LENGTH_SHORT, true).show();
                            }
                        });
                    } else if (error instanceof ServerError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            }
                        });
                    } else if (error instanceof NetworkError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, "Network error.", Toast.LENGTH_LONG, true).show();
                            }
                        });
                    } else if (error instanceof ParseError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, "Network error.", Toast.LENGTH_LONG, true).show();
                            }
                        });
                    } else {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            }
                        });
                    }
                }
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionToken);

                return headers;
            }
        };

        changePinReq.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));

        // Adding request to queue
        requestQueue.add(changePinReq);
    }
}
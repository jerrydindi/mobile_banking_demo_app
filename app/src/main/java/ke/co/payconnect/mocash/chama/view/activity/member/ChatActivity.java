package ke.co.payconnect.mocash.chama.view.activity.member;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Connection;
import io.captano.utility.Time;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.MessageEntity;
import ke.co.payconnect.mocash.chama.util.Access;
import ke.co.payconnect.mocash.chama.util.FireSync;
import ke.co.payconnect.mocash.chama.util.Utility;
import ke.co.payconnect.mocash.chama.view.adapter.member.ChatRecyclerViewAdapter;
import ke.co.payconnect.mocash.chama.viewmodel.MessageViewModel;

@SuppressWarnings("FieldCanBeLocal")
public class ChatActivity extends AppCompatActivity {
    private final Context context = this;
    private final Activity activity = this;

    // Views
    private Toolbar tbChat;
    private TextView tvConnectivityStatusTeacherStudentMessage;
    private RecyclerView rvChat;
    private TextView tvNoDataChat;
    private AutoCompleteTextView actvChat;
    private FloatingActionButton fabSendChat;
    private LinearLayoutManager layoutManager;

    public ChatRecyclerViewAdapter adapter;
    private MessageViewModel messageViewModel;

    private FirebaseFirestore firebaseFirestore;
    private FireSync fireSync;

    private String phoneNumber;
    private String groupNumber;

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork;
            if (cm != null) {
                activeNetwork = cm.getActiveNetworkInfo();

                boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

                if (isConnected)
                    tvConnectivityStatusTeacherStudentMessage.setVisibility(View.GONE);
                else
                    tvConnectivityStatusTeacherStudentMessage.setVisibility(View.VISIBLE);
            } else {
                tvConnectivityStatusTeacherStudentMessage.setVisibility(View.GONE);
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        else
            registerReceiver(connectionReceiver, intentFilter);

        tvConnectivityStatusTeacherStudentMessage = findViewById(R.id.tvConnectivityStatusTeacherStudentMessage);
        tbChat = findViewById(R.id.tbChat);
        setSupportActionBar(tbChat);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbChat.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        rvChat = findViewById(R.id.rvChat);
        tvNoDataChat = findViewById(R.id.tvNoDataChat);
        actvChat = findViewById(R.id.actvChat);
        fabSendChat = findViewById(R.id.fabSendChat);

        messageViewModel = ViewModelProviders.of(this).get(MessageViewModel.class);

        firebaseFirestore = FirebaseFirestore.getInstance();
        fireSync = new FireSync(activity, firebaseFirestore);

        phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();
        groupNumber = AppController.getInstance().getChamaPreferenceManager().getGroupNumber();

        setListeners();

        setRecyclerView();
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
        fireSync.registerMessageListener(messageViewModel);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    private void setListeners() {
        fabSendChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tvConnectivityStatusTeacherStudentMessage.getVisibility() == View.VISIBLE) {
                    Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                } else {
                    if (new Connection(context).IsInternetConnected()) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                            }
                        });
                        sendMessage();
                    } else {
                        Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                    }
                }
            }
        });
    }

    private void sendMessage() {
        final String message = actvChat.getText().toString().trim();
        actvChat.getText().clear();

        if (message.isEmpty())
            return;

        DocumentReference documentReference = firebaseFirestore.collection("message").document();
        String messageID = documentReference.getId();

        String dateTime = Time.getDateTimeNow("yyyy-MM-dd HH:mm:ss");

        final MessageEntity messageEntity = new MessageEntity();
        messageEntity.setUid(messageID);
        messageEntity.setMessage(message);
        messageEntity.setSenderPhoneNumber(phoneNumber);
        messageEntity.setGroupNumber(groupNumber);
        messageEntity.setDateAdded(dateTime);

        messageViewModel.insertMessage(messageEntity);

        CollectionReference collectionReference = firebaseFirestore.collection("message");
        collectionReference.document(messageID)
                .set(messageEntity)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {


                        try {
                            final JSONObject dataObject = new JSONObject();
                            if (message.length() > 60)
                                dataObject.put("body", message.substring(0, 60) + "...");
                            else
                                dataObject.put("body", message);

                            dataObject.put("title", AppController.getInstance().getChamaPreferenceManager().getGroupName());

                            dataObject.put("icon", "ic_unaitas");
                            dataObject.put("senderPhoneNumber", phoneNumber);
                            dataObject.put("groupNumber", groupNumber);
                            dataObject.put("notificationType", "message");

                            Utility.getDeviceFCMTokens(firebaseFirestore, dataObject);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (messageViewModel.doesMessageExistWithUID(messageEntity.getUid()))
                            messageViewModel.deleteMessage(messageEntity);

                        actvChat.setText(messageEntity.getMessage());
                        Toasty.info(context, R.string.sending_message_error, Toast.LENGTH_SHORT, true).show();

                    }
                });

    }

    private void setRecyclerView() {
        messageViewModel.getMessageLiveList(groupNumber).observe((LifecycleOwner) context, new Observer<List<MessageEntity>>() {
            @Override
            public void onChanged(List<MessageEntity> messageEntities) {

                if (messageEntities.size() > 0) {
                    toggleDataViews(true);

                    adapter = new ChatRecyclerViewAdapter(context, messageEntities);

                    layoutManager = new LinearLayoutManager(context);
                    layoutManager.setStackFromEnd(true);
                    rvChat.setAdapter(adapter);
                    rvChat.setLayoutManager(layoutManager);
                    adapter.notifyDataSetChanged();
                }else{
                    toggleDataViews(false);
                }
            }
        });
    }

    private void toggleDataViews(boolean hasItems) {
        if (hasItems)
            tvNoDataChat.setVisibility(View.GONE);
        else
            tvNoDataChat.setVisibility(View.VISIBLE);
    }
}

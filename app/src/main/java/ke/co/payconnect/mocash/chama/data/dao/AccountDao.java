package ke.co.payconnect.mocash.chama.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.entity.AccountEntity;


@Dao
public interface AccountDao {

    @Query("SELECT uid, account_name, account_number, member_phone_number, group_number FROM AccountEntity WHERE member_phone_number == :memberPhoneNumber ORDER BY account_name ASC")
    List<AccountEntity> getAllAccountsForMember(final String memberPhoneNumber);

    @Query("SELECT uid, account_name, account_number, member_phone_number, group_number FROM AccountEntity WHERE group_number == :groupNumber ORDER BY account_name ASC")
    List<AccountEntity> getAllAccountsForGroup(final String groupNumber);

    @Query("SELECT uid, account_name, account_number, member_phone_number, group_number FROM AccountEntity WHERE member_phone_number == :memberPhoneNumber ORDER BY account_name ASC")
    LiveData<List<AccountEntity>> getAllAccountsForMemberLive(final String memberPhoneNumber);

    @Query("SELECT COUNT(*) FROM AccountEntity WHERE member_phone_number == :memberPhoneNumber")
    int getAccountCountForMember(final String memberPhoneNumber);

    @Insert
    void insertAccount(AccountEntity... accountEntities);

    @Update
    void updateAccount(AccountEntity... accountEntities);

    @Delete
    void deleteAccount(AccountEntity... accountEntities);

    @Query("DELETE FROM AccountEntity WHERE member_phone_number == :memberPhoneNumber")
    void deleteAllMemberAccounts(final String memberPhoneNumber);

    @Query("DELETE FROM AccountEntity WHERE group_number == :groupNumber")
    void deleteAllGroupAccounts(final String groupNumber);
}

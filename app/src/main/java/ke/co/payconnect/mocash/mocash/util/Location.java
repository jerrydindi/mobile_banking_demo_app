package ke.co.payconnect.mocash.mocash.util;

import android.app.Activity;
import android.app.ProgressDialog;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import androidx.fragment.app.FragmentActivity;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import es.dmoral.toasty.Toasty;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.mocash.data.entity.LocationEntity;
import ke.co.payconnect.mocash.mocash.view.activity.LocationsActivity;
import ke.co.payconnect.mocash.mocash.viewmodel.LocationViewModel;

import static ke.co.payconnect.mocash.mocash.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.mocash.util.Config.RETRIES;
import static ke.co.payconnect.mocash.mocash.util.Config.TIMEOUT_MS;

public class Location {
    private final Context context;
    private LocationViewModel locationViewModel;

    public Location(Context context) {
        this.context = context;
        locationViewModel = ViewModelProviders.of((FragmentActivity) context).get(LocationViewModel.class);
    }

    public void fetchLocations() {
        final ProgressDialog loading = ProgressDialog.show(context, null, "Please wait...", false, false);


        final String requestTag = "location_request";
        final String url = Config.LOCATION_URL;

        JsonObjectRequest locationReq = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            loading.dismiss();

                            switch (response.getString("status")) {
                                case "success":
                                    // Add locations
                                    JSONArray locationsArray = response.getJSONArray("locations");

                                    // Clear persistent locations
                                    locationViewModel.deleteAllLocations();

                                    if (locationsArray.length() > 0) {

                                        for (int i = 0; i < locationsArray.length(); i++) {
                                            LocationEntity locationEntity = new LocationEntity();

                                            JSONObject locationsObject = locationsArray.getJSONObject(i);

                                            locationEntity.setLabel(locationsObject.getString("name"));
                                            locationEntity.setLatitude(locationsObject.getString("latitude"));
                                            locationEntity.setLongitude(locationsObject.getString("longitude"));

                                            locationViewModel.insertLocation(locationEntity);
                                        }
                                    }

                                    context.startActivity(new Intent(context, LocationsActivity.class));
                                    ((Activity) context).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);

                                    return;
                                case "suspended":
                                    Toasty.info(context, "Location details not available at the moment", Toast.LENGTH_LONG, true).show();
                                    break;

                                case "failed":
                                    Toasty.info(context, "Location details not available at the moment", Toast.LENGTH_LONG, true).show();
                                    break;

                                default:
                                    Toasty.info(context, "Location details not available at the moment", Toast.LENGTH_LONG, true).show();
                                    break;
                            }

                        } catch (JSONException e) {
                            loading.dismiss();
                            Toasty.info(context, "Location details not available at the moment", Toast.LENGTH_LONG, true).show();

                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                loading.dismiss();
                Toasty.info(context, "Location details not available at the moment", Toast.LENGTH_LONG, true).show();
            }
        });

        locationReq.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));

        // Adding request to queue
        AppController.getInstance().addToRequestQueue(locationReq, requestTag);
    }
}

package ke.co.payconnect.mocash.mocash.view.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.text.Html;
import android.text.Spanned;

import io.captano.utility.Money;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.mocash.view.activity.MainActivity;

public class ConfirmTransactionActivitiesDialogFragment extends DialogFragment {
    int title = 0;
    String action = "";
    String businessNumber = "";
    String accountNumber = "";
    double amount = 0;
    String result = "";
    String resultExtra = "";
    private ConfirmTransactionActivitiesListener listener;

    public static ConfirmTransactionActivitiesDialogFragment newInstance(int title, String businessNumber, String accountNumber, double amount, String action, String result, String resultExtra) {
        ConfirmTransactionActivitiesDialogFragment confirmFrag = new ConfirmTransactionActivitiesDialogFragment();

        Bundle args = new Bundle();
        args.putInt("title", title);
        args.putString("businessNumber", businessNumber);
        args.putString("accountNumber", accountNumber);
        args.putDouble("amount", amount);
        args.putString("action", action);
        args.putString("result", result);
        args.putString("resultExtra", resultExtra);

        confirmFrag.setArguments(args);
        return confirmFrag;
    }

    public interface ConfirmTransactionActivitiesListener {
        void onConfirmTransactionActivitiesListener(boolean isConfirmed, String action);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the ConfirmTransactionListener so we can send events to the host
            listener = (ConfirmTransactionActivitiesListener) context;

        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString() + " must implement ConfirmTransactionActivitiesListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);


        if (getArguments().getString("action") != null)
            action = getArguments().getString("action");

        if (getArguments().getInt("title") > 0)
            title = getArguments().getInt("title");

        if (getArguments().getString("businessNumber") != null)
            businessNumber = getArguments().getString("businessNumber");

        if (getArguments().getString("accountNumber") != null)
            accountNumber = getArguments().getString("accountNumber");

        amount = getArguments().getDouble("amount");

        if (getArguments().getString("result") != null)
            result = getArguments().getString("result");

        if (getArguments().getString("resultExtra") != null)
            resultExtra = getArguments().getString("resultExtra");

        final String resultComp = action;

//        Spanned message = Html.fromHtml("<font color='black'>You will be charged KES 0</font>");
        Spanned message = Html.fromHtml("");
        switch (action) {
            case "PB":
                message = Html.fromHtml("<font color='black'>Pay <b><font color='#4f0000'> KES " +
                        Money.format(amount) + "</font></b> to <b><font color='#4f0000'>" +
                        accountNumber + "</font></b> for account number <b><font color='#4f0000'>" +
                        businessNumber + "</font></b> <br /><br /> </font>"
//                       + "<font color='#595959'>You will be charged KES 0</font>");
                );
                break;

            case "AGENT":
                message = Html.fromHtml("<font color='black'>Withdraw <b><font color='#4f0000'> KES " +
                        Money.format(amount) + "</font></b> from account number <b><font color='#4f0000'>" +
                        accountNumber + "</font></b> at Agent <b><font color='#4f0000'>" +
                        businessNumber + "</font></b> <br /><br /> </font>"
//                       + "<font color='#595959'>You will be charged KES 0</font>");
                );
                break;

            case "RESULT":
                message = Html.fromHtml(result + "<br /><br /><font color='#595959'>" + resultExtra + "</font>");
                break;
            default:

                break;
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setTitle(title);
        builder.setMessage(message);

        builder.setPositiveButton(R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int buttonID) {

                        ConfirmTransactionActivitiesDialogFragment.this.dismiss();
                        if (resultComp.contains("RESULT"))
                            listener.onConfirmTransactionActivitiesListener(false, action);
                        else
                            listener.onConfirmTransactionActivitiesListener(true, action);
                    }
                }
        );

        if(!action.contains("RESULT")) {
            builder.setNegativeButton(R.string.cancel,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int buttonID) {
                            ConfirmTransactionActivitiesDialogFragment.this.dismiss();
                            listener.onConfirmTransactionActivitiesListener(false, action);
                        }
                    }
            );
        }

        if (action.contains("RESULT")) {
            builder.setNeutralButton("Home",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int buttonID) {
                            ConfirmTransactionActivitiesDialogFragment.this.dismiss();

                            Intent i = new Intent(getContext(), MainActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);

                            ((Activity) getContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                        }
                    }
            );
        }

        return builder.create();
    }

}

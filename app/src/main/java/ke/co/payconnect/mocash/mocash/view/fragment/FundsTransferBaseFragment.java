package ke.co.payconnect.mocash.mocash.view.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Objects;

import ke.co.payconnect.mocash.R;


public class FundsTransferBaseFragment extends Fragment {

    // Views
    private CardView cvFTBase;

    private FragmentTransaction fragmentTransaction;

    // TODO: Rename parameter arguments, choose names that match
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFundsTransferBaseInteractionListener mListener;

    public FundsTransferBaseFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static FundsTransferBaseFragment newInstance(String param1, String param2) {
        FundsTransferBaseFragment fragment = new FundsTransferBaseFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_funds_transfer_base, container, false);

        cvFTBase = view.findViewById(R.id.cvFTBase);
        cvFTBase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction = Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction();
                fragmentTransaction.remove(new FundsTransferBaseFragment());
                fragmentTransaction.replace(R.id.flFT, new FundsTransferOwnFragment());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null)
            mListener.onFundsTransferBaseInteraction(uri);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFundsTransferBaseInteractionListener)
            mListener = (OnFundsTransferBaseInteractionListener) context;
        else
            throw new RuntimeException(context.toString() + " must implement OnFundsTransferBaseInteractionListener");

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFundsTransferBaseInteractionListener {
        // TODO: Update argument type and name
        void onFundsTransferBaseInteraction(Uri uri);
    }
}

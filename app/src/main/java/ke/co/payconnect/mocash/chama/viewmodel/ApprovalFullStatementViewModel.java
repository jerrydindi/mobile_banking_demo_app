package ke.co.payconnect.mocash.chama.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.ChamaDatabase;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalFullStatementEntity;

public class ApprovalFullStatementViewModel extends AndroidViewModel {

    private ChamaDatabase db;

    public ApprovalFullStatementViewModel(@NonNull Application application) {
        super(application);

        db = ChamaDatabase.getAppDatabaseInstance(this.getApplication());
    }

    public ApprovalFullStatementEntity findFullStatementApprovalById(String approvalId){
        return db.approvalFullStatementDao().findFullStatementApprovalById(approvalId);
    }

    public boolean doesFullStatementApprovalExistWithApprovalId(String approvalId){
        return db.approvalFullStatementDao().doesFullStatementApprovalExistWithApprovalId(approvalId);
    }

    public  void updateApprovalFullStatementSetData(String approvalID, String approvalStatus, String approvalNarration, String approvalDate) {
        db.approvalFullStatementDao().updateApprovalFullStatementSetData(approvalID, approvalStatus, approvalNarration, approvalDate);
    }

    public  LiveData<List<ApprovalFullStatementEntity>> getApprovalFullStatementsByTypeLive(String approvalType) {
        return  db.approvalFullStatementDao().getApprovalFullStatementsByTypeLive(approvalType);
    }

    public void insertApprovalFullStatement(ApprovalFullStatementEntity approvalTransactionEntity) {
        db.approvalFullStatementDao().insertApprovalFullStatement(approvalTransactionEntity);
    }

    public void deleteAllFullStatements() {
        db.approvalFullStatementDao().deleteAllApprovalFullStatements();
    }
}

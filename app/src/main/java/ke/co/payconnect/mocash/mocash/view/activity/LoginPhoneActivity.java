package ke.co.payconnect.mocash.mocash.view.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Connection;
import io.captano.utility.ui.UI;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.mocash.data.entity.EmployerEntity;
import ke.co.payconnect.mocash.mocash.data.object.User;
import ke.co.payconnect.mocash.mocash.util.Config;
import ke.co.payconnect.mocash.mocash.view.activity.loan.CheckoffRegisterActivity;
import ke.co.payconnect.mocash.mocash.viewmodel.EmployerViewModel;

import static ke.co.payconnect.mocash.mocash.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.mocash.util.Config.RETRIES;
import static ke.co.payconnect.mocash.mocash.util.Config.TIMEOUT_MS;

public class LoginPhoneActivity extends AppCompatActivity {
    private final Context context = this;
    private final Activity activity = this;

    // Views
    private TextView tvConnectivityStatusLoginPhone;
    private AutoCompleteTextView actvPhoneLoginPhone;
    private CardView cvLoginPhone;
    private TextView tvPoweredLoginPhone;

    private EmployerViewModel employerViewModel;
    private ProgressDialog loading;

    private final String permission = Manifest.permission.READ_PHONE_STATE;
    private static final int PERMISSION_REQUEST_CODE = 1;

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if (isConnected)
                tvConnectivityStatusLoginPhone.setVisibility(View.GONE);
            else
                tvConnectivityStatusLoginPhone.setVisibility(View.VISIBLE);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_phone);

        if (Build.VERSION.SDK_INT >= 21)
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        } else {
            registerReceiver(connectionReceiver, intentFilter);
        }

        AppController.getInstance().getMocashPreferenceManager().setRememberPhone(false);

        tvConnectivityStatusLoginPhone = findViewById(R.id.tvConnectivityStatusLoginPhone);
        actvPhoneLoginPhone = findViewById(R.id.actvPhoneLoginPhone);
        cvLoginPhone = findViewById(R.id.cvLoginPhone);
        tvPoweredLoginPhone = findViewById(R.id.tvPoweredLoginPhone);

        employerViewModel = ViewModelProviders.of(this).get(EmployerViewModel.class);

        setListeners();

        checkPermission();
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void checkPermission() {
        if (!isPermissionGranted(permission)) {
            requestPermission(permission);
        } else {
//            if (getPhone() != null) {
//                String imei = getPhone().get(0);
//                if (imei != null)
//
//            }
            AppController.getInstance().getMocashPreferenceManager().setIMEI("00223366554477");
        }
    }

    private boolean isPermissionGranted(String permission) {
        if (Build.VERSION.SDK_INT >= 23) {
            int result = ContextCompat.checkSelfPermission(activity, permission);
            return result == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }

    private void requestPermission(String permission) {
        ActivityCompat.requestPermissions(activity, new String[]{permission}, PERMISSION_REQUEST_CODE);
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        if (requestCode == PERMISSION_REQUEST_CODE) {
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                if (getPhone() != null) {
//                    String imei = getPhone().get(0);
//                    if (imei != null)
//                        AppController.getInstance().getChamaPreferenceManager().setIMEI(imei);
//                }
//                Toasty.info(context, "Permission granted", Toast.LENGTH_SHORT, true).show();
//            } else {
//                Toasty.info(context, "Permission denied", Toast.LENGTH_SHORT, true).show();
//            }
//        }
//    }

//    @TargetApi(Build.VERSION_CODES.O)
//    private ArrayList<String> getPhone() {
//        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//
//        if (ActivityCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED)
//            return null;
//
//
//        ArrayList<String> phoneList = new ArrayList<>();
//        phoneList.add(telephonyManager != null ? telephonyManager.getImei() : null);
//        return phoneList;
//    }

    private void setListeners() {
        cvLoginPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Internet connectivity check
                if (tvConnectivityStatusLoginPhone.getVisibility() == View.VISIBLE) {
                    Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                } else {
                    if (new Connection(context).IsInternetConnected())
                        validation(actvPhoneLoginPhone.getText().toString().trim());
                    else
                        Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                }
               // actionProcessLogin();
            }
        });

        tvPoweredLoginPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent webIntent = new Intent(Intent.ACTION_VIEW);
                String websiteUrl = "http://payconnect.co.ke/";
                webIntent.setData(Uri.parse(websiteUrl));
                startActivity(webIntent);
            }
        });
    }

    private void resetForm() {
        actvPhoneLoginPhone.getText().clear();
    }

    private void validation(String phoneNumber) {
        if (phoneNumber.isEmpty()) {
            Toasty.info(context, getString(R.string.phone_required), Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (phoneNumber.substring(0, 3).equals("254"))
            phoneNumber = phoneNumber.substring(3);

        if (phoneNumber.substring(0, 1).equals("0"))
            phoneNumber = phoneNumber.substring(1);

        new UI(activity).hideSoftInput();


        processRequest("254" + phoneNumber);
    }

    private void actionProcessLogin() {
        Intent i = new Intent(context, LoginActivity.class);
        startActivity(i);

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void actionProcessRegistration() {
        Intent i = new Intent(context, CheckoffRegisterActivity.class);
        startActivity(i);

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void notifyRegistrationResponse(String message) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        actionProcessRegistration();
                        break;
                    case DialogInterface.BUTTON_NEUTRAL:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(true);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.yes), dialogClickListener);
        builder.setNegativeButton(getString(R.string.no), dialogClickListener);
        builder.show();
    }

    private void processRequest(final String phoneNumber) {
        loading = ProgressDialog.show(context, null, "Verifying", false, false);

        final String requestTag = "details_mocash";
        final String url = Config.DETAILS_MOCASH_URL;

        Map<String, String> map = new HashMap<>();
        map.put("phone_number", phoneNumber);
        map.put("account", phoneNumber);

        JsonObjectRequest detailsMocashRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    resetForm();

                    switch (response.getString("details_checkoff_status")) {
                        case "success":
                            if (response.getString("details_mocash_status").equals("success")) {
                                // Registered in mocash & exists in checkoff list - 001|000

                                // Set user details in shared prefs
                                User user = new User();
                                user.setPhone(phoneNumber);
                                user.setNationalID(response.getString("id_number"));
                                AppController.getInstance().getMocashPreferenceManager().setUser(user);

                                String loanLimit = response.getString("loan_limit").replace(",", "");
                                AppController.getInstance().getMocashPreferenceManager().setCheckoffLoanLimit(loanLimit);

                                String loanBalance = response.getString("loan_balance").replace(",", "");
                                AppController.getInstance().getMocashPreferenceManager().setCheckoffLoanBalance(loanBalance);
                                AppController.getInstance().getMocashPreferenceManager().setInCheckoff(true);


                                // Add employers
                                String employersString = response.getString("employers");
                                JSONArray employersArray = new JSONArray(employersString);

                                // Validate employers
                                if (employersArray.length() <= 0) {
                                    Toasty.info(context, "Could not fetch employers", Toast.LENGTH_LONG, true).show();

                                    loading.dismiss();
                                    return;
                                }

                                // Clear persistent employers
                                employerViewModel.deleteAllEmployers();

                                if (employersArray.length() > 0) {

                                    for (int i = 0; i < employersArray.length(); i++) {
                                        EmployerEntity employerEntity = new EmployerEntity();

                                        JSONObject employersObject = employersArray.getJSONObject(i);

                                        employerEntity.setEmployerName(employersObject.getString("name"));
                                        employerEntity.setEmployerID(employersObject.getString("id"));

                                        employerViewModel.insertEmployer(employerEntity);
                                    }
                                }

                                loading.dismiss();
                                actionProcessLogin();
                            } else {
                                loading.dismiss();

                                // Set user details in shared prefs
                                User user = new User();
                                user.setPhone(phoneNumber);
                                AppController.getInstance().getMocashPreferenceManager().setUser(user);
                                // Not registered in mocash & exists in checkoff list - 002|000
                                // Send registration request
                                notifyRegistrationResponse("You are not registered for Unaitas Mocash. Do you wish to proceed with registration?");
                            }

                            break;

                        case "suspended":
                            loading.dismiss();

                            AppController.getInstance().getMocashPreferenceManager().setCheckoffLoanLimit("Not Available");
                            AppController.getInstance().getMocashPreferenceManager().setCheckoffLoanBalance("Not Available");

                            Toasty.info(context, "Verification failed", Toast.LENGTH_LONG, true).show();
                            break;

                        case "failed":
                            AppController.getInstance().getMocashPreferenceManager().setCheckoffLoanLimit("Not Available");
                            AppController.getInstance().getMocashPreferenceManager().setCheckoffLoanBalance("Not Available");

                            if (response.getString("details_mocash_status").equals("success")) {
                                // Registered in mocash & does not exist in checkoff list - 001|801

                                // Set user details in shared prefs
                                User user = new User();
                                user.setPhone(phoneNumber);
                                AppController.getInstance().getMocashPreferenceManager().setUser(user);

                                AppController.getInstance().getMocashPreferenceManager().setInCheckoff(false);
                                // Set user details in shared prefs
                                loading.dismiss();
                                actionProcessLogin();
                            } else {
                                loading.dismiss();

                                // Set user details in shared prefs
                                User user = new User();
                                user.setPhone(phoneNumber);
                                AppController.getInstance().getMocashPreferenceManager().setUser(user);

                                // Not registered in mocash & does not exist in checkoff list - 002|801
                                Toasty.info(context, "Your account is not registered for Mocash", Toast.LENGTH_SHORT, true).show();
                                return;
                            }
                            break;

                        default:
                            loading.dismiss();

                            Toasty.info(context, "The account could not be verified at this moment", Toast.LENGTH_LONG, true).show();
                            break;
                    }

                } catch (JSONException e) {
                    loading.dismiss();
                    resetForm();

                    Toasty.info(context, "An error occurred", Toast.LENGTH_LONG, true).show();

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                resetForm();

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error.")) {
                                    Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":
                                /* Validation errors - Sample response
                                  {
                                      "status": "fatal",
                                        "errors": [{
                                          "location": "body",
                                          "param": "agent_number",
                                          "msg": "Validation failed | CO::00-1"
                                        }]
                                 }
                                 */
                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();

                                        break;

                                    default:
                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } else {
                    if (error instanceof NoConnectionError) {
                        Toasty.info(context, "The service is currently unavailable", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof TimeoutError) {
                        Toasty.info(context, "The request has timed out", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof AuthFailureError) {
                        Toasty.info(context, "Authentication failed", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof ServerError) {
                        Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof NetworkError) {
                        Toasty.info(context, "Network error.", Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof ParseError) {
                        Toasty.info(context, "Network error.", Toast.LENGTH_LONG, true).show();
                    } else {
                        Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    }
                }

            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        detailsMocashRequest.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));
        AppController.getInstance().addToRequestQueue(detailsMocashRequest, requestTag);
    }
}


package ke.co.payconnect.mocash.mocash.view.activity.loan;

import android.app.Activity;
import android.app.ProgressDialog;
import androidx.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.transition.Slide;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Connection;
import io.captano.utility.Money;
import io.captano.utility.ui.UIController;
import io.captano.utility.ui.UIHandler;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.mocash.data.entity.EmployerEntity;
import ke.co.payconnect.mocash.mocash.data.object.LoanAccount;
import ke.co.payconnect.mocash.mocash.util.Access;
import ke.co.payconnect.mocash.mocash.util.Config;
import ke.co.payconnect.mocash.mocash.view.activity.LoanActivity;
import ke.co.payconnect.mocash.mocash.view.activity.LoginActivity;
import ke.co.payconnect.mocash.mocash.view.adapter.AccountSpinnerAdapter;
import ke.co.payconnect.mocash.mocash.view.adapter.EmployerSpinnerAdapter;
import ke.co.payconnect.mocash.mocash.view.fragment.LoanRepaymentDialogFragment;
import ke.co.payconnect.mocash.mocash.view.fragment.PasswordInputActivitiesFragment;
import ke.co.payconnect.mocash.mocash.viewmodel.EmployerViewModel;

import static ke.co.payconnect.mocash.mocash.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.mocash.util.Config.RETRIES;
import static ke.co.payconnect.mocash.mocash.util.Config.TIMEOUT_MS;

public class CheckoffLoanActivity extends AppCompatActivity implements
        PasswordInputActivitiesFragment.PasswordInputActivitiesListener,
        LoanRepaymentDialogFragment.loanRepaymentListener  {
    private Context context = this;
    private final Activity activity = this;

    // Views
    private Toolbar tbCheckoffLoan;
    private TextView tvConnectivityStatusCheckoffLoan;
    private ConstraintLayout clContainerCheckoffLoan;
    private Spinner spEmployerIDCheckoffLoan;
    private Spinner spSelectAccountCheckoffLoan;
    private EditText etAmountCheckoffLoan;
    private TextView tvLoanBalanceCheckoffLoan;
    private TextView tvLimitCheckoffLoan;
    private Button btSubmitCheckoffLoan;

    private EmployerViewModel employerViewModel;

    private String accountSelected = null;
    private String employerSelected = null;

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            // With animated visibility transitions
            if (isConnected) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Slide slideVisible = new Slide();
                    slideVisible.setSlideEdge(Gravity.TOP);
                    TransitionManager.beginDelayedTransition(clContainerCheckoffLoan, slideVisible);
                    tvConnectivityStatusCheckoffLoan.setVisibility(View.GONE);
                } else {
                    tvConnectivityStatusCheckoffLoan.setVisibility(View.GONE);
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Slide slideInvisible = new Slide();
                    slideInvisible.setSlideEdge(Gravity.TOP);
                    TransitionManager.beginDelayedTransition(clContainerCheckoffLoan, slideInvisible);
                    tvConnectivityStatusCheckoffLoan.setVisibility(View.VISIBLE);
                } else {
                    tvConnectivityStatusCheckoffLoan.setVisibility(View.VISIBLE);
                }
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkoff_loan);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        } else {
            registerReceiver(connectionReceiver, intentFilter);
        }

        tbCheckoffLoan = findViewById(R.id.tbCheckoffLoan);
        setSupportActionBar(tbCheckoffLoan);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbCheckoffLoan.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        tvConnectivityStatusCheckoffLoan = findViewById(R.id.tvConnectivityStatusCheckoffLoan);
        clContainerCheckoffLoan = findViewById(R.id.clContainerCheckoffLoan);
        spEmployerIDCheckoffLoan = findViewById(R.id.spEmployerIDCheckoffLoan);
        spSelectAccountCheckoffLoan = findViewById(R.id.spSelectAccountCheckoffLoan);
        etAmountCheckoffLoan = findViewById(R.id.etAmountCheckoffLoan);
        tvLoanBalanceCheckoffLoan = findViewById(R.id.tvLoanBalanceCheckoffLoan);
        tvLimitCheckoffLoan = findViewById(R.id.tvLimitCheckoffLoan);
        btSubmitCheckoffLoan = findViewById(R.id.btSubmitCheckoffLoan);

        employerViewModel = ViewModelProviders.of(this).get(EmployerViewModel.class);

        setLoanLimitAndBalance();

        setupAccounts();

        setupEmployers();

        setListeners();

        setupSecuredLending();
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void setLoanLimitAndBalance() {
        try {
            String loanLimit = AppController.getInstance().getMocashPreferenceManager().getCheckoffLoanLimit();
            tvLimitCheckoffLoan.setText("KES. " + Money.format(Double.parseDouble(loanLimit)));
        } catch (Exception e) {
            tvLimitCheckoffLoan.setText("Not Available");
        }

        try {
            String loanBalance = AppController.getInstance().getMocashPreferenceManager().getCheckoffLoanBalance();
            tvLoanBalanceCheckoffLoan.setText("KES. " + Money.format(Double.parseDouble(loanBalance)));
        } catch (Exception e) {
            tvLoanBalanceCheckoffLoan.setText("Not Available");
        }
    }

    private void setupAccounts() {
        LoanAccount loanAccount = AppController.getInstance().getMocashPreferenceManager().getLoanAccount();
        List<LoanAccount> loanAccounts = new ArrayList<>();

        if (loanAccount != null) {
            loanAccounts.add(loanAccount);

            AccountSpinnerAdapter adapter = new AccountSpinnerAdapter(this, loanAccounts);
            spSelectAccountCheckoffLoan.setAdapter(adapter);

        } else {
            Toasty.info(activity, "You do not have valid accounts for this product", Toast.LENGTH_LONG, true).show();

            finish();
            overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
        }
    }

    private void setupEmployers() {
        List<EmployerEntity> employers = employerViewModel.getEmployerList();

        if (employers != null) {

            EmployerSpinnerAdapter adapter = new EmployerSpinnerAdapter(this.context, employers);
            spEmployerIDCheckoffLoan.setAdapter(adapter);


            setListeners();

        } else {
            Toasty.info(context, "Could not load employers", Toast.LENGTH_SHORT, true).show();
            new UIController().redirect(activity, LoanActivity.class);
        }
    }

    private void setListeners() {
        spEmployerIDCheckoffLoan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {

                try {
                    employerSelected = ((TextView) v.findViewById(R.id.tvEmployerIDSpin)).getText().toString();
                } catch (Exception e) {
                    Toasty.info(context, "Your session has timed out", Toast.LENGTH_SHORT, true).show();
                    new UIController().redirect(activity, LoginActivity.class);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });

        spSelectAccountCheckoffLoan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {

                try {
                    accountSelected = ((TextView) v.findViewById(R.id.tvAccountNumberSpin)).getText().toString();
                } catch (Exception e) {
                    Toasty.info(context, "Your session has timed out", Toast.LENGTH_SHORT, true).show();
                    new UIController().redirect(activity, LoginActivity.class);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });

        btSubmitCheckoffLoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvLimitCheckoffLoan.getText().toString().trim().toLowerCase().contains("Not Available")) {
                    Toasty.info(context, "Request can not be sent at this moment.", Toast.LENGTH_SHORT, true).show();
                    return;
                }
                PasswordInputActivitiesFragment fragment = PasswordInputActivitiesFragment.newInstance("CHECKOFF");
                fragment.show(getSupportFragmentManager(), null);
            }
        });
    }

    @Override
    public void onPasswordInputActivitiesDialog(boolean isSuccessful, String tag) {
        if (isSuccessful) {
            if (tag.equals("CHECKOFF")) {
                if (tvConnectivityStatusCheckoffLoan.getVisibility() == View.VISIBLE) {
                    Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                } else {
                    if (new Connection(context).IsInternetConnected())
                        validation(etAmountCheckoffLoan.getText().toString(), accountSelected, employerSelected);
                    else
                        Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();
                }
            } else {
                Toasty.info(context, "Incorrect pin", Toast.LENGTH_SHORT, true).show();
            }
        }
    }

    @Override
    public void onLoanRepaymentDialog(boolean isSuccessful, String tag, String amount) {

    }

    private void validation(String amount, String accountTo, String employerId) {
        if (amount == null || amount.isEmpty()) {
            Toasty.info(context, "Please enter a valid amount", Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (accountTo == null || accountTo.isEmpty()) {
            Toasty.info(context, "Please select an account", Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (employerId == null || employerId.isEmpty()) {
            Toasty.info(context, "Please select an employer", Toast.LENGTH_SHORT, true).show();
            return;
        }

        // Validate limit
        String loanLimit = "0";
        loanLimit = AppController.getInstance().getMocashPreferenceManager().getCheckoffLoanLimit();

        try {
            if (Double.parseDouble(amount) > Double.parseDouble(loanLimit)) {
                Toasty.info(context, "You have exceeded your loan limit", Toast.LENGTH_SHORT, true).show();
                return;
            }
        } catch (Exception ignored) {
        }

        // Validate balance
        String loanBalance = "1";
        loanBalance = AppController.getInstance().getMocashPreferenceManager().getCheckoffLoanBalance();

        try {
            if (Double.parseDouble(loanBalance) > 0) {
                Toasty.info(context, "You do not qualify for this loan", Toast.LENGTH_SHORT, true).show();
                return;
            }
        } catch (Exception ignored) {
        }

        // Proceed
        processRequest(amount, accountTo, employerId, loanLimit);
    }

    private void notifySessionExpired(String message) {
        Access.forceSessionExpiry();

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        boolean stateRemember = AppController.getInstance().getMocashPreferenceManager().getRememberPhone();
                        Access.logout(activity, stateRemember, false);

                        Toasty.info(context, R.string.logout_message_post, Toast.LENGTH_SHORT, true).show();
                        break;
                }
            }
        };
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(false);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.sign_in), dialogClickListener);
        builder.show();
    }

    private void responseAlert(String message) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:


                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.ok), dialogClickListener);
        builder.show();
    }

    private void processRequest(String amount, String accountTo, String employerId, String loanLimit) {

        final ProgressDialog loading = ProgressDialog.show(this, null, "Processing request...", false, false);

        String phoneNumber = AppController.getInstance().getMocashPreferenceManager().getUser().getPhone();

        final String sessionToken = AppController.getInstance().getMocashPreferenceManager().getSessionToken();

        String requestTag = "checkoff_req_request";
        String url = Config.CHECKOFF_LOAN_REQUEST_URL;

        Map<String, String> map = new HashMap<>();
        map.put("phone_number", phoneNumber);
        map.put("account", accountTo);
        map.put("employer_id", employerId);
        map.put("amount", amount);
        map.put("limit", loanLimit);

        JsonObjectRequest checkoffLoanRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            loading.dismiss();

                            etAmountCheckoffLoan.getText().clear();
                            setLoanLimitAndBalance();

                            switch (response.getString("status")) {
                                case "success":
                                    setupSecuredLending();
                                    responseAlert(response.getString("message"));
                                    break;

                                case "failed":
                                    Toasty.info(context, "Request could not be completed", Toast.LENGTH_LONG, true).show();
                                    break;

                                default:
                                    Toasty.info(context, "Request could not be completed", Toast.LENGTH_LONG, true).show();
                                    break;
                            }

                        } catch (JSONException e) {
                            loading.dismiss();
                            Toasty.info(context, "An error occurred", Toast.LENGTH_LONG, true).show();

                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 403:
                            notifySessionExpired("Authorization failed: Your session has expired. Kindly sign-in to access this option");
                            break;
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error.")) {
                                    Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":
                                /* Validation errors - Sample response
                                  {
                                      "status": "fatal",
                                        "errors": [{
                                          "location": "body",
                                          "param": "agent_number",
                                          "msg": "Validation failed | CO::00-1"
                                        }]
                                 }
                                 */
                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();

                                        break;

                                    default:
                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } else {
                    if (error instanceof NoConnectionError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, "The service is currently unavailable.", Toast.LENGTH_SHORT, true).show();
                            }
                        });
                    } else if (error instanceof TimeoutError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, "The request has timed out.", Toast.LENGTH_SHORT, true).show();
                            }
                        });
                    } else if (error instanceof AuthFailureError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, "Authentication failed", Toast.LENGTH_SHORT, true).show();
                            }
                        });
                    } else if (error instanceof ServerError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            }
                        });
                    } else if (error instanceof NetworkError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, "Network error.", Toast.LENGTH_LONG, true).show();
                            }
                        });
                    } else if (error instanceof ParseError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, "Network error.", Toast.LENGTH_LONG, true).show();
                            }
                        });
                    } else {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            }
                        });
                    }
                }

            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionToken);

                return headers;
            }
        };

        checkoffLoanRequest.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));

        // Adding request to queue
        AppController.getInstance().addToRequestQueue(checkoffLoanRequest, requestTag);
    }

    private void setupSecuredLending() {
        String requestTag = "checkoff_employee_details_request";
        String url = Config.CHECKOFF_EMPLOYEE_DETAILS_URL;
        String phoneNumber = AppController.getInstance().getMocashPreferenceManager().getUser().getPhone();

        final Map<String, String> map = new HashMap<>();
        map.put("phone_number", phoneNumber);
        map.put("account", phoneNumber);

        JsonObjectRequest checkoffEmployeeDetailsReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(final JSONObject response) {
                        try {
                            switch (response.getString("status")) {
                                case "success":
                                    String loanLimit = response.getString("loan_limit").replace(",", "");
                                    AppController.getInstance().getMocashPreferenceManager().setCheckoffLoanLimit(loanLimit);

                                    String loanBalance = response.getString("loan_balance").replace(",", "");
                                    AppController.getInstance().getMocashPreferenceManager().setCheckoffLoanBalance(loanBalance);

                                    setLoanLimitAndBalance();
                                    break;
                                case "failed":
                                    AppController.getInstance().getMocashPreferenceManager().setCheckoffLoanLimit("Not Available");
                                    AppController.getInstance().getMocashPreferenceManager().setCheckoffLoanBalance("Not Available");
                                    setLoanLimitAndBalance();
                                    break;
                                default:
                                    setLoanLimitAndBalance();
                                    break;
                            }

                        } catch (JSONException ignored) {
                            setLoanLimitAndBalance();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                AppController.getInstance().getMocashPreferenceManager().setCheckoffLoanLimit("Not Available");
                AppController.getInstance().getMocashPreferenceManager().setCheckoffLoanBalance("Not Available");
                setLoanLimitAndBalance();
            }
        });

        checkoffEmployeeDetailsReq.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));

        AppController.getInstance().addToRequestQueue(checkoffEmployeeDetailsReq, requestTag);
    }
}

package ke.co.payconnect.mocash.chama.view.activity.secretary;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Time;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.chama.util.Access;
import ke.co.payconnect.mocash.chama.util.Auth;
import ke.co.payconnect.mocash.chama.util.Config;
import ke.co.payconnect.mocash.chama.view.activity.MainChamaActivity;
import ke.co.payconnect.mocash.chama.view.activity.member.TransactionActivity;

public class SetSessionTotalActivity extends AppCompatActivity {

    private final Context context = this;
    private final Activity activity = this;

    private Toolbar tbSessionTotal;
    private TextView tvConnectivityStatusGroupSessionTotal;
    private EditText etTotalAmountToPost;
    private CardView cvSubmitSessionTotal;

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if (isConnected)
                tvConnectivityStatusGroupSessionTotal.setVisibility(View.GONE);
            else
                tvConnectivityStatusGroupSessionTotal.setVisibility(View.VISIBLE);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_session_total);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        else
            registerReceiver(connectionReceiver, intentFilter);

        tvConnectivityStatusGroupSessionTotal = findViewById(R.id.tvConnectivityStatusGroupSessionTotal);
        tbSessionTotal = findViewById(R.id.tbSessionTotal);
        setSupportActionBar(tbSessionTotal);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbSessionTotal.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                startActivity(new Intent(context, MainChamaActivity.class));

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        etTotalAmountToPost = findViewById(R.id.etTotalAmountToPost);
        cvSubmitSessionTotal = findViewById(R.id.cvSubmitSessionTotal);

        setListeners();

    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(context, MainChamaActivity.class));

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }


    private void setListeners() {
        cvSubmitSessionTotal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Auth.isAuthorized("secretary")) {
                    Toasty.info(context, R.string.action_not_permitted, Toast.LENGTH_SHORT, true).show();
                    return;
                }
                    validation();
            }
        });
    }

    private void validation() {
        String dateTime = Time.getDateTimeNow("yyyyMMddHHmmss");
        String amount = etTotalAmountToPost.getText().toString().trim();

        if (amount.isEmpty()){
            showErrorDialog();
        } else {
            Config.sessionTotal = amount;
            Config.sessionStamp = dateTime;
            Config.totalPosted = 0;
            showConfirmation();
        }

    }

    private void showErrorDialog() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        startActivity(new Intent(context, SetSessionTotalActivity.class));

                        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:

                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.confirm_action));
        builder.setIcon(R.drawable.ic_unaitas);
        builder.setMessage("Please enter the total amount to post");
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.go_back), dialogClickListener);

        builder.show();

    }

    private void showConfirmation() {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            startActivity(new Intent(context, SelectMemberAddTransactionsActivity.class));

                            overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:

                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
            builder.setTitle(getString(R.string.confirm_action));
            builder.setIcon(R.drawable.ic_unaitas);
            builder.setMessage("Proceed with the request?");
            builder.setCancelable(false);
            builder.setPositiveButton(getString(R.string.yes), dialogClickListener);
            builder.setNegativeButton(getString(R.string.no), dialogClickListener);
            builder.show();
        }

}

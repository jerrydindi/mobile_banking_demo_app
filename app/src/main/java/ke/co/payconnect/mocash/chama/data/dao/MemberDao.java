package ke.co.payconnect.mocash.chama.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;

@Dao
public interface MemberDao {
    @Query("SELECT DISTINCT uid, first_name, last_name, phone_number, id_number, maker_phone_number, date_registered, date_of_birth, signatory, secretary, status FROM MemberEntity ORDER BY first_name ASC")
    LiveData<List<MemberEntity>> getMembersLiveList();

    @Query("SELECT DISTINCT uid, first_name, last_name, phone_number, id_number, maker_phone_number, date_registered, date_of_birth, signatory, secretary, status FROM MemberEntity WHERE signatory == :signatory ORDER BY first_name ASC")
    LiveData<List<MemberEntity>> getMembersTypeLiveList(final String signatory);

    @Query("SELECT DISTINCT uid, first_name, last_name, phone_number, id_number, maker_phone_number, date_registered, date_of_birth, signatory, secretary, status FROM MemberEntity WHERE phone_number <> :phoneNumber AND signatory == :roleValue ORDER BY first_name ASC")
    LiveData<List<MemberEntity>> getNonMembersLiveList(final String phoneNumber, final String roleValue);

    @Query("SELECT DISTINCT uid, first_name, last_name, phone_number, id_number, maker_phone_number, date_registered, date_of_birth, signatory, secretary, status FROM MemberEntity WHERE status ==:isActive ORDER BY first_name ASC")
    List<MemberEntity> getActiveMembers(final boolean isActive);

    @Query("SELECT uid, first_name, last_name, phone_number, id_number, maker_phone_number, date_registered, date_of_birth, signatory, secretary, status FROM MemberEntity WHERE phone_number == :phoneNumber")
    MemberEntity findMemberByPhoneNumber(final String phoneNumber);

    @Query("SELECT EXISTS(SELECT 1 FROM MemberEntity WHERE phone_number == :phoneNumber LIMIT 1)")
    boolean doesMemberExistWithPhoneNumber(final String phoneNumber);

    @Query("SELECT COUNT(*) FROM MemberEntity")
    int getMemberCount();

    @Insert
    void insertMember(MemberEntity... userEntities);

    @Update
    void updateMember(MemberEntity... userEntities);

    @Delete
    void deleteMember(MemberEntity... userEntities);

    @Query("DELETE FROM MemberEntity")
    void deleteAllMembers();
}

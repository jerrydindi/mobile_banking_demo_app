package ke.co.payconnect.mocash.mocash.view.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

import es.dmoral.toasty.Toasty;
import ke.co.payconnect.mocash.R;

public class FavManageAccountsDialogFragment extends DialogFragment {

    // Views
    private TextView tvFavTitle;
    private EditText etFavAccountNameAdd, etFavAccountNumberAdd;
    private Button btFavSubmit;
    private Button btFavCancel;

    private FavManageAccountsListener listener;

    private String tag = "add_acc";

    // Empty constructor (required)
    public FavManageAccountsDialogFragment() {

    }

    public static FavManageAccountsDialogFragment newInstance(String tag, int uid, String accountName, String accountNumber) {

        FavManageAccountsDialogFragment frag = new FavManageAccountsDialogFragment();
        Bundle args = new Bundle();
        args.putString("tag", tag);
        args.putInt("uid", uid);
        args.putString("accountName", accountName);
        args.putString("accountNumber", accountNumber);
        frag.setArguments(args);

        return frag;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            // Verify that the host activity implements the callback interface
            listener = (FavManageAccountsListener) context;

        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString() + " must implement FavManageAccountsListener");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fav_manage_accounts_dialog, container);

        etFavAccountNameAdd = view.findViewById(R.id.etFavAccountNameAdd);
        etFavAccountNumberAdd = view.findViewById(R.id.etFavAccountNumberAdd);
        btFavSubmit = view.findViewById(R.id.btFavSubmit);
        btFavCancel = view.findViewById(R.id.btFavCancel);
        tvFavTitle = view.findViewById(R.id.tvFavTitle);

        if (getArguments() != null) {
            tag = getArguments().getString("tag");
            if (tag.equals("edit_acc")) {

                tvFavTitle.setText(getString(R.string.fav_edit_acc));
                etFavAccountNameAdd.setText(getArguments().getString("accountName"));
                etFavAccountNumberAdd.setText(getArguments().getString("accountNumber"));
            }
        }

        btFavSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String accName = etFavAccountNameAdd.getText().toString().trim();
                String accNumber = etFavAccountNumberAdd.getText().toString().trim();

                if (accName.isEmpty() || accNumber.isEmpty()) {
                    FavManageAccountsDialogFragment.this.dismiss();
                    return;
                }

                getDialog().cancel();
                if (tag.equals("add_acc")) {
                    listener.onManageFavAccountsDialog(true, "add_acc", getArguments().getInt("uid"), accName, accNumber); // For add, the uid will not be used. Default 0

                    if(getContext() != null)
                        Toasty.info(getContext(), "A new billing account has been added", Toast.LENGTH_SHORT, true).show();
                } else if (tag.equals("edit_acc")) {
                    listener.onManageFavAccountsDialog(true, "edit_acc", getArguments().getInt("uid"), accName, accNumber);
                }
            }
        });

        btFavCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().cancel();
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public interface FavManageAccountsListener {
        void onManageFavAccountsDialog(boolean isSelected, String tag, int uid, String accountName, String accountNumber);
    }

}

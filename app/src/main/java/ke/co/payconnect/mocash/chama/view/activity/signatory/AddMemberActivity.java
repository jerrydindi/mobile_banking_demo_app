package ke.co.payconnect.mocash.chama.view.activity.signatory;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Time;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalExitEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalFullStatementEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalGroupBalanceEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalGroupLoanBalanceEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalMemberAddEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalMinistatementEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalSecretaryEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalTransactionEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalWithdrawEntity;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;
import ke.co.payconnect.mocash.chama.util.Access;
import ke.co.payconnect.mocash.chama.util.Auth;
import ke.co.payconnect.mocash.chama.util.Config;
import ke.co.payconnect.mocash.chama.util.SendTo;
import ke.co.payconnect.mocash.chama.util.Utility;
import ke.co.payconnect.mocash.chama.view.activity.MainChamaActivity;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalExitViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalFullStatementViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalGroupBalanceViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalGroupLoanBalanceViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalMemberAddViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalMinistatementViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalSecretaryViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalTransactionViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalWithdrawViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;

import static ke.co.payconnect.mocash.chama.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.chama.util.Config.RETRIES;
import static ke.co.payconnect.mocash.chama.util.Config.TIMEOUT_MS;

public class AddMemberActivity extends AppCompatActivity {
    private final Context context = this;
    private final Activity activity = this;

    private Toolbar tbAddMember;
    private TextView tvConnectivityStatusAddMember;
    private EditText etFirstNameAddMember;
    private EditText etLastNameAddMember;
    private EditText etIdNumberAddMember;
    private CardView cvDateOfBirthAddMember;
    private TextView tvDateOfBirthAddMember;
    private EditText etAddressAddMember;
    private EditText etPhoneNumberAddMember;
    private CardView cvSaveAddMember;
    private TextView tvIdNumberAddMember;

    private String phoneNumber;
    private int groupType;
    private String groupNumber;
    private HashMap<String, String> addMemberMap;

    private int year;
    private int month;
    private int day;

    private MemberViewModel memberViewModel;
    private ApprovalViewModel approvalViewModel;
    private ApprovalTransactionViewModel approvalTransactionViewModel;
    private ApprovalSecretaryViewModel approvalSecretaryViewModel;
    private ApprovalExitViewModel approvalExitViewModel;
    private ApprovalGroupBalanceViewModel approvalGroupBalanceViewModel;
    private ApprovalMinistatementViewModel approvalMinistatementViewModel;
    private ApprovalFullStatementViewModel approvalFullStatementViewModel;
    private ApprovalWithdrawViewModel approvalWithdrawViewModel;
    private ApprovalMemberAddViewModel approvalMemberAddViewModel;
    private ApprovalGroupLoanBalanceViewModel approvalGroupLoanBalanceViewModel;


    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if (isConnected)
                tvConnectivityStatusAddMember.setVisibility(View.GONE);
            else
                tvConnectivityStatusAddMember.setVisibility(View.VISIBLE);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_member);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        else
            registerReceiver(connectionReceiver, intentFilter);

        tvConnectivityStatusAddMember = findViewById(R.id.tvConnectivityStatusAddMember);
        tbAddMember = findViewById(R.id.tbAddMember);
        setSupportActionBar(tbAddMember);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbAddMember.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        tvConnectivityStatusAddMember = findViewById(R.id.tvConnectivityStatusAddMember);
        etFirstNameAddMember = findViewById(R.id.etFirstNameAddMember);
        etLastNameAddMember = findViewById(R.id.etLastNameAddMember);
        etIdNumberAddMember = findViewById(R.id.etIdNumberAddMember);
        cvDateOfBirthAddMember = findViewById(R.id.cvDateOfBirthAddMember);
        tvDateOfBirthAddMember = findViewById(R.id.tvDateOfBirthAddMember);
        etAddressAddMember = findViewById(R.id.etAddressAddMember);
        etPhoneNumberAddMember = findViewById(R.id.etPhoneNumberAddMember);
        cvSaveAddMember = findViewById(R.id.cvSaveAddMember);
        tvIdNumberAddMember = findViewById(R.id.tvIdNumberAddMember);

        memberViewModel = ViewModelProviders.of(this).get(MemberViewModel.class);
        approvalViewModel = ViewModelProviders.of(this).get(ApprovalViewModel.class);
        approvalTransactionViewModel = ViewModelProviders.of(this).get(ApprovalTransactionViewModel.class);
        approvalTransactionViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalTransactionViewModel.class);
        approvalSecretaryViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalSecretaryViewModel.class);
        approvalExitViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalExitViewModel.class);
        approvalGroupBalanceViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalGroupBalanceViewModel.class);
        approvalMinistatementViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalMinistatementViewModel.class);
        approvalFullStatementViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalFullStatementViewModel.class);
        approvalWithdrawViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalWithdrawViewModel.class);
        approvalMemberAddViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalMemberAddViewModel.class);
        approvalGroupLoanBalanceViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalGroupLoanBalanceViewModel.class);

        toggleGroupTypeViews();

        setListeners();
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void setListeners() {
        cvSaveAddMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Auth.isAuthorized("signatory")) {
                    Toasty.info(context, R.string.action_not_permitted, Toast.LENGTH_SHORT, true).show();
                    return;
                }
                validation();
            }
        });

        cvDateOfBirthAddMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();

                year = c.get(Calendar.YEAR) - 18;
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("DefaultLocale")
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                        try {
                            tvDateOfBirthAddMember.setText(String.format("%d-%d-%d", year, monthOfYear + 1, dayOfMonth));
                        } catch (Exception e) {
                            Toasty.info(context, "Date of birth selection is required", Toast.LENGTH_SHORT, true).show();
                        }
                    }
                }, year, month, day);

                datePickerDialog.getDatePicker();
                datePickerDialog.show();
            }
        });
    }

    private void validation() {
        String firstName = etFirstNameAddMember.getText().toString();
        String lastName = etLastNameAddMember.getText().toString();
        String idNumber = etIdNumberAddMember.getText().toString();
        String dateOfBirth = tvDateOfBirthAddMember.getText().toString();
        String address = etAddressAddMember.getText().toString();
        String memberPhoneNumber = etPhoneNumberAddMember.getText().toString();

        if (firstName.isEmpty()) {
            Toasty.info(context, "Name cannot be blank", Toast.LENGTH_SHORT, true).show();
            return;
        }
        if (lastName.isEmpty()) {
            Toasty.info(context, "Name cannot be blank", Toast.LENGTH_SHORT, true).show();
            return;
        }
        if (idNumber.isEmpty()) {
            Toasty.info(context, "ID number cannot be blank", Toast.LENGTH_SHORT, true).show();
            return;
        }
        if (dateOfBirth.isEmpty()) {
            Toasty.info(context, "Date of birth cannot be blank", Toast.LENGTH_SHORT, true).show();
            return;
        }

        // Address is optional

        if (memberPhoneNumber.isEmpty()) {
            Toasty.info(context, "Phone number cannot be blank", Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (memberPhoneNumber.substring(0, 4).equals("+254"))
            memberPhoneNumber = memberPhoneNumber.substring(4);

        if (memberPhoneNumber.substring(0, 3).equals("254"))
            memberPhoneNumber = memberPhoneNumber.substring(3);

        if (memberPhoneNumber.substring(0, 1).equals("0"))
            memberPhoneNumber = memberPhoneNumber.substring(1);

        phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();
        groupType = AppController.getInstance().getChamaPreferenceManager().getGroupType();
        groupNumber = AppController.getInstance().getChamaPreferenceManager().getGroupNumber();
        String dateRegistered = Time.getDateTimeNow("yyyy-MM-dd HH:mm:ss");
        int groupType;
        groupType = AppController.getInstance().getChamaPreferenceManager().getGroupType();

        HashMap<String, String> map = new HashMap<>();
        map.put("group_number", groupNumber);
        map.put("first_name", firstName);
        map.put("last_name", lastName);
        map.put("member_phone_number", "254" + memberPhoneNumber);
        map.put("id_number", idNumber);
        map.put("address", address);
        map.put("date_registered", dateRegistered);
        map.put("date_of_birth", dateOfBirth);
        if (groupType == 1) {
            map.put("isMemberNumber", "Y");
        }
        map.put("phone_number", phoneNumber);


        MemberEntity memberEntity = new MemberEntity();
        memberEntity.setFirstName(map.get("first_name"));
        memberEntity.setLastName(map.get("last_name"));
        memberEntity.setPhoneNumber(map.get("member_phone_number"));
        memberEntity.setIdNumber(map.get("id_number"));
        memberEntity.setDateRegistered(map.get("date_registered"));
        memberEntity.setDateOfBirth(map.get("date_of_birth"));
        memberEntity.setSignatory("0");
        memberEntity.setSecretary("0");
        memberEntity.setMakerPhoneNumber("phone_number");
        memberEntity.setActive(true);

        final String sessionToken = AppController.getInstance().getChamaPreferenceManager().getSessionToken();

        confirmAction(map, sessionToken);
    }

    private void confirmAction(final HashMap<String, String> map, final String sessionToken) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        processRequest(map, sessionToken);
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:

                        break;
                }
            }
        };

        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.confirm_action));
        builder.setIcon(R.drawable.ic_unaitas);
        builder.setMessage("Proceed with the request?");
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.yes), dialogClickListener);
        builder.setNegativeButton(getString(R.string.no), dialogClickListener);
        builder.show();
    }

    SendTo st = new SendTo(context);
    HurlStack hurlStack = new HurlStack() {
        @Override
        protected HttpURLConnection createConnection(URL url) throws IOException {
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
            try {
                httpsURLConnection.setSSLSocketFactory(st.getSSLSocketFactory());
                httpsURLConnection.setHostnameVerifier(st.getHostnameVerifier());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return httpsURLConnection;
        }
    };

    private void processRequest(final HashMap<String, String> map, final String sessionToken) {
        final ProgressDialog loading = ProgressDialog.show(this, null, "Processing request", false, false);

        final String requestTag = "chama_add_member_request";
        final String url = Config.ADD_MEMBER;
        RequestQueue requestQueue = Volley.newRequestQueue(context);

        JsonObjectRequest addMemberReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(final JSONObject response) {
                        try {

                            loading.dismiss();

                            switch (response.getString("status")) {
                                case "success":
                                    loading.show();
                                    loading.setMessage("Please wait");

                                    insertToDatabase(map);
                                    onCompleteAlertDialog("Member details submitted");
                                    String signatory = AppController.getInstance().getChamaPreferenceManager().getSignatoryRole();
                                    if (signatory.equals("1")) {
                                        final HashMap<String, String> approvalMap = new HashMap<>();
                                        approvalMap.put("phone_number", String.valueOf(map.get("phone_number")));
                                        approvalMap.put("group_number", String.valueOf(map.get("group_number")));

                                        fetchApprovals(approvalMap, sessionToken);


                                    } else {
                                        responseAlert("The request has been received and is pending approval");
                                    }
                                    resetMemberForm();

                                    loading.dismiss();

                                    break;

                                case "failed":
                                    onCompleteAlertDialog(response.getString("message"));
                                case "fatal":
                                default:
                                    onCompleteAlertDialog("Could not save member details");
                                    break;
                            }

                        } catch (JSONException e) {
                            loading.dismiss();
                            onCompleteAlertDialog("No response found");

                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                loading.dismiss();
                onCompleteAlertDialog("An error occurred");

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error")) {
                                    Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":
                                    default:
                                        Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                Toasty.info(context, getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } else {
                    if (error instanceof NoConnectionError) {
                        Toasty.info(context, "No connection to the server", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof TimeoutError) {
                        Toasty.info(context, "The request has timed out", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof AuthFailureError) {
                        Toasty.info(context, "Authentication failed", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof ServerError) {
                        Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof NetworkError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof ParseError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else {
                        Toasty.info(context, getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    }
                }

            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionToken);

                return headers;
            }

        };

        addMemberReq.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));
        requestQueue.add(addMemberReq);
    }

    private void responseAlert(String message) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:

                        break;
                }
            }
        };

        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_unaitas);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.ok), dialogClickListener);
        builder.show();
    }

    private void insertToDatabase(HashMap<String, String> map) {
        MemberEntity memberEntity = new MemberEntity();
        memberEntity.setFirstName(map.get("first_name"));
        memberEntity.setLastName(map.get("last_name"));
        memberEntity.setPhoneNumber(map.get("member_phone_number"));
        memberEntity.setIdNumber(map.get("id_number"));
        memberEntity.setMakerPhoneNumber(map.get("phone_number"));
        memberEntity.setDateRegistered(map.get("date_registered"));
        memberEntity.setDateOfBirth(map.get("date_of_birth"));
        memberEntity.setSignatory(map.get("signatory"));
        memberEntity.setSecretary(map.get("secretary"));
        memberEntity.setActive(false);

        memberViewModel.insertMember(memberEntity);
    }

    private void resetMemberForm() {
        etFirstNameAddMember.getText().clear();
        etLastNameAddMember.getText().clear();
        etIdNumberAddMember.getText().clear();
        tvDateOfBirthAddMember.setText("");
        etAddressAddMember.getText().clear();
        etPhoneNumberAddMember.getText().clear();
    }

    private void onCompleteAlertDialog(String message) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(false);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_unaitas);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.ok), dialogClickListener);
        builder.show();
    }

    private void toggleGroupTypeViews() {
        int groupType = AppController.getInstance().getChamaPreferenceManager().getGroupType();

        if (groupType == 1)
            tvIdNumberAddMember.setText(getString(R.string.prompt_customer_number));
        else
            tvIdNumberAddMember.setText(getString(R.string.prompt_id_number));

    }

    private void fetchApprovals(final HashMap<String, String> map, final String sessionToken) {
        final ProgressDialog loading = ProgressDialog.show(context, null, "Synchronising approvals", false, false);

        final String requestTag = "fetch_approvals_post_txn_struct_request";
        final String url = Config.FETCH_GROUP_DATA_URL;
        RequestQueue requestQueue = Volley.newRequestQueue(AddMemberActivity.this);

        JsonObjectRequest fetchApprovalsReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(final JSONObject response) {
                        try {
                            switch (response.getString("status")) {
                                case "success":
                                    approvalViewModel.deleteAllApprovalsWhere("assignSecretary");
                                    approvalViewModel.deleteAllApprovalsWhere("assign-secretary");
                                    approvalSecretaryViewModel.deleteAllSecretaries();
                                    approvalViewModel.deleteAllApprovalsWhere("memberExit");
                                    approvalViewModel.deleteAllApprovalsWhere("exit");
                                    approvalExitViewModel.deleteAllExits();
                                    approvalViewModel.deleteAllApprovalsWhere("groupBalance");
                                    approvalViewModel.deleteAllApprovalsWhere("group-balance");
                                    approvalGroupBalanceViewModel.deleteAllGroupBalances();
                                    approvalViewModel.deleteAllApprovalsWhere("ministatement");
                                    approvalMinistatementViewModel.deleteAllMinistatements();
                                    approvalViewModel.deleteAllApprovalsWhere("full-statement");
                                    approvalFullStatementViewModel.deleteAllFullStatements();
                                    approvalViewModel.deleteAllApprovalsWhere("withdraw");
                                    approvalWithdrawViewModel.deleteAllApprovalWithdraws();
                                    approvalViewModel.deleteAllApprovalsWhere("member-add");
                                    approvalMemberAddViewModel.deleteAllMemberAddApprovals();
                                    approvalViewModel.deleteAllApprovalsWhere("group-loan-balance");
                                    approvalGroupLoanBalanceViewModel.deleteAllGroupLoanBalances();

                                    JSONObject approvalsObject = response.getJSONObject("approvals");
                                    JSONArray approvalTransactionsArray = approvalsObject.getJSONArray("transactionApproval");
                                    addApprovalTransactions(approvalTransactionsArray);

                                    JSONArray addApprovalSecretaries = approvalsObject.getJSONArray("assignSecretaryApproval");
                                    addApprovalSecretaries(addApprovalSecretaries);

                                    JSONArray addApprovalExits = approvalsObject.getJSONArray("memberExitApproval");
                                    addApprovalExits(addApprovalExits);

                                    JSONArray addApprovalGroupBalances = approvalsObject.getJSONArray("accountBalanceApproval");
                                    addApprovalGroupBalances(addApprovalGroupBalances);

                                    JSONArray addApprovalMinistatements = approvalsObject.getJSONArray("miniStatementApproval");
                                    addApprovalMinistatements(addApprovalMinistatements);

                                    JSONArray addApprovalFullStatements = approvalsObject.getJSONArray("fullStatementApproval");
                                    addApprovalFullStatements(addApprovalFullStatements);

                                    JSONArray addApprovalWithdraws = approvalsObject.getJSONArray("withdrawalApproval");
                                    addApprovalWithdraws(addApprovalWithdraws);

                                    JSONArray addApprovalMembersAdd = approvalsObject.getJSONArray("addMemberApproval");
                                    addApprovalMembersAdd(addApprovalMembersAdd);

                                    JSONArray addApprovalGroupLoanBalances = approvalsObject.getJSONArray("loanBalanceApproval");
                                    addApprovalGroupLoanBalances(addApprovalGroupLoanBalances);

                                    break;

                                case "failed":
                                case "fatal":
                                default:
                                    Toasty.info(context, "Failed to synchronise approvals", Toast.LENGTH_LONG, true).show();

                                    break;
                            }

                        } catch (JSONException e) {
                            Toasty.info(context, "Failed to synchronise approvals", Toast.LENGTH_LONG, true).show();

                        } finally {
                            loading.dismiss();
                            onCompleteAlertDialog("The request has been received and is pending approval");
                            startActivity(new Intent(context, MainChamaActivity.class));

                            overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                loading.dismiss();

                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));
                                if (responseErrorString.equals("The custom error module does not recognize this error")) {
                                    Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }
                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":
                                        Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();

                                        break;

                                    default:
                                        Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                Toasty.info(context, context.getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            Toasty.info(context, "Error " + networkResponse.statusCode + ": " + context.getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } else {
                    if (error instanceof NoConnectionError) {
                        Toasty.info(context, "No connection to the server", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof TimeoutError) {
                        Toasty.info(context, "The request has timed out", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof AuthFailureError) {
                        Toasty.info(context, "Authentication failed", Toast.LENGTH_SHORT, true).show();
                    } else if (error instanceof ServerError) {
                        Toasty.info(context, context.getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof NetworkError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else if (error instanceof ParseError) {
                        Toasty.info(context, "Network error", Toast.LENGTH_LONG, true).show();
                    } else {
                        Toasty.info(context, context.getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                    }
                }

            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionToken);

                return headers;
            }

        };

        fetchApprovalsReq.setRetryPolicy(new DefaultRetryPolicy(20000, RETRIES, BACKOFF_MULTIPLIER));
        requestQueue.add(fetchApprovalsReq);
    }

    private void addApprovalTransactions(JSONArray approvalTransactionsArray) throws JSONException {
        if (approvalTransactionsArray.length() > 0) {

            // Clear persistent transactions
            approvalViewModel.deleteAllApprovalsWhere("transaction");
            approvalTransactionViewModel.deleteAllApprovalTransactions();

            for (int i = 0; i < approvalTransactionsArray.length(); i++) {

                JSONObject approvalsObject = approvalTransactionsArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("transaction");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalTransactionEntity approvalTransactionEntity = new ApprovalTransactionEntity();

                approvalTransactionEntity.setAmount(approvalsObject.getString("amount"));
                approvalTransactionEntity.setTxnID(approvalsObject.getString("txnID"));
                approvalTransactionEntity.setTxnDate(approvalsObject.getString("txnDate"));
                approvalTransactionEntity.setTxnType(approvalsObject.getString("txnType"));
                approvalTransactionEntity.setBatchNumber(approvalsObject.getString("batchNumber"));
                approvalTransactionEntity.setMemberPhoneNumber(approvalsObject.getString("memberPhoneNumber"));
                approvalTransactionEntity.setMakerPhoneNumber(approvalsObject.getString("makerPhoneNumber"));
                approvalTransactionEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalTransactionEntity.setApprovalType("transaction");
                approvalTransactionEntity.setApprovalStatus("pending");
                approvalTransactionEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalTransactionEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalTransactionViewModel.insertApproval(approvalTransactionEntity);

            }
        }
    }

    private void addApprovalSecretaries(JSONArray approvalSecretariesArray) throws JSONException {
        if (approvalSecretariesArray.length() > 0) {

            // Clear persistent transactions
            approvalViewModel.deleteAllApprovalsWhere("assignSecretary");
            approvalSecretaryViewModel.deleteAllSecretaries();

            for (int i = 0; i < approvalSecretariesArray.length(); i++) {

                JSONObject approvalsObject = approvalSecretariesArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("secretary");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalSecretaryEntity approvalSecretaryEntity = new ApprovalSecretaryEntity();

                approvalSecretaryEntity.setCurrentSecretaryPhoneNumber(approvalsObject.getString("currentSecretaryPhoneNumber"));
                approvalSecretaryEntity.setProposedSecretaryPhoneNumber(approvalsObject.getString("proposedSecretaryPhoneNumber"));
                approvalSecretaryEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalSecretaryEntity.setAssignDate(approvalsObject.getString("assignDate"));
                approvalSecretaryEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalSecretaryEntity.setApprovalType("secretary");
                approvalSecretaryEntity.setApprovalStatus("pending");
                approvalSecretaryEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalSecretaryEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalSecretaryViewModel.insertApprovalSecretary(approvalSecretaryEntity);

            }
        }
    }

    private void addApprovalExits(JSONArray approvalExitssArray) throws JSONException {
        if (approvalExitssArray.length() > 0) {

            // Clear persistent transactions
            approvalViewModel.deleteAllApprovalsWhere("memberExit");
            approvalExitViewModel.deleteAllExits();

            for (int i = 0; i < approvalExitssArray.length(); i++) {

                JSONObject approvalsObject = approvalExitssArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("exit");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalExitEntity approvalExitEntity = new ApprovalExitEntity();

                approvalExitEntity.setMakerPhoneNumber(approvalsObject.getString("makerPhoneNumber"));
                approvalExitEntity.setMemberPhoneNumber(approvalsObject.getString("memberPhoneNumber"));
                approvalExitEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalExitEntity.setExitDate(approvalsObject.getString("exitDate"));
                approvalExitEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalExitEntity.setApprovalType("exit");
                approvalExitEntity.setApprovalStatus("pending");
                approvalExitEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalExitEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalExitViewModel.insertApprovalExit(approvalExitEntity);

            }
        }
    }

    private void addApprovalGroupBalances(JSONArray approvalGroupBalancesArray) throws JSONException {
        if (approvalGroupBalancesArray.length() > 0) {

            for (int i = 0; i < approvalGroupBalancesArray.length(); i++) {

                JSONObject approvalsObject = approvalGroupBalancesArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("group-balance");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalGroupBalanceEntity approvalGroupBalanceEntity = new ApprovalGroupBalanceEntity();

                approvalGroupBalanceEntity.setMemberPhoneNumber(approvalsObject.getString("memberPhoneNumber"));
                approvalGroupBalanceEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalGroupBalanceEntity.setRequestDate(approvalsObject.getString("transactionDate"));
                approvalGroupBalanceEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalGroupBalanceEntity.setApprovalType("group-balance");
                approvalGroupBalanceEntity.setApprovalStatus("pending");
                approvalGroupBalanceEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalGroupBalanceEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalGroupBalanceViewModel.insertApprovalGroupBalance(approvalGroupBalanceEntity);

            }
        }
    }

    private void addApprovalMinistatements(JSONArray approvalMinistatementsArray) throws JSONException {
        if (approvalMinistatementsArray.length() > 0) {

            for (int i = 0; i < approvalMinistatementsArray.length(); i++) {

                JSONObject approvalsObject = approvalMinistatementsArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("ministatement");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalMinistatementEntity approvalMinistatementEntity = new ApprovalMinistatementEntity();

                approvalMinistatementEntity.setMemberPhoneNumber(approvalsObject.getString("memberPhoneNumber"));
                approvalMinistatementEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalMinistatementEntity.setRequestDate(approvalsObject.getString("transactionDate"));
                approvalMinistatementEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalMinistatementEntity.setApprovalType("ministatement");
                approvalMinistatementEntity.setApprovalStatus("pending");
                approvalMinistatementEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalMinistatementEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalMinistatementViewModel.insertApprovalMinistatement(approvalMinistatementEntity);

            }
        }
    }

    private void addApprovalFullStatements(JSONArray approvalFullStatementsArray) throws JSONException {
        if (approvalFullStatementsArray.length() > 0) {

            for (int i = 0; i < approvalFullStatementsArray.length(); i++) {

                JSONObject approvalsObject = approvalFullStatementsArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("full-statement");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalFullStatementEntity approvalFullStatementEntity = new ApprovalFullStatementEntity();

                approvalFullStatementEntity.setMemberPhoneNumber(approvalsObject.getString("memberPhoneNumber"));
                approvalFullStatementEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalFullStatementEntity.setRequestDate(approvalsObject.getString("transactionDate"));
                approvalFullStatementEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalFullStatementEntity.setApprovalType("full-statement");
                approvalFullStatementEntity.setApprovalStatus("pending");
                approvalFullStatementEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalFullStatementEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalFullStatementViewModel.insertApprovalFullStatement(approvalFullStatementEntity);

            }
        }
    }

    private void addApprovalWithdraws(JSONArray approvalWithdrawsArray) throws JSONException {
        if (approvalWithdrawsArray.length() > 0) {

            // Clear persistent withdraws
            approvalViewModel.deleteAllApprovalsWhere("withdraw");
            approvalWithdrawViewModel.deleteAllApprovalWithdraws();

            for (int i = 0; i < approvalWithdrawsArray.length(); i++) {

                JSONObject approvalsObject = approvalWithdrawsArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("withdraw");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval withdraws
                ApprovalWithdrawEntity approvalWithdrawEntity = new ApprovalWithdrawEntity();

                approvalWithdrawEntity.setTxnID(approvalsObject.getString("txnID"));
                approvalWithdrawEntity.setAmount(approvalsObject.getString("withdrawalAmount"));
                approvalWithdrawEntity.setWithdrawDate(approvalsObject.getString("txnDate"));
                approvalWithdrawEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalWithdrawEntity.setMakerPhoneNumber(approvalsObject.getString("makerPhoneNumber"));
                approvalWithdrawEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalWithdrawEntity.setApprovalType("withdraw");
                approvalWithdrawEntity.setDestination(approvalsObject.getString("destinationAccount"));
                approvalWithdrawEntity.setApprovalStatus("pending");
                approvalWithdrawEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalWithdrawEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalWithdrawViewModel.insertApprovalWithdraw(approvalWithdrawEntity);

            }
        }
    }

    private void addApprovalMembersAdd(JSONArray addApprovalMembersAddArray) throws JSONException {
        if (addApprovalMembersAddArray.length() > 0) {

            // Clear persistent withdraws
            approvalViewModel.deleteAllApprovalsWhere("member-add");
            approvalWithdrawViewModel.deleteAllApprovalWithdraws();

            for (int i = 0; i < addApprovalMembersAddArray.length(); i++) {

                JSONObject approvalsObject = addApprovalMembersAddArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("member-add");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval member add
                ApprovalMemberAddEntity approvalMemberAddEntity = new ApprovalMemberAddEntity();

                approvalMemberAddEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalMemberAddEntity.setMakerPhonenumber(approvalsObject.getString("makerPhoneNumber"));
                approvalMemberAddEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalMemberAddEntity.setApprovalType("withdraw");
                approvalMemberAddEntity.setApprovalStatus("pending");
                approvalMemberAddEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalMemberAddEntity.setApprovalDate(approvalsObject.getString("approvalDate"));

                approvalMemberAddViewModel.insertApprovalMemberAdd(approvalMemberAddEntity);

            }
        }
    }

    private void addApprovalGroupLoanBalances(JSONArray approvalGroupLoanBalancesArray) throws JSONException {
        if (approvalGroupLoanBalancesArray.length() > 0) {

            for (int i = 0; i < approvalGroupLoanBalancesArray.length(); i++) {

                JSONObject approvalsObject = approvalGroupLoanBalancesArray.getJSONObject(i);

                // Add approvals
                ApprovalEntity approvalEntity = new ApprovalEntity();

                approvalEntity.setApprovalType("group-balance");
                approvalEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalEntity.setApprovalStatus("pending");

                approvalViewModel.insertApproval(approvalEntity);

                // Add approval transactions
                ApprovalGroupLoanBalanceEntity approvalGroupLoanBalanceEntity = new ApprovalGroupLoanBalanceEntity();

                approvalGroupLoanBalanceEntity.setMemberPhoneNumber(approvalsObject.getString("memberPhoneNumber"));
                approvalGroupLoanBalanceEntity.setNarration(approvalsObject.getString("txnNarration"));
                approvalGroupLoanBalanceEntity.setRequestDate(approvalsObject.getString("transactionDate"));
                approvalGroupLoanBalanceEntity.setApprovalID(approvalsObject.getString("approvalID"));
                approvalGroupLoanBalanceEntity.setApprovalType("group-balance");
                approvalGroupLoanBalanceEntity.setApprovalStatus("pending");
                approvalGroupLoanBalanceEntity.setApprovalNarration(approvalsObject.getString("approvalNarration"));
                approvalGroupLoanBalanceEntity.setApprovalDate(approvalsObject.getString("approvalDate"));


                approvalGroupLoanBalanceViewModel.insertApprovalGroupLoanBalance(approvalGroupLoanBalanceEntity);

            }
        }
    }

}

package ke.co.payconnect.mocash.chama.view.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.LinkedList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.TransactionEntity;
import ke.co.payconnect.mocash.chama.util.Access;
import ke.co.payconnect.mocash.chama.util.Auth;
import ke.co.payconnect.mocash.chama.view.activity.member.ChangePinChamaActivity;
import ke.co.payconnect.mocash.chama.view.activity.member.ChatActivity;
import ke.co.payconnect.mocash.chama.view.activity.member.MemberActivity;
import ke.co.payconnect.mocash.chama.view.activity.member.RequestsActivity;
import ke.co.payconnect.mocash.chama.view.activity.member.SelectTransactionCategoryActivity;
import ke.co.payconnect.mocash.chama.view.activity.secretary.SelectMemberAssignSecretaryActivity;
import ke.co.payconnect.mocash.chama.view.activity.secretary.SelectMemberTableBankingActivity;
import ke.co.payconnect.mocash.chama.view.activity.secretary.SetSessionTotalActivity;
import ke.co.payconnect.mocash.chama.view.activity.signatory.AddMemberActivity;
import ke.co.payconnect.mocash.chama.view.activity.signatory.ApprovalActivity;
import ke.co.payconnect.mocash.chama.view.activity.signatory.GroupRequestsActivity;
import ke.co.payconnect.mocash.chama.view.adapter.member.GroupTransactionsRecyclerViewAdapter;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.TransactionViewModel;

public class MainChamaActivity extends AppCompatActivity {
    private final Activity activity = this;
    private final Context context = this;

    private Toolbar tbChamaMain;
    private TextView tvGroupChamaMain;
    private RecyclerView rvChamaMain;
    private TextView tvNoDataChamaMain;
    private TextView tvSummaryTransactionsChamaMain;
    private CardView cvChatChamaMain;
    private CardView cvRequestsChamaMain;
    private CardView cvSecretaryChamaMain;
    private CardView cvSignatoryChamaMain;
    private CardView cvAddEntryChamaMain;
    private TextView tvAddEntryChamaMain;
    private CardView cvStructuredTableBankingAddEntry;
    private CardView cvAssignRoleChamaMain;
    private CardView cvGroupRequestsChamaMain;
    private CardView cvAddMemberChamaMain;
    private CardView cvMembersChamaMain;
    private CardView cvApprovalsChamaMain;

    private TransactionViewModel transactionViewModel;
    private MemberViewModel memberViewModel;
    private GroupTransactionsRecyclerViewAdapter groupTransactionsRecyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_chama);

        if (Build.VERSION.SDK_INT >= 21) {
            Window w = getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }

        tbChamaMain = findViewById(R.id.tbChamaMain);

        setSupportActionBar(tbChamaMain);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tvGroupChamaMain = findViewById(R.id.tvGroupChamaMain);
        rvChamaMain = findViewById(R.id.rvChamaMain);
        tvNoDataChamaMain = findViewById(R.id.tvNoDataChamaMain);
        tvSummaryTransactionsChamaMain = findViewById(R.id.tvSummaryTransactionsChamaMain);
        cvChatChamaMain = findViewById(R.id.cvChatChamaMain);
        cvRequestsChamaMain = findViewById(R.id.cvRequestsChamaMain);
        cvSecretaryChamaMain = findViewById(R.id.cvSecretaryChamaMain);
        cvSignatoryChamaMain = findViewById(R.id.cvSignatoryChamaMain);
        cvAddEntryChamaMain = findViewById(R.id.cvAddEntryChamaMain);
        tvAddEntryChamaMain = findViewById(R.id.tvAddEntryChamaMain);
        cvStructuredTableBankingAddEntry = findViewById(R.id.cvStructuredTableBankingAddEntry);
        cvAssignRoleChamaMain = findViewById(R.id.cvAssignRoleChamaMain);
        cvGroupRequestsChamaMain = findViewById(R.id.cvGroupRequestsChamaMain);
        cvAddMemberChamaMain = findViewById(R.id.cvAddMemberChamaMain);
        cvMembersChamaMain = findViewById(R.id.cvMembersChamaMain);
        cvApprovalsChamaMain = findViewById(R.id.cvApprovalsChamaMain);

        memberViewModel = ViewModelProviders.of(this).get(MemberViewModel.class);

        int groupType = AppController.getInstance().getChamaPreferenceManager().getGroupType();
        if (groupType == 2) {
            cvAddEntryChamaMain.setVisibility(View.GONE);
            tvAddEntryChamaMain.setVisibility(View.GONE);
        } else {
            cvAddEntryChamaMain.setVisibility(View.VISIBLE);
            tvAddEntryChamaMain.setVisibility(View.VISIBLE);
        }

        setHeaderData();

        setListeners();

        effectPermissions();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);

        setupRecyclerView();
    }

    @Override
    public void onBackPressed() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        startActivity(new Intent(context, LoginGroupChamaActivity.class));

                        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage("Go back to groups list");
        builder.setPositiveButton(getString(R.string.ok), dialogClickListener);
        builder.setNegativeButton(getString(R.string.cancel), dialogClickListener);
        builder.show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chama_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_logout:
                if (AppController.getInstance().getChamaPreferenceManager().getRememberPhone())
                    Access.logout(activity, true, false);
                else
                    Access.logout(activity, false, false);


                Toasty.info(context, "You have been logged out", Toast.LENGTH_SHORT, true).show();
                break;
            case R.id.action_change_pin:
                startActivity(new Intent(context, ChangePinChamaActivity.class));

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setHeaderData() {
        String groupName = AppController.getInstance().getChamaPreferenceManager().getGroupName();
        tvGroupChamaMain.setText(groupName);
    }

    private void setupRecyclerView() {
        transactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel.class);
        transactionViewModel.getSummaryLimitTransactionsLive(50).observe(this, new Observer<List<TransactionEntity>>() {
            @Override
            public void onChanged(List<TransactionEntity> transactionEntities) {

                if (transactionEntities.size() > 0) {
                    toggleDataViews(true);

                    List<TransactionEntity> filteredTransactions = new LinkedList<>();

                    int count = 0;
                    for (TransactionEntity transactionEntity : transactionEntities) {
                        if (transactionEntity.getTxnCategory().equals("TXN-GRP-POSTING")) {
                            // Group posting
                            List<TransactionEntity> batchTransactions = transactionViewModel.getBatchTransactions(transactionEntity.getBatchNumber());
                            double amount = 0;

                            for (TransactionEntity transaction : batchTransactions)
                                amount = amount + Double.parseDouble(transaction.getAmount());

                            if (transactionEntity.getMemberPhoneNumber().length() > 2) {
                                // Structured
                                if (transactionEntity.getMemberPhoneNumber().substring(0, 4).equals("2547")) {
                                    // Customer type -> Member
                                    if (!memberViewModel.doesMemberExistWithPhoneNumber(transactionEntity.getMemberPhoneNumber()))
                                        continue;
                                }
                            }

                            if (transactionEntity.getTxnProcessingStatus().toLowerCase().trim().equals("processed")) {
                                TransactionEntity txn = new TransactionEntity();
                                txn.setTxnID(transactionEntity.getTxnID());
                                txn.setTxnDate(transactionEntity.getTxnDate());
                                txn.setBatchNumber(transactionEntity.getBatchNumber());
                                txn.setMemberPhoneNumber(transactionEntity.getMemberPhoneNumber());
                                txn.setAmount(String.valueOf(amount));

                                filteredTransactions.add(txn);
                                count++;
                            }
                        } else if (transactionEntity.getTxnCategory().equals("TXN-TBL-BANKING")) {
                            // Table banking
                            List<TransactionEntity> batchTransactions = transactionViewModel.getBatchTransactions(transactionEntity.getBatchNumber());
                            double amount = 0;

                            for (TransactionEntity transaction : batchTransactions)
                                amount = amount + Double.parseDouble(transaction.getAmount());

                            if (transactionEntity.getMemberPhoneNumber().length() > 2) {
                                // Structured
                                if (transactionEntity.getMemberPhoneNumber().substring(0, 4).equals("2547")) {
                                    // Customer type -> Member
                                    if (!memberViewModel.doesMemberExistWithPhoneNumber(transactionEntity.getMemberPhoneNumber()))
                                        continue;
                                }
                            }

                            if (transactionEntity.getTxnProcessingStatus().toLowerCase().trim().equals("authorised")) {
                                TransactionEntity txn = new TransactionEntity();
                                txn.setTxnID(transactionEntity.getTxnID());
                                txn.setTxnDate(transactionEntity.getTxnDate());
                                txn.setBatchNumber(transactionEntity.getBatchNumber());
                                txn.setMemberPhoneNumber(transactionEntity.getMemberPhoneNumber());
                                txn.setAmount(String.valueOf(amount));

                                filteredTransactions.add(txn);
                                count++;
                            }
                        }

                        if (count == 3)
                            break;
                    }

                    if (filteredTransactions.size() > 0) {
                        groupTransactionsRecyclerViewAdapter = new GroupTransactionsRecyclerViewAdapter(context, filteredTransactions);
                        groupTransactionsRecyclerViewAdapter.notifyDataSetChanged();

                        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
                        rvChamaMain.setAdapter(groupTransactionsRecyclerViewAdapter);
                        rvChamaMain.setLayoutManager(layoutManager);
                    } else {
                        toggleDataViews(false);
                    }
                } else {
                    toggleDataViews(false);
                }
            }
        });
    }

    private void setListeners() {

        tvSummaryTransactionsChamaMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, SelectTransactionCategoryActivity.class));

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvRequestsChamaMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, RequestsActivity.class));

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvChatChamaMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, ChatActivity.class));

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvAddEntryChamaMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, SetSessionTotalActivity.class));

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvStructuredTableBankingAddEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, SelectMemberTableBankingActivity.class));

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });


        cvAssignRoleChamaMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, SelectMemberAssignSecretaryActivity.class));

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvGroupRequestsChamaMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, GroupRequestsActivity.class));

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvAddMemberChamaMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, AddMemberActivity.class));

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvMembersChamaMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, MemberActivity.class));

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvApprovalsChamaMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, ApprovalActivity.class));

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });
    }

    private void effectPermissions() {
        cvSecretaryChamaMain.setVisibility(View.GONE);
        cvSecretaryChamaMain.setEnabled(false);

        cvSignatoryChamaMain.setVisibility(View.GONE);
        cvSignatoryChamaMain.setEnabled(false);

        if (Auth.isAuthorized("signatory")) {
            cvSignatoryChamaMain.setVisibility(View.VISIBLE);
            cvSignatoryChamaMain.setEnabled(true);
        }

        if (Auth.isAuthorized("secretary")) {
            cvSecretaryChamaMain.setVisibility(View.VISIBLE);
            cvSecretaryChamaMain.setEnabled(true);
        }
    }

    private void toggleDataViews(boolean hasItems) {
        if (hasItems)
            tvNoDataChamaMain.setVisibility(View.GONE);
        else
            tvNoDataChamaMain.setVisibility(View.VISIBLE);
    }

}

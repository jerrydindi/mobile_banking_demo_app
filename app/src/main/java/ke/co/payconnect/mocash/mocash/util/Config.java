package ke.co.payconnect.mocash.mocash.util;


public class Config {
//    private static final String BASE_URL = "https://mocash.unaitas.com:53512/v1/";
//    private static final String BASE_URL = "http://10.1.2.60:1400/v1/";
    private static final String BASE_URL = "http://3.67.238.114:1400/v1/";

    public static final String DETAILS_MOCASH_URL = BASE_URL + "details-mocash";
    public static final String LOGIN_URL = BASE_URL + "login";
    public static final String CHANGE_PIN_URL = BASE_URL + "change-pin";
    public static final String BALANCE_URL = BASE_URL + "balance";
    public static final String MINISTATEMENT_URL = BASE_URL + "ministatement";
    public static final String FUNDS_TRANSFER_OWN_URL = BASE_URL + "funds-transfer-own";
    public static final String FUNDS_TRANSFER_OTHER_URL = BASE_URL + "funds-transfer-other";
    public static final String MOBILE_MONEY_URL = BASE_URL + "mobile-money";
    public static final String AIRTIME_URL = BASE_URL + "airtime";
    public static final String PAY_BILL_URL = BASE_URL + "paybill";
    public static final String ALERTS_URL = BASE_URL + "alerts";
    public static final String ATM_URL = BASE_URL + "atm";
    public static final String AGENT_WITHDRAW_URL = BASE_URL + "agency-withdraw";
    public static final String FEEDBACK_URL = BASE_URL + "feedback";
    static final String LOCATION_URL = BASE_URL + "location";

    public static final String GET_LOAN_ACCOUNTS = BASE_URL + "loan/get-accounts";

    public static final String CHECKOFF_REGISTER = BASE_URL + "loan/checkoff/register";
    public static final String CHECKOFF_EMPLOYEE_DETAILS_URL = BASE_URL + "loan/checkoff/employee-details";
    public static final String CHECKOFF_LOAN_REQUEST_URL = BASE_URL + "loan/checkoff/request";
    public static final String CHECKOFF_REPAYMENT_URL = BASE_URL + "loan/checkoff/repayment";
    public static final String CHECKOFF_PAYOFF_URL = BASE_URL + "loan/checkoff/payoff";

    public static final String INUKA_CHECK_LIMIT_URL = BASE_URL + "loan/inuka/check-limit";
    public static final String INUKA_CHECK_LOAN_BALANCE_URL = BASE_URL + "loan/inuka/check-loan-balance";
    public static final String INUKA_REQUEST_URL = BASE_URL + "loan/inuka/request";
    public static final String INUKA_REPAYMENT_URL = BASE_URL + "loan/inuka/repayment";
    public static final String INUKA_PAYOFF_URL = BASE_URL + "loan/inuka/payoff";

    public static final String MOCASH_CHECK_LIMIT_URL = BASE_URL + "loan/m-advance/check-limit";
    public static final String MOCASH_CHECK_LOAN_BALANCE_URL = BASE_URL + "loan/m-advance/check-loan-balance";
    public static final String MOCASH_REQUEST_URL = BASE_URL + "loan/m-advance/request";
    public static final String MOCASH_REPAYMENT_URL = BASE_URL + "loan/m-advance/repayment";
    public static final String MOCASH_PAYOFF_URL = BASE_URL + "loan/m-advance/payoff";

    public static final String DATABASE_NAME = "mocashDB";

    // Volley request configs
    // The default socket timeout in milliseconds
    public static final int TIMEOUT_MS = 30000;
    public static final int RETRIES = 0;
    public static final float BACKOFF_MULTIPLIER = 1f;

    // In seconds
    public static int sessionExpiry = 300; // (Default) 1 Minute
}

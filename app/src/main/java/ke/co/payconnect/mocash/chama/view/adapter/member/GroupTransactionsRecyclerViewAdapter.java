package ke.co.payconnect.mocash.chama.view.adapter.member;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import io.captano.utility.Money;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;
import ke.co.payconnect.mocash.chama.data.entity.TransactionEntity;
import ke.co.payconnect.mocash.chama.util.Utility;
import ke.co.payconnect.mocash.chama.view.activity.member.TransactionBatchDetailsActivity;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;

public class GroupTransactionsRecyclerViewAdapter extends RecyclerView.Adapter<GroupTransactionsRecyclerViewAdapter.TransactionViewHolder> {
    private final Context context;
    private List<TransactionEntity> transactionEntities;

    public GroupTransactionsRecyclerViewAdapter(Context context, List<TransactionEntity> transactionEntities) {
        this.context = context;
        this.transactionEntities = transactionEntities;
    }

    @NonNull
    @Override
    public TransactionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_list_transactions, parent, false);
        return new TransactionViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull TransactionViewHolder holder, int position) {
        try {
            final TransactionEntity transactionEntity = transactionEntities.get(position);

            String member;

            if (transactionEntity.getMemberPhoneNumber().length() > 2) {
                // Structured
                if (transactionEntity.getMemberPhoneNumber().substring(0, 4).equals("2547")) {
                    MemberViewModel memberViewModel = ViewModelProviders.of((FragmentActivity) context).get(MemberViewModel.class);

                    MemberEntity memberEntity = memberViewModel.findMemberByPhoneNumber(transactionEntity.getMemberPhoneNumber());
                    member = memberEntity.getFirstName() + " " + memberEntity.getLastName();
                } else {
                    member = AppController.getInstance().getChamaPreferenceManager().getGroupName();
                }
            } else {
                // Unstructured
                // This type uses the group number as memberPhoneNumber
                member = AppController.getInstance().getChamaPreferenceManager().getGroupName();
            }

            holder.tvNoStructTxn.setText(String.valueOf(position + 1));
            holder.tvBatchNoStructTxn.setText(transactionEntity.getBatchNumber());
            holder.tvMemberStructTxn.setText(member);
            holder.tvAmountStructTxn.setText(Money.format(Double.parseDouble(transactionEntity.getAmount())));
            holder.clStructTxn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, TransactionBatchDetailsActivity.class);
                    i.putExtra("txn_id", transactionEntity.getTxnID());
                    context.startActivity(i);

                    ((Activity) context).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                }
            });

            Calendar calendar;
            String date;
            try {
                calendar = Utility.getDateTime(transactionEntity.getTxnDate(), "yyyy-MM-dd");
                @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                date = dateFormat.format(calendar.getTime());
                holder.tvDateStructTxn.setText(date);
            } catch (ParseException e) {
                holder.tvDateStructTxn.setText(transactionEntity.getTxnDate());
            }
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return this.transactionEntities.size();
    }

    class TransactionViewHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout clStructTxn;
        private TextView tvNoStructTxn;
        private TextView tvDateStructTxn;
        private TextView tvBatchNoStructTxn;
        private TextView tvMemberStructTxn;
        private TextView tvAmountStructTxn;

        TransactionViewHolder(View view) {
            super(view);

            clStructTxn = view.findViewById(R.id.clStructTxn);
            tvNoStructTxn = view.findViewById(R.id.tvNoStructTxn);
            tvDateStructTxn = view.findViewById(R.id.tvDateStructTxn);
            tvBatchNoStructTxn = view.findViewById(R.id.tvBatchNoStructTxn);
            tvMemberStructTxn = view.findViewById(R.id.tvMemberStructTxn);
            tvAmountStructTxn = view.findViewById(R.id.tvAmountStructTxn);
        }

    }
}

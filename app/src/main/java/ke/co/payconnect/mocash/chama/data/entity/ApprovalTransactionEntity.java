package ke.co.payconnect.mocash.chama.data.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class ApprovalTransactionEntity {
    @PrimaryKey(autoGenerate = true)
    private int uid;
    @ColumnInfo(name = "member_phone_number")
    private String memberPhoneNumber;
    @ColumnInfo(name = "amount")
    private String amount;
    @ColumnInfo(name = "txn_id")
    private String txnID;
    @ColumnInfo(name = "batch_number")
    private String batchNumber;
    @ColumnInfo(name = "maker_phone_number")
    private String makerPhoneNumber;
    @ColumnInfo(name = "txn_date")
    private String txnDate;
    @ColumnInfo(name = "txn_type")
    private String txnType;
    @ColumnInfo(name = "approval_id")
    private String approvalID;
    @ColumnInfo(name = "approval_status")
    private String approvalStatus;
    @ColumnInfo(name = "approval_type")
    private String approvalType;
    @ColumnInfo(name = "approval_narration")
    private String approvalNarration;
    @ColumnInfo(name = "approval_date")
    private String approvalDate;


    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getMemberPhoneNumber() {
        return memberPhoneNumber;
    }

    public void setMemberPhoneNumber(String memberPhoneNumber) {
        this.memberPhoneNumber = memberPhoneNumber;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTxnID() {
        return txnID;
    }

    public void setTxnID(String txnID) {
        this.txnID = txnID;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getMakerPhoneNumber() {
        return makerPhoneNumber;
    }

    public void setMakerPhoneNumber(String makerPhoneNumber) {
        this.makerPhoneNumber = makerPhoneNumber;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getApprovalID() {
        return approvalID;
    }

    public void setApprovalID(String approvalID) {
        this.approvalID = approvalID;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getApprovalType() {
        return approvalType;
    }

    public void setApprovalType(String approvalType) {
        this.approvalType = approvalType;
    }

    public String getApprovalNarration() {
        return approvalNarration;
    }

    public void setApprovalNarration(String approvalNarration) {
        this.approvalNarration = approvalNarration;
    }

    public String getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(String approvalDate) {
        this.approvalDate = approvalDate;
    }
}

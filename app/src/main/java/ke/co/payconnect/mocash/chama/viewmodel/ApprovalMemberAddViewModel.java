package ke.co.payconnect.mocash.chama.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.ChamaDatabase;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalMemberAddEntity;


public class ApprovalMemberAddViewModel extends AndroidViewModel {

    private ChamaDatabase db;


    public ApprovalMemberAddViewModel(@NonNull Application application) {
        super(application);
        db = ChamaDatabase.getAppDatabaseInstance(this.getApplication());
    }

    public ApprovalMemberAddEntity findMemberAddApprovalById(String approvalId){
        return db.approvalMemberAddDao().findMemberAddApprovalById(approvalId);
    }

    public boolean doesMemberAddApprovalExistWithApprovalId(String approvalID){
        return db.approvalMemberAddDao().doesMemberAddApprovalExistWithApprovalId(approvalID);
    }

    public  void updateApprovalMemberAddSetData(String approvalID, String approvalStatus, String approvalNarration, String approvalDate) {
        db.approvalMemberAddDao().updateApprovalMemberAddSetData(approvalID, approvalStatus, approvalNarration, approvalDate);
    }

    public LiveData<List<ApprovalMemberAddEntity>> getApprovalAddMembersByTypeLive(String approvalType) {
        return  db.approvalMemberAddDao().getApprovalAddMembersByTypeLive(approvalType);
    }

    public void insertApprovalMemberAdd(ApprovalMemberAddEntity approvalTransactionEntity) {
        db.approvalMemberAddDao().insertApprovalMemberAdd(approvalTransactionEntity);
    }

    public void deleteAllMemberAddApprovals() {
        db.approvalMemberAddDao().deleteAllMemberAddApprovals();
    }
}

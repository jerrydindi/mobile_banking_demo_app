package ke.co.payconnect.mocash.chama.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.entity.ApprovalEntity;

@Dao
public interface ApprovalDao {

    @Query("SELECT DISTINCT uid, approval_type, approval_id, approval_status FROM ApprovalEntity WHERE approval_status == :approvalStatus ORDER BY approval_id ASC")
    LiveData<List<ApprovalEntity>> getApprovalsLive(final String approvalStatus);

    @Query("SELECT uid, approval_type, approval_id, approval_status FROM ApprovalEntity WHERE approval_status == :approvalStatus GROUP BY approval_type ORDER BY approval_id ASC")
    LiveData<List<ApprovalEntity>> getSummaryApprovalsLive(final String approvalStatus);

    @Query("SELECT uid, approval_type, approval_id, approval_status FROM ApprovalEntity WHERE approval_status == :approvalStatus AND approval_type == :approvalType ORDER BY approval_id")
    List<ApprovalEntity> getTypeApprovals(final String approvalStatus, final String approvalType);

    @Query("SELECT EXISTS(SELECT 1 FROM ApprovalEntity WHERE approval_id == :approvalId LIMIT 1)")
    boolean doesApprovalExistWithApprovalId(String approvalId);

    @Query("SELECT COUNT(*) FROM ApprovalEntity")
    int getApprovalCount();

    @Insert
    void insertApproval(ApprovalEntity... approvalEntities);

    @Update
    void updateApproval(ApprovalEntity... approvalEntities);

    @Delete
    void deleteApproval(ApprovalEntity... approvalEntities);

    @Query("DELETE FROM ApprovalEntity")
    void deleteAllApprovals();

    @Query("DELETE FROM ApprovalEntity WHERE approval_type == :approvalType")
    void deleteAllApprovalsWhere(final String approvalType);
}

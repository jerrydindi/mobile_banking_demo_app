package ke.co.payconnect.mocash.chama.view.adapter.signatory;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalSecretaryEntity;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;
import ke.co.payconnect.mocash.chama.util.Auth;
import ke.co.payconnect.mocash.chama.util.Utility;
import ke.co.payconnect.mocash.chama.view.activity.signatory.ApproveSecretaryActivity;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;

public class ApprovalSecretariesRecyclerViewAdapter extends RecyclerView.Adapter<ApprovalSecretariesRecyclerViewAdapter.ApprovalViewHolder> {
    private final Context context;
    private List<ApprovalSecretaryEntity> approvalSecretaryEntity;

    public ApprovalSecretariesRecyclerViewAdapter(Context context, List<ApprovalSecretaryEntity> approvalSecretaryEntity) {
        this.context = context;
        this.approvalSecretaryEntity = approvalSecretaryEntity;
    }

    @NonNull
    @Override
    public ApprovalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_approval_secretary, parent, false);
        return new ApprovalViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ApprovalViewHolder holder, int position) {
        try {
            final ApprovalSecretaryEntity approvalEntity = approvalSecretaryEntity.get(position);

            MemberViewModel memberViewModel = ViewModelProviders.of((FragmentActivity) context).get(MemberViewModel.class);

            MemberEntity currentMemberEntity = memberViewModel.findMemberByPhoneNumber(approvalEntity.getCurrentSecretaryPhoneNumber());
            MemberEntity proposedMemberEntity = memberViewModel.findMemberByPhoneNumber(approvalEntity.getProposedSecretaryPhoneNumber());

            holder.tvNoStructApprovalExit.setText(String.valueOf(position + 1));
            holder.tvCurrentStructApprovalExit.setText(currentMemberEntity.getFirstName() + " " + currentMemberEntity.getLastName());
            holder.tvProposedStructApprovalExit.setText(proposedMemberEntity.getFirstName() + " " + proposedMemberEntity.getLastName());

            String assignDate = approvalEntity.getAssignDate();

            try {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                Date date = format.parse(assignDate);
                Date dateToCompare = format.parse("2000-01-01");

                if ((date != null ? date.compareTo(dateToCompare) : 0) == 0) {
                    assignDate = "NA";
                }
            } catch (ParseException ignored) {
            }

            if (!assignDate.equals("NA")) {
                Calendar calendar;
                String date;
                try {
                    calendar = Utility.getDateTime(assignDate, "yyyy-MM-dd");
                    @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    date = dateFormat.format(calendar.getTime());
                    holder.tvDateStructApprovalExit.setText(date);
                } catch (ParseException e) {
                    holder.tvDateStructApprovalExit.setText(approvalEntity.getAssignDate());
                }
            } else {
                holder.tvDateStructApprovalExit.setText(assignDate);
            }

            if (!approvalEntity.getApprovalStatus().toLowerCase().equals("pending"))
                holder.clStructApprovalExit.setBackgroundColor(context.getResources().getColor(R.color.cG6));

            holder.clStructApprovalExit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!Auth.isAuthorized("signatory")) {
                        Toasty.info(context, R.string.action_not_permitted, Toast.LENGTH_SHORT, true).show();
                        return;
                    }

                    Intent i = new Intent(context, ApproveSecretaryActivity.class);
                    i.putExtra("approval_id", approvalEntity.getApprovalID());
                    context.startActivity(i);

                    ((Activity) context).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                }
            });
        } catch (Exception ignored) {
        }
    }

    @Override
    public int getItemCount() {
        return this.approvalSecretaryEntity.size();
    }

    class ApprovalViewHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout clStructApprovalExit;
        private TextView tvNoStructApprovalExit;
        private TextView tvDateStructApprovalExit;
        private TextView tvCurrentStructApprovalExit;
        private TextView tvProposedStructApprovalExit;

        ApprovalViewHolder(View view) {
            super(view);

            clStructApprovalExit = view.findViewById(R.id.clStructApprovalExit);
            tvNoStructApprovalExit = view.findViewById(R.id.tvNoStructApprovalExit);
            tvDateStructApprovalExit = view.findViewById(R.id.tvDateStructApprovalExit);
            tvCurrentStructApprovalExit = view.findViewById(R.id.tvCurrentSecretaryStructApprovalExit);
            tvProposedStructApprovalExit = view.findViewById(R.id.tvProposedSecretaryStructApprovalExit);
        }

    }
}

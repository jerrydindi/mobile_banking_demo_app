package ke.co.payconnect.mocash.chama.data.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class ApprovalMemberAddEntity {

    @PrimaryKey(autoGenerate = true)
    private int uid;

    @ColumnInfo(name = "add_date")
    private String addDate;

    @ColumnInfo(name = "approval_status")
    private String approvalStatus;

    @ColumnInfo(name = "approval_narration")
    private String approvalNarration;

    @ColumnInfo(name = "approval_id")
    private String approvalID;

    @ColumnInfo(name = "approval_type")
    private String approvalType;

    @ColumnInfo(name = "narration")
    private String narration;

    @ColumnInfo(name = "signatory_phonenumber")
    private String signatoryPhonenumber;

    @ColumnInfo(name = "maker_phonenumber")
    private String makerPhonenumber;

    @ColumnInfo(name = "approval_date")
    private String approvalDate;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getAddDate() {
        return addDate;
    }

    public void setAddDate(String addDate) {
        this.addDate = addDate;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getApprovalNarration() {
        return approvalNarration;
    }

    public void setApprovalNarration(String approvalNarration) {
        this.approvalNarration = approvalNarration;
    }

    public String getApprovalID() {
        return approvalID;
    }

    public void setApprovalID(String approvalID) {
        this.approvalID = approvalID;
    }

    public String getApprovalType() {
        return approvalType;
    }

    public void setApprovalType(String approvalType) {
        this.approvalType = approvalType;
    }

    public String getSignatoryPhonenumber() {
        return signatoryPhonenumber;
    }

    public void setSignatoryPhonenumber(String signatoryPhonenumber) {
        this.signatoryPhonenumber = signatoryPhonenumber;
    }

    public String getMakerPhonenumber() {
        return makerPhonenumber;
    }

    public void setMakerPhonenumber(String makerPhonenumber) {
        this.makerPhonenumber = makerPhonenumber;
    }

    public String getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(String approvalDate) {
        this.approvalDate = approvalDate;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }
}

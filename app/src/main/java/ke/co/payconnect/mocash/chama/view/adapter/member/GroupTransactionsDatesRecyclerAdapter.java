package ke.co.payconnect.mocash.chama.view.adapter.member;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.TransactionEntity;
import ke.co.payconnect.mocash.chama.util.Utility;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.TransactionViewModel;

public class GroupTransactionsDatesRecyclerAdapter extends RecyclerView.Adapter<GroupTransactionsDatesRecyclerAdapter.GroupTransactionsDateViewHolder> {

    private final Context context;
    private List<String> transactionDates;

    private GroupTransactionsRecyclerViewAdapter adapter;

    private TransactionViewModel transactionViewModel;
    private String txnCategory;

    public GroupTransactionsDatesRecyclerAdapter(Context context, List<String> transactionDates, String txnCategory) {
        this.context = context;
        this.transactionDates = transactionDates;
        this.txnCategory = txnCategory;
    }

    @NonNull
    @Override
    public GroupTransactionsDateViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_group_transactions_dates, parent, false);
        return new GroupTransactionsDateViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull GroupTransactionsDateViewHolder holder, int position) {
        try {
            final String transactionDate = transactionDates.get(position);
            String phoneNumber = AppController.getInstance().getChamaPreferenceManager().getPhoneNumber();

            Calendar calendar;
            String date = transactionDate;
            try {
                calendar = Utility.getDateTime(transactionDate, "yyyyMMddHHmmss");
                @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                date = dateFormat.format(calendar.getTime());
                holder.tvDateStructGroupTxnDates.setText(date);
//                Parse
            } catch (ParseException e) {
                holder.tvDateStructGroupTxnDates.setText(date);
            }

            setupRecyclerView(holder, transactionDate);
        } catch (Exception ignored) {
        }
    }

    @Override
    public int getItemCount() {
        return this.transactionDates.size();
    }

    class GroupTransactionsDateViewHolder extends RecyclerView.ViewHolder {
        private RecyclerView rvStructGroupTxnDates;
        private TextView tvDateStructGroupTxnDates;
        private TextView tvNoDataStructGroupTxnDates;

        GroupTransactionsDateViewHolder(@NonNull View view) {
            super(view);
            rvStructGroupTxnDates = view.findViewById(R.id.rvStructGroupTxnDates);
            tvDateStructGroupTxnDates = view.findViewById(R.id.tvDateStructGroupTxnDates);
            tvNoDataStructGroupTxnDates = view.findViewById(R.id.tvNoDataStructGroupTxnDates);
        }
    }

    private void setupRecyclerView(final GroupTransactionsDateViewHolder holder, String transactionDate) {
        transactionViewModel = ViewModelProviders.of((FragmentActivity) context).get(TransactionViewModel.class);
        transactionViewModel.getSummaryTransactionsLiveByTimestamp(transactionDate).observe((LifecycleOwner) context, new Observer<List<TransactionEntity>>() {
            @Override
            public void onChanged(List<TransactionEntity> transactionEntities) {
                if (transactionEntities.size() > 0) {

                    List<TransactionEntity> filteredTransactionEntities = new LinkedList<>();

                    for (TransactionEntity transactionEntity : transactionEntities) {
                        if (transactionEntity.getTxnCategory().equals(txnCategory)) {
                            // Group posting
                            // First check if the transactions match the specified category
                            // Then filter the result depending on the txn category
                            if (txnCategory.equals("TXN-GRP-POSTING")) {
                                List<TransactionEntity> batchTransactions = transactionViewModel.getBatchTransactions(transactionEntity.getBatchNumber());
                                double amount = 0;

                                for (TransactionEntity transaction : batchTransactions)
                                    amount = amount + Double.parseDouble(transaction.getAmount());

                                if (transactionEntity.getMemberPhoneNumber().length() > 2) {
                                    // Structured
                                    if (transactionEntity.getMemberPhoneNumber().substring(0, 4).equals("2547")) {
                                        MemberViewModel memberViewModel = ViewModelProviders.of((FragmentActivity) context).get(MemberViewModel.class);
                                        if (!memberViewModel.doesMemberExistWithPhoneNumber(transactionEntity.getMemberPhoneNumber()))
                                            continue;
                                    }
                                }

                                if (transactionEntity.getTxnProcessingStatus().toLowerCase().trim().equals("processed")) {
                                    TransactionEntity txn = new TransactionEntity();
                                    txn.setTxnID(transactionEntity.getTxnID());
                                    txn.setTxnDate(transactionEntity.getTxnDate());
                                    txn.setBatchNumber(transactionEntity.getBatchNumber());
                                    txn.setMemberPhoneNumber(transactionEntity.getMemberPhoneNumber());
                                    txn.setAmount(String.valueOf(amount));

                                    filteredTransactionEntities.add(txn);
                                }
                            } else if (txnCategory.equals("TXN-TBL-BANKING")) {
                                // Table banking
                                List<TransactionEntity> batchTransactions = transactionViewModel.getBatchTransactions(transactionEntity.getBatchNumber());
                                double amount = 0;

                                for (TransactionEntity transaction : batchTransactions)
                                    amount = amount + Double.parseDouble(transaction.getAmount());

                                if (transactionEntity.getMemberPhoneNumber().length() > 2) {
                                    // Structured
                                    if (transactionEntity.getMemberPhoneNumber().substring(0, 4).equals("2547")) {
                                        MemberViewModel memberViewModel = ViewModelProviders.of((FragmentActivity) context).get(MemberViewModel.class);
                                        if (!memberViewModel.doesMemberExistWithPhoneNumber(transactionEntity.getMemberPhoneNumber()))
                                            continue;
                                    }
                                }

                                if (transactionEntity.getTxnProcessingStatus().toLowerCase().trim().equals("authorised")) {
                                    TransactionEntity txn = new TransactionEntity();
                                    txn.setTxnID(transactionEntity.getTxnID());
                                    txn.setTxnDate(transactionEntity.getTxnDate());
                                    txn.setBatchNumber(transactionEntity.getBatchNumber());
                                    txn.setMemberPhoneNumber(transactionEntity.getMemberPhoneNumber());
                                    txn.setAmount(String.valueOf(amount));

                                    filteredTransactionEntities.add(txn);
                                }
                            } else {
                                // None | Won't execute
                                List<TransactionEntity> batchTransactions = transactionViewModel.getBatchTransactions(transactionEntity.getBatchNumber());
                                double amount = 0;

                                for (TransactionEntity transaction : batchTransactions)
                                    amount = amount + Double.parseDouble(transaction.getAmount());

                                if (transactionEntity.getMemberPhoneNumber().length() > 2) {
                                    // Structured
                                    if (transactionEntity.getMemberPhoneNumber().substring(0, 4).equals("2547")) {
                                        MemberViewModel memberViewModel = ViewModelProviders.of((FragmentActivity) context).get(MemberViewModel.class);
                                        if (!memberViewModel.doesMemberExistWithPhoneNumber(transactionEntity.getMemberPhoneNumber()))
                                            continue;
                                    }
                                }

                                TransactionEntity txn = new TransactionEntity();
                                txn.setTxnID(transactionEntity.getTxnID());
                                txn.setTxnDate(transactionEntity.getTxnDate());
                                txn.setBatchNumber(transactionEntity.getBatchNumber());
                                txn.setMemberPhoneNumber(transactionEntity.getMemberPhoneNumber());
                                txn.setAmount(String.valueOf(amount));

                                filteredTransactionEntities.add(txn);
                            }
                        }

                    }


                    if (filteredTransactionEntities.size() > 0) {
                        toggleDataViews(holder, true);

                        adapter = new GroupTransactionsRecyclerViewAdapter(context, filteredTransactionEntities);
                        adapter.notifyDataSetChanged();
                        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
                        holder.rvStructGroupTxnDates.setAdapter(adapter);
                        holder.rvStructGroupTxnDates.setLayoutManager(layoutManager);
                    } else {
                        toggleDataViews(holder, false);
                    }
                }
            }
        });
    }

    private void toggleDataViews(GroupTransactionsDateViewHolder holder, boolean hasTransactions) {
        if (hasTransactions) {
            holder.tvNoDataStructGroupTxnDates.setVisibility(View.GONE);
            holder.rvStructGroupTxnDates.setVisibility(View.VISIBLE);
        } else {
            holder.tvNoDataStructGroupTxnDates.setVisibility(View.VISIBLE);
            holder.rvStructGroupTxnDates.setVisibility(View.GONE);
        }
    }

}

package ke.co.payconnect.mocash.mocash.data.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import ke.co.payconnect.mocash.mocash.data.entity.LocationEntity;

@Dao
public interface LocationDao {
    @Query("SELECT * FROM LocationEntity")
    List<LocationEntity> getAllLocations();

    @Query("SELECT COUNT(*) FROM LocationEntity")
    int locationCount();

    @Insert
    void insertLocation(LocationEntity... locationEntities);

    @Query("DELETE FROM LocationEntity")
    void deleteAllLocations();
}

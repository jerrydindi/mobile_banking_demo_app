package ke.co.payconnect.mocash.chama.data.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class ApprovalFullStatementEntity {
    @PrimaryKey(autoGenerate = true)
    private int uid;
    @ColumnInfo(name = "member_phone_number")
    private String memberPhoneNumber;
    @ColumnInfo(name = "narration")
    private String narration;
    @ColumnInfo(name = "request_date")
    private String requestDate;
    @ColumnInfo(name = "approval_id")
    private String approvalID;
    @ColumnInfo(name = "approval_status")
    private String approvalStatus;
    @ColumnInfo(name = "approval_type")
    private String approvalType;
    @ColumnInfo(name = "approval_narration")
    private String approvalNarration;
    @ColumnInfo(name = "approval_date")
    private String approvalDate;


    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getMemberPhoneNumber() {
        return memberPhoneNumber;
    }

    public void setMemberPhoneNumber(String memberPhoneNumber) {
        this.memberPhoneNumber = memberPhoneNumber;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public String getApprovalID() {
        return approvalID;
    }

    public void setApprovalID(String approvalID) {
        this.approvalID = approvalID;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getApprovalType() {
        return approvalType;
    }

    public void setApprovalType(String approvalType) {
        this.approvalType = approvalType;
    }

    public String getApprovalNarration() {
        return approvalNarration;
    }

    public void setApprovalNarration(String approvalNarration) {
        this.approvalNarration = approvalNarration;
    }

    public String getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(String approvalDate) {
        this.approvalDate = approvalDate;
    }
}

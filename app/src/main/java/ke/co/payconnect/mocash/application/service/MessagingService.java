package ke.co.payconnect.mocash.application.service;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import es.dmoral.toasty.Toasty;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.util.Utility;
import ke.co.payconnect.mocash.chama.view.activity.member.ChatActivity;

public class MessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(final RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if (remoteMessage.getData().size() <= 0) return;

        String title = remoteMessage.getData().get("title");
        String body = remoteMessage.getData().get("body");
        String notificationType = remoteMessage.getData().get("notificationType");
        String senderPhoneNumber = remoteMessage.getData().get("senderPhoneNumber");
        String groupNumber = remoteMessage.getData().get("groupNumber");

        if (groupNumber != null && isNotificationValid(groupNumber)) {

            PendingIntent pendingIntent = null;

            Intent intent;
            if (notificationType != null && notificationType.equals("message")) {
                intent = new Intent(getApplicationContext(), ChatActivity.class);

                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

                // Manage wake lock
                PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
                // Check if screen is on
                boolean isScreenOn = Build.VERSION.SDK_INT >= 20 ? pm != null && pm.isInteractive() : pm != null && pm.isScreenOn();
                if (!isScreenOn) {
                    PowerManager.WakeLock wakeLock = pm != null ? pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "myApp:notificationLock") : null;
                    if (wakeLock != null)
                        wakeLock.acquire(3000); // Milliseconds

                }

                // Build notification
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "channel_id");
                notificationBuilder.setContentTitle(title);
                notificationBuilder.setContentText(body);
                notificationBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);
                notificationBuilder.setStyle(new NotificationCompat.BigTextStyle());
                notificationBuilder.setSmallIcon(R.drawable.ic_unaitas);
                notificationBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
                notificationBuilder.setVibrate(new long[]{500, 500});
                notificationBuilder.setAutoCancel(true);
                if (pendingIntent != null)
                    notificationBuilder.setContentIntent(pendingIntent);
                notificationBuilder.setAutoCancel(true);

                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                Utility.registerNotificationChannel(notificationManager);

                if (notificationManager != null)
                    notificationManager.notify(1, notificationBuilder.build());

            }
        }

    }

    @Override
    public void onNewToken(String token) {
        try {
            Toast.makeText(getBaseContext(), token, Toast.LENGTH_SHORT).show();
            Log.d("tokens", token);
            AppController.getInstance().getChamaPreferenceManager().setDeviceFCMToken(token);

            FirebaseFirestore firebaseFirestore;
            firebaseFirestore = FirebaseFirestore.getInstance();
            Utility.updateDeviceFCMToken(token, firebaseFirestore);
        } catch (Exception e){

        }

    }

    private boolean isNotificationValid(String groupNumber) {
        String savedGroupNumber = AppController.getInstance().getChamaPreferenceManager().getGroupNumber();

        // TODO: Commented because user can have multiple groups. Think about this.
//        if (!groupNumber.equals(savedGroupNumber)) {
//            Toasty.info(getApplicationContext(), "Login to access this message", Toast.LENGTH_SHORT, true).show();
//
//            startActivity(new Intent(getApplicationContext(), ChatActivity.class));
//
//            ((Activity) getApplicationContext()).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
//
//            return false;
//        }

        return true;
    }

}

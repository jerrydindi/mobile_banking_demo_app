package ke.co.payconnect.mocash.mocash.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import android.os.AsyncTask;
import androidx.annotation.NonNull;

import java.util.List;

import ke.co.payconnect.mocash.mocash.data.MocashDatabase;
import ke.co.payconnect.mocash.mocash.data.entity.EmployerEntity;


public class EmployerViewModel extends AndroidViewModel {

    private final LiveData<List<EmployerEntity>> employerList;
    private final List<EmployerEntity> employerListList;
    private MocashDatabase db;

    public EmployerViewModel(@NonNull Application application) {
        super(application);

        db = MocashDatabase.getMocashDatabaseInstance(this.getApplication());
        employerList = db.employerDao().getAllEmployers();
        employerListList = db.employerDao().getAllEmployersList();
    }

    public LiveData<List<EmployerEntity>> getEmployerLiveList() {
        return employerList;
    }

    public List<EmployerEntity> getEmployerList(){
        return employerListList;
    }

    public boolean employerExists(String employerID) {
        return db.employerDao().employerExists(employerID);
    }

    public void insertEmployer(EmployerEntity employer) {
        new InsertAsyncTask(db).execute(employer);
    }

    public void updateEmployer(EmployerEntity employer) {
        new UpdateAsyncTask(db).execute(employer);
    }

    public void deleteEmployer(EmployerEntity employer) {
        new DeleteAsyncTask(db).execute(employer);
    }

    public void deleteAllEmployers() {
        new DeleteAllAsyncTask(db).execute();
    }


    private static class InsertAsyncTask extends AsyncTask<EmployerEntity, Void, Void> {

        private MocashDatabase db;

        InsertAsyncTask(MocashDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(final EmployerEntity... employerModels) {
            db.employerDao().insertEmployer(employerModels[0]);
            return null;
        }
    }

    private static class UpdateAsyncTask extends AsyncTask<EmployerEntity, Void, Void> {

        private MocashDatabase db;

        UpdateAsyncTask(MocashDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(final EmployerEntity... employerModels) {
            db.employerDao().updateEmployer(employerModels[0]);
            return null;
        }
    }

    private static class DeleteAsyncTask extends AsyncTask<EmployerEntity, Void, Void> {

        private MocashDatabase db;

        DeleteAsyncTask(MocashDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(final EmployerEntity... employerModels) {
            db.employerDao().deleteEmployer(employerModels[0]);
            return null;
        }
    }

    private static class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {

        private MocashDatabase db;

        DeleteAllAsyncTask(MocashDatabase appDatabase) {
            db = appDatabase;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            db.employerDao().deleteAllEmployers();
            return null;
        }
    }
}

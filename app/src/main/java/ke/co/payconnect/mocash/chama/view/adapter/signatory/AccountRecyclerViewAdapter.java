package ke.co.payconnect.mocash.chama.view.adapter.signatory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.chama.data.entity.AccountEntity;

public class AccountRecyclerViewAdapter extends RecyclerView.Adapter<AccountRecyclerViewAdapter.AccountViewHolder> {
    private final Context context;
    private List<AccountEntity> accountEntities;

    public AccountRecyclerViewAdapter(Context context, List<AccountEntity> accountEntities) {
        this.context = context;
        this.accountEntities = accountEntities;
    }

    @NonNull
    @Override
    public AccountViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_list_accounts, parent, false);
        return new AccountViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AccountViewHolder holder, int position) {
        try {
            final AccountEntity accountEntity = accountEntities.get(position);

            holder.tvNameStructAccount.setText(accountEntity.getAccountName());
            holder.tvNumberStructAccount.setText(accountEntity.getAccountNumber());

            holder.clStructAccount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        } catch (Exception ignored) {
        }
    }

    @Override
    public int getItemCount() {
        return this.accountEntities.size();
    }

    class AccountViewHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout clStructAccount;
        private TextView tvNameStructAccount;
        private TextView tvNumberStructAccount;

        AccountViewHolder(View view) {
            super(view);

            clStructAccount = itemView.findViewById(R.id.clStructAccount);
            tvNameStructAccount = itemView.findViewById(R.id.tvNameStructAccount);
            tvNumberStructAccount = itemView.findViewById(R.id.tvNumberStructAccount);
        }
    }
}
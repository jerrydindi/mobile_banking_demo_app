package ke.co.payconnect.mocash.chama.view.adapter.signatory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalExitEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalFullStatementEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalGroupBalanceEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalGroupLoanBalanceEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalMemberAddEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalMinistatementEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalSecretaryEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalTransactionEntity;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalWithdrawEntity;
import ke.co.payconnect.mocash.chama.view.activity.signatory.ApprovalAddMemberActivity;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalExitViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalFullStatementViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalGroupBalanceViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalGroupLoanBalanceViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalMemberAddViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalMinistatementViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalSecretaryViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalTransactionViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalWithdrawViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.TransactionViewModel;

public class ApprovalsRecyclerViewAdapter extends RecyclerView.Adapter<ApprovalsRecyclerViewAdapter.ApprovalViewHolder> {
    private final Context context;
    private List<ApprovalEntity> approvalEntities;

    private ApprovalTransactionsRecyclerViewAdapter approvalTransactionsRecyclerViewAdapter;
    private ApprovalExitsRecyclerViewAdapter approvalExitsRecyclerViewAdapter;
    private ApprovalGroupBalancesRecyclerViewAdapter approvalGroupBalancesRecyclerViewAdapter;
    private ApprovalMinistatementsRecyclerViewAdapter approvalMinistatementsRecyclerViewAdapter;
    private ApprovalFullStatementsRecyclerViewAdapter approvalFullStatementsRecyclerViewAdapter;
    private ApprovalSecretariesRecyclerViewAdapter approvalSecretariesRecyclerViewAdapter;
    private ApprovalWithdrawsRecyclerViewAdapter approvalWithdrawsRecyclerViewAdapter;
    private ApprovalMemberAddRecyclerViewAdapter approvalMemberAddRecyclerViewAdapter;
    private ApprovalGroupLoanBalanceRecyclerViewAdapter approvalGroupLoanBalanceRecyclerViewAdapter;

    private LinearLayoutManager layoutManager;

    private ApprovalTransactionViewModel approvalTransactionViewModel;
    private ApprovalExitViewModel approvalExitViewModel;
    private ApprovalSecretaryViewModel approvalSecretaryViewModel;
    private ApprovalGroupBalanceViewModel approvalGroupBalanceViewModel;
    private ApprovalMinistatementViewModel approvalMinistatementViewModel;
    private ApprovalFullStatementViewModel approvalFullStatementViewModel;
    private ApprovalWithdrawViewModel approvalWithdrawViewModel;
    private ApprovalMemberAddViewModel approvalMemberAddViewModel;
    private ApprovalViewModel approvalViewModel;
    private MemberViewModel memberViewModel;
    private TransactionViewModel transactionViewModel;
    private ApprovalGroupLoanBalanceViewModel approvalGroupLoanBalanceViewModel;

    private static final int APPROVAL_TRANSACTION = 0;
    private static final int APPROVAL_EXIT = 1;
    private static final int APPROVAL_SECRETARY = 2;
    private static final int APPROVAL_GROUP_BALANCE = 3;
    private static final int APPROVAL_MINISTATEMENT = 4;
    private static final int APPROVAL_FULL_STATEMENT = 5;
    private static final int APPROVAL_WITHDRAW = 6;
    private static final int APPROVAL_UNDEFINED = 7;
    private static final int APPROVAL_MEMBER_ADD = 8;
    private static final int APPROVAL_GROUP_LOAN_BALANCE = 9;

    public ApprovalsRecyclerViewAdapter(Context context, List<ApprovalEntity> approvalEntities) {
        this.context = context;
        this.approvalEntities = approvalEntities;

        this.approvalTransactionViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalTransactionViewModel.class);
        this.approvalExitViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalExitViewModel.class);
        this.approvalSecretaryViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalSecretaryViewModel.class);
        this.approvalGroupBalanceViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalGroupBalanceViewModel.class);
        this.approvalMinistatementViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalMinistatementViewModel.class);
        this.approvalFullStatementViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalFullStatementViewModel.class);
        this.approvalWithdrawViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalWithdrawViewModel.class);
        this.approvalMemberAddViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalMemberAddViewModel.class);
        this.approvalViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalViewModel.class);
        this.memberViewModel = ViewModelProviders.of((FragmentActivity) context).get(MemberViewModel.class);
        this.transactionViewModel = ViewModelProviders.of((FragmentActivity) context).get(TransactionViewModel.class);
        this.approvalGroupLoanBalanceViewModel = ViewModelProviders.of((FragmentActivity) context).get(ApprovalGroupLoanBalanceViewModel.class);
    }

    @NonNull
    @Override
    public ApprovalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        switch (viewType) {
            case APPROVAL_TRANSACTION:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_container_approval_txn, parent, false);
                break;
            case APPROVAL_EXIT:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_container_approval_exit, parent, false);
                break;
            case APPROVAL_SECRETARY:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_container_approval_secretary, parent, false);
                break;
            case APPROVAL_GROUP_BALANCE:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_container_approval_group_balance, parent, false);
                break;
            case APPROVAL_MINISTATEMENT:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_container_approval_ministatement, parent, false);
                break;
            case APPROVAL_FULL_STATEMENT:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_container_approval_full_statement, parent, false);
                break;
            case APPROVAL_WITHDRAW:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_container_approval_withdraw, parent, false);
                break;
            case APPROVAL_MEMBER_ADD:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_container_approval_group_member_add, parent, false);
                break;
            case APPROVAL_GROUP_LOAN_BALANCE:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_container_approval_group_loan_balance, parent, false);
                break;
            case APPROVAL_UNDEFINED:
            default:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_container_approval_invalid, parent, false);
                break;
        }

        return new ApprovalViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ApprovalViewHolder holder, int position) {
        try {
            ApprovalEntity approvalEntity = approvalEntities.get(position);

            setupRecyclerView(holder, approvalEntity);
        } catch (Exception ignored) {
        }
    }

    @Override
    public int getItemCount() {
        return this.approvalEntities.size();
    }

    @Override
    public int getItemViewType(int position) {
        final ApprovalEntity approvalEntity = approvalEntities.get(position);

        switch (approvalEntity.getApprovalType()) {
            case "transaction":
                return APPROVAL_TRANSACTION;
            case "exit":
                return APPROVAL_EXIT;
            case "secretary":
                return APPROVAL_SECRETARY;
            case "group-balance":
                return APPROVAL_GROUP_BALANCE;
            case "ministatement":
                return APPROVAL_MINISTATEMENT;
            case "full-statement":
                return APPROVAL_FULL_STATEMENT;
            case "withdraw":
                return APPROVAL_WITHDRAW;
            case "member-add":
                return APPROVAL_MEMBER_ADD;
            case "group-loan-balance":
                return APPROVAL_GROUP_LOAN_BALANCE;
            default:
                return APPROVAL_UNDEFINED;
        }

    }

    class ApprovalViewHolder extends RecyclerView.ViewHolder {
        private RecyclerView rvStructContApproval;
        private TextView tvNoDataStructContApproval;

        ApprovalViewHolder(View view) {
            super(view);

            rvStructContApproval = view.findViewById(R.id.rvStructContApproval);
            tvNoDataStructContApproval = view.findViewById(R.id.tvNoDataStructContApproval);
        }

    }

    private void setupRecyclerView(final ApprovalViewHolder holder, final ApprovalEntity approvalEntity) {
        switch (approvalEntity.getApprovalType()) {
            case "transaction":
                approvalTransactionViewModel.getApprovalTransactionsByTypeLive(approvalEntity.getApprovalType()).observe((LifecycleOwner) context, new Observer<List<ApprovalTransactionEntity>>() {
                    @Override
                    public void onChanged(List<ApprovalTransactionEntity> approvalTransactionEntities) {

                        if (approvalTransactionEntities.size() > 0) {

                            List<ApprovalTransactionEntity> filteredApprovalTransactionEntities = new ArrayList<>();

                            for (ApprovalTransactionEntity approvalTransactionEntity : approvalTransactionEntities) {
                                if (approvalTransactionEntity.getMemberPhoneNumber().length() > 2) {
                                    // Structured
                                    if (approvalTransactionEntity.getMemberPhoneNumber().substring(0, 4).equals("2547")) {
                                        if (!memberViewModel.doesMemberExistWithPhoneNumber(approvalTransactionEntity.getMemberPhoneNumber())) {
                                            continue;
                                        }
                                    }
                                }

                                if (!memberViewModel.doesMemberExistWithPhoneNumber(approvalTransactionEntity.getMakerPhoneNumber()))
                                    continue;

                                if (!approvalTransactionViewModel.doesTransactionApprovalExistWithApprovalId(approvalTransactionEntity.getApprovalID()))
                                    continue;

                                if (!approvalViewModel.doesApprovalExistWithApprovalId(approvalTransactionEntity.getApprovalID()))
                                    continue;

                                if (!transactionViewModel.doesTransactionExistWithTxnID(approvalTransactionEntity.getTxnID()))
                                    continue;

                                filteredApprovalTransactionEntities.add(approvalTransactionEntity);
                            }

                            if (filteredApprovalTransactionEntities.size() > 0) {
                                toggleDataViews(holder, true);

                                approvalTransactionsRecyclerViewAdapter = new ApprovalTransactionsRecyclerViewAdapter(context, filteredApprovalTransactionEntities);
                                approvalTransactionsRecyclerViewAdapter.notifyDataSetChanged();

                                holder.rvStructContApproval.setAdapter(approvalTransactionsRecyclerViewAdapter);
                                layoutManager = new LinearLayoutManager(context);
                                holder.rvStructContApproval.setLayoutManager(layoutManager);
                            } else {
                                toggleDataViews(holder, false);
                            }
                        } else {
                            toggleDataViews(holder, false);
                        }

                    }
                });
                break;
            case "exit":
                approvalExitViewModel.getApprovalExitsByTypeLive(approvalEntity.getApprovalType()).observe((LifecycleOwner) context, new Observer<List<ApprovalExitEntity>>() {
                    @Override
                    public void onChanged(List<ApprovalExitEntity> approvalExitEntities) {

                        if (approvalExitEntities.size() > 0) {

                            List<ApprovalExitEntity> filteredApprovalExitEntities = new ArrayList<>();

                            for (ApprovalExitEntity approvalExitEntity : approvalExitEntities) {
                                if (!memberViewModel.doesMemberExistWithPhoneNumber(approvalExitEntity.getMemberPhoneNumber()))
                                    continue;

                                if (!approvalExitViewModel.doesExitApprovalExistWithApprovalId(approvalExitEntity.getApprovalID()))
                                    continue;

                                if (!approvalViewModel.doesApprovalExistWithApprovalId(approvalExitEntity.getApprovalID()))
                                    continue;

                                filteredApprovalExitEntities.add(approvalExitEntity);
                            }

                            if (filteredApprovalExitEntities.size() > 0) {
                                toggleDataViews(holder, true);

                                approvalExitsRecyclerViewAdapter = new ApprovalExitsRecyclerViewAdapter(context, filteredApprovalExitEntities);
                                approvalExitsRecyclerViewAdapter.notifyDataSetChanged();

                                holder.rvStructContApproval.setAdapter(approvalExitsRecyclerViewAdapter);
                                layoutManager = new LinearLayoutManager(context);
                                holder.rvStructContApproval.setLayoutManager(layoutManager);
                            } else {
                                toggleDataViews(holder, false);
                            }
                        } else {
                            toggleDataViews(holder, false);
                        }

                    }
                });
                break;

            case "secretary":
                approvalSecretaryViewModel.getApprovalSecretariesByTypeLive(approvalEntity.getApprovalType()).observe((LifecycleOwner) context, new Observer<List<ApprovalSecretaryEntity>>() {
                    @Override
                    public void onChanged(List<ApprovalSecretaryEntity> approvalSecretaryEntities) {

                        if (approvalSecretaryEntities.size() > 0) {

                            List<ApprovalSecretaryEntity> filteredApprovalSecretaryEntities = new ArrayList<>();

                            for (ApprovalSecretaryEntity approvalSecretaryEntity : approvalSecretaryEntities) {
                                if (!memberViewModel.doesMemberExistWithPhoneNumber(approvalSecretaryEntity.getCurrentSecretaryPhoneNumber()))
                                    continue;

                                if (!memberViewModel.doesMemberExistWithPhoneNumber(approvalSecretaryEntity.getProposedSecretaryPhoneNumber()))
                                    continue;

                                if (!approvalSecretaryViewModel.doesSecretaryApprovalExistWithApprovalId(approvalSecretaryEntity.getApprovalID()))
                                    continue;

                                if (!approvalViewModel.doesApprovalExistWithApprovalId(approvalSecretaryEntity.getApprovalID()))
                                    continue;

                                filteredApprovalSecretaryEntities.add(approvalSecretaryEntity);
                            }

                            if (filteredApprovalSecretaryEntities.size() > 0) {
                                toggleDataViews(holder, true);

                                approvalSecretariesRecyclerViewAdapter = new ApprovalSecretariesRecyclerViewAdapter(context, filteredApprovalSecretaryEntities);
                                approvalSecretariesRecyclerViewAdapter.notifyDataSetChanged();

                                holder.rvStructContApproval.setAdapter(approvalSecretariesRecyclerViewAdapter);
                                layoutManager = new LinearLayoutManager(context);
                                holder.rvStructContApproval.setLayoutManager(layoutManager);
                            } else {
                                toggleDataViews(holder, false);
                            }
                        } else {
                            toggleDataViews(holder, false);
                        }

                    }
                });
                break;

            case "group-balance":
                approvalGroupBalanceViewModel.getApprovalGroupBalancesByTypeLive(approvalEntity.getApprovalType()).observe((LifecycleOwner) context, new Observer<List<ApprovalGroupBalanceEntity>>() {
                    @Override
                    public void onChanged(List<ApprovalGroupBalanceEntity> approvalGroupBalanceEntities) {

                        if (approvalGroupBalanceEntities.size() > 0) {

                            List<ApprovalGroupBalanceEntity> filteredApprovalGroupBalanceEntities = new ArrayList<>();

                            for (ApprovalGroupBalanceEntity approvalGroupBalanceEntity : approvalGroupBalanceEntities) {
                                if (!memberViewModel.doesMemberExistWithPhoneNumber(approvalGroupBalanceEntity.getMemberPhoneNumber()))
                                    continue;

                                if (!approvalGroupBalanceViewModel.doesGroupBalanceApprovalExistWithApprovalId(approvalGroupBalanceEntity.getApprovalID()))
                                    continue;

                                if (!approvalViewModel.doesApprovalExistWithApprovalId(approvalGroupBalanceEntity.getApprovalID()))
                                    continue;

                                filteredApprovalGroupBalanceEntities.add(approvalGroupBalanceEntity);
                            }

                            if (filteredApprovalGroupBalanceEntities.size() > 0) {
                                toggleDataViews(holder, true);

                                approvalGroupBalancesRecyclerViewAdapter = new ApprovalGroupBalancesRecyclerViewAdapter(context, filteredApprovalGroupBalanceEntities);
                                approvalGroupBalancesRecyclerViewAdapter.notifyDataSetChanged();

                                holder.rvStructContApproval.setAdapter(approvalGroupBalancesRecyclerViewAdapter);
                                layoutManager = new LinearLayoutManager(context);
                                holder.rvStructContApproval.setLayoutManager(layoutManager);
                            } else {
                                toggleDataViews(holder, false);
                            }
                        } else {
                            toggleDataViews(holder, false);
                        }

                    }
                });
                break;

            case "ministatement":
                approvalMinistatementViewModel.getApprovalMinistatementsByTypeLive(approvalEntity.getApprovalType()).observe((LifecycleOwner) context, new Observer<List<ApprovalMinistatementEntity>>() {
                    @Override
                    public void onChanged(List<ApprovalMinistatementEntity> approvalMinistatementEntities) {

                        if (approvalMinistatementEntities.size() > 0) {

                            List<ApprovalMinistatementEntity> filteredApprovalMinistatementEntities = new ArrayList<>();

                            for (ApprovalMinistatementEntity approvalMinistatementEntity : approvalMinistatementEntities) {
                                if (!memberViewModel.doesMemberExistWithPhoneNumber(approvalMinistatementEntity.getMemberPhoneNumber()))
                                    continue;

                                if (!approvalMinistatementViewModel.doesMinistatementApprovalExistWithApprovalId(approvalMinistatementEntity.getApprovalID()))
                                    continue;

                                if (!approvalViewModel.doesApprovalExistWithApprovalId(approvalMinistatementEntity.getApprovalID()))
                                    continue;

                                filteredApprovalMinistatementEntities.add(approvalMinistatementEntity);
                            }

                            if (filteredApprovalMinistatementEntities.size() > 0) {
                                toggleDataViews(holder, true);

                                approvalMinistatementsRecyclerViewAdapter = new ApprovalMinistatementsRecyclerViewAdapter(context, filteredApprovalMinistatementEntities);
                                approvalMinistatementsRecyclerViewAdapter.notifyDataSetChanged();

                                holder.rvStructContApproval.setAdapter(approvalMinistatementsRecyclerViewAdapter);
                                layoutManager = new LinearLayoutManager(context);
                                holder.rvStructContApproval.setLayoutManager(layoutManager);
                            } else {
                                toggleDataViews(holder, false);
                            }
                        } else {
                            toggleDataViews(holder, false);
                        }

                    }
                });
                break;

            case "full-statement":
                approvalFullStatementViewModel.getApprovalFullStatementsByTypeLive(approvalEntity.getApprovalType()).observe((LifecycleOwner) context, new Observer<List<ApprovalFullStatementEntity>>() {
                    @Override
                    public void onChanged(List<ApprovalFullStatementEntity> approvalFullStatementEntities) {

                        if (approvalFullStatementEntities.size() > 0) {

                            List<ApprovalFullStatementEntity> filteredApprovalFullStatementEntities = new ArrayList<>();

                            for (ApprovalFullStatementEntity approvalFullStatementEntity : approvalFullStatementEntities) {
                                if (!memberViewModel.doesMemberExistWithPhoneNumber(approvalFullStatementEntity.getMemberPhoneNumber()))
                                    continue;

                                if (!approvalFullStatementViewModel.doesFullStatementApprovalExistWithApprovalId(approvalFullStatementEntity.getApprovalID()))
                                    continue;

                                if (!approvalViewModel.doesApprovalExistWithApprovalId(approvalFullStatementEntity.getApprovalID()))
                                    continue;

                                filteredApprovalFullStatementEntities.add(approvalFullStatementEntity);
                            }

                            if (filteredApprovalFullStatementEntities.size() > 0) {
                                toggleDataViews(holder, true);

                                approvalFullStatementsRecyclerViewAdapter = new ApprovalFullStatementsRecyclerViewAdapter(context, filteredApprovalFullStatementEntities);
                                approvalFullStatementsRecyclerViewAdapter.notifyDataSetChanged();

                                holder.rvStructContApproval.setAdapter(approvalFullStatementsRecyclerViewAdapter);
                                layoutManager = new LinearLayoutManager(context);
                                holder.rvStructContApproval.setLayoutManager(layoutManager);
                            } else {
                                toggleDataViews(holder, false);
                            }
                        } else {
                            toggleDataViews(holder, false);
                        }

                    }
                });
                break;
            case "withdraw":
                approvalWithdrawViewModel.getApprovalWithdrawsByTypeLive(approvalEntity.getApprovalType()).observe((LifecycleOwner) context, new Observer<List<ApprovalWithdrawEntity>>() {
                    @Override
                    public void onChanged(List<ApprovalWithdrawEntity> approvalWithdrawEntities) {

                        if (approvalWithdrawEntities.size() > 0) {

                            List<ApprovalWithdrawEntity> filteredApprovalWithdrawEntities = new ArrayList<>();

                            for (ApprovalWithdrawEntity approvalWithdrawEntity : approvalWithdrawEntities) {
                                if (!memberViewModel.doesMemberExistWithPhoneNumber(approvalWithdrawEntity.getMakerPhoneNumber()))
                                    continue;

                                if (!approvalWithdrawViewModel.doesWithdrawApprovalExistWithApprovalId(approvalWithdrawEntity.getApprovalID()))
                                    continue;

                                if (!approvalViewModel.doesApprovalExistWithApprovalId(approvalWithdrawEntity.getApprovalID()))
                                    continue;

                                filteredApprovalWithdrawEntities.add(approvalWithdrawEntity);
                            }

                            if (filteredApprovalWithdrawEntities.size() > 0) {
                                toggleDataViews(holder, true);

                                approvalWithdrawsRecyclerViewAdapter = new ApprovalWithdrawsRecyclerViewAdapter(context, filteredApprovalWithdrawEntities);
                                approvalWithdrawsRecyclerViewAdapter.notifyDataSetChanged();

                                holder.rvStructContApproval.setAdapter(approvalWithdrawsRecyclerViewAdapter);
                                layoutManager = new LinearLayoutManager(context);
                                holder.rvStructContApproval.setLayoutManager(layoutManager);
                            } else {
                                toggleDataViews(holder, false);
                            }
                        } else {
                            toggleDataViews(holder, false);
                        }

                    }
                });
            case "member-add":
                approvalMemberAddViewModel.getApprovalAddMembersByTypeLive(approvalEntity.getApprovalType()).observe((LifecycleOwner) context, new Observer<List<ApprovalMemberAddEntity>>() {
                    @Override
                    public void onChanged(List<ApprovalMemberAddEntity> approvalMemberAddEntities) {
                        if (approvalMemberAddEntities.size() > 0) {
                            List<ApprovalMemberAddEntity> filteredApprovalMemberAddEntities = new ArrayList<>();

                            for (ApprovalMemberAddEntity approvalMemberAddEntity : approvalMemberAddEntities) {
                                if (!memberViewModel.doesMemberExistWithPhoneNumber(approvalMemberAddEntity.getMakerPhonenumber()))
                                    continue;
                                if (!approvalMemberAddViewModel.doesMemberAddApprovalExistWithApprovalId(approvalMemberAddEntity.getApprovalID()))
                                    continue;

                                if (!approvalViewModel.doesApprovalExistWithApprovalId(approvalMemberAddEntity.getApprovalID()))
                                    continue;

                                filteredApprovalMemberAddEntities.add(approvalMemberAddEntity);
                            }

                            if (filteredApprovalMemberAddEntities.size() > 0) {
                                toggleDataViews(holder, true);
                                approvalMemberAddRecyclerViewAdapter = new ApprovalMemberAddRecyclerViewAdapter(context, filteredApprovalMemberAddEntities);
                                approvalMemberAddRecyclerViewAdapter.notifyDataSetChanged();

                                holder.rvStructContApproval.setAdapter(approvalMemberAddRecyclerViewAdapter);
                                layoutManager = new LinearLayoutManager(context);
                                holder.rvStructContApproval.setLayoutManager(layoutManager);
                            } else {
                                toggleDataViews(holder, false);
                            }
                        } else {
                            toggleDataViews(holder, false);
                        }
                    }
                });
                break;
            case "group-loan-balance":
                approvalGroupLoanBalanceViewModel.getApprovalGroupLoanBalanceByTypeLive(approvalEntity.getApprovalType()).observe((LifecycleOwner) context, new Observer<List<ApprovalGroupLoanBalanceEntity>>() {
                    @Override
                    public void onChanged(List<ApprovalGroupLoanBalanceEntity> approvalGroupLoanBalanceEntities) {

                        if (approvalGroupLoanBalanceEntities.size() > 0) {

                            List<ApprovalGroupLoanBalanceEntity> filteredApprovalGroupLoanBalanceEntities = new ArrayList<>();

                            for (ApprovalGroupLoanBalanceEntity approvalGroupLoanBalanceEntity : approvalGroupLoanBalanceEntities) {
                                if (!memberViewModel.doesMemberExistWithPhoneNumber(approvalGroupLoanBalanceEntity.getMemberPhoneNumber()))
                                    continue;

                                if (!approvalGroupLoanBalanceViewModel.doesGroupLoanBalanceApprovalExistWithApprovalId(approvalGroupLoanBalanceEntity.getApprovalID()))
                                    continue;

                                if (!approvalViewModel.doesApprovalExistWithApprovalId(approvalGroupLoanBalanceEntity.getApprovalID()))
                                    continue;

                                filteredApprovalGroupLoanBalanceEntities.add(approvalGroupLoanBalanceEntity);
                            }

                            if (filteredApprovalGroupLoanBalanceEntities.size() > 0) {
                                toggleDataViews(holder, true);

                                approvalGroupLoanBalanceRecyclerViewAdapter = new ApprovalGroupLoanBalanceRecyclerViewAdapter(context, filteredApprovalGroupLoanBalanceEntities);
                                approvalGroupLoanBalanceRecyclerViewAdapter.notifyDataSetChanged();

                                holder.rvStructContApproval.setAdapter(approvalGroupLoanBalanceRecyclerViewAdapter);
                                layoutManager = new LinearLayoutManager(context);
                                holder.rvStructContApproval.setLayoutManager(layoutManager);
                            } else {
                                toggleDataViews(holder, false);
                            }
                        } else {
                            toggleDataViews(holder, false);
                        }

                    }
                });
                break;
        }
    }

    private void toggleDataViews(ApprovalViewHolder holder, boolean hasTransactions) {
        if (hasTransactions) {
            holder.tvNoDataStructContApproval.setVisibility(View.GONE);
            holder.rvStructContApproval.setVisibility(View.VISIBLE);
        } else {
            holder.tvNoDataStructContApproval.setVisibility(View.VISIBLE);
            holder.rvStructContApproval.setVisibility(View.GONE);
        }
    }
}

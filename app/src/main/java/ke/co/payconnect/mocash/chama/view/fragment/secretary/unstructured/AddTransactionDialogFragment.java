package ke.co.payconnect.mocash.chama.view.fragment.secretary.unstructured;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.text.ParseException;

import es.dmoral.toasty.Toasty;
import fr.ganfra.materialspinner.MaterialSpinner;
import io.captano.utility.Time;
import io.captano.utility.ui.UI;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.TransactionEntity;
import ke.co.payconnect.mocash.chama.util.Utility;

public class AddTransactionDialogFragment extends DialogFragment {

    private MaterialSpinner msSelectTxnTypeDialogAddTxn;
    private EditText etAmountDialogAddTxn;
    private Button btCancelDialogAddTxn;
    private Button btSaveDialogAddTxn;


    private String customerType;
    private String memberPhoneNumber;
    private String txnCategory;
    private String txnType;

    public static AddTransactionListener listener;

    public AddTransactionDialogFragment() {

    }

    public static AddTransactionDialogFragment newInstance(String memberPhoneNumber, String txnCategory, String customerType) {
        AddTransactionDialogFragment fragment = new AddTransactionDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("member_phone_number", memberPhoneNumber);
        bundle.putString("txn_category", txnCategory);
        bundle.putString("customer_type", customerType);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (AddTransactionListener) context;

        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement AddTransactionListener");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_transaction_dialog, container);

        msSelectTxnTypeDialogAddTxn = view.findViewById(R.id.msSelectTxnTypeDialogAddTxn);
        etAmountDialogAddTxn = view.findViewById(R.id.etAmountDialogAddTxn);
        btCancelDialogAddTxn = view.findViewById(R.id.btCancelDialogAddTxn);
        btSaveDialogAddTxn = view.findViewById(R.id.btSaveDialogAddTxn);

        ArrayAdapter adapter = ArrayAdapter.createFromResource(getContext(), R.array.txn_types, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        msSelectTxnTypeDialogAddTxn.setAdapter(adapter);


        if (getArguments() != null) {
            memberPhoneNumber = getArguments().getString("member_phone_number");
            txnCategory = getArguments().getString("txn_category");
            customerType = getArguments().getString("customer_type");

            msSelectTxnTypeDialogAddTxn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {

                    try {
                        txnType = parentView.getItemAtPosition(position).toString();
                    } catch (Exception e) {
                        Toasty.info(getContext(), "Invalid selection", Toast.LENGTH_SHORT, true).show();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {

                }

            });

            btCancelDialogAddTxn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDialog().cancel();
                }
            });

            btSaveDialogAddTxn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Validation
                    if (etAmountDialogAddTxn.getText().toString().isEmpty()) {
                        new UI(getActivity()).hideSoftInput();
                        Toasty.info(getContext(), "Amount is required", Toast.LENGTH_SHORT, true).show();
                        return;
                    }

                    if (Double.parseDouble(etAmountDialogAddTxn.getText().toString()) == 0 || Double.parseDouble(etAmountDialogAddTxn.getText().toString()) > 1000000) {
                        new UI(getActivity()).hideSoftInput();
                        Toasty.info(getContext(), "Invalid amount", Toast.LENGTH_SHORT, true).show();
                        return;
                    }

                    if (txnType == null) {
                        new UI(getActivity()).hideSoftInput();
                        Toasty.info(getContext(), "Validation failed", Toast.LENGTH_SHORT, true).show();
                        return;
                    }

                    if (txnType.toLowerCase().contains("select")) {
                        new UI(getActivity()).hideSoftInput();
                        Toasty.info(getContext(), "Invalid selection for transaction type", Toast.LENGTH_SHORT, true).show();
                        return;
                    }

                    String txnDate = Time.getDateTimeNow("yyyy-MM-dd HH:mm:ss");
                    String truncDate = txnDate;

                    try {
                        truncDate = Utility.getDate(txnDate, "yyyy-MM-dd");
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    String destination;

                    switch (customerType) {
                        case "group":
                            destination = AppController.getInstance().getChamaPreferenceManager().getGroupNumber();
                            break;
                        case "member":
                            destination = memberPhoneNumber;
                            break;
                        default:
                            Toasty.info(getContext(), getContext().getString(R.string.action_not_permitted), Toast.LENGTH_SHORT, true).show();
                            return;
                    }

                    TransactionEntity transactionEntity = new TransactionEntity();
                    transactionEntity.setMemberPhoneNumber(destination);
                    transactionEntity.setAmount(etAmountDialogAddTxn.getText().toString().trim());
                    transactionEntity.setTxnID(Utility.getTxnID());
                    transactionEntity.setTxnDate(txnDate);
                    transactionEntity.setTxnTruncDate(truncDate);
                    transactionEntity.setTxnType(txnType);
                    transactionEntity.setTxnCategory(txnCategory);
                    transactionEntity.setTxnAccount("NA");
                    transactionEntity.setTxnNarration("Financial Transaction");
                    transactionEntity.setMakerPhoneNumber(AppController.getInstance().getChamaPreferenceManager().getPhoneNumber());
                    transactionEntity.setTxnStatus("Pending upload");
                    transactionEntity.setTxnProcessingStatus("Unprocessed");

                    listener.onAddTransactionDialog(transactionEntity);

                    getDialog().cancel();
                }
            });
        }

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = 1200;
            dialog.getWindow().setLayout(width, height);
        }
    }

    public interface AddTransactionListener {
        void onAddTransactionDialog(TransactionEntity transactionEntity);
    }
}

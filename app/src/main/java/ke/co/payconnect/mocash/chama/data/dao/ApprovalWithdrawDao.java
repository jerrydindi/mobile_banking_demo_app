package ke.co.payconnect.mocash.chama.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.entity.ApprovalWithdrawEntity;

@Dao
public interface ApprovalWithdrawDao {

    @Query("SELECT uid, txn_id, maker_phone_number, amount, destination, narration, withdraw_date, approval_id, approval_status, approval_type, approval_narration, approval_date FROM ApprovalWithdrawEntity WHERE approval_type == :approvalType ORDER BY uid DESC")
    LiveData<List<ApprovalWithdrawEntity>> getApprovalWithdrawsByTypeLive(final String approvalType);

    @Query("SELECT uid, txn_id, maker_phone_number, amount, destination, narration, withdraw_date, approval_id, approval_status, approval_type, approval_narration, approval_date FROM ApprovalWithdrawEntity WHERE approval_id == :approvalId ORDER BY uid DESC")
    ApprovalWithdrawEntity findWithdrawApprovalById(String approvalId);

    @Query("SELECT EXISTS(SELECT 1 FROM ApprovalWithdrawEntity WHERE approval_id == :approvalId LIMIT 1)")
    boolean doesWithdrawApprovalExistWithApprovalId(String approvalId);

    @Query("UPDATE ApprovalWithdrawEntity SET approval_status = :approvalStatus, approval_narration = :approvalNarration, approval_date = :approvalDate WHERE approval_id == :approvalID")
    void updateApprovalWithdrawSetData(final String approvalID, final String approvalStatus, final String approvalNarration, final String approvalDate);

    @Insert
    void insertApprovalWithdraw(ApprovalWithdrawEntity... approvalWithdrawEntities);

    @Query("DELETE FROM ApprovalWithdrawEntity")
    void deleteAllApprovalWithdraws();
}

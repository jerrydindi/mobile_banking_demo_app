package ke.co.payconnect.mocash.mocash.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import ke.co.payconnect.mocash.mocash.data.entity.EmployerEntity;

@Dao
public interface EmployerDao {
    @Query("SELECT EXISTS(SELECT 1 FROM EmployerEntity WHERE employer_id == :employerID LIMIT 1)")
    boolean employerExists(String employerID);

    // LiveData List
    @Query("SELECT DISTINCT uid, employer_id, employer_name FROM EmployerEntity ORDER BY employer_id ASC")
    LiveData<List<EmployerEntity>> getAllEmployers();

    // Normal List
    @Query("SELECT DISTINCT uid, employer_id, employer_name FROM EmployerEntity ORDER BY employer_id ASC")
    List<EmployerEntity> getAllEmployersList();

    @Query("SELECT uid, employer_id, employer_name FROM EmployerEntity WHERE uid == :uid")
    EmployerEntity findEmployerByUID(int uid);

    @Query("SELECT uid, employer_id, employer_name FROM EmployerEntity WHERE employer_id == :employerID")
    EmployerEntity findEmployerByAccountNo(String employerID);

    @Query("SELECT COUNT(*) FROM EmployerEntity")
    int getEmployersCount();

    @Query("SELECT uid FROM EmployerEntity WHERE employer_id == :employerID")
    int getEmployerUID(String employerID);

    @Insert
    void insertEmployer(EmployerEntity... employerModels);

    @Update
    void updateEmployer(EmployerEntity... employerModels);

    @Delete
    void deleteEmployer(EmployerEntity employerModel);

    @Query("DELETE FROM EmployerEntity")
    void deleteAllEmployers();
}

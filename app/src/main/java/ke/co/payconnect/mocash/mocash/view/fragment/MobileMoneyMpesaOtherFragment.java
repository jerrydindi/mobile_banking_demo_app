package ke.co.payconnect.mocash.mocash.view.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Connection;
import io.captano.utility.ui.UIController;
import io.captano.utility.ui.UIHandler;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.mocash.data.entity.AccountEntity;
import ke.co.payconnect.mocash.mocash.data.entity.FavEntity;
import ke.co.payconnect.mocash.mocash.util.Access;
import ke.co.payconnect.mocash.mocash.util.Config;
import ke.co.payconnect.mocash.mocash.view.activity.FavouritesActivity;
import ke.co.payconnect.mocash.mocash.view.activity.LoginActivity;
import ke.co.payconnect.mocash.mocash.view.adapter.AccountSpinnerAdapter;
import ke.co.payconnect.mocash.mocash.viewmodel.FavViewModel;

import static ke.co.payconnect.mocash.mocash.util.Config.BACKOFF_MULTIPLIER;
import static ke.co.payconnect.mocash.mocash.util.Config.RETRIES;
import static ke.co.payconnect.mocash.mocash.util.Config.TIMEOUT_MS;

public class MobileMoneyMpesaOtherFragment extends Fragment implements
        ConfirmTransactionDialogFragment.ConfirmTransactionListener,
        PasswordInputFragment.PasswordInputListener {

    private static final int CONTACT_PICKER_RESULT = 1001;
    private static final int RequestPermissionCode = 1;

    // Views
    private Spinner spAccountSourceOtherMpesa;
    private Spinner spCurrencyOtherMpesa;
    private EditText etAccountDestinationOtherMpesa;
    private EditText etAmountOtherMpesa;
    private ImageView ivFavContacts;
    private ImageView ivContacts;
    private ImageView ivSelectedContactRemoveMMO;
    private TextView tvAccountNameDestinationMMO;
    private TextView tvAccountNumberDestinationMMO;
    private Button btSubmitOtherMpesa;
    private TextView tvConnectivityStatusMpesaOther;

    private FavViewModel favViewModel;

    private String accountName = "";
    private String accountNumber = "";
    private String phoneNumber = "";
    private String accountFromSelected = "";

    private boolean isFavAvailable = false;

    private String validatedAccountFrom = "";
    private String validatedAccountTo = "";
    private String validatedAmount = "0";

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnMobileMoneyMpesaOtherInteractionListener mListener;

    public MobileMoneyMpesaOtherFragment() {
        // Required empty public constructor
    }

    private BroadcastReceiver mFavReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            accountName = intent.getStringExtra("accountName");
            accountNumber = intent.getStringExtra("accountNumber");
            phoneNumber = intent.getStringExtra("phoneNumber");

            if (phoneNumber.equals("-")) {
                Toasty.info(context, getString(R.string.invalid_cr), Toast.LENGTH_SHORT, true).show();
                return;
            }

            tvAccountNameDestinationMMO.setText(accountName);
            tvAccountNumberDestinationMMO.setText(phoneNumber);
            etAccountDestinationOtherMpesa.setText(phoneNumber);

            etAccountDestinationOtherMpesa.setVisibility(View.GONE);
            ivFavContacts.setVisibility(View.GONE);
            ivContacts.setVisibility(View.GONE);

            ivSelectedContactRemoveMMO.setVisibility(View.VISIBLE);
            tvAccountNameDestinationMMO.setVisibility(View.VISIBLE);
            tvAccountNumberDestinationMMO.setVisibility(View.VISIBLE);
        }
    };


    // TODO: Rename and change types and number of parameters
    public static MobileMoneyMpesaOtherFragment newInstance(String param1, String param2) {
        MobileMoneyMpesaOtherFragment fragment = new MobileMoneyMpesaOtherFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if (isConnected)
                tvConnectivityStatusMpesaOther.setVisibility(View.GONE);
            else
                tvConnectivityStatusMpesaOther.setVisibility(View.VISIBLE);

        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mFavReceiver, new IntentFilter("INTENT_FAV_MPESA"));

        favViewModel = ViewModelProviders.of(this).get(FavViewModel.class);

        favViewModel.getFavList().observe(getActivity(), new Observer<List<FavEntity>>() {
            @Override
            public void onChanged(@Nullable List<FavEntity> fav) {

                if (fav == null || fav.isEmpty())
                    isFavAvailable = false;
                else
                    isFavAvailable = true;


                if (getContext().checkCallingOrSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                    ivContacts.setVisibility(View.INVISIBLE);
                    ivFavContacts.setVisibility(View.INVISIBLE);
                }

                EnableRuntimePermission();
            }
        });
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Objects.requireNonNull(getActivity()).registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        } else {
            getActivity().registerReceiver(connectionReceiver, intentFilter);
        }

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mobile_money_mpesa_other, container, false);

        spAccountSourceOtherMpesa = view.findViewById(R.id.spAccountSourceOtherMpesa);
        spCurrencyOtherMpesa = view.findViewById(R.id.spCurrencyOtherMpesa);
        etAccountDestinationOtherMpesa = view.findViewById(R.id.etAccountDestinationOtherMpesa);
        etAmountOtherMpesa = view.findViewById(R.id.etAmountOtherMpesa);
        ivFavContacts = view.findViewById(R.id.ivFavContacts);
        ivContacts = view.findViewById(R.id.ivContacts);
        ivSelectedContactRemoveMMO = view.findViewById(R.id.ivSelectedContactRemoveMMO);
        tvAccountNameDestinationMMO = view.findViewById(R.id.tvAccountNameDestinationMMO);
        tvAccountNumberDestinationMMO = view.findViewById(R.id.tvAccountNumberDestinationMMO);
        btSubmitOtherMpesa = view.findViewById(R.id.btSubmitOtherMpesa);
        tvConnectivityStatusMpesaOther = view.findViewById(R.id.tvConnectivityStatusMpesaOther);

        String[] currency = {"KES"};
        ArrayAdapter<String> currencyAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, currency);
        currencyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCurrencyOtherMpesa.setAdapter(currencyAdapter);


        List<AccountEntity> accounts = AppController.getInstance().getAccounts();

        if (accounts != null) {

            AccountSpinnerAdapter adapter = new AccountSpinnerAdapter(this.getContext(), accounts);
            spAccountSourceOtherMpesa.setAdapter(adapter);

            setListeners();

        } else {
            new UIController().redirect(getActivity(), LoginActivity.class);
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        try {
            if (getContext() != null) {
                if (connectionReceiver != null)
                    LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(connectionReceiver);
            }
        } catch (Exception ignored) {
        }

    }

    @Override
    public void onPause() {
        super.onPause();

        try {
            if (getContext() != null) {
                if (connectionReceiver != null)
                    LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(connectionReceiver);
            }
        } catch (Exception ignored) {
        }
    }

    public void EnableRuntimePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_CONTACTS)) {

        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{
                    Manifest.permission.READ_CONTACTS}, RequestPermissionCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {
                    if (getContext() != null)
                        Toasty.info(getContext(), "Permission granted. Mocash can now access contacts", Toast.LENGTH_SHORT, true).show();

                } else {
                    if (getContext() != null)
                        Toasty.info(getContext(), "Permission canceled. Mocash cannot access contacts", Toast.LENGTH_SHORT, true).show();

                }
                break;
        }
    }

    @Override
    public void onActivityResult(int RequestCode, int ResultCode, Intent ResultIntent) {
        super.onActivityResult(RequestCode, ResultCode, ResultIntent);
        switch (RequestCode) {

            case (CONTACT_PICKER_RESULT):
                if (ResultCode == Activity.RESULT_OK) {

                    Uri uri;
                    Cursor cursor1, cursor2;
                    String tempNameHolder;
                    String tempNumberHolder;
                    String tempContactID;
                    String iDresult = "";
                    int iDresultHolder;

                    uri = ResultIntent.getData();

                    cursor1 = getContext().getContentResolver().query(uri, null, null, null, null);

                    if (cursor1.moveToFirst()) {

                        tempNameHolder = cursor1.getString(cursor1.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                        tempContactID = cursor1.getString(cursor1.getColumnIndex(ContactsContract.Contacts._ID));

                        iDresult = cursor1.getString(cursor1.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                        iDresultHolder = Integer.valueOf(iDresult);

                        if (iDresultHolder == 1) {

                            cursor2 = getContext().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + tempContactID, null, null);

                            while (cursor2.moveToNext()) {
                                tempNumberHolder = cursor2.getString(cursor2.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                                accountName = tempNameHolder;
                                phoneNumber = tempNumberHolder;

                                tvAccountNameDestinationMMO.setText(tempNameHolder);
                                tvAccountNumberDestinationMMO.setText(tempNumberHolder);
                                etAccountDestinationOtherMpesa.setText(tempNumberHolder);

                                etAccountDestinationOtherMpesa.setVisibility(View.GONE);
                                ivFavContacts.setVisibility(View.GONE);
                                ivContacts.setVisibility(View.GONE);

                                ivSelectedContactRemoveMMO.setVisibility(View.VISIBLE);
                                tvAccountNameDestinationMMO.setVisibility(View.VISIBLE);
                                tvAccountNumberDestinationMMO.setVisibility(View.VISIBLE);

                                break; // Pick first number only
                            }
                        }

                    }
                }
                break;
        }
    }

    public void setListeners() {
        btSubmitOtherMpesa.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (tvConnectivityStatusMpesaOther.getVisibility() == View.VISIBLE) {
                    Toasty.info(getContext(), getString(R.string.internet_required), Toast.LENGTH_SHORT, true).show();
                } else {
                    if (new Connection(getContext()).IsInternetConnected()) {
                        if (accountName.length() > 0)
//                            validation(accountFromSelected, etAccountDestinationOtherMpesa.getText().toString().trim().replace(" ", "") + " (" + accountName + ") ", etAmountOtherMpesa.getText().toString());
                        validation(accountFromSelected, etAccountDestinationOtherMpesa.getText().toString().trim().replace(" ", ""), etAmountOtherMpesa.getText().toString());
                        else
                            validation(accountFromSelected, etAccountDestinationOtherMpesa.getText().toString().trim().replace(" ", ""), etAmountOtherMpesa.getText().toString());
                    } else {
                        Toasty.info(getContext(), getString(R.string.internet_required), Toast.LENGTH_SHORT, true).show();
                    }
                }
            }
        });

        ivFavContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFavAvailable) {
                    FavSelectDialogFragment dialog;

                    if (MobileMoneyMpesaOtherFragment.this.getActivity().getSupportFragmentManager() != null) {
                        FragmentManager fm = MobileMoneyMpesaOtherFragment.this.getActivity().getSupportFragmentManager();
                        dialog = FavSelectDialogFragment.newInstance("Saved Contacts", "MPESA");
                        dialog.show(fm, FavSelectDialogFragment.TAG);
                    }
                } else {
                    showSnackBar(v);
                }
            }
        });

        ivContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Start the intent of fetch a contact
                Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(contactPickerIntent, CONTACT_PICKER_RESULT);

            }
        });

        ivSelectedContactRemoveMMO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etAccountDestinationOtherMpesa.setVisibility(View.VISIBLE);
                ivFavContacts.setVisibility(View.VISIBLE);
                ivContacts.setVisibility(View.VISIBLE);
                ivSelectedContactRemoveMMO.setVisibility(View.GONE);
                tvAccountNameDestinationMMO.setVisibility(View.GONE);
                tvAccountNumberDestinationMMO.setVisibility(View.GONE);

                etAccountDestinationOtherMpesa.getText().clear();
                accountName = "";
                accountNumber = "";
                phoneNumber = "";
            }
        });

        spAccountSourceOtherMpesa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {

                try {
                    accountFromSelected = ((TextView) v.findViewById(R.id.tvAccountNumberSpin)).getText().toString();
                } catch (Exception e) {
                    if (getContext() != null)
                        Toasty.info(getContext(), "Your session has timed out", Toast.LENGTH_SHORT, true).show();
                    new UIController().redirect(getActivity(), LoginActivity.class);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });
    }

    public void showSnackBar(View view) {
        final Snackbar snackbar = Snackbar
                .make(view, "You do not have any saved contacts", Snackbar.LENGTH_LONG)
                .setAction("ADD", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent i = new Intent(getContext(), FavouritesActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(i);
                    }
                });

        snackbar.setActionTextColor(getResources().getColor(R.color.colorPrimary));
        snackbar.show();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onMobileMoneyMpesaOtherInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMobileMoneyMpesaOtherInteractionListener) {
            mListener = (OnMobileMoneyMpesaOtherInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnMobileMoneyMpesaOtherInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onConfirmTransactionListener(boolean isConfirmed, String action) {
        if (isConfirmed) {
            if (action.equals("MPESA-OTHER")) {
                DialogFragment fragment = PasswordInputFragment.newInstance("MpesaOther");
                fragment.setTargetFragment(MobileMoneyMpesaOtherFragment.this, 1);
                fragment.show(MobileMoneyMpesaOtherFragment.this.getFragmentManager(), "dialog");

            } else {
                if (getContext() != null)
                    Toasty.info(getContext(), "Action could not be completed at the moment", Toast.LENGTH_SHORT, true).show();
            }
        }
    }

    @Override
    public void onPasswordInputDialog(boolean isSuccessful, String tag) {
        if (isSuccessful)
            if (tag.equals("MpesaOther"))
                processTransaction();
            else {
                if (getContext() != null)
                    Toasty.info(getContext(), "Incorrect pin", Toast.LENGTH_SHORT, true).show();
            }
    }

    public interface OnMobileMoneyMpesaOtherInteractionListener {
        // TODO: Update argument type and name
        void onMobileMoneyMpesaOtherInteraction(Uri uri);
    }

    private void validation(String accountFrom, String accountTo, String amount) {
        if (accountTo == null || accountTo.isEmpty() || accountTo.equals("")) {
            if (getContext() != null)
                Toasty.info(getContext(), "Please enter a valid mpesa number", Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (amount == null || amount.isEmpty()) {
            if (getContext() != null)
                Toasty.info(getContext(), "Please enter a valid amount", Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (accountFrom.equals(accountTo)) {
            if (getContext() != null)
                Toasty.info(getContext(), "Please select a different mpesa number", Toast.LENGTH_SHORT, true).show();
            return;
        }

        validatedAccountFrom = accountFrom;
        validatedAccountTo = accountTo;
        validatedAmount = amount;

        if (validatedAccountTo.substring(0, 3).equals("254"))
            validatedAccountTo = validatedAccountTo.substring(3);

        if (validatedAccountTo.substring(0, 4).equals("+254"))
            validatedAccountTo = validatedAccountTo.substring(4);

        if (validatedAccountTo.substring(0, 1).equals("0"))
            validatedAccountTo = validatedAccountTo.substring(1);

        double amountFormat = Double.parseDouble(amount);

        DialogFragment fragment = ConfirmTransactionDialogFragment.newInstance(R.string.confirm_transaction, accountFrom, accountTo, amountFormat, "MPESA-OTHER", "", "");
        fragment.setTargetFragment(MobileMoneyMpesaOtherFragment.this, 1);
        fragment.show(MobileMoneyMpesaOtherFragment.this.getFragmentManager(), "dialog");

    }

    private void notifySessionExpired(String message) {
        Access.forceSessionExpiry();

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        boolean stateRemember = AppController.getInstance().getMocashPreferenceManager().getRememberPhone();
                        Access.logout(getActivity(), stateRemember, false);

                        if (getContext() != null)
                            Toasty.info(getContext(), R.string.logout_message_post, Toast.LENGTH_SHORT, true).show();
                        break;
                }
            }
        };
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getContext(), R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(false);
        builder.setTitle(getString(R.string.notification));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.sign_in), dialogClickListener);
        builder.show();
    }

    public void processTransaction() {
        final ProgressDialog loading = ProgressDialog.show(getContext(), null, "Processing transaction", false, false);

        final String phoneNumber = AppController.getInstance().getMocashPreferenceManager().getUser().getPhone();
        final String idNumber = AppController.getInstance().getMocashPreferenceManager().getUser().getNationalID();
        final String sessionToken = AppController.getInstance().getMocashPreferenceManager().getSessionToken();

        final String requestTag = "mpesa_other_request";
        final String url = Config.MOBILE_MONEY_URL;

        Map<String, String> map = new HashMap<>();
        map.put("phone_number", phoneNumber);
        map.put("id_number", idNumber);
        map.put("account", validatedAccountFrom);
        map.put("mobile_money", "MPESA");
        map.put("mobile_money_account", "254" + validatedAccountTo);
        map.put("amount", validatedAmount);

        JsonObjectRequest mpesaOtherReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(map),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            loading.dismiss();
                            DialogFragment fragment = ConfirmTransactionDialogFragment
                                    .newInstance(R.string.response, "", "", 0, "RESULT",
                                            response.getString("message"),
                                            "");

                            switch (response.getString("status")) {
                                case "success":
                                    etAmountOtherMpesa.getText().clear();
                                    etAccountDestinationOtherMpesa.getText().clear();

                                    etAccountDestinationOtherMpesa.setVisibility(View.VISIBLE);
                                    ivFavContacts.setVisibility(View.VISIBLE);
                                    ivSelectedContactRemoveMMO.setVisibility(View.GONE);
                                    tvAccountNameDestinationMMO.setVisibility(View.GONE);
                                    tvAccountNumberDestinationMMO.setVisibility(View.GONE);

                                    accountName = "";
                                    accountNumber = "";
                                    MobileMoneyMpesaOtherFragment.this.phoneNumber = "";

                                    // Show dialog
                                    fragment.setTargetFragment(MobileMoneyMpesaOtherFragment.this, 1);
                                    fragment.show(MobileMoneyMpesaOtherFragment.this.getFragmentManager(), "dialog");
                                    break;

                                case "failed":
                                    etAmountOtherMpesa.getText().clear();
                                    etAccountDestinationOtherMpesa.getText().clear();

                                    etAccountDestinationOtherMpesa.setVisibility(View.VISIBLE);
                                    ivFavContacts.setVisibility(View.VISIBLE);
                                    ivSelectedContactRemoveMMO.setVisibility(View.GONE);
                                    tvAccountNameDestinationMMO.setVisibility(View.GONE);
                                    tvAccountNumberDestinationMMO.setVisibility(View.GONE);

                                    // Show dialog
                                    fragment.setTargetFragment(MobileMoneyMpesaOtherFragment.this, 1);
                                    fragment.show(MobileMoneyMpesaOtherFragment.this.getFragmentManager(), "dialog");
                                    break;

                                default:
                                    etAmountOtherMpesa.getText().clear();
                                    etAccountDestinationOtherMpesa.getText().clear();

                                    etAccountDestinationOtherMpesa.setVisibility(View.VISIBLE);
                                    ivFavContacts.setVisibility(View.VISIBLE);
                                    ivSelectedContactRemoveMMO.setVisibility(View.GONE);
                                    tvAccountNameDestinationMMO.setVisibility(View.GONE);
                                    tvAccountNumberDestinationMMO.setVisibility(View.GONE);

                                    if (getContext() != null)
                                        Toasty.info(getContext(), "Failed to process the transaction", Toast.LENGTH_LONG, true).show();
                                    break;
                            }

                        } catch (JSONException e) {
                            loading.dismiss();

                            etAmountOtherMpesa.getText().clear();
                            etAccountDestinationOtherMpesa.getText().clear();

                            etAccountDestinationOtherMpesa.setVisibility(View.VISIBLE);
                            ivFavContacts.setVisibility(View.VISIBLE);
                            ivSelectedContactRemoveMMO.setVisibility(View.GONE);
                            tvAccountNameDestinationMMO.setVisibility(View.GONE);
                            tvAccountNumberDestinationMMO.setVisibility(View.GONE);

                            if (getContext() != null)
                                Toasty.info(getContext(), "An error occurred", Toast.LENGTH_LONG, true).show();

                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();

                etAmountOtherMpesa.getText().clear();
                etAccountDestinationOtherMpesa.getText().clear();

                etAccountDestinationOtherMpesa.setVisibility(View.VISIBLE);
                ivFavContacts.setVisibility(View.VISIBLE);
                ivSelectedContactRemoveMMO.setVisibility(View.GONE);
                tvAccountNameDestinationMMO.setVisibility(View.GONE);
                tvAccountNumberDestinationMMO.setVisibility(View.GONE);


                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    JSONObject response;
                    switch (networkResponse.statusCode) {
                        case 403:
                            notifySessionExpired("Authorization failed: Your session has expired. Kindly sign-in to access this option");
                            break;
                        case 422:
                            try {
                                String responseErrorString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers, "utf-8"));

                                if (responseErrorString.equals("The custom error module does not recognize this error.")) {
                                    Toasty.info(getContext(), getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                    return;
                                }

                                response = new JSONObject(responseErrorString);

                                switch (response.getString("status")) {
                                    case "fatal":
                                /* validation errors - Sample response
                                  {
                                      "status": "fatal",
                                        "errors": [{
                                          "location": "body",
                                          "param": "agent_number",
                                          "msg": "validation failed | CO::00-1"
                                        }]
                                 }
                                 */

                                        if (getContext() != null)
                                            Toasty.info(getContext(), getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();

                                        break;

                                    default:
                                        if (getContext() != null)
                                            Toasty.info(getContext(), getString(R.string.validation_failed), Toast.LENGTH_LONG, true).show();
                                        break;
                                }
                            } catch (JSONException e) {
                                if (getContext() != null)
                                    Toasty.info(getContext(), getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            } catch (UnsupportedEncodingException e) {
                                if (getContext() != null)
                                    Toasty.info(getContext(), getString(R.string.validation_failed), Toast.LENGTH_SHORT, true).show();
                            }
                            break;
                        default:
                            if (getContext() != null)
                                Toasty.info(getContext(), "Error " + networkResponse.statusCode + ": " + getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            break;
                    }
                } else {
                    if (error instanceof NoConnectionError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(getContext(), "The service is currently unavailable.", Toast.LENGTH_SHORT, true).show();
                            }
                        });
                    } else if (error instanceof TimeoutError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(getContext(), "The request has timed out.", Toast.LENGTH_SHORT, true).show();
                            }
                        });
                    } else if (error instanceof AuthFailureError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(getContext(), "Authentication failed", Toast.LENGTH_SHORT, true).show();
                            }
                        });
                    } else if (error instanceof ServerError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(getContext(), getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            }
                        });
                    } else if (error instanceof NetworkError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(getContext(), "Network error.", Toast.LENGTH_LONG, true).show();
                            }
                        });
                    } else if (error instanceof ParseError) {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(getContext(), "Network error.", Toast.LENGTH_LONG, true).show();
                            }
                        });
                    } else {
                        UIHandler.runOnUI(new Runnable() {
                            public void run() {
                                Toasty.info(getContext(), getString(R.string.service_unavailable), Toast.LENGTH_LONG, true).show();
                            }
                        });
                    }
                }
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + sessionToken);

                return headers;
            }
        };

        mpesaOtherReq.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT_MS, RETRIES, BACKOFF_MULTIPLIER));

        // Adding request to queue
        AppController.getInstance().addToRequestQueue(mpesaOtherReq, requestTag);
    }
}

package ke.co.payconnect.mocash.chama.view.activity.member;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.captano.utility.Time;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.chama.util.Access;
import ke.co.payconnect.mocash.chama.util.Utility;
import ke.co.payconnect.mocash.chama.view.activity.signatory.structured.FTChamaRequestActivity;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalExitViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalFullStatementViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalGroupBalanceViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalMinistatementViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalSecretaryViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalTransactionViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalViewModel;
import ke.co.payconnect.mocash.chama.viewmodel.ApprovalWithdrawViewModel;

public class SelectTransactionCategoryActivity extends AppCompatActivity {
    private final Context context = this;
    private final Activity activity = this;

    private Toolbar tbTxnCategory;
    private TextView tvConnectivityStatusTxnCategory;
    private CardView cvTxnGrpTxnCategory;
    private CardView cvTxnTblTxnCategory;

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if (isConnected)
                tvConnectivityStatusTxnCategory.setVisibility(View.GONE);
            else
                tvConnectivityStatusTxnCategory.setVisibility(View.VISIBLE);

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_transaction_category);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        else
            registerReceiver(connectionReceiver, intentFilter);

        tvConnectivityStatusTxnCategory = findViewById(R.id.tvConnectivityStatusTxnCategory);
        tbTxnCategory = findViewById(R.id.tbTxnCategory);
        setSupportActionBar(tbTxnCategory);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbTxnCategory.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvTxnGrpTxnCategory = findViewById(R.id.cvTxnGrpTxnCategory);
        cvTxnTblTxnCategory = findViewById(R.id.cvTxnTblTxnCategory);

        setListeners();
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void setListeners() {
        cvTxnGrpTxnCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, TransactionActivity.class);
                i.putExtra("txn_category", "TXN-GRP-POSTING");
                startActivity(i);

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        cvTxnTblTxnCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, TransactionActivity.class);
                i.putExtra("txn_category", "TXN-TBL-BANKING");
                startActivity(i);

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });
    }
}

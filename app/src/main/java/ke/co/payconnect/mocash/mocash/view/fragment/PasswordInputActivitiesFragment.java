package ke.co.payconnect.mocash.mocash.view.fragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import io.captano.utility.ui.UIController;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.mocash.data.object.User;
import ke.co.payconnect.mocash.mocash.view.activity.LoginActivity;


public class PasswordInputActivitiesFragment extends DialogFragment {

    // Views
    private LinearLayout llPinLayout;
    private LinearLayout llOne;
    private LinearLayout llTwo;
    private LinearLayout llThree;
    private LinearLayout llFour;
    private LinearLayout llFive;
    private LinearLayout llSix;
    private LinearLayout llSeven;
    private LinearLayout llEight;
    private LinearLayout llNine;
    private LinearLayout llZero;
    private LinearLayout llDeletePin;
    private Button btGo;
    private ImageView ivPin;

    private StringBuffer pinString = new StringBuffer();

    private PasswordInputActivitiesListener listener;

    private User user;

    private String tag;


    public static PasswordInputActivitiesFragment newInstance(String tag) {
        PasswordInputActivitiesFragment fragment = new PasswordInputActivitiesFragment();
        Bundle bundle = new Bundle();
        bundle.putString("tag", tag);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup viewGroup, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_password_input_dialog, viewGroup);

        llPinLayout = view.findViewById(R.id.llPinLayout);
        llOne = view.findViewById(R.id.llOne);
        llTwo = view.findViewById(R.id.llTwo);
        llThree = view.findViewById(R.id.llThree);
        llFour = view.findViewById(R.id.llFour);
        llFive = view.findViewById(R.id.llFive);
        llSix = view.findViewById(R.id.llSix);
        llSeven = view.findViewById(R.id.llSeven);
        llEight = view.findViewById(R.id.llEight);
        llNine = view.findViewById(R.id.llNine);
        llZero = view.findViewById(R.id.llZero);
        llDeletePin = view.findViewById(R.id.llDeletePin);
        btGo = view.findViewById(R.id.btGo);

        setListeners();

        user = AppController.getInstance().getMocashPreferenceManager().getUser();

        return view;
    }

    private void setListeners() {
        llOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (pinString.length() < 4) {
                    pinString.append("1");

                    ivPin = new ImageView(getContext());
                    ivPin.setImageResource(R.drawable.dot);
                    ivPin.setScaleX(0.3f);
                    ivPin.setScaleY(0.3f);
                    ivPin.setPadding(0, 0, 0, 0);
                    llPinLayout.addView(ivPin);

                    ivPin.getLayoutParams().height = 100;
                    ivPin.getLayoutParams().width = 100;
                }

            }
        });

        llTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pinString.length() < 4) {
                    pinString.append("2");

                    ivPin = new ImageView(getContext());
                    ivPin.setImageResource(R.drawable.dot);
                    ivPin.setScaleX(0.3f);
                    ivPin.setScaleY(0.3f);
                    ivPin.setPadding(0, 0, 0, 0);
                    llPinLayout.addView(ivPin);

                    ivPin.getLayoutParams().height = 100;
                    ivPin.getLayoutParams().width = 100;
                }
            }
        });

        llThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pinString.length() < 4) {
                    pinString.append("3");

                    ivPin = new ImageView(getContext());
                    ivPin.setImageResource(R.drawable.dot);
                    ivPin.setScaleX(0.3f);
                    ivPin.setScaleY(0.3f);
                    ivPin.setPadding(0, 0, 0, 0);
                    llPinLayout.addView(ivPin);

                    ivPin.getLayoutParams().height = 100;
                    ivPin.getLayoutParams().width = 100;
                }
            }
        });

        llFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pinString.length() < 4) {
                    pinString.append("4");

                    ivPin = new ImageView(getContext());
                    ivPin.setImageResource(R.drawable.dot);
                    ivPin.setScaleX(0.3f);
                    ivPin.setScaleY(0.3f);
                    ivPin.setPadding(0, 0, 0, 0);
                    llPinLayout.addView(ivPin);

                    ivPin.getLayoutParams().height = 100;
                    ivPin.getLayoutParams().width = 100;
                }
            }
        });

        llFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pinString.length() < 4) {
                    pinString.append("5");

                    ivPin = new ImageView(getContext());
                    ivPin.setImageResource(R.drawable.dot);
                    ivPin.setScaleX(0.3f);
                    ivPin.setScaleY(0.3f);
                    ivPin.setPadding(0, 0, 0, 0);
                    llPinLayout.addView(ivPin);

                    ivPin.getLayoutParams().height = 100;
                    ivPin.getLayoutParams().width = 100;
                }
            }
        });

        llSix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pinString.length() < 4) {
                    pinString.append("6");

                    ivPin = new ImageView(getContext());
                    ivPin.setImageResource(R.drawable.dot);
                    ivPin.setScaleX(0.3f);
                    ivPin.setScaleY(0.3f);
                    ivPin.setPadding(0, 0, 0, 0);
                    llPinLayout.addView(ivPin);

                    ivPin.getLayoutParams().height = 100;
                    ivPin.getLayoutParams().width = 100;
                }
            }
        });

        llSeven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pinString.length() < 4) {
                    pinString.append("7");

                    ivPin = new ImageView(getContext());
                    ivPin.setImageResource(R.drawable.dot);
                    ivPin.setScaleX(0.3f);
                    ivPin.setScaleY(0.3f);
                    ivPin.setPadding(0, 0, 0, 0);
                    llPinLayout.addView(ivPin);

                    ivPin.getLayoutParams().height = 100;
                    ivPin.getLayoutParams().width = 100;
                }
            }
        });

        llEight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pinString.length() < 4) {
                    pinString.append("8");

                    ivPin = new ImageView(getContext());
                    ivPin.setImageResource(R.drawable.dot);
                    ivPin.setScaleX(0.3f);
                    ivPin.setScaleY(0.3f);
                    ivPin.setPadding(0, 0, 0, 0);
                    llPinLayout.addView(ivPin);

                    ivPin.getLayoutParams().height = 100;
                    ivPin.getLayoutParams().width = 100;
                }
            }
        });

        llNine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pinString.length() < 4) {
                    pinString.append("9");

                    ivPin = new ImageView(getContext());
                    ivPin.setImageResource(R.drawable.dot);
                    ivPin.setScaleX(0.3f);
                    ivPin.setScaleY(0.3f);
                    ivPin.setPadding(0, 0, 0, 0);
                    llPinLayout.addView(ivPin);

                    ivPin.getLayoutParams().height = 100;
                    ivPin.getLayoutParams().width = 100;
                }
            }
        });

        llZero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pinString.length() < 4) {
                    pinString.append("0");

                    ivPin = new ImageView(getContext());
                    ivPin.setImageResource(R.drawable.dot);
                    ivPin.setScaleX(0.3f);
                    ivPin.setScaleY(0.3f);
                    ivPin.setPadding(0, 0, 0, 0);
                    llPinLayout.addView(ivPin);

                    ivPin.getLayoutParams().height = 100;
                    ivPin.getLayoutParams().width = 100;
                }
            }
        });

        llDeletePin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = llPinLayout.getChildCount();
                if (count > 0) {
                    pinString.deleteCharAt(count - 1);
                    llPinLayout.removeViewAt(count - 1);
                }
            }
        });

        llDeletePin.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                int count = llPinLayout.getChildCount();
                if (count > 0) {
                    for (int i = 0; i < count; i++) {
                        pinString.deleteCharAt(count - (1 + i));
                        llPinLayout.removeViewAt(count - (1 + i));
                    }
                    return false;
                }
                return false;
            }
        });
        btGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = llPinLayout.getChildCount();

                if (count >= 4)
                    authenticate(count);
                else {
                    if (getContext() != null)
                        Toasty.info(getContext(), getString(R.string.valid_pin), Toast.LENGTH_SHORT, true).show();
                }
            }
        });
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        tag = getArguments().getString("tag");

        // Request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        pinString = new StringBuffer();

        return dialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (PasswordInputActivitiesListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement PasswordInputActivitiesListener");
        }
    }

    private void authenticate(int count) {
        if (user != null) {
            String pin = pinString.toString().trim();
            String savedPin = user.getPin();
            if (savedPin != null) {
                if (savedPin.equals(pin)) {
                    PasswordInputActivitiesFragment.this.dismiss();
                    listener.onPasswordInputActivitiesDialog(true, tag);
                } else {
                    if (count > 0) {
                        for (int i = 0; i < count; i++) {
                            pinString.deleteCharAt(count - (1 + i));
                            llPinLayout.removeViewAt(count - (1 + i));
                        }
                    }
                    listener.onPasswordInputActivitiesDialog(false, tag);
                }
            } else {
                if (count > 0) {
                    for (int i = 0; i < count; i++) {
                        pinString.deleteCharAt(count - (1 + i));
                        llPinLayout.removeViewAt(count - (1 + i));
                    }
                }

                if(getContext() != null)
                    Toasty.info(getContext(), "Your session has timed out", Toast.LENGTH_SHORT, true).show();
                new UIController().redirect(getActivity(), LoginActivity.class);
            }
        } else {
            if(getContext() != null)
                Toasty.info(getContext(), "Your session has timed out", Toast.LENGTH_SHORT, true).show();
            new UIController().redirect(getActivity(), LoginActivity.class);
        }
    }

    public interface PasswordInputActivitiesListener {
        void onPasswordInputActivitiesDialog(boolean isSuccessful, String tag);
    }
}

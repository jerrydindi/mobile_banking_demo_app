package ke.co.payconnect.mocash.chama.view.activity.signatory;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.chama.data.entity.MemberEntity;
import ke.co.payconnect.mocash.chama.util.Access;
import ke.co.payconnect.mocash.chama.view.adapter.secretary.SelectMemberAssignSecretaryRecyclerViewAdapter;
import ke.co.payconnect.mocash.chama.view.adapter.signatory.SelectRemoveMemberRecyclerViewAdapter;
import ke.co.payconnect.mocash.chama.viewmodel.MemberViewModel;

public class SelectMemberToRemoveActivity extends AppCompatActivity
        implements SelectRemoveMemberRecyclerViewAdapter.MemberAdapterSearchListener {

    private final Activity activity = this;
    private final Context context = this;

    private Toolbar tbRemoveMember;
    private TextView tvConnectivityStatusRemoveMember;
    private RecyclerView rvRemoveMember;
    private TextView tvNoDataRemoveMember;

    private SearchManager searchManager;
    private SearchView searchView;
    private LinearLayoutManager layoutManager;
    private DividerItemDecoration divideItemDecoration;
    public SelectRemoveMemberRecyclerViewAdapter adapter;

    private MemberViewModel memberViewModel;

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork;
            if (cm != null) {
                activeNetwork = cm.getActiveNetworkInfo();

                boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

                if (isConnected)
                    tvConnectivityStatusRemoveMember.setVisibility(View.GONE);
                else
                    tvConnectivityStatusRemoveMember.setVisibility(View.VISIBLE);
            } else {
                tvConnectivityStatusRemoveMember.setVisibility(View.GONE);
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_member_to_remove);

        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        else
            registerReceiver(connectionReceiver, intentFilter);

        tvConnectivityStatusRemoveMember = findViewById(R.id.tvConnectivityStatusRemoveMember);
        tbRemoveMember = findViewById(R.id.tbRemoveMember);
        setSupportActionBar(tbRemoveMember);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbRemoveMember.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        rvRemoveMember = findViewById(R.id.rvRemoveMember);
        tvNoDataRemoveMember = findViewById(R.id.tvNoDataRemoveMember);

        layoutManager = new LinearLayoutManager(context);
        divideItemDecoration = new DividerItemDecoration(context, LinearLayoutManager.VERTICAL);
        rvRemoveMember.addItemDecoration(divideItemDecoration);

        memberViewModel = ViewModelProviders.of(this).get(MemberViewModel.class);

        setupRecyclerView();
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();

        if (id == R.id.action_search)
            return true;

        return super.onOptionsItemSelected(menuItem);
    }

    private void setupRecyclerView() {
        memberViewModel.getNormalMembersLiveList().observe((LifecycleOwner) context, new Observer<List<MemberEntity>>() {
            @Override
            public void onChanged(List<MemberEntity> memberEntities) {

                if (memberEntities.size() > 0) {
                    toggleDataViews(true);

                    adapter = new SelectRemoveMemberRecyclerViewAdapter(context, memberEntities);
                    adapter.notifyDataSetChanged();

                    rvRemoveMember.setAdapter(adapter);
                    rvRemoveMember.setLayoutManager(layoutManager);
                } else
                    toggleDataViews(false);

            }
        });
    }

    @Override
    public void onMemberSearched(MemberEntity memberEntity) {

    }

    private void toggleDataViews(boolean hasItems) {
        if (hasItems)
            tvNoDataRemoveMember.setVisibility(View.GONE);
        else
            tvNoDataRemoveMember.setVisibility(View.VISIBLE);
    }

}

package ke.co.payconnect.mocash.chama.view.activity.member;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.chama.util.Access;
import ke.co.payconnect.mocash.chama.view.fragment.member.GroupTransactionDateFragment;
import ke.co.payconnect.mocash.chama.view.fragment.member.GroupTransactionFragment;
import ke.co.payconnect.mocash.chama.view.fragment.member.PersonalTransactionFragment;
import ke.co.payconnect.mocash.mocash.view.adapter.ViewPagerAdapter;

public class TransactionActivity extends AppCompatActivity
        implements PersonalTransactionFragment.OnPersonalInteractionListener,
        GroupTransactionFragment.OnGroupInteractionListener, GroupTransactionDateFragment.OnGroupTransactionDateInteractionListener {

    private final Context context = this;
    private final Activity activity = this;

    private Toolbar tbTransactionBase;
    private TextView tvConnectivityStatusTransactionBase;
    private TextView tvHeaderTransactionBase;
    private TabLayout tlTransactionBase;
    private ViewPager vpTransactionsBase;

    public static String txnCategory;

    private BroadcastReceiver connectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if (isConnected)
                tvConnectivityStatusTransactionBase.setVisibility(View.GONE);
            else
                tvConnectivityStatusTransactionBase.setVisibility(View.VISIBLE);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);

        tvConnectivityStatusTransactionBase = findViewById(R.id.tvConnectivityStatusTransactionBase);
        tbTransactionBase = findViewById(R.id.tbTransactionBase);
        setSupportActionBar(tbTransactionBase);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        tbTransactionBase.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        tvHeaderTransactionBase = findViewById(R.id.tvHeaderTransactionBase);

        getData();

        vpTransactionsBase = findViewById(R.id.vpTransactionsBase);
        setupViewPager(vpTransactionsBase);

        //Tabs inside toolbar
        tlTransactionBase = findViewById(R.id.tlTransactionBase);
        tlTransactionBase.setupWithViewPager(vpTransactionsBase);

    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (connectionReceiver != null)
                unregisterReceiver(connectionReceiver);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void getData() {
        Intent i = new Intent(getIntent());
        txnCategory = i.getStringExtra("txn_category");

        switch (txnCategory) {
            case "TXN-GRP-POSTING":
                tvHeaderTransactionBase.setText("Group Posting Transactions");
                break;
            case "TXN-TBL-BANKING":
                tvHeaderTransactionBase.setText("Table Banking Transactions");
                break;
            default:
                tvHeaderTransactionBase.setText("Transactions");
                break;
        }

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        PersonalTransactionFragment personalTransactionFragment = new PersonalTransactionFragment();
        viewPagerAdapter.addFragment(personalTransactionFragment, "Personal");

        GroupTransactionDateFragment groupTransactionFragment = new GroupTransactionDateFragment();
        viewPagerAdapter.addFragment(groupTransactionFragment, "Group");

        viewPager.setAdapter(viewPagerAdapter);
    }

    @Override
    public void OnGroupInteraction(Uri uri) {

    }

    @Override
    public void OnPersonalInteraction(Uri uri) {

    }

    @Override
    public void OnGroupTransactionDateInteraction(Uri uri) {

    }
}
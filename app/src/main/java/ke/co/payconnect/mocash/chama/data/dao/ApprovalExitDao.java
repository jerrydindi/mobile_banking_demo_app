package ke.co.payconnect.mocash.chama.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import ke.co.payconnect.mocash.chama.data.entity.ApprovalExitEntity;

@Dao
public interface ApprovalExitDao {

    @Query("SELECT uid, maker_phone_number, member_phone_number, narration, exit_date, approval_id, approval_status, approval_type, approval_narration, approval_date FROM ApprovalExitEntity WHERE approval_type == :approvalType ORDER BY uid DESC")
    LiveData<List<ApprovalExitEntity>> getApprovalExitsByTypeLive(final String approvalType);

    @Query("SELECT uid, maker_phone_number, member_phone_number, narration, exit_date, approval_id, approval_status, approval_type, approval_narration, approval_date FROM ApprovalExitEntity WHERE approval_id == :approvalId ORDER BY uid DESC")
    ApprovalExitEntity findExitApprovalById(String approvalId);

    @Query("SELECT EXISTS(SELECT 1 FROM ApprovalExitEntity WHERE approval_id == :approvalId LIMIT 1)")
    boolean doesExitApprovalExistWithApprovalId(String approvalId);

    @Query("UPDATE ApprovalExitEntity SET approval_status = :approvalStatus, approval_narration = :approvalNarration, approval_date = :approvalDate WHERE approval_id == :approvalID")
    void updateApprovalExitSetData(final String approvalID, final String approvalStatus, final String approvalNarration, final String approvalDate);

    @Insert
    void insertApprovalExit(ApprovalExitEntity... approvalExitEntities);

    @Query("DELETE FROM ApprovalExitEntity")
    void deleteAllApprovalExits();
}

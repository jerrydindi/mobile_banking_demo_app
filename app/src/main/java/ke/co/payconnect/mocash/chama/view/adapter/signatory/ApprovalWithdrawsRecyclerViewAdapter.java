package ke.co.payconnect.mocash.chama.view.adapter.signatory;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;
import io.captano.utility.Money;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.chama.data.entity.ApprovalWithdrawEntity;
import ke.co.payconnect.mocash.chama.util.Auth;
import ke.co.payconnect.mocash.chama.util.Utility;
import ke.co.payconnect.mocash.chama.view.activity.signatory.ApproveWithdrawActivity;

public class ApprovalWithdrawsRecyclerViewAdapter extends RecyclerView.Adapter<ApprovalWithdrawsRecyclerViewAdapter.ApprovalViewHolder> {
    private final Context context;
    private List<ApprovalWithdrawEntity> approvalWithdrawEntities;

    public ApprovalWithdrawsRecyclerViewAdapter(Context context, List<ApprovalWithdrawEntity> approvalWithdrawEntities) {
        this.context = context;
        this.approvalWithdrawEntities = approvalWithdrawEntities;
    }

    @NonNull
    @Override
    public ApprovalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.struct_approval_withdraw, parent, false);
        return new ApprovalViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ApprovalViewHolder holder, int position) {
        try {
            final ApprovalWithdrawEntity approvalEntity = approvalWithdrawEntities.get(position);

            holder.tvNoStructApprovalWithdraw.setText(String.valueOf(position + 1));
            holder.tvDateStructApprovalWithdraw.setText(approvalEntity.getApprovalDate());
            holder.tvDestinationStructApprovalWithdraw.setText(approvalEntity.getDestination());
            holder.tvAmountStructApprovalWithdraw.setText(Money.format(Double.parseDouble(approvalEntity.getAmount())));

            String approvalDate = approvalEntity.getApprovalDate();

            try {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                Date date = format.parse(approvalDate);
                Date dateToCompare = format.parse("2000-01-01");

                if ((date != null ? date.compareTo(dateToCompare) : 0) == 0) {
                    approvalDate = "NA";
                }
            } catch (ParseException ignored) {
            }

            if (!approvalDate.equals("NA")) {
                Calendar calendar;
                String date;
                try {
                    calendar = Utility.getDateTime(approvalDate, "yyyy-MM-dd");
                    @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    date = dateFormat.format(calendar.getTime());
                    holder.tvDateStructApprovalWithdraw.setText(date);
                } catch (ParseException e) {
                    holder.tvDateStructApprovalWithdraw.setText(approvalEntity.getApprovalDate());
                }
            } else {
                holder.tvDateStructApprovalWithdraw.setText(approvalDate);
            }

            if (!approvalEntity.getApprovalStatus().toLowerCase().equals("pending"))
                holder.clStructApprovalWithdraw.setBackgroundColor(context.getResources().getColor(R.color.cG6));

            holder.clStructApprovalWithdraw.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!Auth.isAuthorized("signatory")) {
                        Toasty.info(context, R.string.action_not_permitted, Toast.LENGTH_SHORT, true).show();
                        return;
                    }

                    Intent i = new Intent(context, ApproveWithdrawActivity.class);
                    i.putExtra("approval_id", approvalEntity.getApprovalID());
                    context.startActivity(i);

                    ((Activity) context).overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
                }
            });
        } catch (Exception ignored) {
        }
    }

    @Override
    public int getItemCount() {
        return this.approvalWithdrawEntities.size();
    }

    class ApprovalViewHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout clStructApprovalWithdraw;
        private TextView tvNoStructApprovalWithdraw;
        private TextView tvDateStructApprovalWithdraw;
        private TextView tvDestinationStructApprovalWithdraw;
        private TextView tvAmountStructApprovalWithdraw;

        ApprovalViewHolder(View view) {
            super(view);

            clStructApprovalWithdraw = view.findViewById(R.id.clStructApprovalWithdraw);
            tvNoStructApprovalWithdraw = view.findViewById(R.id.tvNoStructApprovalWithdraw);
            tvDateStructApprovalWithdraw = view.findViewById(R.id.tvDateStructApprovalWithdraw);
            tvDestinationStructApprovalWithdraw = view.findViewById(R.id.tvMemberStructApprovalWithdraw);
            tvAmountStructApprovalWithdraw = view.findViewById(R.id.tvAmountStructApprovalWithdraw);
        }

    }
}

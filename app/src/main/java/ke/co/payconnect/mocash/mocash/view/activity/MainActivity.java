package ke.co.payconnect.mocash.mocash.view.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.navigation.NavigationView;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;
import io.captano.utility.Connection;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.mocash.data.object.User;
import ke.co.payconnect.mocash.mocash.data.entity.AccountDestinationEntity;
import ke.co.payconnect.mocash.mocash.data.entity.AccountEntity;
import ke.co.payconnect.mocash.mocash.util.Access;
import ke.co.payconnect.mocash.mocash.util.Location;
import ke.co.payconnect.mocash.mocash.view.adapter.MainViewPagerAdapter;
import ke.co.payconnect.mocash.mocash.view.fragment.MainServicesFragment;
import ke.co.payconnect.mocash.mocash.view.fragment.MainTransactFragment;
import ke.co.payconnect.mocash.mocash.view.fragment.MainViewAllFragment;
import ke.co.payconnect.mocash.mocash.viewmodel.AccountDestinationViewModel;
import ke.co.payconnect.mocash.mocash.viewmodel.AccountViewModel;

public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        MainViewAllFragment.OnMainViewAllInteractionListener,
        MainTransactFragment.OnMainTransactInteractionListener,
        MainServicesFragment.OnMainServicesInteractionListener {

    private final Context context = this;
    private final Activity activity = this;

    // Views
    private Toolbar tbMain;
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private ViewPager viewPager;
    private CircleImageView circleImageView;
    private ActionBarDrawerToggle toggle;
    private TextView tvHeaderText;
    private TextView tvSubHeaderText;
    private Button btViewAll;
    private Button btTransact;
    private Button btOtherServices;

    private User user;

    private AccountViewModel accountViewModel;
    private AccountDestinationViewModel accountDestinationViewModel;

    private ProgressDialog loading;

    private final int sdk = android.os.Build.VERSION.SDK_INT;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Translucent status bar
        if (Build.VERSION.SDK_INT >= 21) {
            Window w = getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }

        tbMain = findViewById(R.id.tbMain);

        setSupportActionBar(tbMain);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }


        user = AppController.getInstance().getMocashPreferenceManager().getUser();

        navigationView = findViewById(R.id.nvLanding);
        drawer = findViewById(R.id.dlLandingDrawer);

        toggle = new ActionBarDrawerToggle(this, drawer, tbMain, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        viewPager = findViewById(R.id.vpLanding);

        setupViewPager(viewPager);

        circleImageView = findViewById(R.id.dePhoto);

        NavigationView navigationView = findViewById(R.id.nvLanding);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setBackgroundColor(getResources().getColor(R.color.cW0));
        View headerView = navigationView.getHeaderView(0);

        navigationView.setItemIconTintList(null);
//        navigationView.setItemIconTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));

        tvHeaderText = headerView.findViewById(R.id.tvHeaderText);
        tvSubHeaderText = headerView.findViewById(R.id.tvSubHeaderText);

        btViewAll = findViewById(R.id.btViewAll);
        btTransact = findViewById(R.id.btTransact);
        btOtherServices = findViewById(R.id.btOtherServices);

        loading = ProgressDialog.show(context, null, "Loading", false, true);

        setListeners();

        // Initialize view models
        accountViewModel = ViewModelProviders.of(this).get(AccountViewModel.class);
        accountDestinationViewModel = ViewModelProviders.of(this).get(AccountDestinationViewModel.class);

        setAccountsInSharedPrefs();
        updateAccountsInSharedPrefs();

        initializeProfile();

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawers();

                Intent i = new Intent(MainActivity.this, ProfileActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(i);

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        };

        tvHeaderText.setOnClickListener(listener);
        tvSubHeaderText.setOnClickListener(listener);
        circleImageView.setOnClickListener(listener);
    }

    @Override
    public void onResume() {
        super.onResume();

        loading.dismiss();
        Access.sessionStatusCheck(activity);
    }

    @Override
    public void onBackPressed() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        if (AppController.getInstance().getMocashPreferenceManager().getRememberPhone())
                            Access.logout(activity, true, false);
                        else
                            Access.logout(activity, false, false);

                        Toasty.info(context, "You have been logged out", Toast.LENGTH_SHORT, true).show();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.confirm_action));
        builder.setIcon(R.drawable.ic_app_logo);
        builder.setMessage("You will be logged out");
        builder.setPositiveButton(getString(R.string.proceed), dialogClickListener);
        builder.setNegativeButton(getString(R.string.cancel), dialogClickListener);
        builder.show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_logout) {
            if (AppController.getInstance().getMocashPreferenceManager().getRememberPhone())
                Access.logout(activity, true, false);
            else
                Access.logout(activity, false, false);


            Toasty.info(context, "You have been logged out", Toast.LENGTH_SHORT, true).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void onMainViewAllInteraction(Boolean inputText) {

    }

    @Override
    public void onMainTransactionInteraction(Uri uri) {

    }

    @Override
    public void onMainOtherServicesInteraction(Uri uri) {

    }

    private void setupViewPager(ViewPager viewPager) {
        MainViewPagerAdapter adapter = new MainViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new MainViewAllFragment());
        adapter.addFragment(new MainTransactFragment());
        adapter.addFragment(new MainServicesFragment());

        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            btViewAll.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.bt_round_highlight));
                            btTransact.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.bt_round));
                            btOtherServices.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.bt_round));
                        } else {
                            btViewAll.setBackground(ContextCompat.getDrawable(context, R.drawable.bt_round_highlight));
                            btTransact.setBackground(ContextCompat.getDrawable(context, R.drawable.bt_round));
                            btOtherServices.setBackground(ContextCompat.getDrawable(context, R.drawable.bt_round));
                        }
                        break;
                    case 1:
                        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            btViewAll.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.bt_round));
                            btTransact.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.bt_round_highlight));
                            btOtherServices.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.bt_round));
                        } else {
                            btViewAll.setBackground(ContextCompat.getDrawable(context, R.drawable.bt_round));
                            btTransact.setBackground(ContextCompat.getDrawable(context, R.drawable.bt_round_highlight));
                            btOtherServices.setBackground(ContextCompat.getDrawable(context, R.drawable.bt_round));
                        }
                        break;
                    case 2:
                        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            btViewAll.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.bt_round));
                            btTransact.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.bt_round));
                            btOtherServices.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.bt_round_highlight));
                        } else {
                            btViewAll.setBackground(ContextCompat.getDrawable(context, R.drawable.bt_round));
                            btTransact.setBackground(ContextCompat.getDrawable(context, R.drawable.bt_round));
                            btOtherServices.setBackground(ContextCompat.getDrawable(context, R.drawable.bt_round_highlight));
                        }
                        break;
                    default:
                        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            btViewAll.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.bt_round_highlight));
                            btTransact.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.bt_round));
                            btOtherServices.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.bt_round));
                        } else {
                            btViewAll.setBackground(ContextCompat.getDrawable(context, R.drawable.bt_round_highlight));
                            btTransact.setBackground(ContextCompat.getDrawable(context, R.drawable.bt_round));
                            btOtherServices.setBackground(ContextCompat.getDrawable(context, R.drawable.bt_round));
                        }
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void setAccountsInSharedPrefs() {
        AppController.getInstance().setAccounts(accountViewModel.getAccountList());
        AppController.getInstance().setAccountsDestination(accountDestinationViewModel.getAccountList());
    }

    private void updateAccountsInSharedPrefs() {
        // Update accounts to shared preferences
        accountViewModel.getAccountLiveList().observe(MainActivity.this, new Observer<List<AccountEntity>>() {
            @Override
            public void onChanged(@Nullable List<AccountEntity> accounts) {

                if (accounts != null)
                    AppController.getInstance().setAccounts(accounts);


            }
        });

        // Update destination accounts to shared preferences
        accountDestinationViewModel.getAccountLiveList().observe(MainActivity.this, new Observer<List<AccountDestinationEntity>>() {
            @Override
            public void onChanged(@Nullable List<AccountDestinationEntity> accounts) {

                if (accounts != null)
                    AppController.getInstance().setAccountsDestination(accounts);


            }
        });
    }

    public void initializeProfile() {
        if (user != null) {

            if (user.getPhoto() != null && user.getPhoto().length() > 0) {
                Picasso.get()
                        .load(user.getPhoto())
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .placeholder(R.drawable.ic_user)
                        .error(R.drawable.ic_user)
                        .into(circleImageView);
            }

        }
    }

    private void setListeners() {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                        drawer.closeDrawers();
                        Intent i;

                        switch (item.getItemId()) {
                            case R.id.action_accounts:
                                i = new Intent(context, AccountsActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(i);

                                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);

                                break;
                            case R.id.action_enquiries:
                                i = new Intent(context, EnquiriesActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(i);

                                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);

                                break;
                            case R.id.action_airtime:
                                i = new Intent(context, AirtimeActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(i);

                                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);

                                break;
                            case R.id.action_withdraw:
                                i = new Intent(context, MobileMoneyOwnActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(i);

                                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);

                                break;
                            case R.id.action_ft:
                                i = new Intent(context, FundsTransferBaseActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(i);

                                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);

                                break;
                            case R.id.action_paybill:
                                i = new Intent(context, PayBillsActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(i);

                                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);

                                break;
                            case R.id.action_cards:
                                i = new Intent(context, CardsActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(i);

                                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);

                                break;
                            case R.id.action_changepin:
                                i = new Intent(context, ChangePinActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(i);

                                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);

                                break;
                            case R.id.action_profile:
                                i = new Intent(context, ProfileActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(i);

                                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);

                                break;
                            case R.id.action_alerts:
                                i = new Intent(context, AlertsActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(i);

                                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);

                                break;
                            case R.id.action_fav:
                                i = new Intent(context, FavouritesActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(i);

                                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);

                                break;
                            case R.id.action_share:
                                loading = ProgressDialog.show(context, null, "Loading", false, true);

                                i = new Intent(android.content.Intent.ACTION_SEND);
                                i.setType("text/plain");
                                i.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.share_subject));
                                i.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.share_text));
                                startActivity(Intent.createChooser(i, getString(R.string.share_via)));

                                break;
                            case R.id.action_feedback:
                                i = new Intent(context, FeedbackActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(i);

                                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);

                                break;
                            case R.id.action_contact_about:
                                i = new Intent(context, InfoActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(i);

                                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);

                                break;
                            case R.id.action_locate:
                                if (new Connection(context).IsInternetConnected()) {
                                    new Location(context).fetchLocations();
                                } else
                                    Toasty.info(context, R.string.internet_required, Toast.LENGTH_SHORT, true).show();

                                break;
                            case R.id.action_faq:
                                i = new Intent(context, FaqActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(i);

                                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);

                                break;
                        }

                        return true;
                    }
                });

        drawer.addDrawerListener(
                new DrawerLayout.DrawerListener() {
                    @Override
                    public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
                        // Respond when the drawer's position changes
                    }

                    @Override
                    public void onDrawerOpened(@NonNull View drawerView) {
                        // Respond when the drawer is opened
                    }

                    @Override
                    public void onDrawerClosed(@NonNull View drawerView) {
                        // Respond when the drawer is closed
                    }

                    @Override
                    public void onDrawerStateChanged(int newState) {
                        // Respond when the drawer motion state changes
                    }
                }
        );

        btViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    btViewAll.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.bt_round_highlight));
                    btTransact.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.bt_round));
                    btOtherServices.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.bt_round));
                } else {
                    btViewAll.setBackground(ContextCompat.getDrawable(context, R.drawable.bt_round_highlight));
                    btTransact.setBackground(ContextCompat.getDrawable(context, R.drawable.bt_round));
                    btOtherServices.setBackground(ContextCompat.getDrawable(context, R.drawable.bt_round));
                }

                viewPager.setCurrentItem(0);
            }
        });

        btTransact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    btViewAll.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.bt_round));
                    btTransact.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.bt_round_highlight));
                    btOtherServices.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.bt_round));
                } else {
                    btViewAll.setBackground(ContextCompat.getDrawable(context, R.drawable.bt_round));
                    btTransact.setBackground(ContextCompat.getDrawable(context, R.drawable.bt_round_highlight));
                    btOtherServices.setBackground(ContextCompat.getDrawable(context, R.drawable.bt_round));
                }

                viewPager.setCurrentItem(1);
            }
        });

        btOtherServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    btViewAll.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.bt_round));
                    btTransact.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.bt_round));
                    btOtherServices.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.bt_round_highlight));
                } else {
                    btViewAll.setBackground(ContextCompat.getDrawable(context, R.drawable.bt_round));
                    btTransact.setBackground(ContextCompat.getDrawable(context, R.drawable.bt_round));
                    btOtherServices.setBackground(ContextCompat.getDrawable(context, R.drawable.bt_round_highlight));
                }

                viewPager.setCurrentItem(2);

            }
        });
    }

}

package ke.co.payconnect.mocash.chama.util;

import android.app.Activity;
import android.util.Log;

import androidx.annotation.Nullable;

import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.IOException;

import ke.co.payconnect.mocash.application.AppController;
import ke.co.payconnect.mocash.chama.data.entity.MessageEntity;
import ke.co.payconnect.mocash.chama.viewmodel.MessageViewModel;

public class FireSync {
    private final Activity activity;
    private final FirebaseFirestore firebaseFirestore;

    public FireSync(Activity activity, FirebaseFirestore firebaseFirestore) {
        this.activity = activity;
        this.firebaseFirestore = firebaseFirestore;
    }

    public void registerMessageListener(final MessageViewModel messageViewModel) {
        final String groupNumber = AppController.getInstance().getChamaPreferenceManager().getGroupNumber();

        firebaseFirestore.collection("message")
                .addSnapshotListener(activity, new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.d("FIRE", "listen:error", e);
                            return;
                        }

                        for (DocumentChange documentChange : snapshots.getDocumentChanges()) {
                            MessageEntity messageEntity;

                            switch (documentChange.getType()) {
                                case ADDED:
                                    Log.d("FIRE", "New Message: " + documentChange.getDocument().toObject(MessageEntity.class));
                                    messageEntity = documentChange.getDocument().toObject(MessageEntity.class);


                                    if (messageEntity.getGroupNumber().equals(groupNumber)) {
                                        // When this condition passes, only messages relevant to the logged in user will be stored
                                        // Note: FireStore sends out an event to all devices that have an active listener running, for any change inside a collection
                                        if (!messageViewModel.doesMessageExistWithUID(messageEntity.getUid())) {
                                            messageViewModel.insertMessage(messageEntity);
                                        }
                                    }

                                    break;
                                case MODIFIED:
                                    Log.d("FIRE", "Modified Message: " + documentChange.getDocument().toObject(MessageEntity.class));
                                    messageEntity = documentChange.getDocument().toObject(MessageEntity.class);


                                    if (messageEntity.getGroupNumber().equals(groupNumber)) {
                                        if (messageViewModel.doesMessageExistWithUID(messageEntity.getUid())) {
                                            messageViewModel.updateMessage(messageEntity);
                                        } else {
                                            messageViewModel.insertMessage(messageEntity);
                                        }
                                    }

                                    break;
                                case REMOVED:
                                    Log.d("FIRE", "Removed Message: " + documentChange.getDocument().toObject(MessageEntity.class));
                                    messageEntity = documentChange.getDocument().toObject(MessageEntity.class);


                                    if (messageEntity.getGroupNumber().equals(groupNumber)) {
                                        if (messageViewModel.doesMessageExistWithUID(messageEntity.getUid()))
                                            messageViewModel.deleteMessage(messageEntity);
                                    }

                                    break;
                            }
                        }
                    }

                });
    }
}

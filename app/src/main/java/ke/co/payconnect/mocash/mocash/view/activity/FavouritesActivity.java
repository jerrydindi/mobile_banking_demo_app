package ke.co.payconnect.mocash.mocash.view.activity;

import android.app.Activity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import es.dmoral.toasty.Toasty;
import ke.co.payconnect.mocash.R;
import ke.co.payconnect.mocash.mocash.data.MocashDatabase;
import ke.co.payconnect.mocash.mocash.data.entity.FavEntity;
import ke.co.payconnect.mocash.mocash.util.Access;
import ke.co.payconnect.mocash.mocash.view.adapter.FavouritesRecyclerViewAdapter;
import ke.co.payconnect.mocash.mocash.view.fragment.ConfirmActionDialogFragment.ConfirmActionListener;
import ke.co.payconnect.mocash.mocash.view.fragment.FavManageAccountsDialogFragment;
import ke.co.payconnect.mocash.mocash.view.fragment.FavManageDialogFragment;
import ke.co.payconnect.mocash.mocash.view.fragment.FavManageDialogFragment.FavManageListener;
import ke.co.payconnect.mocash.mocash.viewmodel.FavViewModel;

public class FavouritesActivity extends AppCompatActivity implements
        FavManageListener,
        FavManageAccountsDialogFragment.FavManageAccountsListener,
        ConfirmActionListener {
    private final Context context = this;
    private final Activity activity = this;

    // Views
    private Toolbar toolbar;
    private RecyclerView rvFav;
    private TextView tvFav;
    private FloatingActionButton fbAddFav;
    private FloatingActionButton fbAddFavContact;
    private FloatingActionButton fbAddFavAccount;
    private ImageView ivSearchFav;
    private SearchView svSearchFav;
    private LinearLayout llFAvContacts;
    private LinearLayout llFavAccounts;

    private Boolean isFabOpen = false;
    private Animation fab_open, fab_close, rotate_forward, rotate_backward;


    // Adapter
    public FavouritesRecyclerViewAdapter adapter;

    // Database
    private MocashDatabase db;

    private List<FavEntity> favList;

    private FavViewModel favViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourites);

        // Toolbar
        toolbar = findViewById(R.id.tbFav);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
            }
        });

        db = MocashDatabase.getMocashDatabaseInstance(this);

        // Initialize adapter
//        adapter = new FavouritesRecyclerViewAdapter(this, favList, "manage");

        rvFav = findViewById(R.id.rvFav);

        favViewModel = ViewModelProviders.of(this).get(FavViewModel.class);

        favViewModel.getFavList().observe(FavouritesActivity.this, new Observer<List<FavEntity>>() {
            @Override
            public void onChanged(@Nullable List<FavEntity> favs) {
                // Update UI
                favList = favs;

                if (favList == null || favList.isEmpty()) {
                    tvFav.setVisibility(View.VISIBLE);
                    ivSearchFav.setVisibility(View.GONE);
                    rvFav.setVisibility(View.GONE);
                } else {
                    tvFav.setVisibility(View.GONE);
//                    ivSearchFav.setVisibility(View.VISIBLE); // Disabled search functionality
                    rvFav.setVisibility(View.VISIBLE);
                }

                adapter = new FavouritesRecyclerViewAdapter(context, favList, "manage");
                adapter.notifyDataSetChanged();

                LinearLayoutManager layoutManager = new LinearLayoutManager(context);
                rvFav.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
                rvFav.setAdapter(adapter);
                rvFav.setLayoutManager(layoutManager);
            }
        });

        tvFav = findViewById(R.id.tvFav);
        ivSearchFav = findViewById(R.id.ivSearchFav);
        ivSearchFav = findViewById(R.id.ivSearchFav);
        fbAddFav = findViewById(R.id.fbAddFav);
        fbAddFavContact = findViewById(R.id.fbAddFavContact);
        fbAddFavAccount = findViewById(R.id.fbAddFavAccount);
        svSearchFav = findViewById(R.id.svSearchFav);
        llFAvContacts = findViewById(R.id.llFAvContacts);
        llFavAccounts = findViewById(R.id.llFavAccounts);

        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_backward);

        setListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Access.sessionStatusCheck(activity);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.xml.fade_in, R.xml.fade_out);
    }

    private void setListeners() {

        ivSearchFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (svSearchFav.getVisibility() == View.VISIBLE) {
                    svSearchFav.setVisibility(View.GONE);
                    ivSearchFav.setImageResource(R.drawable.ic_search);
                } else {
                    svSearchFav.setVisibility(View.VISIBLE);
                    ivSearchFav.setImageResource(R.drawable.ic_remove);
                }
            }
        });

        fbAddFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateFAB();
            }
        });

        fbAddFavContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = ((FragmentActivity) activity).getSupportFragmentManager();
                FavManageDialogFragment fragment = FavManageDialogFragment.newInstance("add", 0, "", "", "");
                fragment.show(fm, "dialog");

                animateFAB();
            }
        });

        fbAddFavAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = ((FragmentActivity) activity).getSupportFragmentManager();
                FavManageAccountsDialogFragment fragment = FavManageAccountsDialogFragment.newInstance("add_acc", 0, "", "");
                fragment.show(fm, "dialog");

                animateFAB();
            }
        });

    }

    @Override
    public void onManageFavDialog(boolean isSelected, String tag, int uid, String accountName, String accountNumber, String phoneNumber) {
        FavEntity fav = new FavEntity();

        if (tag.equals("add") && isSelected) {
            // Add fav

            fav.setAccountName(accountName);
            fav.setAccountNumber(accountNumber);
            fav.setPhoneNumber(phoneNumber);

            favViewModel.insertFav(fav);

        } else if (tag.equals("edit") && isSelected) {
            // Update fav

            fav.setUid(uid);
            fav.setAccountName(accountName);
            fav.setAccountNumber(accountNumber);
            fav.setPhoneNumber(phoneNumber);

            favViewModel.updateFav(fav);
        }
    }

    @Override
    public void onManageFavAccountsDialog(boolean isSelected, String tag, int uid, String accountName, String accountNumber) {
        FavEntity fav = new FavEntity();

        if (tag.equals("add_acc") && isSelected) {
            // Add fav

            fav.setAccountName(accountName);
            fav.setAccountNumber(accountNumber);
            fav.setPhoneNumber("-");

            favViewModel.insertFav(fav);

        } else if (tag.equals("edit_acc") && isSelected) {
            // Update fav

            fav.setUid(uid);
            fav.setAccountName(accountName);
            fav.setAccountNumber(accountNumber);
            fav.setPhoneNumber("-");

            favViewModel.updateFav(fav);
        }
    }

    @Override
    public void onConfirmActionListener(boolean inputText, String accountName) {
        if (inputText) {
            // Update UI
            adapter = new FavouritesRecyclerViewAdapter(this, favList, "manage");
            adapter.notifyDataSetChanged();

            Toasty.info(context, accountName + " has been deleted", Toast.LENGTH_SHORT, true).show();
        }
    }

    public void animateFAB() {
        if (isFabOpen) {
            fbAddFav.startAnimation(rotate_backward);
            fbAddFavContact.startAnimation(fab_close);
            fbAddFavAccount.startAnimation(fab_close);
            fbAddFavContact.setClickable(false);
            fbAddFavAccount.setClickable(false);
            llFAvContacts.setVisibility(View.GONE);
            llFavAccounts.setVisibility(View.GONE);
            isFabOpen = false;
        } else {
            fbAddFav.startAnimation(rotate_forward);
            fbAddFavContact.startAnimation(fab_open);
            fbAddFavAccount.startAnimation(fab_open);
            fbAddFavContact.setClickable(true);
            fbAddFavAccount.setClickable(true);
            llFAvContacts.setVisibility(View.VISIBLE);
            llFavAccounts.setVisibility(View.VISIBLE);
            isFabOpen = true;
        }
    }
}
